<?php

namespace Tests\Feature;

use App\DiscountCode;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DiscountTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_discount_requires_a_name()
    {
        $this->publishDiscount(['name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_discount_requires_a_code()
    {
        $this->publishDiscount(['code' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_discount_requires_a_amount_type()
    {
        $this->publishDiscount(['amount_type' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_discount_requires_products()
    {
        $this->publishDiscount(['start_date' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_discount_requires_start_date()
    {
        $this->publishDiscount(['start_date' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_discount_requires_end_date()
    {
        $this->publishDiscount(['end_date' => null])
            ->assertStatus(400);
    }

    public function publishDiscount($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $discount = factory(DiscountCode::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/discounts',$discount);

        return $response;
    }

    /** @test */
    public function a_token_invalid_in_discounts_get()
    {
        $token = 'kdjshgad';
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('GET', '/api/business/discounts');

        $response->assertStatus(401);

    }

    /** @test */
    public function a_business_invalid_in_discounts_get()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // no create business for this user

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('GET', '/api/business/discounts');

        $response->assertStatus(400);
    }

    /** @test */
    public function a_user_can_delete_discount()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $discount = factory(DiscountCode::class)->create(['business_id' => $business->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/discounts/' .$discount->id);

        $response->assertStatus(200);
    }

    /** @test */
    public function a_user_can_not_select_repeat_discount_code()
    {
        $discount = factory(DiscountCode::class)->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/business/discount/check/code',[
            'code' => $discount->code
        ]);

        $response->assertStatus(400);
    }
}
