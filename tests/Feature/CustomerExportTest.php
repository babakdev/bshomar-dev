<?php

namespace Tests\Feature;

use App\BusinessModels\CustomerExport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CustomerExportTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_cust_requires_a_name()
    {
        $this->publishCust(['name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cust_requires_a_country()
    {
        $this->publishCust(['country' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cust_requires_a_state()
    {
        $this->publishCust(['state' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cust_requires_a_product_name()
    {
        $this->publishCust(['product_name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cust_requires_a_image()
    {
        $this->publishCust(['certificate_number' => null])
            ->assertStatus(400);
    }

    public function publishCust($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $cust = factory(CustomerExport::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/customer/export', [
            'cust_name' => $cust['name'],
            'cust_country' => $cust['country'],
            'cust_state' => $cust['state'],
            'cust_product_name' => $cust['product_name'],
            'cust_turnover' => $cust['turn_over_year'],
            'cust_image' => $cust['image']
        ]);

        return $response;
    }

    public function updateCust()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/customer/export/update/12',[] );

        return $response;
    }

    /** @test */
    public function a_cust_not_found_in_update()
    {
        $this->updateCust()
            ->assertStatus(404);

    }

    /** @test */
    public function a_user_can_delete_cust()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $cust = factory(CustomerExport::class)->create([ 'business_id' => $business->id ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/productive/customer/export/'. $cust->id );

        $response->assertStatus(200);
    }
}
