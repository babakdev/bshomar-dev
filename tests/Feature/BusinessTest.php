<?php

namespace Tests\Feature;

use App\Business;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BusinessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_info_requires_a_name()
    {
        $this->publishBusinessInfo(['name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_info_requires_a_category_id()
    {
        $this->publishBusinessInfo(['category_id' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_info_requires_a_city_id()
    {
        $this->publishBusinessInfo(['city_id' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_info_requires_a_found_date()
    {
        $this->publishBusinessInfo(['found_date' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_info_requires_a_image()
    {
        $this->publishBusinessInfo(['image' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_info_requires_a_business_type()
    {
        $this->publishBusinessInfo(['business_type' => null])
            ->assertStatus(400);
    }

    public function publishBusinessInfo($overrides = [])
    {
        $user = factory(User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $overrides['user_id'] = $user->id;

        $info = factory(Business::class)->make($overrides);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/information/update', (array)$info);

        return $response;
    }
}
