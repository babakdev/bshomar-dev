<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ChartTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_token_invalid_in_chart_statistics()
    {
        $token = 'kdjshgad';
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/chart/statistics', []);

        $response->assertStatus(401);
    }

    /** @test */
    public function business_not_found_in_chart_statistics()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // no create business for this user

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/chart/statistics', []);

        $response->assertStatus(400);
    }

    /** @test */
    public function a_chart_statistics_requires_a_type()
    {
        $this->publishChart(['type' => null, 'sub' => rand(1, 4), 'from' => now(), 'to' => now(), 'date' => 'year'])
            ->assertStatus(400);
    }

    /** @test */
    public function a_chart_statistics_requires_a_sub()
    {
        $this->publishChart(['type' => 'sale', 'sub' => null, 'from' => now(), 'to' => now(), 'date' => 'year'])
            ->assertStatus(400);
    }

    /** @test */
    public function a_chart_statistics_requires_a_from()
    {
        $this->publishChart(['type' => 'sale', 'sub' => rand(1, 4), 'from' => null, 'to' => now(), 'date' => 'year'])
            ->assertStatus(400);
    }

    /** @test */
    public function a_chart_statistics_requires_a_to()
    {
        $this->publishChart(['type' => 'sale', 'sub' => rand(1, 4), 'from' => now(), 'to' => null, 'date' => 'year'])
            ->assertStatus(400);
    }

    /** @test */
    public function a_chart_statistics_requires_a_date()
    {
        $this->publishChart(['type' => 'sale', 'sub' => rand(1, 4), 'from' => now(), 'to' => now(), 'date' => null])
            ->assertStatus(400);
    }

    public function publishChart($data = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $result = [
            'type' => $data['type'],
            'sub' => $data['sub'],
            'from' => $data['from'],
            'to' => $data['to'],
            'date' => $data['date'],
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/chart/statistics', $result);

        return $response;
    }
}
