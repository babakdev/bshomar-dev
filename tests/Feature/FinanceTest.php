<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FinanceTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function invalid_token_can_not_get_sale_history()
    {
        $token = 'kdjshgad';
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/get/sales/history');
        $response->assertStatus(401);
    }

    /** @test */
    public function invalid_business_get_error_in_sale_history()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // no create business

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/get/sales/history');

        $response->assertStatus(400);
    }

    /** @test */
    public function invalid_user_can_not_get_buy_history()
    {
        $token = 'kdjshgad';
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/get/buy/history');

        $response->assertStatus(401);
    }

    /** @test */
    public function a_submit_delivery_requires_a_id()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/submit/delivery/code',['id' => null , 'delivery_code' => 'foo']);

        $response->assertStatus(400);
    }

    /** @test */
    public function a_submit_delivery_requires_a_delivery_code()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/submit/delivery/code',['id' => null , 'delivery_code' => 'foo']);

        $response->assertStatus(400);
    }

    /** @test */
    public function invalid_token_in_submit_delivery_code()
    {
        $token = 'kdjshgad';
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/submit/delivery/code');

        $response->assertStatus(401);
    }

    /** @test */
    public function invalid_business_in_submit_delivery_code()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/submit/delivery/code',['id' => 1 , 'delivery_code' => 'foo']);
        $response->assertStatus(400);
    }

    //todo not completed
    public function not_owner_business_cant_not_submit_delivery_code()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        // no create business

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/submit/delivery/code',['id' => 1 , 'delivery_code' => 'foo']);

        $response->assertStatus(400);
    }

}
