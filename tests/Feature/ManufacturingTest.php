<?php

namespace Tests\Feature;

use App\BusinessModels\ManufacturingLine;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ManufacturingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_manu_requires_a_name()
    {
        $this->publishManu(['name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_manu_requires_a_image()
    {
        $this->publishManu(['image' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_manu_requires_a_description()
    {
        $this->publishManu(['description' => null])
            ->assertStatus(400);
    }

    public function publishManu($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $manu = factory(ManufacturingLine::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/manu', $manu);

        return $response;
    }

    public function updateManu()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/manu/update/12',[] );

        return $response;
    }

    /** @test */
    public function a_manu_not_found_in_update()
    {
        $this->updateManu()
            ->assertStatus(404);

    }

    /** @test */
    public function a_user_can_delete_manu()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $manu = factory(ManufacturingLine::class)->create(['business_id' => $business->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/productive/manu/'. $manu->id );

        $response->assertStatus(200);
    }
}
