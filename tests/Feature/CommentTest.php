<?php

namespace Tests\Feature;

use App\Business;
use App\Comment;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_comment_insert()
    {
        $comment = factory(Comment::class)->create();
        $user = User::find($comment->user_id);
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('GET', '/api/business/get/self/comments');

        $response->assertStatus(200);
    }

    /** @test */
    public function get_product_comments()
    {
        $comment = factory(Comment::class)->create();
        $user = User::find($comment->user_id);
        $business = factory(Business::class)->create(['user_id' => $user->id ]);
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('GET', '/api/business/get/products/comments');

        $response->assertStatus(200);
    }
}
