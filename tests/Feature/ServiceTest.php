<?php

namespace Tests\Feature;

use App\BusinessModels\Service;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_service_requires_a_type()
    {
        $this->publishService(['type' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_service_requires_a_service_info()
    {
        $this->publishService(['service_info' => null])
            ->assertStatus(400);
    }

    public function publishService($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $service = factory(Service::class)->make($overrides);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/service/update', [
            'service_type' => $service->type,
            'service_info' => $service->service_info,
        ]);

        return $response;
    }
}
