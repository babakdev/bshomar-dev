<?php

namespace Tests\Feature;

use App\BusinessModels\Certificate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CertificateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_cert_requires_a_owner_name()
    {
        $this->publishCert(['owner_name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cert_requires_a_type()
    {
        $this->publishCert(['certificate_type' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cert_requires_a_cert_number()
    {
        $this->publishCert(['certificate_number' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cert_requires_a_cert_name()
    {
        $this->publishCert(['certificate_name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cert_requires_a_cert_exporter()
    {
        $this->publishCert(['exporter' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cert_requires_a_cert_start_date()
    {
        $this->publishCert(['start_date' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_cert_requires_a_cert_scope()
    {
        $this->publishCert(['scope' => null])
            ->assertStatus(400);
    }

    public function publishCert($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $cert = factory(Certificate::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/certificate', [
            'cert_owner_name' => $cert['owner_name'],
            'cert_type' => $cert['certificate_type'],
            'cert_num' => $cert['certificate_number'],
            'cert_name' => $cert['certificate_name'],
            'cert_exporter' => $cert['exporter'],
            'cert_start_date' => $cert['start_date'],
            'cert_end_date' => $cert['end_date'],
            'cert_scope' => $cert['scope'],
            'cert_image' => $cert['image'],
        ]);

        return $response;
    }

    public function updateCert()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/certificate/update/12',[] );

        return $response;
    }

    /** @test */
    public function a_cert_not_found_in_update()
    {
        $this->updateCert()
            ->assertStatus(404);

    }

    /** @test */
    public function a_user_can_delete_cert()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $cert = factory(Certificate::class)->create(['business_id' => $business->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/productive/certificate/'. $cert->id );

        $response->assertStatus(200);
    }
}
