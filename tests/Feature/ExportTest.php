<?php

namespace Tests\Feature;

use App\BusinessModels\Export;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExportTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_export_requires_a_ex_value()
    {
        $this->publishExport(['ex_value' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_export_requires_a_ex_of_products()
    {
        $this->publishExport(['ex_of_products' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_export_requires_a_target_market()
    {
        $this->publishExport(['target_market' => null])
            ->assertStatus(400);
    }

    public function publishExport($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $export = factory(Export::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/export', [
            'ex_value' => $export['ex_value'],
            'ex_of_products' => $export['ex_of_products'],
            'target_market' => $export['target_market'],
        ]);

        return $response;
    }

    public function updateExport()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/export/update/12',[] );

        return $response;
    }

    /** @test */
    public function a_export_not_found_in_update()
    {
        $this->updateExport()
            ->assertStatus(404);

    }

    /** @test */
    public function a_user_can_delete_export()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $export = factory(Export::class)->create(['business_id' => $business->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/productive/export/'. $export->id );

        $response->assertStatus(200);
    }
}
