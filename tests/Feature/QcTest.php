<?php

namespace Tests\Feature;

use App\BusinessModels\Qc;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QcTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_qc_requires_a_name()
    {
        $this->publishQc(['name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_qc_requires_a_image()
    {
        $this->publishQc(['image' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_qc_requires_a_description()
    {
        $this->publishQc(['description' => null])
            ->assertStatus(400);
    }

    public function publishQc($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $qc = factory(Qc::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/qc', $qc);

        return $response;
    }

    public function updateQc()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/qc/update/12',[] );

        return $response;
    }

    /** @test */
    public function a_qc_not_found_in_update()
    {
        $this->updateQc()
            ->assertStatus(404);

    }

    /** @test */
    public function a_user_can_delete_qc()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $qc = factory(Qc::class)->create(['business_id' => $business->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/productive/qc/'. $qc->id );

        $response->assertStatus(200);
    }
}
