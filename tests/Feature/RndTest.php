<?php

namespace Tests\Feature;

use App\BusinessModels\ResearchDevelop;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RndTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_rnd_requires_a_name()
    {
        $this->publishRnd(['name' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_rnd_requires_a_image()
    {
        $this->publishRnd(['image' => null])
            ->assertStatus(400);
    }

    /** @test */
    public function a_rnd_requires_a_description()
    {
        $this->publishRnd(['description' => null])
            ->assertStatus(400);
    }

    public function publishRnd($overrides = [])
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $rnd = factory(ResearchDevelop::class)->make($overrides)->toArray();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/rnd', $rnd);

        return $response;
    }

    public function updateRnd()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/business/productive/rnd/update/12',[] );

        return $response;
    }

    /** @test */
    public function a_manu_not_found_in_update()
    {
        $this->updateRnd()
            ->assertStatus(404);

    }

    /** @test */
    public function a_user_can_delete_rnd()
    {
        $user = factory(\App\User::class)->create([
            'phone' => '09111111111',
            'password' => bcrypt('12345678')
        ]);

        // generate authorization token
        $token = \Tymon\JWTAuth\Facades\JWTAuth::fromUser($user);

        $business = factory(\App\Business::class)->create([
            'user_id' => $user->id
        ]);

        $rnd = factory(ResearchDevelop::class)->create(['business_id' => $business->id]);

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $token,
        ])->json('DELETE', '/api/business/productive/rnd/'. $rnd->id );

        $response->assertStatus(200);
    }
}
