<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\BusinessModels\ResearchDevelop::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'name' => $faker->word,
        'description' => $faker->paragraph,
        'image' => $faker->imageUrl(600,400),
    ];
});
