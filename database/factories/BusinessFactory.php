<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Business::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(User::class)->create()->id;
        },
//        'category_id' => $faker->numberBetween(21,171),
        'city_id' => $faker->numberBetween(33,1188),
        'name' => $faker->firstName,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
//        'is_seller' => (int) $faker->boolean,
        'sheba' => $faker->numberBetween(10000,10000000),
        'email' => $faker->email,
        'is_active' => 1,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
        'b2b' => (int) $faker->boolean,
        'b2c' => (int) $faker->boolean,
        'description' => $faker->paragraph,
        'expires_at' => $faker->date('Y/m/d'),
        'has_qc' => (int) $faker->boolean,
//        'panel_id' => 1,
        'product_lines' => $faker->numberBetween(1,100),
        'header_image' => $faker->imageUrl(600,400),
        'logo' => $faker->imageUrl(600,400),
        'advantages' => $faker->paragraph,
        'website' => $faker->url,
        'found_date' => $faker->date('Y/m/d'),
        'agencies' => $faker->paragraph,
        'rules' => $faker->paragraph,
        'business_type' => $faker->numberBetween(1,5)
    ];
});
