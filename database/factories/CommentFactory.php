<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Comment::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
        'product_id' => function(){
            return factory(\App\Product::class)->create()->id;
        },
        'comment_id' => $faker->numberBetween(1,10),
        'body' => $faker->paragraph,
        'is_active' => 1,
    ];
});
