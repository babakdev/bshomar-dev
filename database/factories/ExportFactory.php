<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\BusinessModels\Export::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'ex_value' => $faker->numberBetween(1,6),
        'ex_of_products' => $faker->numberBetween(1,99),
        'target_market' => $faker->word,
        'ex_start_year' => $faker->date('Y/m/d',now()),
        'ex_type' => $faker->word,
        'ex_avg' => $faker->numberBetween(1,30),
        'ex_rules' => $faker->paragraph,
        'langs' => $faker->word,
    ];
});
