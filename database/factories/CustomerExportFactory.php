<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\BusinessModels\CustomerExport::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'name' => $faker->word,
        'country' => $faker->country,
        'state' => $faker->word,
        'product_name' => $faker->word,
        'turn_over_year' => $faker->randomFloat(2,100,10000),
        'image' => $faker->imageUrl(600,400),
    ];
});
