<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\BusinessModels\Service::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'type' => (int) $faker->boolean,
        'service_info' => $faker->paragraph,
    ];
});
