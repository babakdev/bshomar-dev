<?php

use Faker\Generator as Faker;

$factory->define(App\Image::class, function (Faker $faker) {
    return [
        'imageable_id' => $faker->numberBetween(1,100),
        'imageable_type' => 'App\Product',
        'path' => $faker->imageUrl(400,400),
    ];
});
