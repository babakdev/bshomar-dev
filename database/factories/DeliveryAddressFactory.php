<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\DeliveryAddress::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
        'name' => $faker->word,
        'email' => $faker->email,
        'city_id' => $faker->numberBetween(33,1188),
        'address' => $faker->address,
        'post_code' => $faker->postcode,
        'phone' => $faker->phoneNumber,
    ];
});
