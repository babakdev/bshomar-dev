<?php

use App\Business;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(Business::class)->create()->id;
        },
        'category_id' => '',

        'name' => $faker->name,
        'code' => $faker->languageCode,
        'country' => $faker->country,
        'unit' => $faker->randomLetter,
        'min_product' => $faker->numberBetween(1,1000),
        'description' => $faker->paragraph,
        'price' => $faker->numberBetween(1000,1000000),
        'stock' => $faker->numberBetween(0,1000),
        'min_stock' => $faker->numberBetween(1,1000),
        'is_active' => 1,
        'meta_keywords' => $faker->word,
        'discount_cycle' => $faker->numberBetween(1,100),
        'discount' => $faker->numberBetween(1,99),
        'discount_expire_date' => $faker->date('Y/m/d',now()),
        'status' => 1,
        'time_to_delivery' => $faker->numberBetween(1,15),
        'consider' => $faker->paragraph,
        'sales_number' => 0,
    ];
});
