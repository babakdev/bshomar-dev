<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\BusinessModels\Certificate::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'owner_name' => $faker->name,
        'certificate_type' => $faker->word,
        'certificate_number' => $faker->numberBetween(100000,999999),
        'certificate_name' => $faker->word,
        'exporter' => $faker->word,
        'start_date' => $faker->date('Y/m/d' ,now()),
        'end_date' => $faker->date('Y/m/d' ,now()),
        'scope' => $faker->country,
        'image' => $faker->imageUrl(600,400),
    ];
});
