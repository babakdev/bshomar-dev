<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\DiscountCode::class, function (Faker $faker) {
    return [
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'name' => $faker->word,
        'amount_type' => $faker->numberBetween(1,2),
        'percent' => $faker->numberBetween(1,99),
        'amount' => $faker->numberBetween(1000,10000),
        'code' => $faker->unique()->word,
        'start_date' => $faker->date('Y/m/d' ,now()),
        'end_date' => $faker->date('Y/m/d' ,\Carbon\Carbon::now()->addDays(3)),
    ];
});
