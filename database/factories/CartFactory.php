<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Cart::class, function (Faker $faker) {
    return [
        'product_id' => function(){
            return factory(\App\Product::class)->create()->id;
        },
        'business_id' => function(){
            return factory(\App\Business::class)->create()->id;
        },
        'product_name' => $faker->word,
        'product_code' => $faker->numberBetween(1000,99999),
        'price' => $faker->randomFloat(2,1000,100000),
        'quantity' => $faker->numberBetween(1,100),
        'user_phone' => $faker->phoneNumber,
        'session_id' => Str::random(40),
    ];
});
