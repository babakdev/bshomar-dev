<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Request::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
        'product_name' => $faker->word,
        'quantity' => $faker->numberBetween(100,1000),
        'unit' => $faker->word,
        'type' => $faker->numberBetween(1,3)
    ];
});
