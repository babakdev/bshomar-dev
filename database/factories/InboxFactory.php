<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Inbox::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
        'message_id' => $faker->numberBetween(1,10),
        'subject' => $faker->words(2),
        'sent_by' => $faker->email,
        'message' => $faker->paragraph,
        'sender_id' => function(){
            return factory(\App\User::class)->create()->id;
        },
        'seen' => (int) $faker->boolean,
    ];
});
