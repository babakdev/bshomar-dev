<?php

use Illuminate\Database\Seeder;

class CategoryPropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('category_properties')->delete();
        \DB::table('category_properties')->insert([
            [
                'id' => 1,
                'property_id' => 1,
                'category_id' => 2
            ],
            [
                'id' => 2,
                'property_id' => 2,
                'category_id' => 2
            ],
            [
                'id' => 3,
                'property_id' => 3,
                'category_id' => 2
            ],
            [
                'id' => 4,
                'property_id' => 4,
                'category_id' => 2
            ],
            [
                'id' => 5,
                'property_id' => 5,
                'category_id' => 2
            ],
            [
                'id' => 6,
                'property_id' => 6,
                'category_id' => 2
            ],
            [
                'id' => 7,
                'property_id' => 7,
                'category_id' => 2
            ],
            [
                'id' => 8,
                'property_id' => 8,
                'category_id' => 2
            ],
            [
                'id' => 9,
                'property_id' => 9,
                'category_id' => 2
            ],
            [
                'id' => 10,
                'property_id' => 10,
                'category_id' => 2
            ],
            [
                'id' => 11,
                'property_id' => 11,
                'category_id' => 2
            ],
            [
                'id' => 12,
                'property_id' => 12,
                'category_id' => 2
            ],
            [
                'id' => 13,
                'property_id' => 13,
                'category_id' => 2
            ],
            [
                'id' => 14,
                'property_id' => 14,
                'category_id' => 2
            ],
            [
                'id' => 15,
                'property_id' => 15,
                'category_id' => 2
            ],
            [
                'id' => 23,
                'property_id' => 19,
                'category_id' => 2
            ],
            [
                'id' => 24,
                'property_id' => 20,
                'category_id' => 2
            ],
            [
                'id' => 25,
                'property_id' => 21,
                'category_id' => 2
            ],
            [
                'id' => 16,
                'property_id' => 2,
                'category_id' => 1
            ],
            [
                'id' => 17,
                'property_id' => 4,
                'category_id' => 1
            ],
            [
                'id' => 18,
                'property_id' => 6,
                'category_id' => 1
            ],
            [
                'id' => 19,
                'property_id' => 9,
                'category_id' => 1
            ],
            [
                'id' => 20,
                'property_id' => 16,
                'category_id' => 1
            ],
            [
                'id' => 21,
                'property_id' => 17,
                'category_id' => 1
            ],
            [
                'id' => 22,
                'property_id' => 18,
                'category_id' => 1
            ],
            [
                'id' => 26,
                'property_id' => 9,
                'category_id' => 14
            ],
            [
                'id' => 27,
                'property_id' => 3,
                'category_id' => 3
            ],
            [
                'id' => 28,
                'property_id' => 22,
                'category_id' => 3
            ],
            [
                'id' => 29,
                'property_id' => 6,
                'category_id' => 3
            ],
            [
                'id' => 30,
                'property_id' => 23,
                'category_id' => 3
            ],
            [
                'id' => 31,
                'property_id' => 5,
                'category_id' => 3
            ],
            [
                'id' => 56,
                'property_id' => 35,
                'category_id' => 3
            ],
            [
                'id' => 62,
                'property_id' => 38,
                'category_id' => 3
            ],
            [
                'id' => 32,
                'property_id' => 9,
                'category_id' => 17
            ],
            [
                'id' => 33,
                'property_id' => 9,
                'category_id' => 13
            ],
            [
                'id' => 36,
                'property_id' => 1,
                'category_id' => 13
            ],
            [
                'id' => 37,
                'property_id' => 3,
                'category_id' => 13
            ],
            [
                'id' => 34,
                'property_id' => 9,
                'category_id' => 17
            ],
            [
                'id' => 35,
                'property_id' => 9,
                'category_id' => 17
            ],
            [
                'id' => 45,
                'property_id' => 2,
                'category_id' => 11
            ],
            [
                'id' => 46,
                'property_id' => 4,
                'category_id' => 11
            ],
            [
                'id' => 47,
                'property_id' => 9,
                'category_id' => 11
            ],
            [
                'id' => 41,
                'property_id' => 3,
                'category_id' => 17
            ],
            [
                'id' => 42,
                'property_id' => 8,
                'category_id' => 17
            ],
            [
                'id' => 43,
                'property_id' => 23,
                'category_id' => 17
            ],
            [
                'id' => 44,
                'property_id' => 24,
                'category_id' => 17
            ],
            [
                'id' => 51,
                'property_id' => 31,
                'category_id' => 17
            ],
            [
                'id' => 52,
                'property_id' => 32,
                'category_id' => 17
            ],
            [
                'id' => 53,
                'property_id' => 33,
                'category_id' => 17
            ],
            [
                'id' => 54,
                'property_id' => 34,
                'category_id' => 17
            ],
            [
                'id' => 55,
                'property_id' => 35,
                'category_id' => 17
            ],
            [
                'id' => 57,
                'property_id' => 11,
                'category_id' => 17
            ],
            [
                'id' => 60,
                'property_id' => 2,
                'category_id' => 17
            ],
            [
                'id' => 63,
                'property_id' => 39,
                'category_id' => 17
            ],
            [
                'id' => 64,
                'property_id' => 9,
                'category_id' => 6
            ],
            [
                'id' => 65,
                'property_id' => 9,
                'category_id' => 15
            ],
            [
                'id' => 66,
                'property_id' => 1,
                'category_id' => 9
            ],
            [
                'id' => 67,
                'property_id' => 2,
                'category_id' => 9
            ],
            [
                'id' => 68,
                'property_id' => 3,
                'category_id' => 9
            ],
            [
                'id' => 69,
                'property_id' => 6,
                'category_id' => 9
            ],
            [
                'id' => 70,
                'property_id' => 23,
                'category_id' => 9
            ],
            [
                'id' => 71,
                'property_id' => 39,
                'category_id' => 9
            ],
            [
                'id' => 72,
                'property_id' => 40,
                'category_id' => 9
            ],
            [
                'id' => 73,
                'property_id' => 41,
                'category_id' => 9
            ],
            [
                'id' => 74,
                'property_id' => 42,
                'category_id' => 9
            ],
            [
                'id' => 75,
                'property_id' => 43,
                'category_id' => 9
            ],
            [
                'id' => 76,
                'property_id' => 44,
                'category_id' => 9
            ],
            [
                'id' => 77,
                'property_id' => 45,
                'category_id' => 9
            ],
            [
                'id' => 78,
                'property_id' => 46,
                'category_id' => 9
            ],
            [
                'id' => 79,
                'property_id' => 47,
                'category_id' => 9
            ],
            [
                'id' => 80,
                'property_id' => 48,
                'category_id' => 9
            ],
            [
                'id' => 81,
                'property_id' => 49,
                'category_id' => 9
            ],
            [
                'id' => 82,
                'property_id' => 50,
                'category_id' => 9
            ],
            [
                'id' => 83,
                'property_id' => 51,
                'category_id' => 9
            ],
            [
                'id' => 84,
                'property_id' => 52,
                'category_id' => 9
            ],
            [
                'id' => 85,
                'property_id' => 53,
                'category_id' => 9
            ],
            [
                'id' => 86,
                'property_id' => 54,
                'category_id' => 9
            ],
            [
                'id' => 87,
                'property_id' => 55,
                'category_id' => 9
            ],
            [
                'id' => 88,
                'property_id' => 56,
                'category_id' => 9
            ],
            [
                'id' => 89,
                'property_id' => 57,
                'category_id' => 9
            ],
            [
                'id' => 90,
                'property_id' => 58,
                'category_id' => 9
            ],
            [
                'id' => 91,
                'property_id' => 59,
                'category_id' => 9
            ],
            [
                'id' => 92,
                'property_id' => 60,
                'category_id' => 9
            ],
            [
                'id' => 93,
                'property_id' => 61,
                'category_id' => 9
            ],
            [
                'id' => 94,
                'property_id' => 62,
                'category_id' => 9
            ],
            [
                'id' => 95,
                'property_id' => 63,
                'category_id' => 9
            ],
            [
                'id' => 96,
                'property_id' => 64,
                'category_id' => 9
            ],
            [
                'id' => 97,
                'property_id' => 65,
                'category_id' => 9
            ],
            [
                'id' => 98,
                'property_id' => 66,
                'category_id' => 9
            ],
            [
                'id' => 99,
                'property_id' => 67,
                'category_id' => 9
            ],
            [
                'id' => 100,
                'property_id' => 68,
                'category_id' => 9
            ],
            [
                'id' => 101,
                'property_id' => 69,
                'category_id' => 9
            ],
            [
                'id' => 102,
                'property_id' => 70,
                'category_id' => 9
            ],
            [
                'id' => 103,
                'property_id' => 71,
                'category_id' => 9
            ],
            [
                'id' => 104,
                'property_id' => 72,
                'category_id' => 9
            ],
            [
                'id' => 105,
                'property_id' => 73,
                'category_id' => 9
            ],
            [
                'id' => 106,
                'property_id' => 74,
                'category_id' => 9
            ],
            [
                'id' => 107,
                'property_id' => 75,
                'category_id' => 9
            ],
            [
                'id' => 108,
                'property_id' => 76,
                'category_id' => 9
            ],
            [
                'id' => 109,
                'property_id' => 77,
                'category_id' => 9
            ],
            [
                'id' => 110,
                'property_id' => 78,
                'category_id' => 9
            ],
            [
                'id' => 111,
                'property_id' => 79,
                'category_id' => 9
            ],
            [
                'id' => 112,
                'property_id' => 80,
                'category_id' => 9
            ],
            [
                'id' => 113,
                'property_id' => 81,
                'category_id' => 9
            ],
            [
                'id' => 114,
                'property_id' => 82,
                'category_id' => 9
            ],
            [
                'id' => 115,
                'property_id' => 83,
                'category_id' => 9
            ],
            [
                'id' => 116,
                'property_id' => 84,
                'category_id' => 9
            ],
            [
                'id' => 117,
                'property_id' => 85,
                'category_id' => 9
            ],
            [
                'id' => 118,
                'property_id' => 86,
                'category_id' => 9
            ],
            [
                'id' => 125,
                'property_id' => 9,
                'category_id' => 9
            ],
            [
                'id' => 119,
                'property_id' => 1,
                'category_id' => 7
            ],
            [
                'id' => 120,
                'property_id' => 2,
                'category_id' => 7
            ],
            [
                'id' => 121,
                'property_id' => 87,
                'category_id' => 7
            ],
            [
                'id' => 122,
                'property_id' => 88,
                'category_id' => 7
            ],
            [
                'id' => 123,
                'property_id' => 89,
                'category_id' => 7
            ],
            [
                'id' => 124,
                'property_id' => 9,
                'category_id' => 7
            ],
            [
                'id' => 126,
                'property_id' => 9,
                'category_id' => 17
            ],
            [
                'id' => 127,
                'property_id' => 9,
                'category_id' => 12
            ],
            [
                'id' => 128,
                'property_id' => 9,
                'category_id' => 17
            ]
        ]);
    }
}
