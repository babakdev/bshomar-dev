<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        \DB::table('roles')->insert([
            [
                'id'   => 1,
                'name' => 'مدیر'
            ],
            [
                'id' =>2,
                'name' =>'پشتیبان'
            ],
            [
                'id' =>3,
                'name' => 'کسب و کار'
            ],
            [
                'id' => 4,
                'name' => 'کاربر عادی'
            ],
            [
                'id' => 5,
                'name' => 'پشتیبان اصناف'
            ],
            [
                'id' => 6,
                'name' => 'پیک'
            ]
        ]);
    }
}
