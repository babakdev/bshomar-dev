<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        Schema::disableForeignKeyConstraints();
        $this->call([
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            CategoryTableSeeder::class,
            CitiesTableSeeder::class,
            PanelTableSeeder::class,
            InvoiceTableSeeder::class,
            PropertiesTableSeeder::class,
            CategoryPropertiesTableSeeder::class,
            CategoryLearnTableSeeder::class,
        ]);
//         factory(App\Product::class,100)->create();
//         factory(App\Image::class,100)->create();
//        Schema::enableForeignKeyConstraints();
    }
}
