<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        DB::table('categories')->insert([
            [
                'id' => 1,
                'parent_id' => 0,
                'image' => '2.webp',
                'name' => 'مواد غذایی , شیرینی , پروتئینی',
                'slug' => 'daily-needs',
                'is_active' => 1
            ],
            [
                'id' => 2,
                'parent_id' => 0,
                'image' => '1.webp',
                'name' => 'آرایشی , بهداشتی',
                'slug' => 'cosmetic',
                'is_active' => 1
            ],
            [
                'id' => 3,
                'parent_id' => 0,
                'image' => '4.webp',
                'name' => 'پوشاک , منسوجات',
                'slug' => 'clothing-leather',
                'is_active' => 1
            ],
            [
                'id' => 4,
                'parent_id' => 0,
                'image' => '5.webp',
                'name' => 'رستوان , تالار , کافه , اغذیه',
                'slug' => 'restaurants',
                'is_active' => 1
            ],
            [
                'id' => 5,
                'parent_id' => 0,
                'image' => '41.webp',
                'name' => 'کیف ، کفش , کمربند',
                'slug' => 'bag-shoes-belt',
                'is_active' => 1
            ],
            [
                'id' => 6,
                'parent_id' => 0,
                'image' => '11.webp',
                'name' => 'صنعت ساختمان',
                'slug' => 'building',
                'is_active' => 1
            ],
            [
                'id' => 7,
                'parent_id' => 0,
                'image' => '14.webp',
                'name' => 'لوازم خانگی و اداری',
                'slug' => 'appliances',
                'is_active' => 1
            ],
            [
                'id' => 8,
                'parent_id' => 0,
                'image' => '8.webp',
                'name' => 'وسایل نقلیه، خدمات مربوطه',
                'slug' => 'vehicles',
                'is_active' => 1
            ],
            [
                'id' => 9,
                'parent_id' => 0,
                'image' => '13.webp',
                'name' => 'دیجیتال',
                'slug' => 'digitals',
                'is_active' => 1
            ],
            [
                'id' => 10,
                'parent_id' => 0,
                'image' => '73.webp',
                'name' => 'کشاورزی',
                'slug' => 'farming',
                'is_active' => 1
            ],
            [
                'id' => 11,
                'parent_id' => 0,
                'image' => '9.webp',
                'name' => 'دام ، طیور، شیلات',
                'slug' => 'fish-hen-sheep',
                'is_active' => 1
            ],
            [
                'id' => 12,
                'parent_id' => 0,
                'image' => '16.webp',
                'name' => 'ورزش و سرگرمی',
                'slug' => 'sport-fun',
                'is_active' => 1
            ],
            [
                'id' => 13,
                'parent_id' => 0,
                'image' => '6.webp',
                'name' => 'چاپ نشر تبلیغات',
                'slug' => 'advertising',
                'is_active' => 1
            ],
            [
                'id' => 14,
                'parent_id' => 0,
                'image' => '3.webp',
                'name' => 'بهداشت و درمان',
                'slug' => 'health',
                'is_active' => 1
            ],
            [
                'id' => 15,
                'parent_id' => 0,
                'image' => '12.webp',
                'name' => 'علمی آموزشی',
                'slug' => 'science',
                'is_active' => 1
            ],
            [
                'id' => 16,
                'parent_id' => 0,
                'image' => '201.webp',
                'name' => 'صنایع و کارخانجات',
                'slug' => 'industries',
                'is_active' => 1
            ],
            [
                'id' => 17,
                'parent_id' => 0,
                'image' => '7.webp',
                'name' => 'خدمات',
                'slug' => 'servicing',
                'is_active' => 1
            ],
            [
                'id' => 18,
                'parent_id' => 0,
                'image' => '122.webp',
                'name' => 'صنایع دستی',
                'slug' => 'handycrafts',
                'is_active' => 1
            ],

            //SubCategories
            //1 aqzie
            [
                'id' => 22,
                'parent_id' => 1,
                'image' => '27.webp',
                'name' => 'فروشگاههای موادغذایی , هایپرمارکتها , سوپرمارکتها',
                'slug' => 'markets',
                'is_active' => 1
            ],
            [
                'id' => 23,
                'parent_id' => 1,
                'image' => '24.webp',
                'name' => 'شیرینی ،بستنی , فتیر',
                'slug' => 'confectionaries',
                'is_active' => 1
            ],
            [
                'id' => 24,
                'parent_id' => 1,
                'image' => '29.webp',
                'name' => 'تولید و فروش آجیل',
                'slug' => 'nuts',
                'is_active' => 1
            ],
            [
                'id' => 25,
                'parent_id' => 1,
                'image' => '30.webp',
                'name' => 'لبنیات ،ترشی , سبزی خانگی',
                'slug' => 'vegetables',
                'is_active' => 1
            ],
            [
                'id' => 26,
                'parent_id' => 1,
                'image' => '32.webp',
                'name' => 'میوه , سبزی و تره بار',
                'slug' => 'fruits',
                'is_active' => 1
            ],
            [
                'id' => 27,
                'parent_id' => 1,
                'image' => '33.webp',
                'name' => 'پروتئینی, مرغ و ماهی , سوسیس کالباس',
                'slug' => 'proteins',
                'is_active' => 1
            ],
            [
                'id' => 28,
                'parent_id' => 1,
                'image' => '27.webp', //TODO
                'name' => 'فروشگاههای موادغذایی ارگانیک',
                'slug' => 'organic-shops',
                'is_active' => 1
            ],

            //2 arayeshi
            [
                'id' => 31,
                'parent_id' => 2,
                'image' => '21.webp',
                'name' => 'آرایشگاه های مردانه',
                'slug' => 'menbarber',
                'is_active' => 1
            ],
            [
                'id' => 32,
                'parent_id' => 2,
                'image' => '22.webp',
                'name' => 'سالن های آرایش زنانه',
                'slug' => 'womensalons',
                'is_active' => 1
            ],
            [
                'id' => 33,
                'parent_id' => 2,
                'image' => '23.webp',
                'name' => 'لوازم آرایشی و بهداشتی',
                'slug' => 'cosmetics-perfume',
                'is_active' => 1
            ],
            //3 pooshak
            [
                'id' => 36,
                'parent_id' => 3,
                'image' => '39.webp',
                'name' => 'پوشاک فروشان',
                'slug' => 'clothing',
                'is_active' => 1
            ],
            [
                'id' => 37,
                'parent_id' => 3,
                'image' => '42.webp',
                'name' => 'خیاطان , تریکوبافان , تولیدکنندگان پوشاک',
                'slug' => 'sewing-supplies',
                'is_active' => 1
            ],
            [
                'id' => 38,
                'parent_id' => 3,
                'image' => '40.webp',
                'name' => 'تولید و فروش منسوجات خانگی و پرده و پارچه',
                'slug' => 'productive-fabrics',
                'is_active' => 1
            ],
            //4 restaurants
            [
                'id' => 41,
                'parent_id' => 4,
                'image' => '25.webp',
                'name' => 'رستوران , کبابی و جگرکی',
                'slug' => 'rests-kebabs',
                'is_active' => 1
            ],
            [
                'id' => 42,
                'parent_id' => 4,
                'image' => '28.webp',
                'name' => 'ساندویچی , اغذیه , فست فود',
                'slug' => 'fastfoods',
                'is_active' => 1
            ],
            [
                'id' => 43,
                'parent_id' => 4,
                'image' => '31.webp',
                'name' => 'طباخی , حلیم پزی و غذاپزی ها',
                'slug' => 'halim-tabaqi',
                'is_active' => 1
            ],
            [
                'id' => 44,
                'parent_id' => 4,
                'image' => '26.webp',
                'name' => 'تالارهای پذیرایی',
                'slug' => 'halls',
                'is_active' => 1
            ],
            [
                'id' => 45,
                'parent_id' => 4,
                'image' => '45.webp', //todo
                'name' => 'کافی شاپ ها',
                'slug' => 'coffeshops',
                'is_active' => 1
            ],
            //5 kif kafsh belt
            [
                'id' => 47,
                'parent_id' => 5,
                'image' => '113.webp', //todo
                'name' => 'تولیدکنندگان کیف ، کفش , کمربند',
                'slug' => 'bag-belt-prods',
                'is_active' => 1
            ],
            [
                'id' => 48,
                'parent_id' => 5,
                'image' => '114.webp', //todo
                'name' => 'فروشندگان کیف ،کفش , کمربند',
                'slug' => 'bag-belt-sellers',
                'is_active' => 1
            ],
            //6 sakhteman
            [
                'id' => 51,
                'parent_id' => 6,
                'image' => '79.webp',
                'name' => 'مصالح  ساختمانی',
                'slug' => 'materials-productives',
                'is_active' => 1
            ],
            [
                'id' => 52,
                'parent_id' => 6,
                'image' => '78.webp', //todo
                'name' => ' کابینت و تزیینات ساختمان',
                'slug' => 'cabinets',
                'is_active' => 1
            ],
            [
                'id' => 53,
                'parent_id' => 6,
                'image' => '80.webp', //todo
                'name' => 'صنایع آهن ، آلومینیوم ، شیشه و رنگ ساختمان',
                'slug' => 'iron-alm-pain-glass',
                'is_active' => 1
            ],
            [
                'id' => 54,
                'parent_id' => 6,
                'image' => '56.webp', //todo
                'name' => 'تاسیسات و برق ساختمان',
                'slug' => 'builing-electronics',
                'is_active' => 1
            ],
            [
                'id' => 55,
                'parent_id' => 6,
                'image' => '55.webp', //todo
                'name' => 'لوازم بهداشتی و یراق آلات ساختمان',
                'slug' => 'builing-equis',
                'is_active' => 1
            ],
            [
                'id' => 59,
                'parent_id' => 6,
                'image' => '81.webp', //todo
                'name' => 'سایر لوازم و تجهیزات ساختمانی',
                'slug' => 'builing-others',
                'is_active' => 1
            ],
            //7 khanegi edari
            [
                'id' => 60,
                'parent_id' => 7,
                'image' => '97.webp',
                'name' => ' لوازم خانگی',
                'slug' => 'decorating',
                'is_active' => 1
            ],
            [
                'id' => 61,
                'parent_id' => 7,
                'image' => '100.webp',
                'name' => 'لوازم اداری و دفتری',
                'slug' => 'office-equipments',
                'is_active' => 1
            ],
            [
                'id' => 62,
                'parent_id' => 7,
                'image' => '103.webp',
                'name' => 'مبلمان و سرویس خواب',
                'slug' => 'furniture-bed-set',
                'is_active' => 1
            ],
            [
                'id' => 63,
                'parent_id' => 7,
                'image' => '99.webp',
                'name' => 'فرش و موکت',
                'slug' => 'carpet',
                'is_active' => 1
            ],
            [
                'id' => 64,
                'parent_id' => 7,
                'image' => '102.webp',
                'name' => 'تعمیرکاران لوازم خانگی و برقی',
                'slug' => 'home-electric-equipments',
                'is_active' => 1
            ],
            [
                'id' => 65,
                'parent_id' => 7,
                'image' => '98.webp',
                'name' => 'ظرف و ظروف ، لوستر و تزیینات',
                'slug' => 'dishes-equipments',
                'is_active' => 1
            ],
            [
                'id' => 69,
                'parent_id' => 7,
                'image' => '67.webp', //todo
                'name' => 'سایر لوازم خانگی',
                'slug' => 'homeapp-others',
                'is_active' => 1
            ],
            //8 naghlie
            [
                'id' => 70,
                'parent_id' => 8,
                'image' => '64.webp',
                'name' => 'لوازم یدکی اتومبیل',
                'slug' => 'vehicle-equipments',
                'is_active' => 1
            ],
            [
                'id' => 71,
                'parent_id' => 8,
                'image' => '60.webp',
                'name' => 'تعمیرکاران و سرویسکاران اتومبیل',
                'slug' => 'vehicle-services',
                'is_active' => 1
            ],
            [
                'id' => 72,
                'parent_id' => 8,
                'image' => '63.webp',
                'name' => 'حمل و نقل بار و مسافر',
                'slug' => 'vehicle-cargo',
                'is_active' => 1
            ],
            [
                'id' => 73,
                'parent_id' => 8,
                'image' => '66.webp',
                'name' => 'کارواش و پارکینگ',
                'slug' => 'carwash-parkings',
                'is_active' => 1
            ],
            [
                'id' => 74,
                'parent_id' => 8,
                'image' => '62.webp',
                'name' => 'نمایشگاه های اتومبیل',
                'slug' => 'vehicle-shops',
                'is_active' => 1
            ],
            [
                'id' => 79,
                'parent_id' => 8,
                'image' => '61.webp',
                'name' => 'سایر خدمات اتومبیل',
                'slug' => 'vehicle-others',
                'is_active' => 1
            ],
            //9 digital
            [
                'id' => 80,
                'parent_id' => 9,
                'image' => '92.webp',
                'name' => 'موبایل , تبلت و تجهیزات جانبی',
                'slug' => 'mobile-tablet',
                'is_active' => 1
            ],
            [
                'id' => 81,
                'parent_id' => 9,
                'image' => '91.webp',
                'name' => 'کامپیوتر و لپ تاپ',
                'slug' => 'computer-services',
                'is_active' => 1
            ],
            [
                'id' => 82,
                'parent_id' => 9,
                'image' => '93.webp',
                'name' => 'دوربین و لوازم‌جانبی',
                'slug' => 'camera',
                'is_active' => 1
            ],
            [
                'id' => 83,
                'parent_id' => 9,
                'image' => '95.webp',
                'name' => 'لوازم الکترونیک',
                'slug' => 'electronics',
                'is_active' => 1
            ],
            [
                'id' => 84,
                'parent_id' => 9,
                'image' => '96.webp',
                'name' => 'کافی نت ها',
                'slug' => 'cnets',
                'is_active' => 1
            ],
            [
                'id' => 85,
                'parent_id' => 9,
                'image' => '51.webp',
                'name' => 'آتلیه و عکاسی ها',
                'slug' => 'atelier',
                'is_active' => 1
            ],
            [
                'id' => 89,
                'parent_id' => 9,
                'image' => '94.webp', //todo
                'name' => 'سایر خدمات و لوازم دیجیتال',
                'slug' => 'software',
                'is_active' => 1
            ],
            //10 keshavarzi
            [
                'id' => 90,
                'parent_id' => 10,
                'image' => '73.webp',
                'name' => 'تولیدکنندگان محصولات کشاورزی و باغبانی',
                'slug' => 'farms',
                'is_active' => 1
            ],
            [
                'id' => 91,
                'parent_id' => 10,
                'image' => '72.webp',
                'name' => 'ماشین الات و تجهیزات کشاورزی',
                'slug' => 'farming-equipments',
                'is_active' => 1
            ],
            [
                'id' => 92,
                'parent_id' => 10,
                'image' => '71.webp',
                'name' => 'سم و کود فروشی ها',
                'slug' => 'poison-dung-shops',
                'is_active' => 1
            ],
            //11 dam toyour shilat
            [
                'id' => 96,
                'parent_id' => 11,
                'image' => '69.webp',
                'name' => 'تولید کنندگان دام ، طیور و شیلات',
                'slug' => 'birds-fishes',
                'is_active' => 1
            ],
            [
                'id' => 97,
                'parent_id' => 11,
                'image' => '70.webp',
                'name' => ' فروشندگان خوراک دام ، طیور و شیلات',
                'slug' => 'fisheries-foods',
                'is_active' => 1
            ],
            //12 varzeshi
            [
                'id' => 100,
                'parent_id' => 12,
                'image' => '111.webp',
                'name' => 'فروشگاه های لوازم ورزشی',
                'slug' => 'sport-equipment-shops',
                'is_active' => 1
            ],
            [
                'id' => 101,
                'parent_id' => 12,
                'image' => '112.webp',
                'name' => 'اسباب بازی و لوازم سرگرمی',
                'slug' => 'toys',
                'is_active' => 1
            ],
            [
                'id' => 102,
                'parent_id' => 12,
                'image' => '110.webp',
                'name' => 'باشگاه‌ها و اماکن ورزشی',
                'slug' => 'clubs',
                'is_active' => 1
            ],
            [
                'id' => 109,
                'parent_id' => 12,
                'image' => '109.webp', //todo
                'name' => 'سایر',
                'slug' => 'sport-others',
                'is_active' => 1
            ],
            //13 chap
            [
                'id' => 110,
                'parent_id' => 13,
                'image' => '47.webp',
                'name' => 'انتشارات و چاپخانه ها',
                'slug' => 'publishers',
                'is_active' => 1
            ],
            [
                'id' => 111,
                'parent_id' => 13,
                'image' => '48.webp',
                'name' => 'کانون های آگهی و تبلیغات',
                'slug' => 'advertising-offices',
                'is_active' => 1
            ],
            [
                'id' => 112,
                'parent_id' => 13,
                'image' => '49.webp',
                'name' => 'کتاب و لوازم التحریر',
                'slug' => 'book-sellers',
                'is_active' => 1
            ],
            [
                'id' => 119,
                'parent_id' => 13,
                'image' => '6.webp',
                'name' => 'سایر',
                'slug' => 'chap-others',
                'is_active' => 1
            ],
            //14 darman
            [
                'id' => 120,
                'parent_id' => 14,
                'image' => '34.webp',
                'name' => 'ازمایشگاهها',
                'slug' => 'labs',
                'is_active' => 1
            ],
            [
                'id' => 121,
                'parent_id' => 14,
                'image' => '35.webp',
                'name' => 'درمانگاه و کلینیک ها',
                'slug' => 'hospitals',
                'is_active' => 1
            ],
            [
                'id' => 122,
                'parent_id' => 14,
                'image' => '36.webp',
                'name' => 'داروخانه و عطاری ها',
                'slug' => 'pharmacologies',
                'is_active' => 1
            ],
            [
                'id' => 123,
                'parent_id' => 14,
                'image' => '38.webp',
                'name' => 'لوازم و تجهیزات پزشکی',
                'slug' => 'medical-equipments',
                'is_active' => 1
            ],
            [
                'id' => 124,
                'parent_id' => 14,
                'image' => '37.webp',
                'name' => 'مطب ها',
                'slug' => 'doctors',
                'is_active' => 1
            ],
            //15 elmi
            [
                'id' => 130,
                'parent_id' => 15,
                'image' => '87.webp',
                'name' => 'آموزشگاه های علمی',
                'slug' => 'learning-helpers',
                'is_active' => 1
            ],
            [
                'id' => 131,
                'parent_id' => 15,
                'image' => '88.webp',
                'name' => 'آموزشگاه های فنی',
                'slug' => 'skill-learning',
                'is_active' => 1
            ],
            [
                'id' => 132,
                'parent_id' => 15,
                'image' => '89.webp',
                'name' => 'آموزشگاه های هنری',
                'slug' => 'art-learning',
                'is_active' => 1
            ],
            [
                'id' => 133,
                'parent_id' => 15,
                'image' => '83.webp',
                'name' => 'مدارس و مهد کودک ها',
                'slug' => 'basic-learning',
                'is_active' => 1
            ],
            //16 sanat
            [
                'id' => 140,
                'parent_id' => 16,
                'image' => '201.webp',
                'name' => 'مستقر در شهرکهای صنعتی',
                'slug' => 'industries-area',
                'is_active' => 1
            ],
            [
                'id' => 141,
                'parent_id' => 16,
                'image' => '201.webp',
                'name' => 'مستقر در سایر مناطق',
                'slug' => 'industries-general',
                'is_active' => 1
            ],
            //17 khadamat
            [
                'id' => 150,
                'parent_id' => 17,
                'image' => '107.webp',
                'name' => 'دفاتر پیش خوان دولت',
                'slug' => 'government-offices',
                'is_active' => 1
            ],
            [
                'id' => 151,
                'parent_id' => 17,
                'image' => '15.webp',
                'name' => 'دفاتر خدمات ارتباطی',
                'slug' => 'arts',
                'is_active' => 1
            ],
            [
                'id' => 152,
                'parent_id' => 17,
                'image' => '108.webp',
                'name' => 'پلیس +10',
                'slug' => 'police+10',
                'is_active' => 1
            ],
            [
                'id' => 153,
                'parent_id' => 17,
                'image' => '58.webp',
                'name' => 'نظافت منازل و آپارتمانها',
                'slug' => 'cleaning',
                'is_active' => 1
            ],
            [
                'id' => 154,
                'parent_id' => 17,
                'image' => '75.webp',
                'name' => 'طلا و جواهر , ساعت',
                'slug' => 'gold-jewelry',
                'is_active' => 1
            ],
            [
                'id' => 155,
                'parent_id' => 17,
                'image' => '59.webp',
                'name' => 'هتل و مسافر خانه ها',
                'slug' => 'hotel-motel',
                'is_active' => 1
            ],
            [
                'id' => 156,
                'parent_id' => 17,
                'image' => '57.webp',
                'name' => 'آژانس های مسافرتی',
                'slug' => 'trip-agencies',
                'is_active' => 1
            ],
            [
                'id' => 157,
                'parent_id' => 17,
                'image' => '53.webp', //todo
                'name' => 'مشاورین املاک',
                'slug' => 'amlak-cons',
                'is_active' => 1
            ],
            [
                'id' => 158,
                'parent_id' => 17,
                'image' => '55.webp',
                'name' => 'گل فروشی ها',
                'slug' => 'flowers',
                'is_active' => 1
            ],
            [
                'id' => 159,
                'parent_id' => 17,
                'image' => '76.webp',
                'name' => 'عینک',
                'slug' => 'glasses',
                'is_active' => 1
            ],
            [
                'id' => 169,
                'parent_id' => 17,
                'image' => '54.webp',
                'name' => 'سایر خدمات',
                'slug' => 'service-others',
                'is_active' => 1
            ],
            //18 handycrafts
            [
                'id' => 170,
                'parent_id' => 18,
                'image' => '122.webp',
                'name' => 'تولیدکنندگان صنایع دستی',
                'slug' => 'handycrafts-prods',
                'is_active' => 1
            ],
            [
                'id' => 171,
                'parent_id' => 18,
                'image' => '122.webp',
                'name' => 'فروشندگان صنایع دستی',
                'slug' => 'handycrafts-sellers',
                'is_active' => 1
            ],
        ]);
    }
}
