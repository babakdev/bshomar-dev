<?php

use App\Invoice;
use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->delete();
        DB::table('invoices')->insert([
            [
                'id'   => 1,
                'last_invoice_number' => '989',
                'date_update' => \Carbon\Carbon::now()
            ]
        ]);
    }
}
