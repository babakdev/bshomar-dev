<?php

use Illuminate\Database\Seeder;

class PanelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // type:1 free, type:2 non-free,type:3 unlimited
        DB::table('panels')->delete();
        \DB::table('panels')->insert([
            [
                'id' => 1,
                'name' => 'رایگان',
                'amount' => '0',
                'time' => 'دائمی',
                'description' => '<li><b>10</b> عدد کالا</li>
                            <li><b>پشتیبانی</b> 8/6</li>
                            <li>امکان فروش کالا با سبد خرید سایت</li>
                            <li>شامل تبلیغات در پنل شما</li>',
                'product_limit' => 10,
                'type' => '1',
            ],
           [
                'id' => 2,
                'name' => 'برنزی',
                'amount' => '60000',
                'time' => 'یک سال',
                'description' => '<li><b>20</b> عدد کالا</li>
                            <li><b>پشتیبانی</b> 8/6</li>
                            <li>امکان فروش کالا با سبد خرید سایت</li>
                            <li>شامل تبلیغات محدود در پنل شما</li>',
               'product_limit' => 20,
                'type' => '2',
            ],
            [
                'id' => 3,
                'name' => 'نقره ای',
                'amount' => '100000',
                'time' => 'یک سال',
                'description' => '<li><b>40</b> عدد کالا</li>
                            <li><b>پشتیبانی</b> 16/7</li>
                            <li>امکان فروش کالا با سبد خرید سایت</li>
                            <li>بدون تبلیغات در پنل شما</li>',
                'product_limit' => 40,
                'type' => '3',
            ],
            [
                'id' => 4,
                'name' => 'طلایی',
                'amount' => '200000',
                'time' => 'یک سال',
                'description' => '<li> نامحدود کالا</li>
                            <li><b>پشتیبانی</b> 16/7</li>
                            <li>امکان فروش کالا با سبد خرید سایت</li>
                            <li>بدون تبلیغات در پنل شما</li>',
                'product_limit' => 1000,
                'type' => '4',
            ]
        ]);
    }
}
