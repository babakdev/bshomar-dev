<?php

use Illuminate\Database\Seeder;

class CategoryLearnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('category_learns')->delete();
        \DB::table('category_learns')->insert([
            [
                'id'   => 1,
                'title' => 'ورود و ثبت نام',
                'icon' => 'fas fa-user-plus'
            ],
            [
                'id'   => 2,
                'title' => 'روند خرید کالا',
                'icon' => 'fas fa-shopping-cart'
            ],
            [
                'id'   => 3,
                'title' => 'پیگیری ارسال سفارش',
                'icon' => 'fas fa-truck-moving'
            ],
            [
                'id'   => 4,
                'title' => 'کد تخفیف',
                'icon' => 'fas fa-tags'
            ],[
                'id'   => 5,
                'title' => 'چند مرسوله ای',
                'icon' => 'fas fa-mail-bulk'
            ],[
                'id'   => 6,
                'title' => 'بازگرداندن کالا',
                'icon' => 'fas fa-undo-alt'
            ],[
                'id'   => 7,
                'title' => 'بازاریابی مشارکتی',
                'icon' => 'fas fa-hand-holding-usd'
            ],[
                'id'   => 8,
                'title' => 'پنل مدیریت کسب و کار',
                'icon' => 'fas fa-address-card'
            ],
            [
                'id'   => 9,
                'title' => 'سایر موارد',
                'icon' => 'fas fa-braille'
            ]
        ]);
    }
}
