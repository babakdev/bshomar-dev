<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
        [   'id' => 1,
            'role_id' => 1,
            'is_active' => 1,
            'name' => 'bshomarAdmin',
            'email' => 'admin@bshomar.com',
            'phone' => '88888888',
            'password' => bcrypt('88888888')
        ],
            [   'id' => 2,
                'role_id' => 2,
                'is_active' => 1,
                'name' => 'bshomarSupport',
                'email' => 'info@bshomar.com',
                'phone' => '99999999911',
                'password' => bcrypt('bshomar@info1')
            ]
         ]);
    }
}
