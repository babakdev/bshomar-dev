<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment('buyer')->index();
            $table->unsignedBigInteger('address_id');
            $table->enum('order_status', ['در انتظار پرداخت','پرداخت شد', 'ارسال شد', 'کنسل شد', 'تسویه شد']);
            $table->float('shipping_charges')->default('0'); //money for post office
            $table->float('total_amount',191,2);

            $table->unsignedBigInteger('trans_id')->nullable();//Bank Gate
            $table->unsignedBigInteger('id_get')->nullable();//Bank Gate
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
