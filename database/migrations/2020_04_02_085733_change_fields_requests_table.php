<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->string('quantity')->nullable()->change();
            $table->string('unit')->nullable()->change();
            $table->string('type')->nullable()->change();
            $table->dropColumn('product_name');
            $table->unsignedBigInteger('business_id')->after('user_id')->nullable();
            $table->unsignedBigInteger('product_id')->after('user_id')->nullable();
            $table->text('message')->nullable()->after('user_id');

            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->string('quantity')->nullable(false)->change();
            $table->string('unit')->nullable(false)->change();
            $table->string('type')->nullable(false)->change();

            $driver = Schema::connection($this->getConnection())->getConnection()->getDriverName();
            if($driver != 'sqlite') {
                $table->dropForeign(['business_id','product_id']);
            }
            $table->dropColumn(['business_id', 'product_id', 'message']);
        });
    }
}
