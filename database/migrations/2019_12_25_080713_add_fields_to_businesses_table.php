<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->string('found_date',191)->comment('تاریخ تاسیس')->nullable();
            $table->string('website',191)->nullable();
            $table->string('advantages',256)->comment('مزایای کلیدی')->nullable();
            $table->string('logo',191)->nullable();
            $table->string('header_image',191)->nullable();
            $table->text('my_rules')->nullable();
            $table->integer('product_lines')->comment('تعداد خط تولید')->nullable();
            $table->integer('max_products')->default('10');
            $table->boolean('has_qc')->default('0');
            $table->integer('tax')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->dropColumn(['found_date','website','advantages','logo','header_image','my_rules','product_lines','max_products','has_qc','tax']);
        });
    }
}
