<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('link')->comment('Auto advertisement link or manual image link');
            $table->string('href')->comment('Here is the link to the image for manual ads')->nullable();
            $table->tinyInteger('page')->comment('1 => index, 2 => panel ,3 => pages');
            $table->tinyInteger('type')->comment('1 => auto advertising, 2 => manual advertising');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisings');
    }
}
