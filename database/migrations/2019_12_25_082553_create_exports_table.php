<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('business_id')->index();
            $table->tinyInteger('ex_value')
                ->comment('1 => under 1 million, 2 => between 1 and 3 million, 3 => between 3 and 5 million, 4 => between 5 and 10 million, 5 => between 10 and 100 million, 6 => over 100 million')->nullable();
            $table->integer('ex_of_products')->comment('درصد از تولید مادر')->nullable();
            $table->string('target_market')->nullable();
            $table->string('ex_start_year')->nullable();
            $table->string('ex_type')->comment('هوایی، ریلی، زمینی، بندرامام خمینی')->nullable();
            $table->integer('ex_avg')->comment('میانگین تحویل کالا برحسب روز')->nullable();
            $table->text('ex_rules')->nullable();
            $table->text('langs')->nullable();
            $table->timestamps();

            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exports');
    }
}
