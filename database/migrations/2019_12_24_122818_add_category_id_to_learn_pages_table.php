<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToLearnPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('learn_pages', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->default(9)->index()->after('id');
            $table->foreign('category_id')->references('id')->on('category_learns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('learn_pages', function (Blueprint $table) {
            $driver = Schema::connection($this->getConnection())->getConnection()->getDriverName();
            if ($driver != 'sqlite') {

                $table->dropForeign(['category_id']);
            }
            $table->dropIndex('learn_pages_category_id_index');
            $table->dropColumn('category_id');
        });
    }
}
