<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeDeliveryAndReturnFieldsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('delivery_type')->comment('1 => in day , 2 => in hour')->default(1)->after('status');
            $table->integer('product_return_type')->comment('1 => in day , 2 => in hour')->default(1)->after('status');
            $table->integer('product_return')->nullable()->after('status');
            $table->integer('min_shipping_cost')->nullable()->after('status');
            $table->integer('shipping_cost')->nullable()->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['delivery_type','product_return_type','product_return','min_shipping_cost','shipping_cost']);
        });
    }
}
