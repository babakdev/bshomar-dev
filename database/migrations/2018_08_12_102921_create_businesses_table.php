<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('category_id')->index()->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('name', 100)->index();
            $table->longText('address')->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('sheba', 24)->nullable();
            $table->tinyInteger('panel_id')->default(1)->comment('1 is starter');
            $table->string('email')->nullable();
            $table->boolean('is_active')->default(0);
            $table->string('latitude',10)->nullable();
            $table->string('longitude',10)->nullable();
            $table->boolean('b2b')->default(0);
            $table->boolean('b2c')->default(0);
            $table->longText('description')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->boolean('indust')->default(0)->comment('check it for industry');
            $table->boolean('feature')->default(0)->comment('check it for feature');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
