<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders' ,function(Blueprint $table){
            $table->dropColumn(['address_id','order_status','shipping_charges','total_amount','trans_id','id_get']);

            $table->unsignedBigInteger('panel_id')->index()->after('user_id')->default(1);
            $table->string('price',255)->after('user_id')->default(0);
            $table->string('date_create',20)->after('user_id')->default('');
            $table->string('invoice_id')->unique()->after('user_id')->default('');
            $table->unsignedTinyInteger('status')->comment('1 => new order , 2 => cancel')->default(1)->after('user_id');
            $table->string('description',512)->nullable()->after('user_id');

            $table->foreign('panel_id')->references('id')->on('panels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('address_id');
            $table->enum('order_status', ['در انتظار پرداخت','پرداخت شد', 'ارسال شد', 'کنسل شد', 'تسویه شد']);
            $table->float('shipping_charges')->default('0');
            $table->float('total_amount',191,2);
            $table->unsignedBigInteger('trans_id')->nullable();
            $table->unsignedBigInteger('id_get')->nullable();
            $table->dropForeign(['panel_id']);
            $table->dropColumn(['panel_id','price','date_create','invoice_id','status','description']);

        });

    }
}
