<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $driver = Schema::connection($this->getConnection())->getConnection()->getDriverName();
            $table->unsignedBigInteger('address_id')->nullable()->after('price');
            $table->float('shipping_charges')->default('0')->after('price'); //money for post office
            $table->unsignedBigInteger('discount_id')->nullable()->after('price');

            if ($driver != 'sqlite') {
                $table->dropForeign('orders_panel_id_foreign');
            }
            $table->dropIndex('orders_panel_id_index');
            if ($driver != 'sqlite') {
                $table->unsignedBigInteger('panel_id')->nullable()->change();
            }
            $table->foreign('address_id')->references('id')->on('delivery_addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $driver = Schema::connection($this->getConnection())->getConnection()->getDriverName();
            $table->dropForeign(['address_id']);
            $table->dropColumn(['discount_id', 'address_id', 'shipping_charges']);
            if ($driver != 'sqlite') {
                $table->foreign('panel_id')->references('id')->on('panels')->onDelete('cascade');
            }
        });
    }
}
