<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->unsignedBigInteger('business_id')->index();
            $table->string('owner_name',191)->comment('گواهینامه به نام چه کسی است')->nullable();
            $table->string('certificate_type');
            $table->string('certificate_number');
            $table->string('certificate_name');
            $table->string('exporter');
            $table->string('start_date');
            $table->string('end_date')->nullable();
            $table->string('scope',1024);
            $table->string('image');

            $table->foreign('business_id')->references('id')->on('businesses')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
