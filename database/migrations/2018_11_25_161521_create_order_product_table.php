<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(/**
         * @param Blueprint $table
         */
            'order_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id')->index();
            $table->unsignedBigInteger('product_id')->index();
            $table->string('name', 100);
            $table->string('code', 20)->nullable();
            $table->float('price',191,2);
            $table->unsignedBigInteger('quantity');
            $table->enum('status', ['در انتظار پرداخت','پرداخت شد', 'ارسال شد', 'کنسل شد', 'تسویه شد']);
            $table->string('delivery_code')->nullable()->comment('by post office');
            $table->timestamp('sent_date')->nullable();
            $table->timestamp('admin_checkout_date')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
    }
}
