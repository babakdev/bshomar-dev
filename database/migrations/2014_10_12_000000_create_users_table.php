<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('role_id')->default(4);
            $table->unsignedInteger('city_id')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->string('name', 191);
            $table->string('phone', 191)->unique()->index();
            $table->string('email', 191)->unique()->nullable();
            $table->string('nationalcode', 20)->nullable();
            $table->boolean('is_active')->default(0)->comment('actives after verify by mobile');
            $table->longText('address')->nullable();
            $table->string('password');
            $table->smallInteger('code')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
