<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->longText('consider')->after('feature')->nullable();
            $table->tinyInteger('time_to_delivery')->comment('in day')->after('feature')->default(1);
            $table->boolean('status')->comment('0 => not confirmed ,1 => confirmed')->default(0)->after('feature');
            $table->tinyInteger('discount')->comment('in percent')->default(0)->after('feature');
            $table->tinyInteger('discount_cycle')->comment('in day')->default(0)->after('feature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['consider' , 'time_to_delivery','status','discount' ,'discount_cycle']);
        });
    }
}
