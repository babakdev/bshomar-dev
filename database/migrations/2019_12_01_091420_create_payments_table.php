<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_id',255);
            $table->UnsignedBigInteger('user_id')->index();
            $table->string('price',255);
            $table->string('authority',255)->nullable();
            $table->string('date_create',20);
            $table->boolean('payed')->default(0);
            $table->string('reference',255)->nullable();
            $table->tinyInteger('payment_method')->comment('1 => online , 2 => at home');
            $table->string('gateway',20)->comment('example : zarinpal');
            $table->tinyInteger('type')->comment('1 => increase panel , 2 => order products , 3 => increase wallet');
            $table->string('description',512)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
