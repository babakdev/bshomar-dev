import Chart from 'chart.js';

export const graph = {
    template: `
        <div>
<!--                <h3 class="text-center">{{ title }}</h3>-->
               <canvas width="400" height="200" ref="canvas"></canvas>
        </div>
    `,

    methods: {
        createChart(data) {
            new Chart(this.$refs.canvas, {
                type: data.type,
                data,
                options: {
                    responsive: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                min:0,
                                // max:data.max
                            }
                        }]
                    }
                }
            });
        }
    }
};
