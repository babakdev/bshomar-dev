require('./bootstrap');

window.Vue = require('vue');

Vue.component('SellsGraph', require('./components/SellsGraph').default);
Vue.component('UsersGraph', require('./components/UsersGraph').default);
Vue.component('BusinessGraph', require('./components/BusinessGraph').default);
Vue.component('AllSoldItemsGraph', require('./components/allSoldItemsGraph').default);
Vue.component('TrendingBusinessGraph', require('./components/TrendingBusinessGraph').default);
Vue.component('BusinessByCategoryGraph', require('./components/BusinessByCategoryGraph').default);


const app = new Vue({
    el: '#app'
});
