<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute باید پذیرفته شده باشد.',
    'active_url' => 'آدرس :attribute معتبر نیست',
    'after' => ':attribute باید تاریخی بعد از :date باشد.',
    'alpha' => ':attribute باید شامل حروف الفبا باشد.',
    'alpha_dash' => ':attribute باید شامل حروف الفبا و عدد و خظ تیره(-) باشد.',
    'alpha_num' => ':attribute باید شامل حروف الفبا و عدد باشد.',
    'array' => ':attribute باید شامل آرایه باشد.',
    'before' => ':attribute باید تاریخی قبل از :date باشد.',
    'between' => array(
        'numeric' => ':attribute باید بین :min و :max باشد.',
        'file' => ':attribute باید بین :min و :max کیلوبایت باشد.',
        'string' => ':attribute باید بین :min و :max کاراکتر باشد.',
        'array' => ':attribute باید بین :min و :max آیتم باشد.',
    ),
    'boolean' => 'The :attribute field must be true or false',
    'confirmed' => ':attribute با تاییدیه مطابقت ندارد.',
    'date' => ':attribute یک تاریخ معتبر نیست.',
    'date_format' => ':attribute با الگوی :format مطاقبت ندارد.',
    'different' => ':attribute و :other باید متفاوت باشند.',
    'digits' => ':attribute باید :digits رقم باشد.',
    'digits_between' => ':attribute باید بین :min و :max رقم باشد.',
    'email' => 'فرمت :attribute معتبر نیست.',
    'exists' => ':attribute انتخاب شده، معتبر نیست.',
    'image' => ':attribute باید تصویر باشد.',
    'in' => ':attribute انتخاب شده، معتبر نیست.',
    'integer' => ':attribute باید نوع داده ای عددی (integer) باشد.',
    'ip' => ':attribute باید IP آدرس معتبر باشد.',
    'max' => array(
        'numeric' => ':attribute نباید بزرگتر از :max باشد.',
        'file' => ':attribute نباید بزرگتر از :max کیلوبایت باشد.',
        'string' => ':attribute نباید بیشتر از :max کاراکتر باشد.',
        'array' => ':attribute نباید بیشتر از :max آیتم باشد.',
    ),
    'mimes' => ':attribute باید یکی از فرمت های :values باشد.',
    'min' => array(
        'numeric' => ':attribute نباید کوچکتر از :min باشد.',
        'file' => ':attribute نباید کوچکتر از :min کیلوبایت باشد.',
        'string' => ':attribute نباید کمتر از :min کاراکتر باشد.',
        'array' => ':attribute نباید کمتر از :min آیتم باشد.',
    ),
    'not_in' => ':attribute انتخاب شده، معتبر نیست.',
    'numeric' => ':attribute باید شامل عدد باشد.',
    'regex' => ':attribute یک فرمت معتبر نیست',
    'required' => 'فیلد :attribute الزامی است',
    'required_if' => 'فیلد :attribute هنگامی که :other برابر با :value است، الزامیست.',
    'required_with' => ':attribute الزامی است زمانی که :values موجود است.',
    'required_with_all' => ':attribute الزامی است زمانی که :values موجود است.',
    'required_without' => ':attribute الزامی است زمانی که :values موجود نیست.',
    'required_without_all' => ':attribute الزامی است زمانی که :values موجود نیست.',
    'same' => ':attribute و :other باید مانند هم باشند.',
    'size' => array(
        'numeric' => ':attribute باید برابر با :size باشد.',
        'file' => ':attribute باید برابر با :size کیلوبایت باشد.',
        'string' => ':attribute باید برابر با :size کاراکتر باشد.',
        'array' => ':attribute باسد شامل :size آیتم باشد.',
    ),
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => ':attribute قبلا انتخاب شده است.',
    'url' => 'فرمت آدرس :attribute اشتباه است.',
    'recaptcha' => ':attribute صحیح نمی باشد',
    'spamfree' => ' :attribute شامل کلمات غیرمجاز است',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => array(
        'name' => 'نام',
        'username' => 'نام کاربری',
        'email' => 'پست الکترونیکی',
        'first_name' => 'نام',
        'last_name' => 'نام خانوادگی',
        'password' => 'رمز عبور',
        'passwordLogin' => 'رمز عبور',
        'password_confirmation' => 'تاییدیه ی رمز عبور',
        'city' => 'شهر',
        'country' => 'کشور',
        'address' => 'نشانی',
        'phone' => 'تلفن همراه',
        'phoneLogin' => 'تلفن همراه',
        'mobile' => 'تلفن همراه',
        'age' => 'سن',
        'sex' => 'جنسیت',
        'gender' => 'جنسیت',
        'day' => 'روز',
        'month' => 'ماه',
        'year' => 'سال',
        'hour' => 'ساعت',
        'minute' => 'دقیقه',
        'second' => 'ثانیه',
        'title' => 'عنوان',
        'text' => 'متن',
        'content' => 'محتوا',
        'description' => 'توضیحات',
        'excerpt' => 'گلچین کردن',
        'date' => 'تاریخ',
        'time' => 'زمان',
        'available' => 'موجود',
        'size' => 'اندازه',
        'body' => 'متن',
        'imageUrl' => 'تصویر',
        'videoUrl' => 'آدرس ویدیو',
        'slug' => 'نامک',
        'tags' => 'تگ ها',
        'state_id' => 'استان',
        'city_id' => 'شهر',
        'category_id' => 'دسته بندی',
        'photo_id' => 'تصویر',
        'filter' => 'فیلتر',
        'postCode' => 'کدپستی',
        'category' => 'دسته',
        'job_title' => 'عنوان شغلی',
        'birthday' => 'تاریخ تولد',
        'marriage' => 'وضعیت تاهل',
        'military' => 'وضعیت سربازی',
        'study_field' => 'رشته تحصیلی',
        'pre_jobs' => 'شغل های قبلی',
        'skills' => 'مهارت ها',
        'summary' => 'خلاصه ای در مورد من',
        'message' => 'متن پیام',
        'total_amount' => 'مبلغ فاکتور',
        'link' => 'لینک',
        'image' => 'تصویر',
        'sheba' => 'شماره شبا',
        'g-recaptcha-response' => 'من روبات نیستم',
        'product_name' => 'نام کالا',
        'newPwd' => 'رمز عبور جدید',
        'confirm_password' => 'تایید رمز عبور جدید',
        'current_password' => 'رمز عبور فعلی',
        'national_code' => 'کد ملی',
        'msg' => 'مخاطب گیرنده پیام',
        'amount' => 'مبلغ',
        'bank_name' => 'نام بانک',
        'bank_card' => 'شماره کارت بانکی',
        'marketer' => 'کد معرف',
        'unit' => 'واحد اندازه گیری',
        'time_to_delivery' => 'مدت زمان آماده سازی تا ارسال',
        'stock' => 'موجودی انبار',
        'price' => 'قیمت محصول',
        'cert_owner_name' => 'صادر شده به نام',
        'cert_type' => 'نوع گواهینامه',
        'cert_num' => 'شماره گواهینامه',
        'cert_exporter' => 'نام صادر کننده گواهینامه',
        'cert_start_date' => 'تاریخ شروع',
        'cert_end_date' => 'تاریخ پایان',
        'cert_scope' => 'محدوده اعتبار گواهینامه',
        'cert_name' => 'عنوان گواهینامه',
        'cert_image' => 'تصویر گواهینامه',
        'ex_value' => 'ارزش صادرات',
        'ex_of_products' => 'درصد از تولید مادر',
        'target_market' => 'بازارهای هدف',
        'cust_name' => 'نام شرکت واردکننده محصول',
        'cust_country' => 'کشور مقصد صادرات',
        'cust_state' => 'استان مقصد صادرات',
        'cust_product_name' => 'نام محصول صادراتی',
        'cust_image' => 'تصویر شرکت واردکننده',
        'service_type' => 'نوع خدمت',
        'service_info' => 'نحوه ارائه خدمت',
        'code' => 'کد تخفیف',
        'percent' => 'درصد تخفیف',
        'products' => 'محصولات',
        'amount_type' => 'نوع تخفیف',
        'start_date' => 'تاریخ شروع',
        'end_date' => 'تاریخ پایان',
        'found_date' => 'تاریخ تاسیس',
        'business_type' => 'نوع کسب و کار',
        'agencies' => 'اطلاعات نمایندگی ها',
        'subject' => 'عنوان پیام',
        'post_code' => 'کد پستی',
        'product_id' => 'نام کالا',
        'kasb_number' => 'شماره پروانه کسب',
        'min_shipping_cost' => 'حداقل مبلغ ارسال پست',
        'shipping_cost' => 'هزینه پست',
        'delivery_type' => 'واحد',
        'product_return' => 'مدت زمان مرجوعی کالا',
        'product_return_type' => 'واحد',


    ),
);
