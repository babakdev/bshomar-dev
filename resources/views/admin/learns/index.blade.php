@extends('layouts.adminLayout.admin_design')

@section('content')
    <div class="container-fluid">
        @include('includes.show-sessions')

        <div class="card">
            <div class="card-header">
                <a class="btn btn-success" href="{{ url('/irenadmin/learns/create') }}">
                    ساخت آموزش جدید <span class="fa fa-plus"></span>
                </a>
                <h3 class="card-title text-center">لیست آموزش ها</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" dir="rtl">
                @if($learns)
                    <table class="table table-striped text-right">
                        <thead>
                        <tr>
                            <th>عنوان</th>
                            <th>دسته بندی</th>
                            <th>محتوا</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($learns as $learn)

                            <tr>
                                <td>{{$learn->title}}</td>
                                <td>{{$learn->category->title}}</td>

                                <td style="direction: rtl"
                                    class="text-right">{!! Illuminate\Support\Str::limit(strip_tags($learn->body),80) !!}
                                </td>

                                <td class="row mx-auto">
                                    <form action="{{url('irenadmin/learns/'. $learn->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link deleteLearn">
                                            <i class="fa fa-trash text-danger" title="حذف"></i>
                                        </button>
                                    </form>
                                    <a class="p-1" href="{{ url('irenadmin/learns/'.$learn->id. '/edit') }}">
                                        <i class="fa fa-edit text-info" title="ویرایش"></i>
                                    </a>
                                    <a class="p-1" href="{{ url($learn->path()) }}" target="_blank">
                                        <i class="fa fa-eye text-success" title="مشاهده"></i>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <hr>
                    <p class="text-center">هنوز آموزشی ثبت نشده است</p>
                    <div class="row">
                        <div class="col-sm-6 offset-5">
                            {{$learns->render()}}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.deleteLearn').click(function () {
                return confirm('آیا از حذف آموزش مورد نظر اطمینان دارید؟');
            });
        });
    </script>
@endsection
