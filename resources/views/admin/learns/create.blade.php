@extends('layouts.adminLayout.admin_design')

@section('content')
    <div dir="rtl" class="container text-right mt-3">
        <form method="POST" action="{{ url('irenadmin/learns') }}">
            @include ('admin.learns._form')
        </form>
    </div>
@endsection


