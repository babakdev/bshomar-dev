@extends($page)

@section('content')
    <div dir="rtl" class="container text-right">

        <form method="POST" action='{{ url("irenadmin/learns/{$learn->id}") }}'>
            @method('PATCH')
            @include ('admin.learns._form', ['buttonText' => 'ویرایش آموزش'])
        </form>
    </div>
@endsection
