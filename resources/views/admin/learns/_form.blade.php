@include('includes.show-sessions')

@csrf
    <div class="row">
        <div class="form-group col-md-6">
            <label for="title" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">عنوان آموزش</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $learn->title) }}"
                   required>
        </div>
        <div class="form-group col-md-6">
            <label for="category_id" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">دسته بندی</label>
            <select class="form-control" id="category_id" name="category_id" required="required">
               <option value="9">انتخاب کنید</option>
                @foreach($cats as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
               @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="body" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">محتوای آموزش</label>
        <textarea type="text" name="body" id="body" class="form-control my-editor" rows="30"
                  dir="rtl">{{ old('body', $learn->body) }}</textarea>
    </div>

<div class="mb-4">
    <button type="submit" class="btn btn-success font-20">{{ $buttonText ?? 'ثبت آموزش' }}</button>
</div>

<a href="{{ url('irenadmin/learns') }}" class="btn btn-link">صفحه قبل</a>

@include('includes.form-error')

@section('scripts')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bwwivsywiq8s8c628683jhooygk3jlc9kd22wq4093syaqab"></script>

    <script src="{{ asset('js/backend/use_tinymce.js') }}"></script>
    <script src="{{ asset('vendor/laravel-filemanager/js/lfm.js') }}"></script>
@endsection
