@extends($page)

@section('content')
    @include('includes.show-sessions')
    @include('includes.form-error')


    <div class="container-fluid" style="min-height: 700px;direction: rtl">
        <h4 class="text-center text-info">کالاهای ارسال شده آماده تسویه توسط امور مالی بی‌شمار</h4>
        <hr>
        @if(count($factorNumbers)>0)

            <p style="white-space: pre-line; text-align: right; font-size: 14px" dir="rtl"> پشتیبان گرامی
                مراحل زیر را برای هر ردیف فاکتور به دقت انجام دهید:
                1. با خریدار تماس بگیرید و مطمئن شوید که کالا به دستشان رسیده و مشکلی نباشد.
                2.در دفتر مالی کلیه اطلاعات فاکتور را بنویسید.
                3. اگر کالا به دست مشتری رسیده باشد، روی واریز به کیف پول کلیک کنید.
            </p>
            <hr>
            @foreach($factorNumbers as $item)
                <div class="col-md-12 text-center card border shadow border-secondary mb-5"
                     style="background-color: #eeeeee">
                    <div class="card-header row bg-warning text-dark py-3">
                        <h5 class="col-md-3"><b> فاکتور: </b>{{ $item->order->invoice_id }}</h5>
                        <p class="col-md-3">نام خریدار: {{ $item->order->user->name }}</p>
                        <p class="col-md-3">تلفن خریدار: {{ $item->order->user->phone }}</p>
                    </div>

                    <?php $salesSent = \App\OrdersProduct::where('order_id', $item->order_id)
                        ->where('order_product.status', 'ارسال شد')
                        ->groupBy('business_id')
                        ->select('business_id', 'shipping_cost', 'product_id', 'id')
                        ->get(); ?>

                    @foreach($salesSent as $saleByBusiness)
                        <div class="card-body row">
                            <div class="col-md-1" style="z-index: 999">
                                <b>مشخصات فروشنده</b> <br>
                                <a href="#" class="btn btn-link btn-sm" data-toggle="modal"
                                   data-target="#shebaModal{{$saleByBusiness->id}}">
                                    {{$saleByBusiness->business->name}} <br>
                                </a>
                                <span>{{$saleByBusiness->business->phone}}</span>
                            </div>

                            <div class="modal fade" id="shebaModal{{$saleByBusiness->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close"
                                                    data-dismiss="modal">&times;
                                            </button>
                                        </div>
                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <b>{{ $saleByBusiness->business->sheba ?? 'شماره شبا ثبت نکرده، تماس بگیرید' }}</b>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-11 row">

                                <table class="col-md-10 table table-responsive table-hover text-center"
                                       dir="rtl">
                                    <thead>
                                    <tr class="text-light bg-dark">
                                        <th style="width: 30em">شرح کالا</th>
                                        <th style="width: 18em">کد رهگیری</th>
                                        <th>تعداد</th>
                                        <th style="width: 12em">مبلغ</th>
                                        <th>زمان فروش</th>
                                        <th>زمان ارسال</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sales = \App\OrdersProduct::where('order_id', $item->order_id)->get(); ?>

                                    @foreach($sales as $sale)
                                        @if($sale->business_id === $saleByBusiness->business_id)
                                            <tr class="border-bottom border-secondary">
                                                <td>{{$sale->name}}</td>

                                                @if($sale->delivery_code != Null)
                                                    <td>{{$sale->delivery_code}}</td>
                                                @else
                                                    <td>ارسال نشده</td>
                                                @endif

                                                <td>{{$sale->quantity}}</td>
                                                <td>
                                                    @php $discount_price = 0; @endphp
                                                    @if($sale->discount != null)
                                                        @if($sale->amount_type == 1)
                                                            @php $discount_price = $sale->percent * ($sale->price * $sale->quantity) / 100; @endphp
                                                        @else
                                                            @php $discount_price = $sale->amount; @endphp
                                                        @endif
                                                    @endif
                                                    {{ number_format(($sale->price * $sale->quantity)-$discount_price) }}
                                                    تومان
                                                </td>
                                                <td dir="ltr">{{verta($sale->created_at)->formatDate()}}</td>
                                                <td dir="ltr">{{verta($sale->updated_at)->formatDate()}}</td>

                                                <td class="p-0 pt-3">
                                                    <a href="#" class="btn btn-danger btn-sm" data-toggle="modal"
                                                       data-target="#myModal{{$sale->id}}">کنسل
                                                        کن</a>
                                                    <div class="modal fade" id="myModal{{$sale->id}}">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">

                                                                <!-- Modal Header -->
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal">&times;
                                                                    </button>
                                                                </div>

                                                                <!-- Modal body -->
                                                                <form action="{{ url('/cancel-buy')}}"
                                                                      method="POST">
                                                                    @csrf
                                                                    <input type="hidden" name="orderProdId"
                                                                           value="{{$sale->id}}">
                                                                    <input type="hidden" name="businessUserId"
                                                                           value="{{$sale->business->user_id}}">
                                                                    <input type="hidden" name="userId"
                                                                           value="{{$sale->order->user_id}}">
                                                                    <input type="hidden" name="orderId"
                                                                           value="{{$sale->order_id}}">
                                                                    <input type="hidden" name="factorPrice"
                                                                           value="{{$sale->price * $sale->quantity}}">
                                                                    <h5 class="text-center pt-2">آیا از این کار
                                                                        اطمینان دارید؟</h5>

                                                                    <!-- Modal footer -->
                                                                    <div class="modal-footer">
                                                                        <button type="submit"
                                                                                class="btn btn-danger btn-block mx-auto">
                                                                            کنسل
                                                                            کن
                                                                        </button>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>

                                <div class="col-md-2 bg-success text-light rounded shadow-sm">
                                    <h5 class="pt-3">مبلغ نهایی برای تسویه با این کسب و کار: </h5>
                                    <hr>
                                    <?php $total = 0; ?>
                                    @foreach($sales as $sale)
                                        @if($sale->business_id === $saleByBusiness->business_id)
                                            <?php $total += ($sale->price * $sale->quantity);?>
                                            <?php $shippingPerFacore = $sale->shipping_cost ?? false ?>
                                        @endif
                                    @endforeach

                                    <div class="p-2 mb-2">
                                        <h5>{{ number_format($total) }} تومان</h5>
                                        <h6>هزینه ارسال: {{$shippingPerFacore}} تومان</h6>
                                        <button type="submit" class="btn btn-primary mx-auto" data-toggle="modal"
                                                data-target="#settleModal{{$saleByBusiness->id}}">
                                            واریز به کیف پول
                                        </button>

                                        <div class="modal fade" id="settleModal{{$saleByBusiness->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">

                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;
                                                        </button>
                                                    </div>

                                                    <div class="modal-body text-dark">
                                                        @if($shippingPerFacore > 0)
                                                            <h6>برای نهایی شدن واریز پول برای تامین کننده، انتخاب کنید
                                                                که
                                                                هزینه ارسال برای تامین کننده واریز گردد یا برای باربری
                                                                داخلی
                                                                سامانه</h6>
                                                            <hr>
                                                            <div class="row mx-auto mb-3">
                                                                <p class="pt-2">واریز هزینه ارسال برای </p> &nbsp;
                                                                <form action="{{ url('/send-checkout-msg')}}"
                                                                      method="POST">
                                                                    @csrf
                                                                    <input type="hidden" name="isForBusiness" value="1">
                                                                    <input type="hidden" name="orderInvoiceId"
                                                                           value="{{$item->order->invoice_id}}">
                                                                    <input type="hidden" name="orderId"
                                                                           value="{{$item->order_id}}">
                                                                    <input type="hidden" name="shippingPerFacore"
                                                                           value="{{$shippingPerFacore}}">
                                                                    <input type="hidden" name="total"
                                                                           value="{{$total}}">
                                                                    <input type="hidden" name="businessId"
                                                                           value="{{$sale->business_id}}">
                                                                    <input type="hidden" name="businessUserId"
                                                                           value="{{$sale->business->user_id}}">
                                                                    <button type="submit" class="btn btn-info mx-auto">
                                                                        تامین کننده
                                                                    </button>
                                                                </form>
                                                            </div>

                                                            <div class="row mx-auto">
                                                                <p class="pt-2">واریز هزینه ارسال برای </p> &nbsp;

                                                                <form action="{{ url('/send-checkout-msg')}}"
                                                                      method="POST">
                                                                    @csrf
                                                                    <input type="hidden" name="isForBusiness" value="0">
                                                                    <input type="hidden" name="orderInvoiceId"
                                                                           value="{{$item->order->invoice_id}}">
                                                                    <input type="hidden" name="orderId"
                                                                           value="{{$item->order_id}}">
                                                                    <input type="hidden" name="shippingPerFacore"
                                                                           value="{{$shippingPerFacore}}">
                                                                    <input type="hidden" name="total"
                                                                           value="{{$total}}">
                                                                    <input type="hidden" name="businessId"
                                                                           value="{{$sale->business_id}}">
                                                                    <input type="hidden" name="businessUserId"
                                                                           value="{{$sale->business->user_id}}">
                                                                    <button type="submit"
                                                                            class="btn btn-instagram mx-auto">
                                                                        باربری داخلی سامانه
                                                                    </button>
                                                                </form>
                                                            </div>
                                                            @else
                                                            <form action="{{ url('/send-checkout-msg')}}"
                                                                  method="POST">
                                                                @csrf
                                                                <input type="hidden" name="isForBusiness" value="0">
                                                                <input type="hidden" name="orderInvoiceId"
                                                                       value="{{$item->order->invoice_id}}">
                                                                <input type="hidden" name="orderId"
                                                                       value="{{$item->order_id}}">
                                                                <input type="hidden" name="shippingPerFacore"
                                                                       value="{{$shippingPerFacore}}">
                                                                <input type="hidden" name="total"
                                                                       value="{{$total}}">
                                                                <input type="hidden" name="businessId"
                                                                       value="{{$sale->business_id}}">
                                                                <input type="hidden" name="businessUserId"
                                                                       value="{{$sale->business->user_id}}">
                                                                <button type="submit"
                                                                        class="btn btn-instagram mx-auto">
                                                                    واریز به کیف پول
                                                                </button>
                                                            </form>
                                                        @endif
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                </div>


                            </div>
                        </div>
                </div>

            @endforeach

            @endforeach

            <div class="row mt-2">
                <div class="mx-auto">
                    {{$factorNumbers->render()}}
                </div>
            </div>
        @else
            <h5 class="text-center text-info">موردی جهت تسویه موجود نمی باشد</h5>
        @endif
    </div>
@endsection

