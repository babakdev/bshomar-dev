@extends($page)

@section('content')
    @include('includes.show-sessions')
    @include('includes.form-error')


    <div class="container-fluid table-responsive" style="min-height: 800px;direction: rtl">
        <h4 class="text-center text-info mt-3">تمامی فروش های تسویه یا کنسل شده یا ناموفق</h4>
        @if(count($salesProduct)>0)

            <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center"
                   dir="rtl">
                <thead>
                <tr class="text-light bg-megna">
                    <th>شماره فاکتور</th>
                    <th>مبلغ کل فاکتور</th>
                    <th>نام خریدار</th>
                    <th>یوزرنیم خریدار</th>
                    <th>زمان فروش</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($salesProduct as $sale)
                    <tr>
                        <td>{{$sale->order->invoice_id}}</td>
                        <td>{{$sale->price * $sale->quantity}} تومان</td>
                        <td>
                            <small>{{$sale->order->user->name}}</small>
                        </td>
                        <td>
                            <small>{{$sale->order->user->phone}}</small>
                        </td>
                        <td dir="ltr">{{verta($sale->created_at)->formatDate()}}</td>
                        <td class="
                        @if($sale->status == 'تسویه شد')
                            alert-success
                        @elseif($sale->status == 'کنسل شد')
                            alert-danger
                        @endif">{{ $sale->status }}</td>
                        <td>
                            <a data-toggle="modal" data-target="#myModal{{ $sale->id }}"
                               class="btn btn-link text-primary" title="مشاهده جزییات">
                                <i class="fa fa-eye"></i>
                            </a>

                            {{--                            @if($sale->status == 'در انتظار پرداخت')--}}
                            {{--                                <form action="{{ url('/delete-sell/') }}" method="POST">--}}
                            {{--                                    @csrf--}}
                            {{--                                    <input type="hidden" name="sellId" value="{{ $sale->id }}">--}}
                            {{--                                    <button type="submit" class="btn btn-link delSell" title="حذف"><i--}}
                            {{--                                            class="fa fa-trash text-danger"></i></button>--}}
                            {{--                                </form>--}}
                            {{--                            @endif--}}

                            <div class="modal hide container" id="myModal{{$sale->id}}">
                                <div class="">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <table class="table table-bordered text-center" dir="rtl">
                                                <thead>
                                                <tr class="text-light bg-info">
                                                    <th>شرح کالا</th>
                                                    <th>قیمت</th>
                                                    <th>تعداد</th>
                                                    <th>قیمت کل</th>
                                                    <th>نام فروشنده</th>
                                                    <th>شماره تماس فروشنده</th>
                                                    <th>نام خریدار</th>
                                                    <th>یوزرنیم خریدار</th>
                                                    <th>زمان فروش</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{$sale->name}}</td>
                                                    <td>{{$sale->price}} تومان</td>
                                                    <td>{{$sale->quantity}}</td>
                                                    <td>{{$sale->price * $sale->quantity}} تومان</td>
                                                    <td>
                                                        <small>{{$sale->business->name}}</small>
                                                    </td>
                                                    <td>
                                                        <small>{{$sale->business->phone}}</small>
                                                    </td>
                                                    <td>
                                                        <small>{{$sale->order->user->name}}</small>
                                                    </td>
                                                    <td>
                                                        <small>{{$sale->order->user->phone}}</small>
                                                    </td>
                                                    <td dir="ltr">{{verta($sale->created_at)->formatDate()}}</td>

                                                </tr>

                                                </tbody>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


            <div class="row mt-2">
                <div class="mx-auto">
                    {{$salesProduct->render()}}
                </div>
            </div>
        @else
            <hr>
            <h5 class="text-center text-info">سابقه موجود نمی باشد</h5>
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        $().ready(function () {
            $('.delSell').click(function () {
                return confirm('آیا از حذف فاکتور مورد نظر اطمینان دارید؟');
            });
        });
    </script>
@endsection
