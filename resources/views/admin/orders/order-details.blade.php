@extends($page)

@section('content')
    @include('includes.show-sessions')
    @include('includes.form-error')

    <div class="breadcrumb text-info col-sm-12 p-1 m-0">
        <div class="container-fluid text-right">
            <a href="/">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
            <a href="{{ url('/new-sales')}}">سابقه خرید </a><i class="fa fa-chevron-left breadcrumb-item"></i>
            <a>جزییات سفارش </a>
        </div>
    </div> <!--END BREADCRUMB-->

    <div class="container-fluid table-responsive" style="min-height: 800px;">
        <h3 class="text-center" dir="rtl">شماره فاکتور: {{ $orderCustomerDetails->invoice_id }}</h3>
        <hr>
        <h4 class="text-center text-info">مشخصات خریدار</h4>
        <table class="table table-bordered table-hover text-center" dir="rtl">
            <thead>
            <tr class="text-light bg-megna">
                <th>نام خریدار</th>
                <th>یوزرنیم خریدار</th>
                <th>ایمیل خریدار</th>
                <th>شماره تماس خریدار</th>
                <th>آدرس ارسال</th>
                <th>کدپستی</th>
            </tr>
            </thead>
            <tbody>
            <tr class="bg-light">
                <td>{{ $orderCustomerDetails->user->name }}</td>
                <td>{{ $orderCustomerDetails->user->phone }}</td>
                <td>{{ $address->email ?? 'ندارد' }}</td>
                <td>{{ $address->phone ?? 'ندارد' }}</td>
                <td>{{ $address->address ?? 'ندارد' }}</td>
                <td>{{ $address->post_code ?? 'ندارد' }}</td>
            </tr>

            </tbody>
        </table>
        <hr>
        <br>

        <h4 class="text-center text-info">ریز فاکتور</h4>
        <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center" dir="rtl">
            <thead>
            <tr class="text-light bg-dark">
                <th>مشخصات فروشنده</th>
                <th>نام کالا</th>
                <th>تعداد کالا</th>
                <th>قیمت کالا</th>
                <th>قیمت کل</th>
                <th>وضعیت ارسال</th>
                <th>هزینه ارسال</th>
                <th>کد رهگیری مرسوله</th>
                <th>تاریخ ارسال</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>

            @foreach($orderDetails as $item)
                <tr>

                    <td>
                        {{ $item->business->name }}
                        <br>
                        {{ $item->business->user->phone }}
                    </td>

                    <td>{{ $item->name ?? 'مشخص نشده' }}</td>
                    <td>{{ $item->quantity ?? 'مشخص نشده' }}</td>
                    <td>{{ $item->price ?? 'مشخص نشده' }}</td>
                    <td>{{ number_format($item->price * $item->quantity) ?? 'مشخص نشده' }}</td>
                    <td class=" @if($item->status == 'کنسل شد') text-danger @elseif($item->status == 'تسویه شد') text-success @endif">{{ $item->status }}</td>
                    <td>{{ $item->shipping_cost }}</td>
                    <td>{{ $item->delivery_code ?? 'نامشخص'}}</td>
                    <td>{{ $item->sent_date ? verta($item->sent_date)->formatDate() : 'ارسال نشده'}}</td>

                    <td class="row mx-auto">
                        @if($item->status != 'کنسل شد' && $item->status != 'تسویه شد')
                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal{{ $item->id }}">کنسل
                            کن</a>
                        @endif
                        <div class="modal fade" id="myModal{{ $item->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;
                                        </button>
                                    </div>

                                    <!-- Modal body -->
                                    <form action="{{ url('/cancel-buy')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="orderProdId" value="{{ $item->id }}">
                                        <input type="hidden" name="businessUserId"
                                               value="{{ $item->business->user_id }}">
                                        <input type="hidden" name="userId" value="{{ $item->order->user_id }}">
                                        <input type="hidden" name="orderId" value="{{ $item->order->id }}">
                                        <input type="hidden" name="factorPrice"
                                               value="{{ $item->price * $item->quantity }}">
                                        <h5 class="text-center pt-2">آیا از این کار اطمینان دارید؟</h5>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger btn-block mx-auto">کنسل
                                                کن
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        &nbsp;
                        <form action="{{ url('/alert-to-business') }}" method="post">
                            @csrf
                            <input type="hidden" name="user_id"
                                   value="{{ $item->business->user_id }}">
                            <input type="hidden" name="product_name" value="{{ $item->name }}">
                            @if(!$item->sent_date && $item->status != 'کنسل شد')
                                <button type="submit" class="btn btn-warning">اخطار تاخیر ارسال بفرست</button>
                            @endif
                        </form>

                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection

