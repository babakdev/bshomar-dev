@extends($page)

@section('content')
    @include('includes.show-sessions')
    @include('includes.form-error')

    <div class="container-fluid table-responsive" style="min-height: 700px;direction: rtl">
        <h4 class="text-center text-info">فروش های جدید ارسال نشده</h4>
        @if(count($salesNew)>0)

            <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center"
                   dir="rtl">
                <thead>
                <tr class="text-light bg-megna">
                    <th>شماره فاکتور</th>
                    <th>نام خریدار</th>
                    <th>یوزرنیم خریدار</th>
                    <th>مبلغ کل فاکتور</th>
                    <th>زمان فروش</th>
                    <th>نوع پرداخت</th>
                    <th>وضعیت پرداخت</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($salesNew as $sale)
                    @if($sale->payment_method != 1 ||  $sale->payed != 0)
                    <tr>
                        <td>{{ $sale->invoice_id }}</td>
                        <td>{{ $sale->user_name }}</td>
                        <td>{{ $sale->phone }}</td>
                        <td>{{ number_format($sale->price) }} تومان</td>
                        <td dir="ltr">{{ $sale->date_create }}</td>
                        <td style="font-weight: bolder" class="{{ $sale->payment_method == 1 ? 'text-success' : 'text-warning' }}">{{ $sale->payment_method == 1 ? 'آنلاین' : 'درب منزل' }}</td>
                        <td style="font-weight: bolder" class="{{ $sale->payed == 1 ? 'text-success' : 'text-warning' }}">{{ $sale->payed == 1 ? 'پرداخت شد' : 'بدون پرداخت' }}</td>
                        <td>
                            <a href="sales/{{$sale->order_id}}"
                               class="btn btn-dark">مشاهده جزییات
                            </a>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

            <div class="row mt-2">
                <div class="mx-auto">
                    {{$salesNew->render()}}
                </div>
            </div>
        @else
            <hr>
            <h5 class="text-center text-info">سفارشی موجود نمی باشد</h5>
        @endif
    </div>
@endsection

