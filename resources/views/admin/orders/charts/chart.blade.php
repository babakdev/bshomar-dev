@extends('layouts.adminLayout.admin_design')

@section('content')
    <div class="container-fluid text-center" dir="rtl">

        <a href="/irenadmin/business-chart">Business</a>
        <div class="card bg-secondary text-light p-3">


    <h3>نمودار فروش</h3>
    <form action="{{ url('irenadmin/post-sells-chart') }}" method="POST">
        @csrf
        <div class="form-group row">
            <label for="type" class="form-control-label col-2"> نوع نمودار:</label>
            <select class="form-control col-2" name="type" id="type">
                <option value="line">خطی</option>
                <option value="pie">دایره ای</option>
                <option value="doughnut">پیراشکی</option>
                <option value="bar">میله ای</option>
                <option value="radar">رادار</option>
                <option value="polarArea">قطبی</option>
            </select>

            <label for="year" class="form-control-label col-2 mr-5"> سال آماری:</label>
            <select class="form-control col-2" name="year" id="year">
                <option value="2019">سال جاری</option>
                <option value="2018">سال گذشته</option>
            </select>

        </div>

        <div class="form-group row">
            <label for="on" class="form-control-label col-2"> بر حسب:</label>
            <select class="form-control col-2" id="on" name="on">
                <option value="all">کل سایت</option>
                <option value="category">دسته بندی</option>
                <option value="business">پذیرنده</option>
                <option value="item">آیتم</option>
                <option value="user">خریدار</option>
            </select>

            <button type="submit" class="btn btn-danger mr-5"><i class="fa fa-filter"></i> بساز </button>
        </div>

    </form>
</div>


    </div>
@endsection
