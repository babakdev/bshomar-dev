@extends('layouts.adminLayout.admin_design')

@section('content')
    <div class="container-fluid text-center">
        <a href="{{ url('irenadmin/charts') }}" class="btn btn-secondary float-left">تولید نمودار جدید</a>
        <business-graph labels="{{ $labels }}"
                        businesses="{{ $businesses }}"
                        max="{{ $max }}"
        ></business-graph>
    </div>
@stop
