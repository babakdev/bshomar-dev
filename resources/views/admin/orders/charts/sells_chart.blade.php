@extends('layouts.adminLayout.admin_design')

@section('content')
    <div class="container-fluid text-center">
        <a href="{{ url('irenadmin/charts') }}" class="btn btn-secondary float-left">تولید نمودار جدید</a>
        <sells-graph orders="{{ $orders }}"
                         labels="{{ $labels }}"
                         title="{{ $title }}"
                         type="{{ $type }}"
                         max="{{ $max }}"
        ></sells-graph>
    </div>
@stop
