@extends($page)

@section('content')
    <h3 class="page-title text-center pt-2">نمودارها</h3>
    @include('includes.show-sessions')

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-3">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-users m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $users }}</h5>
                                        <small class="font-light">تعداد کل کاربران</small>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-ban m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $deactiveUsers }}</h5>
                                        <small class="font-light">کاربران غیرفعال</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-user m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $users - $business }}</h5>
                                        <small class="font-light">تعداد مشتریان</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-cart-plus m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $business }}</h5>
                                        <small class="font-light">کسب و کارها</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fa fa-tag m-b-5 font-16"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $products }}</h5>
                                        <small class="font-light">کل محصولات</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="fas fa-exclamation text-danger"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $ordersProducts }}</h5>
                                        <small class="font-light">کالای ارسال نشده (فروش های موفق)</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="far fa-credit-card"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $sentProducts }}</h5>
                                        <small class="font-light">خریدهای موفق که نیاز به تسویه دارند</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="far fa-credit-card"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $payedOnline }}</h5>
                                        <small class="font-light">خریدهای آنلاین</small>
                                    </div>
                                </div>
                                <div class="col-3 m-t-15">
                                    <div class="bg-dark p-10 text-white text-center">
                                        <i class="far fa-credit-card"></i>
                                        <h5 class="m-b-0 m-t-5">{{ $payedHome }}</h5>
                                        <small class="font-light">خریدهای درب منزل</small>
                                    </div>
                                </div>

                                {{--                                    <div class="col-6 m-t-15">--}}
                                {{--                                        <div style="background-color: #1f75a5" class="p-10 text-white text-center">--}}
                                {{--                                            <i class="fas fa-comment-alt"></i>--}}
                                {{--                                            <h5 class="m-b-0 m-t-5">{{ Smsirlaravel::credit() }}</h5>--}}
                                {{--                                            <small class="font-light" dir="rtl">اعتبار باقیمانده sms</small>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
       <business-by-category-graph labels="{{$catNames}}"
                                   scores="{{$catScores}}">
       </business-by-category-graph>
        <hr>

        <trending-business-graph labels="{{$trending}}" dir="rtl"
                                 scores="{{$trendingScore}}"></trending-business-graph>

    </div>

@endsection
