@extends($page)

@section('styles')
    <style>
        .flash {
            animation-name: flash;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-direction: alternate;
            animation-play-state: running;
        }

        @keyframes flash {
            from {color: red;}
            to {color: white;}
        }
    </style>
    @stop
@section('content')
    <div class="container-fluid">
        @include('includes.show-sessions')

        <div class="breadcrumb text-info col-sm-12 mb-0 bg-white">
            <div class="col-12 text-right p-0 pt-2 pb-2">
                <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a href="{{ url('/finance')}}">مالی </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a>بدهی های جاری </a>
            </div>
        </div> <!--END BREADCRUMB-->

        <h5 class="text-center py-4 alert alert-info flash">پس از واریز فیزیکی به بانک و ثبت شماره رهگیری در فایل و دفتر مالی، میتوانید تایید کنید</h5>

        <h1 class="text-info h2 text-center mt-3">درخواست های واریز</h1>
        <hr>
        <table class="table table-striped table-hover text-center shadow" dir="rtl">
            <thead>
            <tr style="background-image: linear-gradient(135deg,#72234e,#9d5c6b);color: white">
                <td>نام کاربر</td>
                <td>موبایل</td>
                <td>مبلغ درخواست شده</td>
                <td>نام بانک</td>
                <td>شماره کارت بانکی</td>
                <td>شماره شبا</td>
                <td>تاریخ درخواست</td>
                <td></td>
            </tr>
            </thead>

            <tbody class="text-center">
            @foreach($notConfirmeds as $item)
                <tr>
                    @php $user = \App\User::where('id', $item->payable_id)->first(); @endphp
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->phone }}</td>
                    <td class="h5"><span dir="ltr">{{ number_format($item->amount) }}</span> تومان</td>

                    <td>{{ json_decode($item->meta, true)[1] }}</td>
                    <td>{{ json_decode($item->meta, true)[2] }}</td>
                    <td>{{ json_decode($item->meta, true)[3] }}</td>
                    <td dir="ltr">{{ verta($item->created_at)}}</td>
                    <td class="row">
                        <form action="{{url('/settle-user/'. $item->id)}}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-link confirm text-success" title="تایید پرداخت">
                                <i class="fa fa-check-circle"></i>
                            </button>
                        </form>
                        <form action="{{url('/dont-settle-user/'. $item->id)}}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-link unconfirm text-danger" title="رد پرداخت">
                                <i class="fa fa-times"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-center mx-auto mt-3">
            {{ $notConfirmeds->links() }}
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.confirm').click(function () {
            return confirm('آیا از تایید پرداخت مورد نظر اطمینان دارید؟');
        });
        $('.unconfirm').click(function () {
            return confirm('آیا از رد پرداخت مورد نظر اطمینان دارید؟');
        });
    </script>
@endsection
