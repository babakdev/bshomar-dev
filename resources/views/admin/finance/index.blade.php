@extends($page)

@section('content')
    <div class="container-fluid">
        @include('includes.show-sessions')

        <div class="breadcrumb text-info col-sm-12 mb-0 bg-white">
            <div class="col-12 text-right p-0 pt-2 pb-2">
                <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a>مالی </a>
            </div>
        </div> <!--END BREADCRUMB-->

        <h1 class="text-info h2 text-center">بخش مالی</h1>
        <hr>
        <div class="text-center">
            <a href="/debts" class="btn btn-lg btn-warning">بدهی های جاری</a>
            <a href="/withdraw-requests" class="btn btn-lg btn-danger" dir="rtl">درخواست های واریز
                @if($newWithdrawRequests > 0)
                    <i class="badge badge-light badge-pill font-weight-bold">{{ $newWithdrawRequests }}</i>
                @endif
            </a>
            <a href="/all-paids" class="btn btn-lg btn-success">کل پرداخت های انجام شده</a>
            <button type="button" class="btn btn-secondary btn-lg" data-toggle="modal" data-target="#myModal">
                پرداخت های انجام شده به تفکیک کاربر
            </button>
        </div>
{{--        <div class="card mt-5">--}}
{{--            <div class="card-header row mx-auto" dir="rtl">--}}
{{--                <h4>کل پرداخت های انجام شده</h4>&nbsp;&nbsp;--}}
{{--                <h4>{{ number_format(auth()->user()->balance) }} تومان </h4>--}}
{{--            </div>--}}
{{--        </div>--}}

        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body text-center">
                        <h4>شماره موبایل کاربر را وارد نمایید</h4>
                        <form action="/user/wallet/" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="09xxxxxxx" required>
                            </div>
                            <button type="submit" class="btn btn-secondary mt-2">پیدا کن</button>
                        </form>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف</button>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
