@extends($page)

@section('content')
    <div class="container-fluid">

        <div class="breadcrumb text-info col-sm-12 mb-0 bg-white">
            <div class="col-12 text-right p-0 pt-2 pb-2">
                <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a href="{{ url('/finance')}}">مالی </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a>بدهی های جاری </a>
            </div>
        </div> <!--END BREADCRUMB-->

        <h1 class="text-info h2 text-center mt-3">کل پرداخت های انجام شده</h1>
        <hr>

        <table class="table table-striped table-hover text-center shadow" dir="rtl">
            <thead>
            <tr style="background-image: linear-gradient(135deg,#183972,#5c949d);color: white">
                <td>نام دریافت کننده</td>
                <td>شماره موبایل دریافت کننده</td>
                <td>مبلغ </td>
                <td>تاریخ واریز</td>
            </tr>
            </thead>

            <tbody class="text-center">
            @foreach($allPaids as $item)
                <tr>
                    <td>{{ $item->meta[0] }}</td>
                    <td>{{ $item->meta[1] }}</td>
                    <td class="h5"><span dir="ltr">{{ number_format($item->amount) }}</span> تومان</td>
                    <td dir="ltr">{{ verta($item->created_at)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection
