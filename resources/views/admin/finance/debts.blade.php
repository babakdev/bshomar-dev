@extends($page)

@section('content')
    <div class="container-fluid">

        <div class="breadcrumb text-info col-sm-12 mb-0 bg-white">
            <div class="col-12 text-right p-0 pt-2 pb-2">
                <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a href="{{ url('/finance')}}">مالی </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a>بدهی های جاری </a>
            </div>
        </div> <!--END BREADCRUMB-->

        <h1 class="text-info h2 text-center mt-3">بدهی های جاری</h1>
        <hr>

        <table class="table table-striped table-hover text-center shadow" dir="rtl">
            <thead>
            <tr style="background-image: linear-gradient(135deg,#72234e,#9d5c6b);color: white">
                <td>نام کاربر</td>
                <td>موبایل</td>
                <td>موجودی</td>
            </tr>
            </thead>

            <tbody class="text-center">
            @foreach($users as $user)
                @if(!$user->isAdmin)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ number_format($user->balance) }} تومان</td>
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>

    </div>

@endsection
