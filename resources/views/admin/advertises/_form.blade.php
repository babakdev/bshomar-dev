@include('includes.show-sessions')

@csrf
<div class="row">
    <div class="form-group col-md-6">
        <label for="title" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">نوع تبلیغ</label>
        <select class="form-control" id="type" name="type" required>
            <option value="">انتخاب کنید</option>
            <option value="1" {{old('type') || $adv->type == 1 ? 'selected' : ''}}>تبلیغات از سایتهای ثالث</option>
            <option value="2" {{old('type') || $adv->type == 2 ? 'selected' : ''}}>تبلیغات خود سایت</option>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="link" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">اسکریپت تبلیغ ( مخصوص تبلیغات از سایتهای ثالث )</label>
        <input type="text" class="form-control" id="link" name="link" value="{{ $adv && $adv->type == 1 ? old('link', $adv->link) : ''}}">
    </div>
</div>
@if($adv->type == 2)
    <img src="/images/front_images/ads/{{ $adv->link}}" alt="advImage">
@endif
<div class="row">
    <div class="form-group col-6">
        <label for="image">عکس تبلیغ ( مخصوص تبلیغات خود سایت )</label>
        <input type="file" name="image" class="form-control"/>
    </div>
    <div class="form-group col-md-6">
        <label for="href" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">لینک صفحه ای که با کلیک روی تبلیغ به آن هدایت میشود ( مخصوص تبلیغات خود سایت )</label>
        <input type="text" class="form-control" id="href" name="href" value="{{ old('href', $adv->href) }}">
    </div>
</div>

<div class="mb-4">
    <button type="submit" class="btn btn-success font-20">{{ $buttonText ?? 'ثبت تبلیغ' }}</button>
</div>

<a href="{{ url('irenadmin/advertising') }}" class="btn btn-link">صفحه قبل</a>

@include('includes.form-error')
