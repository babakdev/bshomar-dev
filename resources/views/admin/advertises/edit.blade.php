@extends('layouts.adminLayout.admin_design')

@section('content')
    <div dir="rtl" class="container-fluid mt-3 text-right">

        <form method="POST" action='{{ url("irenadmin/advertising/{$adv->id}") }}' enctype="multipart/form-data">
            @method('PATCH')
            @include ('admin.advertises._form', ['buttonText' => 'ویرایش تبلیغ'])
        </form>
    </div>
@endsection
