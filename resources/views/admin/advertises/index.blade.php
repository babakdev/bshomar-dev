@extends('layouts.adminLayout.admin_design')

@section('content')
    <div class="container-fluid mt-3">
        @include('includes.show-sessions')

        <div class="card">
            <div class="card-header">
                <a class="btn btn-success" href="{{ url('/irenadmin/advertising/create') }}">
                    درج تبلیغات جدید <span class="fa fa-plus"></span>
                </a>
                <h3 class="card-title text-center">تبلیغات</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-center table-striped">
                    <thead>
                    <tr>
                        <th>حذف</th>
                        <th>ویرایش</th>
                        <th>لینک تبلیغ</th>
                        <th>صفحه تبلیغ</th>
                        <th>آدرس اسکریپت / عکس</th>
                        <th>نوع تبلیغ</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach( $advertises as $adv)
                        <tr>
                            <td>
                                <form action="{{url('irenadmin/advertising/'. $adv->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-link deleteSlide" onclick="return confirm('آیا از حذف این مورد اطمینان دارید؟')">
                                        <i class="fa fa-trash text-danger" title="حذف"></i>
                                    </button>
                                </form>
                            </td>
                            <td class="text-center">
                                <a href="{{ url('irenadmin/advertising/'. $adv->id . '/edit') }}" class="btn btn-link">
                                    <i class="fa fa-edit text-info" title="ویرایش"></i>
                                </a>
                            </td>
                            <td>{{ $adv->href }}</td>
                            <td class="text-center">
                                @if($adv->page == 1)
                                    صفحه اصلی
                                @elseif($adv->page == 2)
                                    پنل
                                @else
                                    سایر صفحات
                                @endif
                            </td>
                            <td class="text-center">
                                @if($adv->type == 2)
                                    <img src="/images/front_images/ads/{{$adv->link }}"
                                         style="width:200px;" class="lazyload blur-up" alt="advertising">
                                @else
                                    {{$adv->link}}
                                @endif
                            </td>
                            <td class="text-center">
                                @if($adv->type == 1)
                                    تبلیغات از سایتهای ثالث
                                @else
                                    تبلیغات خود سایت
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6 offset-5">
                        {{$advertises->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection
