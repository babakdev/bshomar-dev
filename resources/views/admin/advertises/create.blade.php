@extends('layouts.adminLayout.admin_design')

@section('content')
    <div class="breadcrumb text-info col-sm-12 mb-0 bg-white">
        <div class="col-12 text-right p-0 pt-2 pb-2">
            <a href="{{ url('irenadmin/advertising')}}">تبلیغات </a><i class="fa fa-chevron-left breadcrumb-item"></i>
        </div>
    </div> <!--END BREADCRUMB-->

    <div dir="rtl" class="container-fluid mt-3 text-right">
        <form method="POST" action="{{ url('irenadmin/advertising') }}" enctype="multipart/form-data">
            @include ('admin.advertises._form')
        </form>
    </div>
@endsection
