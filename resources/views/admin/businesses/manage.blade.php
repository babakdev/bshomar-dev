@extends($page)

@section('styles')
    <style>
        fieldset {
            overflow: hidden
        }

        li.ui-state-default.ui-state-hidden[role=tab]:not(.ui-tabs-active) {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="container text-right" dir="rtl">
        @if(auth()->user()->isAdmin || auth()->user()->isSupport)
            <a href="/approved-businesses" class="btn btn-success mt-3">> بازگشت به لیست کسب و کارها</a>
            <hr>
        @endif
        @include('includes.form-error')
        @include('includes.show-sessions')
        <div class="row pt-2">
            <h4 class="row mx-auto">
                <span class="pt-3">رفتن به صفحه اختصاصی کسب و کار -> </span>&nbsp;
                <a href="{{ 'https://bshomar.com/' . $business->id}}" class="bg-secondary text-light rounded p-3">Bshomar.com/{{$business->id}}</a>
            </h4>
            <div class="col-md-12">
                <form action="{{ url('/business-information/' . $business->id) }}" method="POST" class="p-4 border"
                      enctype="multipart/form-data">
                    @csrf
                    <h4 class="text-center mb-3 border py-3 text-white"
                        style="background-color: #ed5780">{{ $business->category_id ? $business->category->name : 'یک دسته بندی را انتخاب نمایید' }}</h4>

                    {{--Set Business Type--}}
                    <div class="row mt-3">
                        <div class="col-md-12"><label for="business_type">نوع کسب و کار شما :</label></div>
                        <fieldset>
                            <div class="col-md-12">
                                <input type="radio" class="radio" name="business_type" value="1"
                                       id="business_type1" {{$business->business_type == 1 ? 'checked' : ''}}/>
                                <label for="business_type1">تولیدی</label>
                                <input type="radio" class="radio" name="business_type" value="2"
                                       id="business_type2" {{$business->business_type == 2 ? 'checked' : ''}}/>
                                <label for="business_type2">توزیعی</label>
                                <input type="radio" class="radio" name="business_type" value="3"
                                       id="business_type3" {{$business->business_type == 3 ? 'checked' : ''}}/>
                                <label for="business_type3">خدماتی</label>
                                <input type="radio" class="radio" name="business_type" value="4"
                                       id="business_type4" {{$business->business_type == 4 ? 'checked' : ''}}/>
                                <label for="business_type4">خرده فروشی</label>
                            </div>
                        </fieldset>
                    </div>

                    <br>

                    <div id="MyTabSelector">
                        <ul class="nav nav-tabs" dir="rtl">
                            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">اطلاعات
                                    اصلی</a>
                            </li>

                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#manu">قدرت تولید</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#qc">کنترل کیفیت</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#rnd">تحقیق و توسعه</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#cert">گواهینامه ها</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#export">صادرات</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#agency">نمایندگی ها</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#service">خدمات</a></li>
                        </ul>

                        <div class="tab-content">

                            <div id="home" class="tab-pane active">
                                @include('admin.businesses.partials._main_information')
                            </div>

                            <div id="manu" class="tab-pane fade">
                                @include('admin.businesses.partials._manu')
                            </div>

                            <div id="qc" class="tab-pane fade">
                                @include('admin.businesses.partials._qc')
                            </div>

                            <div id="rnd" class="tab-pane fade">
                                @include('admin.businesses.partials._rnd')
                            </div>

                            <div id="cert" class="tab-pane fade">
                                @include('admin.businesses.partials._cert')
                            </div>

                            <div id="export" class="tab-pane fade">
                                @include('admin.businesses.partials._export')
                            </div>

                            <div id="agency" class="tab-pane fade">
                                @include('admin.businesses.partials._agencies')
                            </div>

                            <div id="service" class="tab-pane fade">
                                @include('admin.businesses.partials._services')
                            </div>


                        </div>
                    </div>

                    <button type="submit" class="btn btn-success btn-rounded font-20 col-3 fixed-bottom">
                        <i class="fas fa-exclamation"></i>&nbsp;&nbsp;
                        ثبت اطلاعات
                    </button>
                </form>
            </div>
            </div>

            <!-- Admin Products Management -->
            @if(auth()->user()->isAdmin || auth()->user()->isSupport)
                @if ($business->category_id != null)
                    <hr>
                    <div class="text-center card shadow mt-5">
                        <h3 class="card-header">کالاها</h3>
                        <div style="background-color: #eeeeee" class="p-2 card-body">

                            <a class="btn btn-success text-white btn-block"
                               href='{{ url("/products/create?businessId={$business->id}") }}'>
                                ساخت کالا جدید <span class="fa fa-plus"></span>
                            </a>

                            <button type="button" class="btn btn-block btn-linkedin" data-toggle="modal"
                                    data-target="#myModal">
                                مشاهده کالاهای این پذیرنده
                            </button>
                        </div>
                    </div>
            @endif
        @endif

        <!-- The Products Modal -->
            <div class="modal fade" id="myModal" dir="rtl">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body row">
                            @foreach($business->products as $product)
                                <div class="p-2 text-center col-md-4 card">

                                    <img height="150" alt="productPhoto" class="lazyload blur-up mb-2"
                                         src="{{ (count($product->images) > 0) ?
                                                      asset('images/front_images/products/large/'.$product->images[0]->path) :
                                                       asset('images/front_images/NoImageAvailable.webp')
                                                       }}">

                                    <h4 class="card-title">{{ $product->name }}</h4>
                                    <div class="row mx-auto">
                                        <form action="{{url('/products/'. $product->id . '/edit')}}"
                                              method="GET">
                                            <input type="hidden" name="businessId"
                                                   value="{{ $business->id }}">
                                            <button type="submit" class="btn btn-link pt-0"
                                                    title="ویرایش">
                                                <i class="fa fa-edit text-orange"></i>
                                            </button>
                                        </form>

                                        <form action="{{url('/products/'. $product->id)}}"
                                              method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-link pt-0 deleteItem">
                                                <i class="fa fa-trash text-danger" title="حذف"
                                                   onclick="return confirm('آیا از حذف آیتم مورد نظر اطمینان دارید؟ با این کار ممکن است رضایت کسب و کار کاهش یابد')"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    @yield('js')

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>
        var MyTabSelector = $('#MyTabSelector');

        function disableAllTabs() {
            MyTabSelector.disableTab(1, true);
            MyTabSelector.disableTab(2, true);
            MyTabSelector.disableTab(3, true);
            MyTabSelector.disableTab(4, true);
            MyTabSelector.disableTab(5, true);
            MyTabSelector.disableTab(6, true);
            MyTabSelector.disableTab(7, true);
        }

        $(document).ready(function () {
            disableAllTabs();
            enable_disable_tabs();
        });

        $("input[name='business_type']").on('change', function () {
            enable_disable_tabs();
        });

        function enable_disable_tabs() {
            var value = $("input[type='radio']:checked").val();
            if (value == 1) {
                disableAllTabs();

                MyTabSelector.enableTab(1);
                MyTabSelector.enableTab(2);
                MyTabSelector.enableTab(3);
                MyTabSelector.enableTab(4);
                MyTabSelector.enableTab(5);
            } else if (value == 2) {
                disableAllTabs();

                MyTabSelector.enableTab(6);
            } else if (value == 3) {
                disableAllTabs();

                MyTabSelector.enableTab(7);
            } else {
                disableAllTabs();
            }
        }

        (function ($) {
            $.fn.disableTab = function (tabIndex, hide) {
                // Get the array of disabled tabs, if any
                var disabledTabs = this.tabs("option", "disabled");

                if ($.isArray(disabledTabs)) {
                    var pos = $.inArray(tabIndex, disabledTabs);

                    if (pos < 0) {
                        disabledTabs.push(tabIndex);
                    }
                } else {
                    disabledTabs = [tabIndex];
                }

                this.tabs("option", "disabled", disabledTabs);
                if (hide === true) {
                    $(this).find('li:eq(' + tabIndex + ')').addClass('ui-state-hidden');
                }

                // Enable chaining

                return this;
            };

            $.fn.enableTab = function (tabIndex) {
                $(this).find('li:eq(' + tabIndex + ')').removeClass('ui-state-hidden');
                this.tabs("enable", tabIndex);
                return this;
            };
        })(jQuery);

        MyTabSelector.tabs();
    </script>
@endsection
