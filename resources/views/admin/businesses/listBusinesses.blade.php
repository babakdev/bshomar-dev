@extends($page)

@section('content')
    <div class="text-center container-fluid" dir="rtl">
        @include('includes.show-sessions')

        <h3 class="text-center pt-2 text-info">کسب و کار ها</h3>
        {{--                <input class="form-control w-50 mx-auto mt-2" id="myInput" type="text" placeholder="جستجو..">--}}
        <table class="table table-hover table-responsive">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">عکس</th>
                <th scope="col">نام پذیرنده</th>
                <th scope="col">نام کاربری</th>
                @if(!auth()->user()->isAsnaf)
                    <th scope="col">یوزر کاربر</th>
                @endif
                <th scope="col">شهر</th>
                <th scope="col">آخرید ورود</th>
                <th scope="col">ارسال پیام</th>
                <th scope="col">عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($businesses as $business)
                <tr>
                    <th scope="row">{{$business->id}}</th>
                    <td><img height="80" alt="{{$business->name}}" class="rounded" src="{{ $business->image() }}"></td>
                    <td><a href="{{ 'https://bshomar.com/' . $business->id}}" target="_blank">{{$business->name}}</a>
                    </td>
                    <td><a href="{{url('/users/'. $business->user->id . '/edit')}}" target="_blank"
                           title="ویرایش کاربر">{{ $business->user->name ?? 'نامشخص' }}</a></td>
                    @if(!auth()->user()->isAsnaf)
                        <td>{{ $business->user->phone ?? 'نامشخص' }}</td>
                    @endif
                    <td>{{$business->city->title ?? 'نامشخص'}}</td>
                    <td dir="ltr">{{ isset($business->user->last_login_at) ? verta($business->user->last_login_at) : '-' }}</td>
                    <td>
                        @if(!auth()->user()->isAsnaf)
                            <a href="{{ url('/send-message', $business->user_id )}}"
                               class="btn btn-link">
                                <i class="fa fa-envelope">&nbsp;</i>
                            </a>
                        @endif
                    </td>
                    <td class="text-center row">
                        <a href="#myModal{{ $business->id }}" data-toggle="modal"
                           class="btn btn-link" title="دیدن پذیرنده"><i class="fa fa-eye"></i></a>

                        <a href="{{ url('/business-is-active/'.$business->id) }}"
                           class="btn btn-link text-danger" title="غیرفعال کردن"><i
                                class="fa fa-times-circle"></i></a>
                        <a href="{{ url('/business-information/'.$business->id) }}"
                           class="btn btn-link text-success" title="ویرایش"><i
                                class="fa fa-edit"></i></a>
{{--                        @if(!auth()->user()->isAsnaf)--}}
{{--                            <form action="{{ url('/business/'.$business->id) }}" method="POST">--}}
{{--                                @csrf--}}
{{--                                @method('delete')--}}
{{--                                <button type="submit" class="btn btn-link delBusiness" title="حذف"><i--}}
{{--                                        class="fa fa-trash text-danger"></i></button>--}}
{{--                            </form>--}}
{{--                        @endif--}}
                    </td>
                </tr>

                <div id="myModal{{ $business->id }}"
                     class="modal fade text-right">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3><a href="{{ 'https://bshomar.com/' . $business->id}}" target="_blank"
                                       title="رفتن به صفحه کسب و کار">{{ $business->name}}</a></h3>&nbsp;&nbsp;
                                <small>رفتن به صفحه کسب و کار</small>
                                <button data-dismiss="modal" class="close" type="button">×</button>
                            </div>

                            <div class="modal-body" style="direction: rtl">
                                <img height="300" alt="businessPhoto" class="lazyload blur-up col mb-2"
                                     src="{{ $business->image() }}">
                                <p><strong>متعلق به کاربر:</strong> {{ $business->user->name ?? 'نامشخص' }}
                                </p>
                                <p><strong>دسته بندی:</strong> {{ $business->category->name ?? 'نامشخص' }}
                                </p>
                                <p>
                                    <strong>وضعیت:</strong> {{ $business->is_active ==1 ? 'فعال' : 'غیرفعال'  }}
                                </p>
                                <p>
                                    <strong>استان:</strong> {{ $business->city ? $business->city->state->title : 'نامشخص'}}
                                <p>
                                    <strong>شهر:</strong> {{ $business->city_id ? $business->city->title : 'نامشخص'}}
                                <p><strong>آدرس:</strong> {{ $business->address ?? 'نامشخص'}}</p>
                                <p><strong>شماره تماس:</strong> {{ $business->phone ?? 'نامشخص'}}</p>
                                <p><strong>شماره شبا:</strong> {{ $business->sheba ?? 'نامشخص'}}</p>
                                <p><strong>ایمیل:</strong> {{ $business->email ?? 'نامشخص' }}</p>
                                <p><strong>پنل:</strong> {{ $business->panel->name }}</p>
                                <p><strong>b2c:</strong> {{ $business->b2c ==1 ? 'بله' : 'خیر' }}</p>
                                <p><strong>b2b:</strong> {{ $business->b2b ==1 ? 'بله' : 'خیر' }}</p>
                                {{--                                <p><strong>موجودی کیف--}}
                                {{--                                        پول: </strong>{{ $business->user->balance ?? 'نامشخص' }} تومان </p>--}}
                                <p><strong>تاریخ
                                        ثبت:</strong> {{ verta($business->created_at)->formatDate() ?? 'نامشخص'}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>

        <div class="mt-3 text-center mx-auto">
            {{ $businesses->links() }}
        </div>

    </div>

@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.delBusiness').click(function () {
                return confirm('آیا از حذف کسب و کار مورد نظر اطمینان دارید؟');
            });
        });
    </script>
@endsection
