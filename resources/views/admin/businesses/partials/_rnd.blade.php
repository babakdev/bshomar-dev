<div class="mt-2">
    <div class="form-group row">
        <b>آیا مرحله تحقیق و توسعه دارید؟</b>
        <label for="yes_rnd" class="form-check-label mr-3">بله</label>
        <input type="radio" class="form-check-inline" id="yes_rnd" name="has_rnd"
               value="1" onclick="$('.rnd').css({'display': 'block'});">
        <label for="no_rnd" class="form-check-label mr-2">خیر</label>
        <input type="radio" class="form-check-inline" id="no_rnd" name="has_rnd"
               value="0" onclick="$('.rnd').css({'display': 'none'});"
               checked>
    </div>
</div>

<div class="rnd" style="display: none">
    <div class="form-group">
        <label for="rnd_name">نام مرحله تحقیق و توسعه<span class="text-danger"> *</span>
        </label>
        <input id="rnd_name" type="text" name="rnd_name" class="form-control"
               value="{{ $business->rnd_name ?? old('rnd_name')}}" autofocus>
    </div>

    <div class="row py-2 mb-2">
        <div
            class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
            <label for="image">تصویر از واحد تحقیق و توسعه<span
                    class="text-danger"> *</span>
                <small>jpg png gif</small>
            </label>
            <input type="file" name="rnd_image" accept="image/*"/>
            @error('rnd_image')
            <span class="text-danger text-center" role="alert"><strong
                    style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong></span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label for="rnd_desc">شرح این مرحله از تحقیق و توسعه <span
                class="text-danger"> *</span></label>

        <textarea type="text" name="rnd_desc" id="rnd_desc"
                  class="form-control"
                  rows="10"
                  dir="rtl">{{ old('rnd_desc', $business->rnd_desc) }}</textarea>
    </div>
</div>

@if(count($business->researchDevelop) > 0)
    <hr>
    <h3 class="text-center mb-4 border-bottom pb-3">آیتم های معرفی شده</h3>
    @foreach($business->researchDevelop as $item)
        <div class="card border border-secondary rounded rnd-item-{{$item->id}}">
            <div class="card-header row">
                <h4 class="col-11">{{ $item->name }}</h4>
                <span href="{{ $item->id }}" onclick="removeRnd({{$item->id}})" class="btn"><i
                        class="fa fa-trash text-danger"></i></span>
            </div>
            <div class="card-body row">
                <img class="col-sm-3" src="{{ asset('images/front_images/businesses/rnd/'.$item->image) }}" alt="">
                <p class="col-sm-9">{{ $item->description }}</p>
            </div>
        </div>
    @endforeach
@endif

<script>
    let rndUrl = '/api/business/productive/rnd/';

    function removeRnd(id) {
        axios.delete(rndUrl + id + '?admin=1')
            .then(function () {
                alert('آیتم با موفقیت حذف شد');
                $(".rnd-item-" + id).css({"display": "none"});
            });
    }
</script>
