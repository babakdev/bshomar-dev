<div class="mt-2">
    <div class="form-group row">
        <b>آیا صادرات دارید؟</b>
        <label for="yes_export" class="form-check-label mr-3">بله</label>
        <input type="radio" class="form-check-inline" id="yes_export"
               name="has_export" onclick="$('.ex').css({'display': 'block'});"
               value="1">
        <label for="no_export" class="form-check-label mr-2">خیر</label>
        <input type="radio" class="form-check-inline" id="no_export"
               name="has_export" onclick="$('.ex').css({'display': 'none'});"
               value="0" checked>
    </div>
</div>

<div class="ex" style="@if($business->export) display:block @else display: none; @endif background-color: #f4f6f6">
    <div class="form-group">
        <label for="ex_value">ارزش صادرات:</label>
        <select class="form-control" id="ex_value" name="ex_value">
            <option @if($business->export && $business->export->ex_value == 0) selected
                    @endif value="{{ $business->export && $business->export->ex_value ?? 0}}">
                زیر 1 میلیون دلار
            </option>
            <option @if($business->export && $business->export->ex_value == 1) selected
                    @endif value="{{ $business->export && $business->export->ex_value ?? 1}}">
                از 1 تا 3 میلیون دلار
            </option>
            <option @if($business->export && $business->export->ex_value == 2) selected
                    @endif value="{{ $business->export && $business->export->ex_value ?? 2}}">
                از 3 تا 5 میلیون دلار
            </option>
            <option @if($business->export && $business->export->ex_value == 3) selected
                    @endif value="{{ $business->export && $business->export->ex_value ?? 3}}">
                5 میلیون دلار به بالا
            </option>
        </select>
    </div>
    <div class="form-group">
        <label for="ex_of_products">چند درصد از تولید صادر میشود؟</label>
        <input type="number" class="form-control" id="ex_of_products"
               name="ex_of_products"
               min="1" max="100"
               value="{{ $business->export ? $business->export->ex_of_products : old('ex_of_products') }}">
    </div>
    <div class="form-group">
        <label for="target_market">بازارهای هدف:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="target_market" name="target_market"
               placeholder="عراق، گرجستان، اوکراین، ..."
               value="{{ $business->export ? $business->export->target_market : old('target_market') }}">
    </div>
    <div class="form-group">
        <label for="ex_start_year">سال شروع صادرات:</label>
        <input type="number" class="form-control" id="ex_start_year"
               name="ex_start_year"
               placeholder="1385"
               value="{{ $business->export ? $business->export->ex_start_year : old('ex_start_year') }}">
    </div>
    <div class="form-group">
        <label for="ex_type">روش صادرات</label>
        <input type="text" class="form-control" id="ex_type" name="ex_type"
               placeholder="هوایی، ریلی، زمینی، بندر امام خمینی"
               value="{{ $business->export ? $business->export->ex_type : old('ex_type') }}">
    </div>
    <div class="form-group">
        <label for="ex_avg">میانگین زمان تحویل محموله صادراتی:</label>
        <input type="text" class="form-control" id="ex_avg" name="ex_avg"
               placeholder="3 روز"
               value="{{ $business->export ? $business->export->ex_avg : old('ex_avg') }}">
    </div>
    <div class="form-group">
        <label for="ex_rules">قوانین بین المللی صادرات که در شرکت ما اجرایی
            هستند:</label>
        <input type="text" class="form-control" id="ex_rules" name="ex_rules"
               placeholder="FOB, EXW, CFR, CIF, FAS, ..."
               value="{{ $business->export ? $business->export->ex_rules : old('ex_rules') }}">
    </div>
    <div class="form-group">
        <label for="ex_langs">زبان هایی که برای مشتریان، پشتیبانی می کنیم:</label>
        <input type="text" class="form-control" id="ex_langs" name="ex_langs"
               placeholder="انگلیسی، فارسی، چینی، ..."
               value="{{ $business->export ? $business->export->langs : old('ex_langs') }}">
    </div>
</div>

<hr>

<h3>رزومه صادرات</h3>

<div class="mt-2">
    <div class="form-group row">
        <b>در صورت تمایل می توانید سابقه ای از مشتریان مقصد خود در کشورهای دیگر وارد
            نمایید</b>
        <label for="yes_cust" class="form-check-label mr-3">تمایل دارم</label>
        <input type="radio" class="form-check-inline" id="yes_cust" name="has_cust"
               value="1" onclick="$('.cust').css({'display': 'block'});">
        <label for="no_cust" class="form-check-label mr-2">تمایل ندارم</label>
        <input type="radio" class="form-check-inline" id="no_cust" name="has_cust"
               value="0" onclick="$('.cust').css({'display': 'none'});"
               checked>
    </div>
</div>

<div class="cust" style="display: none">
    <div class="form-group">
        <label for="cust_name">نام شرکت واردکننده محصول ما:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cust_name" name="cust_name"
               placeholder="">
    </div>
    <div class="form-group">
        <label for="cust_country">کشور مقصد صادرات:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cust_country" name="cust_country"
               placeholder="">
    </div>
    <div class="form-group">
        <label for="cust_state">ایالت / استان مقصد صادرات:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cust_state" name="cust_state"
               placeholder="">
    </div>
    <div class="form-group">
        <label for="cust_product_name">محصول صادراتی به این شرکت:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cust_product_name"
               name="cust_product_name"
               placeholder="">
    </div>
    <div class="form-group">
        <label for="cust_turnover">ترن اور سالیانه با این شرکت:</label>
        <input type="text" class="form-control" id="cust_turnover" name="cust_turnover"
               placeholder="">
    </div>

    <div class="row py-2 mb-2">
        <div
            class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
            <label for="image">تصویر شرکت واردکننده محصول ما و جلسه عقد قرارداد<span
                    class="text-danger"> *</span>
                <small>jpg png gif</small>
            </label>
            <input type="file" name="cust_image" accept="image/*"/>
            @error('cust_image')
            <span class="text-danger text-center" role="alert">
            <strong style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong>
        </span>
            @enderror
        </div>
    </div>
</div>

@if(count($business->customerExports) > 0)
    <hr>
    <h3 class="text-center mb-4 border-bottom pb-3">آیتم های معرفی شده</h3>
    @foreach($business->customerExports as $item)
        <div class="card border border-secondary rounded cust-item-{{$item->id}}">
            <div class="card-header row">
                <h4 class="col-11"></h4>
                <span href="{{ $item->id }}" onclick="removeCust({{$item->id}})" class="btn"><i
                        class="fa fa-trash text-danger"></i></span>
            </div>
            <div class="card-body row">
                <img class="col-sm-3" src="{{ asset('images/front_images/businesses/cust/'.$item->image) }}" alt="">
                <div class="col-sm-9">
                    <p>{{ $item->name }}</p>
                    <p>{{ $item->country }}</p>
                    <p>{{ $item->state }}</p>
                    <p>{{ $item->product_name }}</p>
                    <p>{{ $item->turn_over_year }}</p>
                </div>
            </div>
        </div>
    @endforeach
@endif

<script>
    let custUrl = '/api/business/productive/customer/export/';

    function removeCust(id) {
        axios.delete(custUrl + id + '?admin=1')
            .then(function () {
                alert('آیتم با موفقیت حذف شد');
                $(".cust-item-" + id).css({"display": "none"});
            });
    }
</script>


