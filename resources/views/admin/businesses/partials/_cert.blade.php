<div class="mt-2">
    <div class="form-group row">
        <b>آیا گواهینامه دارید؟</b>
        <label for="yes_cert" class="form-check-label mr-3">بله</label>
        <input type="radio" class="form-check-inline" id="yes_cert" name="has_cert"
               value="1" onclick="$('.cert').css({'display': 'block'});">
        <label for="no_cert" class="form-check-label mr-2">خیر</label>
        <input type="radio" class="form-check-inline" id="no_cert" name="has_cert"
               value="0" onclick="$('.cert').css({'display': 'none'});"
               checked>
    </div>
</div>

<div class="cert" style="display: none">
    <div class="form-group">
        <label for="cert_owner_name">صادر شده به نام:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cert_owner_name"
               name="cert_owner_name"
               placeholder="" value="{{  old('cert_owner_name') }}">
    </div>
    <div class="form-group">
        <label for="cert_type">نوع گواهینامه:<span class="text-danger"> *</span></label>
        <select class="form-control" id="cert_type" name="cert_type">
            <option value="سیستم مدیریت">سیستم مدیریت</option>
            <option value="محصول">محصول</option>
            <option value="استاندارد صنعتی">استاندارد صنعتی</option>
            <option value="سند مجوز نظارتی">سند مجوز نظارتی</option>
        </select>
    </div>
    <div class="form-group">
        <label for="cert_num">شماره گواهینامه:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cert_num" name="cert_num"
               placeholder="654" value="{{  old('cert_num') }}">
    </div>
    <div class="form-group">
        <label for="cert_name">عنوان گواهینامه:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cert_name" name="cert_name" value="{{  old('cert_name') }}">
    </div>
    <div class="form-group">
        <label for="cert_exporter">صادرکننده گواهینامه؛<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cert_exporter" name="cert_exporter"
               placeholder="موسسه ..." value="{{  old('cert_exporter') }}">
    </div>
    <div class="form-group">
        <label for="cert_start_date">تاریخ شروع دوره:<span
                class="text-danger"> *</span></label>
        <input type="text" class="form-control" id="cert_start_date"
               name="cert_start_date"
               placeholder="1395" value="{{  old('cert_start_date') }}">
    </div>
    <div class="form-group">
        <label for="cert_end_date">تاریخ پایان دوره:</label>
        <input type="text" class="form-control" id="cert_end_date" name="cert_end_date"
               placeholder="1398" value="{{  old('cert_end_date') }}">
    </div>
    <div class="form-group">
        <label for="cert_scope">محدوده اعتبار گواهینامه (کدام کشورها) <span
                class="text-danger"> *</span></label>

        <textarea type="text" name="cert_scope" id="cert_scope"
                  class="form-control" rows="2"
                  dir="rtl">{{ old('cert_scope') }}</textarea>
    </div>

    <div class="row py-2 mb-2">
        <div
            class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
            <label for="image">تصویر گواهینامه<span class="text-danger"> *</span>
                <small>jpg png gif</small>
            </label>
            <input type="file" name="cert_image" class="dropify" data-show-loader="true"
                   autofocus
                   data-max-file-size="15000K"
                   data-allowed-file-extensions="jpg png jpeg gif"
                   accept="image/*"/>
            @error('cert_image')
            <span class="text-danger text-center" role="alert">
                                            <strong
                                                style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong>
                                        </span>
            @enderror
        </div>
    </div>
</div>

@if(count($business->certificates) > 0)
    <hr>
    <h3 class="text-center mb-4 border-bottom pb-3">آیتم های معرفی شده</h3>
    @foreach($business->certificates as $item)
        <div class="card border border-secondary rounded cert-item-{{$item->id}}">
            <div class="card-header row">
                <h4 class="col-11"></h4>
                <span href="{{ $item->id }}" onclick="removeCert({{$item->id}})" class="btn"><i
                        class="fa fa-trash text-danger"></i></span>
            </div>
            <div class="card-body row">
                <img class="col-sm-3" src="{{ asset('images/front_images/businesses/cert/'.$item->image) }}" alt="">
                <div class="col-sm-9">
                    <p>صادر شده به نام: {{ $item->owner_name }}</p>
                    <p>نوع گواهینامه:{{ $item->certificate_type }}</p>
                    <p>شماره گواهینامه: {{ $item->certificate_number }}</p>
                    <p>عنوان گواهینامه: {{ $item->certificate_name }}</p>
                    <p>صادرکننده گواهینامه؛ {{ $item->exporter }}</p>
                    <p>تاریخ شروع دوره: {{ $item->start_date }}</p>
                    <p>تاریخ پایان دوره: {{ $item->end_date ?? '' }}</p>
                    <p>محدوده اعتبار گواهینامه {{ $item->scope }}</p>
                </div>
            </div>
        </div>
    @endforeach
@endif

<script>
    let certUrl = '/api/business/productive/certificate/';

    function removeCert(id) {
        axios.delete(certUrl + id + '?admin=1')
            .then(function () {
                alert('آیتم با موفقیت حذف شد');
                $(".cert-item-" + id).css({"display": "none"});
            });
    }
</script>
