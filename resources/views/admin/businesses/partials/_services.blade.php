<div class="form-group row pt-3">
    <b>آیا نیاز به ثبت انواع خدمات دارید؟</b>
    <label for="yes_service" class="form-check-label mr-3">بله</label>
    <input type="radio" class="form-check-inline" id="yes_service"
           name="has_service" onclick="$('.serv').css({'display': 'block'});"
           value="1">
    <label for="no_service" class="form-check-label mr-2">خیر</label>
    <input type="radio" class="form-check-inline" id="no_service"
           name="has_service" onclick="$('.serv').css({'display': 'none'});"
           value="0" checked>
</div>

<div class="serv" style="@if($business->service) display:block @else display: none @endif">
    <div class="form-group row">
        <b>نوع خدمت : </b>
        <label for="in_person" class="form-check-label mr-3">حضوری</label>
        <input type="radio" class="form-check-inline" id="in_person"
               name="service_type"
               @if($business->service)
               {{$business->service->type == 1 ? 'checked' : ''}}
               @endif
               value="1">
        <label for="no_in_person" class="form-check-label mr-2">غیر حضوری</label>
        <input type="radio" class="form-check-inline" id="no_in_person"
               name="service_type"
               @if($business->service)
               {{$business->service->type == 0 ? 'checked' : ''}}
               @endif
               value="0">
    </div>
    <div class="form-group row">
        <label for="service_info" class="form-label">نحوه ارائه خدمت</label>
        <textarea
            class="form-control @error('service_info') is-danger @enderror"
            rows="15" id="service_info"
            name="service_info">{{$business->service->service_info ?? old('service_info') }}</textarea>
    </div>
</div>
