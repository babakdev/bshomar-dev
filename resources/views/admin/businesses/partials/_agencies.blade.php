<div class="form-group row pt-3">
    <b>آیا نیاز به ثبت نمایندگی دارید؟</b>
    <label for="yes_agency" class="form-check-label mr-3">بله</label>
    <input type="radio" class="form-check-inline" id="yes_agency"
           name="has_agency" onclick="$('.agen').css({'display': 'block'});"
           value="1">
    <label for="no_agency" class="form-check-label mr-2">خیر</label>
    <input type="radio" class="form-check-inline" id="no_agency"
           name="has_agency" onclick="$('.agen').css({'display': 'none'});"
           value="0" checked>
</div>

<div class="agen" style="@if($business->agencies) display:block @else display: none @endif">
    <div class="form-group row">
        <label for="agency_info" class="form-label">اطلاعات نمایندگی ها </label>
        <textarea
            class="form-control @error('agency_info') is-danger @enderror"
            rows="15" id="agency_info"
            name="agency_info">{{$business->agencies ?? old('agency_info')}}</textarea>
    </div>
</div>
