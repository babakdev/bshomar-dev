<div class="mt-2">
    <div class="form-group row">
        <b>آیا کنترل کیفیت دارید؟</b>
        <label for="yes_qc" class="form-check-label mr-3">بله</label>
        <input type="radio" class="form-check-inline" id="yes_qc" name="has_qc"
               value="1" onclick="$('.qc').css({'display': 'block'});">
        <label for="no_qc" class="form-check-label mr-2">خیر</label>
        <input type="radio" class="form-check-inline" id="no_qc" name="has_qc"
               value="0" onclick="$('.qc').css({'display': 'none'});"
               checked>
    </div>
</div>

<div class="qc" style="display: none">
    <div class="form-group">
        <label for="qc_name">نام مرحله کنترلینگ<span class="text-danger"> *</span>
        </label>
        <input id="qc_name" type="text" name="qc_name" class="form-control"
               value="{{ old('qc_name')}}" autofocus>
    </div>

    <div class="row py-2 mb-2">
        <div
            class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
            <label for="image">تصویر از واحد کنترل کیفیت<span
                    class="text-danger"> *</span>
                <small>jpg png gif</small>
            </label>
            <input type="file" name="qc_image" class="dropify" data-show-loader="true"
                   autofocus
                   data-max-file-size="15000K"
                   data-allowed-file-extensions="jpg png jpeg gif"
                   accept="image/*"/>
            @error('qc_image')
            <span class="text-danger text-center" role="alert"><strong
                    style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong></span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label for="qc_desc">شرح این مرحله از کنترل کیفیت <span
                class="text-danger"> *</span></label>

        <textarea type="text" name="qc_desc" id="qc_desc"
                  class="form-control"
                  rows="10"
                  dir="rtl">{{ old('qc_desc') }}</textarea>
    </div>
</div>

@if(count($business->qc) > 0)
    <hr>
    <h3 class="text-center mb-4 border-bottom pb-3">آیتم های معرفی شده</h3>
    @foreach($business->qc as $item)
        <div class="card border border-secondary rounded qc-item-{{$item->id}}">
            <div class="card-header row">
                <h4 class="col-11">{{ $item->name }}</h4>
                <span href="{{ $item->id }}" onclick="removeQc({{$item->id}})" class="btn"><i
                        class="fa fa-trash text-danger"></i></span>
            </div>
            <div class="card-body row">
                <img class="col-sm-3" src="{{ asset('images/front_images/businesses/qc/'.$item->image) }}" alt="">
                <p class="col-sm-9">{{ $item->description }}</p>
            </div>
        </div>
    @endforeach
@endif

<script>
    let qcUrl = '/api/business/productive/qc/';

    function removeQc(id) {
        axios.delete(qcUrl + id + '?admin=1')
            .then(function () {
                alert('آیتم با موفقیت حذف شد');
                $(".qc-item-" + id).css({"display": "none"});
            });
    }
</script>
