<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.css"/>

<style>
    .leaflet-control-coordinates {
        background: rgba(5, 5, 5, 0.5);
        border-radius: 20px;
        cursor: not-allowed;
        padding: 10px;
    }

    .leaflet-control-coordinates::after {
        color: white;
        font-size: 16px;
        font-weight: bold;
        font-family: roya, sans-serif;
        content: 'موقعیت کسب و کار شما روی نقشه به روز شد، جهت ثبت قطعی، دکمه ویرایش اطلاعات را فشار دهید';
    }

    .leaflet-control-coordinates.hidden {
        display: none;
    }
</style>

<div class="row mt-3 pt-3" style="background-color: #edefef">

    <div class="form-group col-md-6">
        <label for="header_image">عکس سردر صفحه شخصی شما</label>
        <input type="file" class="form-control" id="header_image" name="header_image">
        @if($business->header_image)
            <img src="{{ asset('images/front_images/businesses/headers/'.$business->header_image) }}"
                 alt="headerImage" class="img-fluid rounded" style="width: 340px">
        @endif
    </div>

    <div class="form-group col-md-6">
        <label for="logo">لوگو</label>
        <input type="file" class="form-control" id="logo" name="logo">
        @if($business->logo)
            <img src="{{ asset('images/front_images/businesses/logos/'.$business->logo) }}"
                 alt="logoImage" class="img-fluid rounded">
        @endif
    </div>
</div>
<hr>

<div class="row mt-2">

    <div class="col-md-4">
        <div class="form-group">
            <label for="name">نام کسب و کار <span class="text-danger"> *</span></label>
            <input type="text" class="form-control @error('name') is-danger @enderror"
                   maxlength=40
                   id="name" name="name" value="{{ $business->name ?? old('name') }}"
                   required autofocus>
            @error('name')
            <span class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="kasb_number">شماره پروانه صنفی </label>
            <input type="text" class="form-control @error('kasb_number') is-danger @enderror"
                   maxlength=11 minlength="3"
                   id="kasb_number" name="kasb_number" value="{{ $business->kasb_number ?? old('kasb_number') }}"
                    autofocus>
            @error('kasb_number')
            <span class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="col-md-4 form-group">
        <label for="phone">شماره تماس
            <span class="text-danger"> *</span>
        </label>
        <input id="phone" type="number" name="phone"
               class="form-control @error('phone') is-danger @enderror"
               value="{{ $business->phone ?? old('phone')}}" required autofocus>
        @error('phone')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

</div>

<div class="row py-2 mb-2" style="background-color: #e7e7e7">
    <div class="col-md-6 form-group">
        <label for="mainCat">دسته بندی اصلی
            <span class="text-danger"> *</span>
        </label>
        <select class="form-control" id="mainCat" name="mainCat" type="text">
            @if($business->category_id)
                <option>در صورت نیاز دسته بندی خود را تغییر دهید</option>
            @else
                <option>یک دسته را انتخاب نمایید</option>
            @endif
            @foreach($categories->where('parent_id', 0) as $mainCat)
                <option value="{{$mainCat->id}}">{{$mainCat->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="col-md-6 form-group">
        <label for="category"> دسته بندی فرعی
            <span class="text-danger"> *</span>
        </label>
        <select class="form-control @error('category_id') is-danger @enderror"
                id="category"
                name="category_id">
            <option value="{{ $business->category_id ?? '' }}"
                {{ old('category_id') }}>
                {{ $business->category->name ?? '' }}
            </option>
        </select>
        @error('category_id')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="row">
    <div class="col-md-6 form-group">
        <label for="sheba">شماره شبا
        </label>
        <input id="sheba" type="text" name="sheba" class="form-control"
               value="{{ $business->sheba ?? old('sheba')}}" autofocus>
    </div>
    <div class="col-md-6 form-group">
        <label for="email">ایمیل</label>
        <input id="email" type="email" name="email" autofocus
               class="form-control" value="{{ $business->email ?? old('email') }}">
    </div>
</div>

<div class="row py-2 mb-2" style="background-color: #e7e7e7">
    <div class="col-md-6 form-group">
        <label for="state">انتخاب استان<span class="text-danger"> *</span></label>
        <select class="form-control" id="state" name="state_id" type="text">
            <option>برای تغییر، استان جدید را انتخاب نمایید</option>
            @foreach($cities->where('parent_id', 0) as $state)
                <option value="{{$state->id}}">{{$state->title}}</option>
            @endforeach
        </select>
    </div>

    <div class="col-md-6 form-group">
        <label for="city">انتخاب شهر
            <span class="text-danger"> *</span>
        </label>
        <select class="form-control @error('city_id') is-danger @enderror" id="city" name="city_id">
            <option value="{{ $business->city_id ?? '' }}"
                {{ old('city_id') }}>
                {{ $business->city->title ?? '' }}
            </option>
        </select>
        @error('city_id')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="row">
    <div class="col-md-12 form-group">
        <label for="addresses">آدرس</label>
        <textarea class="form-control border rounded" id="addresses" maxlength=255 autofocus
                  name="address">{{ $business->address ?? old('address') }}</textarea>
    </div>
</div>

<div class="row" style="background-color: #e7e7e7">
    <div class="col-md-4 form-group">
        <label for="website">وبسایت شرکت</label>
        <input type="text" class="form-control" id="website"
               name="website" placeholder="www.example.ir"
               value="{{ $business->website ?? old('website') }}">
    </div>

    <div class="form-group col-md-4">
        <label for="min_shipping_cost">سقف رایگان شدن هزینه پست <small>(تومان)</small>:</label>
        <input type="number" class="form-control" id="min_shipping_cost" name="min_shipping_cost"
               min="0" max="10000000" value="{{ old('min_shipping_cost') ?? $business->min_shipping_cost }}">
    </div>
    <div class="form-group col-md-4">
        <label for="shipping_cost">هزینه پست <small>(تومان)</small>:</label>
        <input type="number" class="form-control" id="shipping_cost" name="shipping_cost"
               value="{{ old('shipping_cost') ?? $business->shipping_cost }}">
    </div>
</div>

<div class="mt-2 form-group">
    <label for="advantages">مزایای کلیدی شرکت ما </label>
    <textarea type="text" name="advantages" id="advantages"
              class="form-control" rows="3"
              dir="rtl">{{ old('advantages', $business->advantages) }}</textarea>
</div>

<hr>

<div class="row p-3" style="background-color: #f1f1f1">
    <div class="col-md-4 form-group">
        <label for="is_active">وضعیت
            <span class="text-danger"> *</span>
        </label>
        <select type="text" class="form-control" id="is_active" name="is_active">
            <option value="1" {{ $business->is_active == 1 ? 'selected' : '' }}>فعال
            </option>
            <option value="0" {{ $business->is_active == 0 ? 'selected  ' : '' }}>
                غیرفعال
            </option>
        </select>
    </div>
    @if(!auth()->user()->isAsnaf)
        <div class="col-md-6 form-group">
            <label for="panel">نوع پنل</label>
            <select type="text" class="form-control" id="panel" name="panel">
                <option value="1" {{ $business->panel_id == 1 ? 'selected' : '' }}>رایگان</option>
                <option value="2" {{ $business->panel_id == 2 ? 'selected' : '' }}>60 تومانی</option>
                <option value="3" {{ $business->panel_id == 3 ? 'selected' : '' }}>100 تومانی</option>
                <option value="4" {{ $business->panel_id == 4 ? 'selected' : '' }}>200 تومانی</option>
            </select>
        </div>
    @endif
</div>

<div class="row">
    <div class="col-md-12 form-group">
        <label for="rules">قوانین این کسب و کار </label>
        <textarea type="text" name="rules" id="rules"
                  class="form-control" rows="5"
                  dir="rtl">{{ old('rules', $business->rules) }}</textarea>
        @error('rules')
        <span class="text-danger text-center" role="alert">
            <strong style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="row py-2 mb-2">
    <div class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
        <label for="image">تصویر کسب و کار
            <span style="font-size: 12px"> (حداکثر 4 و حداقل 1 تصویر)</span>
            <small>jpg png gif</small>
            <br><small>برای انتخاب چند تصویر بصورت همزمان کلید ctrl را پایین نگه دارید و روی تصویر کلیک کنید.</small>
        </label>
        <input type="file" name="image[]" multiple="multiple"
               data-show-loader="true" autofocus
               data-max-file-size="15000K"
               data-allowed-file-extensions="jpg png jpeg gif"
               accept="image/*"/>
        @error('image')
        <span class="text-danger text-center" role="alert">
            <strong style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="form-group col-md-12">
    <div class="row" id="image-group">
        @foreach($business->images as $image)
            <div class="form-group col-3 mx-auto text-center"
                 id="imageFull{{ $image->id }}">
                <img src="{{ asset('images/front_images/businesses/small/'.$image->path) }}"
                    alt="productImage" class="img-fluid rounded" style="width: 240px">
                <a id="image{{ $image->id }}" class="removePicBtn btn btn-link"
                   data-imageid="{{$image->id}}" data-businessid="{{$business->id}}" title="حذف تصویر">
                    <i class="fa fa-times-circle text-danger fa-2x mt-2"></i>
                </a>
            </div>
        @endforeach
    </div>
</div>

{{--MAP--}}
<div id="map" style="width: 90%; height: 400px;margin:auto;" class="mt-3 p-0 "></div>
<div id="address"></div>

<br>
<div class="row">
    <div class="col-md-12 form-group">
        <label for="description">توضیحاتی راجع به کسب و کار </label>

        <textarea type="text" name="description" id="description"
                  class="form-control my-editor" rows="8"
                  dir="rtl">{{ old('description', $business->description) }}</textarea>
        @error('description')
        <span class="text-danger text-center" role="alert">
            <strong style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

@section('js')
    <script
        src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bwwivsywiq8s8c628683jhooygk3jlc9kd22wq4093syaqab"></script>
    <script>
        var editor_config = {
            path_absolute: "/",
            selector: "textarea.my-editor",
            language_url: '../../../js/backend/fa.js',
            directionality: 'rtl',
            plugins: [
                "advlist autolink lists link charmap print preview hr",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime save contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link ",
            relative_urls: false,
            file_browser_callback: function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        };
        tinymce.init(editor_config);
    </script>
    <script rel="script" src="{{ asset('js/AjaxCity.js') }}"></script>
    <script rel="script" src="{{ asset('js/AjaxCategory.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.5.1/leaflet.js"></script>
    <script src="{{asset('js/front_js/Control.Coordinates.js')}}"></script>
    <script src="{{asset('js/front_js/L.Control.Locate.js')}}"></script>
    <script>
        $('.removePicBtn').on('click', function () {
            var image = $(this).data('imageid');
            var BusId = $(this).data('businessid');
            axios.get('/business-image/' + image + '/' + BusId)
                .then(function (response) {
                    alert('تصویر با موفقیت حذف شد');
                    $("#imageFull").css({"display": "none"});
                    $("#image-group").empty().append(response.data.data);
                });
        });
    </script>
    <script>
        // initialize the map
        let mymap = L.map('map').setView([33.2148679, 52.1922632], 5);

        // load a tile layer
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: ' &copy; <a href="/">bShomar.com</a>',
            id: 'mapbox.streets',
            // tileSize: 550
        }).addTo(mymap);

        //get coordinates by click
        let coord = new L.Control.Coordinates();
        coord.addTo(mymap);
        mymap.on('click', function (e) {
            coord.setCoordinates(e);
            let lat = $('#latitude').val();
            let lng = $('#longitude').val();
            $('#address').html('<input type="hidden" id="latM" name="latitude" value="' + lat + '">', '<input type="hidden" id="lngM" name="longitude" value="' + lng + '">');
        });
        // click show where am I
        mymap.options.singleClickTimeout = 250;
        mymap.on('click', function (e) {
            L.popup().setLatLng(e.latlng)
                .setContent('<p>من اینجام</p>')
                .openOn(mymap);
        });
        // Get location
        L.control.locate().addTo(mymap);

    </script>
@endsection
