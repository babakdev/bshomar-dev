<div class="mt-2">
    <div class="form-group row">
        <b>آیا تمایل به نمایش خط تولید خود دارید؟</b>
        <label for="yes_manu" class="form-check-label mr-3">بله</label>
        <input type="radio" class="form-check-inline" id="yes_manu" name="has_manu"
               value="1" onclick="$('.manu').css({'display': 'block'});">
        <label for="no_manu" class="form-check-label mr-2">خیر</label>
        <input type="radio" class="form-check-inline" id="no_manu" name="has_manu"
               value="0" onclick="$('.manu').css({'display': 'none'});"
               checked>
    </div>
</div>

<div class="manu" style="display: none">

    <div class="form-group">
        <label for="manu_name">نام مرحله تولید<span class="text-danger"> *</span>
        </label>
        <input id="manu_name" type="text" name="manu_name" class="form-control"
               value="{{ old('manu_name')}}" autofocus>
    </div>

    <div class="row py-2 mb-2">
        <div
            class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
            <label for="image">تصویر از مرحله تولید<span class="text-danger"> *</span>
                <small>jpg png gif</small>
            </label>
            <input type="file" name="manu_image" data-show-loader="true" autofocus
                   class="dropify"
                   data-max-file-size="15000K"
                   data-allowed-file-extensions="jpg png jpeg gif"
                   accept="image/*"/>
            @error('manu_image')
            <span class="text-danger text-center" role="alert"><strong
                    style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong></span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label for="manu_desc">شرح این مرحله از تولید <span
                class="text-danger"> *</span></label>

        <textarea type="text" name="manu_desc" id="manu_desc"
                  class="form-control"
                  rows="10"
                  dir="rtl">{{ old('manu_desc') }}</textarea>
    </div>
</div>

@if(count($business->manufacturingLines) > 0)
    <hr>
    <h3 class="text-center mb-4 border-bottom pb-3">آیتم های معرفی شده</h3>
    @foreach($business->manufacturingLines as $item)
        <div class="card border shadow rounded manu-item-{{$item->id}}">
            <div class="card-header row">
                <h4 class="col-11">{{ $item->name }}</h4>
                <span href="{{ $item->id }}" onclick="removeManu({{$item->id}})" class="btn"><i
                        class="fa fa-trash text-danger"></i></span>
            </div>
            <div class="card-body row">
                <img class="col-sm-3" src="{{ asset('images/front_images/businesses/manu/'.$item->image) }}" alt="">
                <p class="col-sm-9">{{ $item->description }}</p>
            </div>
        </div>
    @endforeach
@endif

<script>
    let manuUrl = '/api/business/productive/manu/';

    function removeManu(id) {
        axios.delete(manuUrl + id + '?admin=1')
            .then(function () {
                alert('آیتم با موفقیت حذف شد');
                $(".manu-item-" + id).css({"display": "none"});
            });
    }
</script>
