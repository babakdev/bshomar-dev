@extends($page)

@section('content')
    <div class="container-fluid card text-center" style="min-height: 600px;">
        @include('includes.show-sessions')
        <div class="card">
            <h3 class="text-center pt-2 card-title">{{ $business->name }}</h3>
            <hr>
            <div class="row mx-auto">
                <b>ID: </b>&nbsp;<span>{{$business->id}}</span>
            </div>

            <div class="card-body p-0">
                @if($business->header_image)
                    <b>عکس سردر</b>
                    <img src="{{ asset('images/front_images/businesses/headers/'.$business->header_image) }}"
                         alt="headerImage" class="img-fluid rounded" style="width: 340px">
                    <br>
                @endif
                @if($business->logo)
                    <b>لوگو</b>
                    <img src="{{ asset('images/front_images/businesses/logos/'.$business->logo) }}"
                         alt="logoImage" class="img-fluid rounded">
                    <br>
                @endif

                <div class="form-group col-md-12">
                    <div class="row" id="image-group">
                        @foreach($business->images as $image)
                            <div class="form-group col-3 mx-auto text-center"
                                 id="imageFull{{ $image->id }}">
                                <img src="{{ asset('images/front_images/businesses/large/'.$image->path) }}"
                                     alt="productImage" class="img-fluid rounded" style="width: 300px">
                            </div>
                        @endforeach
                    </div>
                </div>
                <hr>
                <b>نام کاربری:</b>&nbsp;<span>{{ $business->user->name ?? 'نامشخص' }}</span>
                <hr>
                @if(!auth()->user()->isAsnaf)
                    <b>یوزر کاربر:</b>&nbsp;<span>{{ $business->user->phone ?? 'نامشخص' }}</span>
                    <hr>
                @endif
                <span dir="rtl">
                <b>آخرین ورود:</b>&nbsp;<span>{{ isset($business->user->last_login_at) ? verta($business->user->last_login_at) : '-' }}</span>
                </span>
                <hr>

                <b>نام شهر:</b>&nbsp;<span
                    class="{{ $business->city_id ? '' : 'text-danger' }}">{{$business->city->title ?? 'نامشخص'}}</span>
                <hr>

                <b>دسته بندی:</b>&nbsp;<span
                    class="{{ $business->category_id ? '' : 'text-danger' }}">{{$business->category_id ? $business->category->name : 'نامشخص'}}</span>
                <hr>

                <span>
                    <strong>آدرس:</strong> {{ $business->address ?: 'نامشخص'}}
                </span>
                <hr>

                <span><strong>شماره تماس:</strong> {{ $business->phone ?? 'نامشخص'}}</span>
                <hr>

                <span><strong>شماره شبا:</strong> {{ $business->sheba ?? 'نامشخص'}}</span>
                <hr>

                <span><strong>ایمیل:</strong> {{ $business->email ?? 'نامشخص' }}</span>
                <hr>

                <span dir="rtl"><strong>وبسایت:</strong> {{ $business->website ?? 'نامشخص' }}</span>
                <hr>

                <span><strong>پنل:</strong> {{ $business->panel->name  ?? 'نامشخص' }}</span>
                <hr>

                <span dir="rtl">
                    <strong>توضیحات:</strong> {!! $business->description ?? 'نامشخص' !!}
                </span>
                <hr>

                <span><strong>قوانین شخصی:</strong> {{ $business->rules ?? 'ندارد' }}</span>
                <hr>

                <span><strong>تاریخ
                        ثبت:</strong> {{ verta($business->created_at)->formatDate() ?? 'نامشخص'}}
                </span>
                <hr>

                <span>@if(!auth()->user()->isAsnaf)
                        <a href="{{ url('/send-message', $business->user_id )}}"
                           class="btn btn-link">
                            <i class="fa fa-envelope"></i> ارسال پیام
                        </a>

                    @endif
                </span>
                <a href="{{ url('/business-information/'.$business->id) }}"
                   class="btn btn-link text-success" title="ویرایش"><i
                        class="fa fa-edit"></i> ویرایش </a>


                <h6 dir="rtl" class="card border py-3" style="background-color: #d8d8d8">
                    <b>تمامی اطلاعات تامین کننده را مشاهده نمودم و با مسئولیت کاربری خودم تایید میکنم</b>
                    <a href="{{ url('/business-is-active/'.$business->id) }}"
                       class="btn btn-success mt-3" title="تایید"> تایید <i
                            class="fa fa-check-circle"></i>
                    </a>
                </h6>


                {{--                                    @if(!auth()->user()->isAsnaf)--}}
                {{--                                        <form action="{{ url('/business/'.$business->id) }}" method="POST">--}}
                {{--                                            @csrf--}}
                {{--                                            @method('delete')--}}
                {{--                                            <button type="submit" class="btn btn-link delBusiness" title="حذف"><i--}}
                {{--                                                    class="fa fa-trash text-danger"></i></button>--}}
                {{--                                        </form>--}}
                {{--                                    @endif--}}
                </td>
                </tr>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.delBusiness').click(function () {
                return confirm('آیا از حذف کسب و کار مورد نظر اطمینان دارید؟');
            });
        });
    </script>
@endsection
