@extends($page)

@section('content')
    <div class="container-fluid card text-center" style="min-height: 600px;">
        @include('includes.show-sessions')
        <div class="card">
            <h3 class="text-center pt-2 card-title">کسب و کار های نیازمند تایید</h3>
            @if(count($businesses)>0)
                <div class="card-body table-responsive p-0">

                    <table class="table table-striped" dir="rtl">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>عکس</th>
                            <th>نام پذیرنده</th>
                            <th>نام کاربری</th>
                            @if(!auth()->user()->isAsnaf)
                                <th>یوزر کاربر</th>
                            @endif
                            <th>آخرید ورود</th>
                            <th>نام شهر</th>
                            <th>دسته بندی</th>
                            <th>ارسال پیام</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($businesses as $business)
                                <tr>
                                <td>{{$business->id}}</td>
                                <td><img height="80" alt="{{$business->name}}" class="rounded"
                                         src="{{ $business->image() }}">
                                </td>
                                <td><a href="{{ 'https://bshomar.com/' . $business->id}}"
                                       target="_blank">{{$business->name}}</a></td>
                                <td>{{ $business->user->name ?? 'نامشخص' }}</td>
                                @if(!auth()->user()->isAsnaf)
                                    <td>{{ $business->user->phone ?? 'نامشخص' }}</td>
                                @endif
                                <td dir="ltr">{{ isset($business->user->last_login_at) ? verta($business->user->last_login_at) : '-' }}</td>
                                <td class="{{ $business->city_id ? '' : 'text-danger' }}">{{$business->city->title ?? 'نامشخص'}}</td>
                                <td class="{{ $business->category_id ? '' : 'text-danger' }}">{{$business->category_id ? $business->category->name : 'نامشخص'}}</td>
                                <td>
                                    @if(!auth()->user()->isAsnaf)
                                        <a href="{{ url('/send-message', $business->user_id )}}"
                                           class="btn btn-link">
                                            <i class="fa fa-envelope">&nbsp;</i>
                                        </a>
                                    @endif
                                </td>
                                <td class="text-center row">

                                    <a href="{{ url('/business/details/'.$business->id) }}"
                                       class="btn btn-link text-success" title="مرحله بعد"><i
                                            class="fa fa-arrow-circle-left"></i> مرحله تایید
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <div class="col-md-8">
                                <h3 class="text-info mt-5 text-right" style="direction: rtl">کسب و کار نیازمند تایید
                                    موجود نمی
                                    باشد
                                </h3>
                            </div>
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="mx-auto">
                        {{$businesses->render()}}
                    </div>
                </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.delBusiness').click(function () {
                return confirm('آیا از حذف کسب و کار مورد نظر اطمینان دارید؟');
            });
        });
    </script>
@endsection
