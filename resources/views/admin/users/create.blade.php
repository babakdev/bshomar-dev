@extends($page)
@section('styles')
    <link rel="stylesheet" href="/css/chosen.min.css">
    <style>
        #load {
            border: 4px solid #f3f3f3;
            border-top: 4px solid #da542e;
            border-radius: 50%;
            width: 8px;
            height: 8px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@stop

@section('content')
    <div class="container-fluid text-right" dir="rtl">

        <div class="breadcrumb text-secondary col-sm-12 p-1 bg-white font-16 border-bottom ">
            <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}" class="link">داشبورد </a><i
                class="fa fa-chevron-left breadcrumb-item"></i>
            <a href="{{url('/users')}}" class="link"> لیست کاربران </a><i
                class="fa fa-chevron-left breadcrumb-item"></i>
            <a>ساخت کاربر </a>
        </div> <!--END BREADCRUMB-->

        <form action="{{url('/users')}}" method="POST">
            @csrf

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="name">نام و نام خانوادگی<span class="text-danger"> *</span></label>
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-danger' : '' }}"
                               id="name" name="name" value="{{ old('name') }}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="phone">شماره موبایل
                            <small> (نام کاربری)</small> <span class="text-danger"> *</span>
                        </label>
                        <input id="phone" type="text" name="phone" class="form-control"
                               value="{{ old('phone')}}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email">ایمیل</label>
                        <input id="email" type="email" name="email"
                               class="form-control" value="{{ old('email') }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nationalcode">کد ملی</label>
                        <input type="number" class="form-control {{ $errors->has('nationalcode') ? 'is-danger' : '' }}"
                               id="nationalcode" name="nationalcode" value="{{ old('nationalcode') }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="city">انتخاب استان</label>
                        <select class="form-control" id="state" name="state_id" type="text">
                            <option>انتخاب استان</option>
                            @foreach($cities->where('parent_id', 0) as $state)
                                <option value="{{$state->id}}">{{$state->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="city">انتخاب شهر</label>
                        <select class="form-control" id="city" name="city_id">
                            <option value="{{old('city_id')}}">ابتدا استان انتخاب شود</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-12 form-group">
                <div class="form-group">
                    <label for="address">آدرس محل سکونت:</label>
                    <textarea class="form-control" id="address" name="address" rows="">{{ old('address') }}</textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="is_active">وضعیت</label>
                    <select type="text" class="form-control" id="is_active" name="is_active">
                        <option value="1">فعال</option>
                        {{--                        <option value="0">غیرفعال</option>--}}
                    </select>
                </div>
                <div class="col-md-6 form-group">
                    <label for="role_id">دسترسی</label>
                    <select type="text" class="form-control" id="role_id" name="role_id">
                        @foreach($roles as $role)
                            @if(auth()->user()->role_id == 1 )
                                <option
                                    value="{{$role->id}}" {{ old('role_id') == $role->id ? 'selected':'' }}>{{ $role->name }}</option>
                            @elseif(auth()->user()->role_id == 2 )
                                @if($role->id == 5 || $role->id == 3)
                                    <option value="{{$role->id}}" {{ $role->id }}>{{ $role->name }}</option>
                                @endif
                            @elseif($role->id == 3)
                                <option
                                    value="{{$role->id}}" {{ old('role_id') == $role->id ? 'selected':'' }}>{{ $role->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <p>رمز عبور همان شماره موبایل وارد شده خواهد شد</p>

            <div class="form-group col-12" id="cats">
                <label for="category_id">دسته بندی های پشتیبان اتحادیه</label>
                {{Form::select('category_id', $categories->where('parent_id', '!=', 0)->pluck('name', 'id'), null,
                            array('multiple'=>'multiple','name'=>'category_id[]',
                            'class' => 'chosen-select form-control' ,'style' => 'height:300px'))}}
            </div>

            <button type="submit" class="btn btn-success btn-block font-20">ایجاد کاربر</button>
        </form>

        @include('includes.form-error')
    </div>
@endsection
@section('scripts')
    <script rel="script" src="{{ asset('js/AjaxCity.js') }}"></script>
    <script src="{{asset('/js/chosen.jquery.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#cats').hide();
            $('#role_id').on('click', function () {
                if ($('#role_id').val() == 5) {
                    $('#cats').show();
                } else {
                    $('#cats').hide();
                }
            })
        });
    </script>
@endsection
