@extends($page)

@section('content')
    <div dir="rtl">
        @include('includes.show-sessions')

        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-center">لیست کاربران غیرفعال</h3>
                {{--                <input class="form-control w-50 mx-auto mt-2" id="myInput" type="text" placeholder="جستجو..">--}}

            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-center">
                    <thead>
                    <tr>
                        <th>نام</th>
                        <th>نام کاربری</th>
                        <th>نقش</th>
                        <th>وضعیت</th>
                        <th class="text-left">عملیات</th>
                    </tr>
                    </thead>
                    @foreach($users as $user)
                        <tbody id="myTable">
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->role->name }}</td>
                            <td>
                        <span class="p-1 text-light badge bg-{{ $user->is_active == 1 ? 'success': 'danger' }}">
                            {{$user->is_active == 1 ? 'فعال' : 'تایید نشده'}}
                        </span>
                            </td>
                            <td class="row float-left" style="font-size: 18px;">
                                <a href="{{ url('/user-is-active/'.$user->id) }}"
                                   class="btn btn-link text-success" title="تایید"><i
                                        class="fa fa-check-circle"></i>
                                </a>
                                <a href="{{url('/users/'. $user->id . '/edit')}}">
                                    <i class="fa fa-edit text-info btn btn-link" title="ویرایش"></i>
                                </a>&nbsp;
                            </td>
                        </tr>
                        </tbody>

                    @endforeach
                </table>
            </div>
            <!-- /.card-body -->
            <div class="text-center mx-auto mt-3">
                {{ $users->links() }}
            </div>
        </div>
        <!-- /.card -->
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#myInput").on("keyup", function () {
                let value = $(this).val();
                $("#myTable tr").filter(function () {
                    $(this).toggle($(this).text().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script>
        $('.deleteUser').click(function () {
            return confirm('آیا از حذف کاربر مورد نظر اطمینان دارید؟');
        });
    </script>
@endsection
