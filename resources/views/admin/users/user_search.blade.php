@extends($page)

@section('content')

    <div class=" mb-2 mx-auto pt-5" style="min-height: 800px;">
        @if($user)
            <div class="card shadow pt-3 m-3" style="height: 400px;">
                <div class="card-body text-justify" dir="rtl">
                    <div class="row">
                        <img alt="{{ $user->name }}" class="img-fluid mr-3 pr-2 rounded-circle"
                             style="height: 100px"
                             src="{{ $user->image ? asset($user->image) : asset('images/users/user.png')}}">
                        <h2 class="card-title pt-4 pr-3">{{$user->name}}</h2>

                    </div>
                    <hr>
                    <p class="card-text">یوزرنیم: {{ $user->phone ?? 'نامشخص' }}</p>
                    <p class="card-text">وضعیت: {{ $user->is_active == 1 ? 'فعال' : 'غیرفعال' }}</p>

                    <button type="button" class="btn btn-link"
                            data-toggle="modal" data-target="#userModal{{ $user->id }}">
                        مشاهده اطلاعات کاربر
                    </button>

                    <a href="{{url('/users/'.$user->id.'/edit')}}" class="card-link float-left text-primary"><i
                            class="fa fa-caret-left">&nbsp;</i>ویرایش کاربر</a>
                    @if($business)
                        <a href="{{ url('/business-information/'. $business->id) }}"
                           class="card-link float-left text-active ml-5"><i
                                class="fa fa-caret-left">&nbsp;</i>ویرایش کسب و کار</a>
                    @endif

                    @if(!auth()->user()->isAsnaf)
                        <a href="{{ url('/send-message', $user->id ) }}"
                           class="card-link float-left text-active bg-white border-0 text-primary ml-5"
                           style="cursor: pointer"><i class="fa fa-caret-left">&nbsp;</i>ارسال پیام
                        </a>
                    @endif


                    {{--                    <form action="{{url('/users/'. $user->id)}}" method="POST">--}}
                    {{--                        @csrf--}}
                    {{--                        @method('DELETE')--}}
                    {{--                        <button type="submit" class="btn btn-link deleteUser"--}}
                    {{--                                @if($user->isAdmin || $user->isSupport)--}}
                    {{--                                disabled="disabled"--}}
                    {{--                            @endif--}}
                    {{--                        >--}}
                    {{--                            <i class="fa fa-trash text-danger" title="حذف">--}}
                    {{--                            </i>--}}
                    {{--                        </button>--}}
                    {{--                    </form>--}}

                </div>
            </div>

            <!-- User Modal -->
            <div class="modal fade" id="userModal{{ $user->id }}" dir="rtl">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;
                            </button>
                            <h3 class="modal-title">{{ $user->name }}</h3>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body text-right">
                            <img src="{{ $user->image ? asset($user->image) : asset('images/users/user.png')}}"
                                 alt="UserAvatar"
                                 width="120" class="mb-3 rounded-circle">
                            <p>نام کاربری(موبایل): {{ $user->phone }}</p>
                            <p>نقش: {{ $user->role->name }}</p>
                            <p>ایمیل: {{ $user->email ?? 'نامشخص' }}</p>
                            <p>شهر: {{ $user->city->title ?? 'نامشخص' }}</p>
                            <p>موجودی کیف پول: {{ $user->balance ?? 'نامشخص' }} تومان </p>
                            <p>آخرید ورود:
                                <b dir="ltr">{{ isset($business->user->last_login_at) ? verta($business->user->last_login_at) : '-' }}</b>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        @endif

        @if(count($dataB) > 0)
            <hr>
            <div class="row mx-auto">

                @foreach($dataB as $bus)
                    <div class="card col-3 shadow pt-3">
                        <div class="card-body text-justify" dir="rtl">
                            <div class="row">
                                <h2 class="card-title pt-4 pr-3">{{$bus->name}}</h2>
                            </div>
                            <hr>
                            <p class="card-text">کاربر: {{ $bus->user->name ?? 'نامشخص' }}</p>
                            <p class="card-text">یوزرنیم: {{ $bus->user->phone ?? 'نامشخص' }}</p>
                            <p>آخرید ورود:
                                <b dir="ltr">{{ isset($bus->user->last_login_at) ? verta($bus->user->last_login_at) : '-' }}</b>
                            </p>
                            <a href="{{ url('/business-information/'. $bus->id) }}"
                               class="card-link float-left text-active ml-5"><i
                                    class="fa fa-caret-left">&nbsp;</i>ویرایش کسب و کار</a>
                            @if(!auth()->user()->isAsnaf)
                                <a href="{{ url('/send-message', $bus->user->id ) }}"
                                   class="card-link float-left text-active bg-white border-0 text-primary ml-5"
                                   style="cursor: pointer"><i class="fa fa-caret-left">&nbsp;</i>ارسال پیام</a>
                            @endif
                        </div>
                    </div>

                @endforeach
            </div>

        @endif
    </div>

@endsection
