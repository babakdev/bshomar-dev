@extends($page)

@section('content')
    <div dir="rtl">
        @include('includes.show-sessions')

        <div class="card">
            <div class="card-header">
                <a class="btn btn-success" href="{{ url('/users/create') }}">
                    ساخت کاربر جدید <span class="fa fa-plus"></span>
                </a>
                <h3 class="card-title text-center">لیست کاربران</h3>
                {{--                <input class="form-control w-50 mx-auto mt-2" id="myInput" type="text" placeholder="جستجو..">--}}

            </div>
        @if(!auth()->user()->isAsnaf)
            <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th>آواتار</th>
                            <th>نام</th>
                            <th>نام کاربری</th>
                            <th>نقش</th>
                            <th>وضعیت</th>
                            <th>آخرید ورود</th>
                            <th class="text-left">عملیات</th>
                        </tr>
                        </thead>
                        @foreach($users as $user)
                            <tbody id="myTable">
                            <tr>
                                <td><img src="{{url($user->image ? "images/users/{$user->image}" : 'images/users/user.png')}}"
                                         alt="user" class="rounded-circle" style="width: 40px">
                                </td>
                                <td>{{ $user->name }}</td>
                                @if(!$user->isAdmin)
                                    <td>{{ $user->phone }}</td>
                                @else
                                    <td></td>
                                @endif
                                <td>{{ $user->role->name }}</td>
                                <td>
                                    <span class="p-1 text-light badge bg-{{ $user->is_active == 1 ? 'success': 'danger' }}">
                                    {{$user->is_active == 1 ? 'فعال' : 'غیرفعال'}}
                                    </span>
                                </td>
                                <td dir="ltr">{{ $user->last_login_at ? verta($user->last_login_at) : '-' }}</td>

                                <td class="row float-left" style="font-size: 18px;">
                                    @if(auth()->user()->isAdmin)
                                        <a href="/user/wallet/{{$user->phone}}">
                                            <i class="fa fa-wallet text-success btn btn-link"
                                               title="تراکنش های مالی"></i>
                                        </a>
                                    @endif

                                    <button type="button" class="btn btn-link" title="مشاهده اطلاعات کاربر"
                                            data-toggle="modal" data-target="#userModal{{ $user->id }}">
                                        <i class="fa fa-eye"></i>
                                    </button>

                                    <a href="{{url('/users/'. $user->id . '/edit')}}">
                                        <i class="fa fa-edit text-info btn btn-link" title="ویرایش"></i>
                                    </a>&nbsp;
                                </td>
                            </tr>
                            </tbody>

                            <!-- User Modal -->
                            <div class="modal fade" id="userModal{{ $user->id }}">
                                <div class="modal-dialog">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;
                                            </button>
                                            <h3 class="modal-title">{{ $user->name }}</h3>
                                        </div>

                                        <!-- Modal body -->
                                        <div class="modal-body text-right">
                                            <img src="{{ asset($user->image ?? 'images/users/user.png' )}}"
                                                 alt="UserAvatar"
                                                 width="120" class="mb-3 rounded-circle">
                                            <p>نام کاربری(موبایل): {{ $user->phone }}</p>
                                            <p>نقش: {{ $user->role->name }}</p>
                                            <p>تاریخ عضویت: <span
                                                    dir="ltr">{{verta( $user->created_at)->formatDate() }}</span></p>
                                            <p>کدملی: {{ $user->nationalcode ?? 'نامشخص' }}</p>
                                            <p>ایمیل: {{ $user->email ?? 'نامشخص' }}</p>
                                            <p>شهر: {{ $user->city->title ?? 'نامشخص' }}</p>
                                            <p><strong>تاریخ ثبت:</strong> {{ verta($user->created_at) ?? 'نامشخص'}}</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </table>
                </div>
                <!-- /.card-body -->
                <div class="text-center mx-auto mt-3">
                    {{ $users->links() }}
                </div>
        </div>
    @endif
    <!-- /.card -->
    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $("#myInput").on("keyup", function () {
                let value = $(this).val();
                $("#myTable tr").filter(function () {
                    $(this).toggle($(this).text().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script>
        $('.deleteUser').click(function () {
            return confirm('آیا از حذف کاربر مورد نظر اطمینان دارید؟');
        });
    </script>
@endsection
