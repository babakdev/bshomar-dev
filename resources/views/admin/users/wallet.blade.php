@extends($page)
@section('pageTitle')کیف پول@endsection
@section('content')

    <div class="container-fluid" dir="rtl">
        <div class="breadcrumb text-info col-sm-12 mb-0 p-0">
            <div class="col-12 text-right p-0 pt-2 pb-2 bg-white">
                <a href="/">بی‌شمار </a><i class="fas fa-angle-left"></i>
                @if(auth()->user()->isAdmin)
                    <a href="{{ url('/finance')}}">مالی </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                @endif
                <a>کیف پول </a>
            </div>
        </div> <!--END BREADCRUMB-->

        @include('includes.show-sessions')
        @include('includes.form-error')

        <h1 class="h3 text-center text-info">کیف پول {{ $user->name }}</h1>

        <div class="row">

            <div class=" col">
                @if(count($user->transactions->where('confirmed', 1)) > 0)
                    <table class="table table-striped table-hover text-center" dir="rtl">
                        <thead>
                        <tr style="background-image: linear-gradient(135deg,#283f72,#5b8a9d);color: white">
                            <td>ماهیت</td>
                            <td>مبلغ</td>
                            <td>وضعیت</td>
                            <td>شرح</td>
                            <td>تاریخ</td>
                        </tr>
                        </thead>

                        <tbody class="text-center">
                        @foreach($user->transactions->where('confirmed', 1) as $trans)
                            <tr>
                                <td>{{ $trans->type == 'deposit' ? 'واریز به کیف پول' : 'برداشت از کیف پول' }}</td>
                                <td><span dir="ltr">{{ $trans->amount }}</span> <span> تومان </span></td>
                                <td class="{{ $trans->meta[0] == 'درخواست برداشت از کیف پول' ? 'text-warning' : 'text-success' }}">{{ $trans->meta[0] == 'درخواست برداشت از کیف پول' ? 'تایید نشده' : 'تایید نهایی شد' }}</td>
                                <td class="font-weight-bold">{{ $trans->meta[0] }}</td>
                                <td dir="ltr">{{ verta($trans->created_at) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{--                    <div class="row mt-2">--}}
                    {{--                        <div class="mx-auto">--}}
                    {{--                            {{$user->transactions->render()}}--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                @endif
                <div class="card text-center mb-3 shadow">
                    <div class="card-header h3">
                        موجودی
                        {{ number_format($user->balance) }} تومان
                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn btn-info float-left" data-toggle="modal"
                                data-target="#myModal">
                            درخواست واریز وجه
                        </button>
                    </div>
                </div>
            </div>

        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body text-right" style="font-size:14px">
                        <h4>نکته</h4>
                        <ul>
                            <li>به ازای هر درخواست واریز نقدی، باید حداقل یک پنل غیر رایگان کسب و کار را فروخته باشید، در غیر اینصورت درخواست شما برگشت خواهد خورد.</li>
                            <li> حداقل درآمد قابل برداشت شما در هنگام درخواست واریز وجه باید <strong>50,000</strong>
                                تومان باشد.
                            </li>
                            <li>
                                <span>در حال حاضر درآمد قابل برداشت شما </span>
                                <strong>@if($user->balance > 50000) {{$user->balance}} @else 0 @endif</strong>
                                <span>تومان می باشد.</span>
                            </li>
                        </ul>
                        <hr>
                        <h4 class="text-center">درخواست واریز وجه به حساب بانکی</h4>
                        <form action="/settle" method="POST">
                            @csrf
                            <input type="hidden" name="userId" value="{{ $user->id }}">
                            <div class="form-group">
                                <label for="amount"><strong class="text-danger">*</strong>مبلغ درخواستی (تومان):</label>
                                <input type="number" class="form-control" id="amount"
                                       name="amount" min="50000" required value="{{ old('amount') ?? $user->balance }}">
                            </div>
                            <div class="form-group">
                                <label for="bank-name"><strong class="text-danger">*</strong>نام بانک:</label>
                                <input type="text" class="form-control" value="{{ old('bank-name') }}"
                                       id="bank-name" name="bank-name" required>
                            </div>
                            <div class="form-group">
                                <label for="bank-card"><strong class="text-danger">*</strong>شماره کارت بانکی:<small>(بدون
                                        خط فاصله)</small></label>
                                <input type="number" class="form-control" id="bank-card"
                                       onKeyDown="if(this.value.length===16 && event.keyCode!==8) return false;"
                                       name="bank-card" min="0" required value="{{ old('bank-card') }}">
                            </div>
                            <div class="form-group">
                                <label for="sheba"><strong class="text-danger">*</strong>شماره شبا:</label>
                                <div class="input-group" dir="ltr">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">IR</span>
                                    </div>
                                    <input type="number" class="form-control" id="sheba" name="sheba" required
                                           onKeyDown="if(this.value.length===24 && event.keyCode!==8) return false;"
                                           value="{{ old('sheba') }}">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success"
                                    @if($user->balance < 50000) disabled @endif>درخواست واریز
                            </button>
                        </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف</button>
                    </div>

                </div>
            </div>
        </div>


        @if(count($user->transactions->where('confirmed', 0)) > 0)
            <h4 class="text-center text-info mt-4">درخواست های واریز در حال بررسی</h4>

            <table class="table table-striped table-hover text-center shadow" dir="rtl">
                <thead>
                <tr style="background-image: linear-gradient(135deg,#72234e,#9d5c6b);color: white">
                    <td>ماهیت</td>
                    <td>مبلغ</td>
                    <td>شرح</td>
                    <td>تاریخ</td>
                </tr>
                </thead>

                <tbody class="text-center">
                @foreach($user->transactions->where('confirmed', 0) as $trans)
                    <tr class="{{ $trans->type == 'deposit' ? 'alert-info' : 'text-danger' }}">
                        <td>{{ $trans->type == 'deposit' ? 'واریز به کیف پول' : 'برداشت از کیف پول' }}</td>
                        <td><span dir="ltr">{{ $trans->amount }}</span> <span> تومان </span></td>
                        <td>{{ $trans->meta[0] }}</td>
                        <td dir="ltr">{{ verta($trans->created_at) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif

    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/front_js/sweetalert2.all.min.js') }}"></script>

    <script type="text/javascript">
        function myFunction() {
            /* Get the text field */
            var copyText = document.getElementById("myInput");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            // alert("Copied the text: " + copyText.value);
            Swal.fire({
                title: 'کپی شد',
                text: 'حالا میتونی توی هر شبکه اجتماعی که خواستی به اشتراک بذاری و درآمد بدست بیاری',
                type: 'success',
                confirmButtonText: 'حله'
            });
        }
    </script>
@endsection
