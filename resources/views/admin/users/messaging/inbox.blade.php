@extends($page)

@section('pageTitle')صندوق پیام@endsection
@section('content')
    <div class="breadcrumb text-info col-sm-12 mb-0 p-0">
        <div class="col-12 text-right p-0 pt-2 pb-2 bg-white">
            <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}">داشبورد </a><i class="fas fa-angle-left"></i>
            <a>صندوق پیام </a>
        </div>
    </div> <!--END BREADCRUMB-->
    <div class="container-fluid" dir="rtl">

        @include('includes.show-sessions')
        @include('includes.form-error')

        <h1 class="h3 text-center text-info">صندوق پیام</h1>
        <div class="row">

            <div class="col">
                @if(count($data)>0)
                    <table class="table table-striped table-hover text-center" dir="rtl">
                        <thead>
                        <tr>
                            <th>عنوان</th>
                            <th class="text-center">متن پیام</th>
                            <th></th>
                        </tr>
                        </thead>
                        @foreach($data as $message)
                            <input type="hidden" value="{{$message->id}}" id="mId{{$message->id}}">
                            <tbody class="text-center">
                            <tr>
                                <td id="subject{{$message->id}}"><span class="@if($message->seen == 0) bold @else small font-italic @endif">{{Illuminate\Support\Str::limit($message->subject,30)}}</span></td>
                                <td id="body{{ $message->id }}"><span class=" @if($message->seen == 0) bold @else small font-italic @endif">{{Illuminate\Support\Str::limit($message->message,80)}}</span></td>
                                <td class="text-center">
                                    <button data-target="#myModal{{ $message->id }}" data-toggle="modal"
                                            class="btn btn-secondary btn-sm"
                                            onclick="seen({{$message->id}})" id="msg{{$message->id}}">مشاهده
                                    </button>
                                </td>
                            </tr>
                            <div id="myModal{{ $message->id }}" dir="rtl"
                                 class="modal fade text-right">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <h4 class="text-center mt-2">{{ $message->subject }}</h4>
                                        <div class="modal-body" style="background-color: #efebed">
                                            <p>{{ $message->message }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            </tbody>
                    </table>
            </div>

            @endif
        </div>

        <div class="row mt-2">
            <div class="mx-auto">
                {{$data->render()}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function seen(id) {
            axios.get('/update-inbox?msgId=' + id)
                .then(function (response) {
                    var res = response.data;
                    var msg = res.message;
                    var subj = res.subject;
                    document.getElementById('subject'+id).innerHTML = subj;
                    document.getElementById('body'+id).innerHTML = msg;
                });
        }
    </script>
@endsection
