@extends($page)

@section('content')

    <div class="container-fluid" style="min-height: 830px;background-color: #fff">
        <h3 class="page-title text-dark text-center pt-2">داشبورد</h3>
        <hr>
    @include('includes.show-sessions')

        <!-- ============================================================== -->
        <!-- Sales Cards  -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <a href="{{url('/irenadmin/charts')}}">
                            <h1 class="font-light text-white"><i class="fa fa-chart-area"></i></h1>
                            <h6 class="text-white">نمودارها</h6>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card card-hover">
                    <a href="#">
                    <div class="box bg-warning text-center">
                        <h4 class="font-light text-white"><i class="fas fa-exclamation"></i></h4>
                        <h5 class="m-b-0 m-t-5 text-white">{{ $ordersProducts }}</h5>
                        <h6 class="text-white">کالای ارسال نشده</h6>
                    </div></a>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card card-hover">
                    <a href="{{url('/telescope')}}">
                        <div class="box bg-danger text-center">
                            <h4 class="font-light text-white"><i class="far fa-credit-card"></i></h4>
                            <h5 class="m-b-0 m-t-5 text-white">{{ $sentProducts }}</h5>
                            <h6 class="text-white">فاکتورهای قابل پرداخت</h6>
                        </div></a>
                </div>
            </div>

        </div>

        <div class="col-md-12 mt-5">
            <div class="card card-hover">
                <div class="text-center" style="background-color: #4fa87b">
                    <h5 class="text-white p-3" style="cursor: pointer" id="showUsers">مشاهده تعداد کاربران و کسب و کار ها (کلیک
                        کنید)</h5>
                </div>
            </div>
        </div>
    </div>        <!-- End Container fluid  -->

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/front_js/sweetalert2.all.min.js') }}"></script>
<script>
    $('#showUsers').click(function () {
            axios.get('/irenadmin/')
                .then((resp) => {
                    let users = resp.data.users;
                    let businesses = resp.data.businesses;

                    Swal.fire({
                        title: 'تعدادکاربران و کسب و کارها',
                        text: 'کل کاربران:' + users + ' - کسب و کارها:' + businesses + ' - مشتریان عادی:' + (users-businesses),
                        type: 'info',
                        confirmButtonText: 'باشه'
                    });
                });
        });
</script>
@endsection
