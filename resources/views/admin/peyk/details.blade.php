@extends('layouts.peykLayout.peyk_design')

@section('content')
    <h3 class="text-center col mt-3 py-4" style="background-color: #b1f3e3">جزییات فاکتور</h3>
    <div class="table-responsive" style="min-height: 800px;">
        <h4 class="text-center text-info">مشخصات خریدار</h4>
        <table class="table table-bordered table-hover text-center" dir="rtl">
            <thead>
            <tr class="text-light bg-megna">
                <th>نام خریدار</th>
                <th>یوزرنیم خریدار</th>
                <th>ایمیل خریدار</th>
                <th>شماره تماس خریدار</th>
                <th>آدرس ارسال</th>
                <th>کدپستی</th>
            </tr>
            </thead>
            <tbody>
            <tr class="bg-light">
                <td>{{ $orderCustomerDetails->user->name }}</td>
                <td>{{ $orderCustomerDetails->user->phone }}</td>
                <td>{{ $address->email ?? 'ندارد' }}</td>
                <td>{{ $address->phone ?? 'ندارد' }}</td>
                <td>{{ $address->address ?? 'ندارد' }}</td>
                <td>{{ $address->post_code ?? 'ندارد' }}</td>
            </tr>

            </tbody>
        </table>
        <hr>
        <br>

        <h4 class="text-center text-info">ریز فاکتور</h4>
        <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center" dir="rtl">
            <thead>
            <tr class="text-light bg-dark">
                <th>مشخصات فروشنده</th>
                <th>نام کالا</th>
                <th>تعداد کالا</th>
                <th>قیمت کالا</th>
                <th>قیمت کل</th>
                <th>وضعیت ارسال</th>
                <th>هزینه ارسال</th>
                <th>کد رهگیری مرسوله</th>
                <th>تاریخ ارسال</th>
            </tr>
            </thead>
            <tbody>

            @foreach($orderDetails as $item)
                <tr>
                    <td>
                        {{ $item->business->name }}
                        <br>
                        {{ $item->business->user->phone }}
                    </td>

                    <td>{{ $item->name ?? 'مشخص نشده' }}</td>
                    <td>{{ $item->quantity ?? 'مشخص نشده' }}</td>
                    <td>{{ $item->price ?? 'مشخص نشده' }}</td>
                    <td>{{ number_format($item->price * $item->quantity) ?? 'مشخص نشده' }}</td>
                    <td class=" @if($item->status == 'کنسل شد') text-danger @elseif($item->status == 'تسویه شد') text-success @endif">{{ $item->status }}</td>
                    <td>{{ $item->shipping_cost }}</td>
                    <td>{{ $item->delivery_code ?? 'ارسال نشده'}}</td>
                    <td>{{ $item->sent_date ? verta($item->sent_date)->formatDate() : 'ارسال نشده'}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@stop
