@extends('layouts.adminLayout.admin_design')

@section('content')
    <div dir="rtl" class="container text-right">

        <form method="POST" action='{{ url("irenadmin/categories/{$category->slug}") }}'>

            @method('PATCH')
            @include ('admin.categories._form', ['buttonText' => 'ویرایش دسته بندی'])
        </form>
    </div>
@endsection
