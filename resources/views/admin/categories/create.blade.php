@extends('layouts.adminLayout.admin_design')

@section('content')
    <div dir="rtl" class="container text-right">
        <form method="POST" action="{{ url('irenadmin/categories') }}">
            @include ('admin.categories._form')
        </form>
    </div>
@endsection
