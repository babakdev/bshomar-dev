@include('includes.show-sessions')

@csrf
<div class="row">
    <div class="form-group col-md-6">
        <label for="name" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">نام</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $category->name) }}"
               required>
    </div>

    <div class="form-group col-md-6">
        <label for="slug" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">نامک</label>
        <input type="text" class="form-control" id="slug" name="slug" value="{{ old('slug', $category->slug) }}"
               required>
    </div>
</div>

{{--<div class="row">--}}

{{--    <div class="form-group col-md-6">--}}
{{--        <label for="parent_id" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">شاخه</label>--}}
{{--        <select type="text" class="form-control" id="parent_id" name="parent_id">--}}
{{--            @foreach($categories->where('parent_id', 0) as $category)--}}
{{--                <option value="{{$category->id}}">{{ $category->name }}</option>--}}
{{--            @endforeach--}}
{{--        </select>--}}
{{--    </div>--}}

{{--    <div class="form-group col-md-6">--}}
{{--        <label for="is_active" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">وضعیت</label>--}}

{{--        <select name="is_active" id="is_active" class="form-control">--}}
{{--            <option value="1" {{ old('is_active', $category->is_active) ? 'selected' : '' }}>فعال</option>--}}
{{--            <option value="0" {{ old('is_active', $category->is_active) ? '' : 'selected' }}>غیرفعال</option>--}}
{{--        </select>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="row border p-3 mb-3">
    <div class="form-group col-md-6">
        <label for="meta_description" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">متن سئو</label>
        <input type="text" class="form-control" id="meta_description" name="meta_description" value="{{ old('meta_description', $category->meta_description) }}"
               required>
    </div>

    <div class="form-group col-md-6">
        <label for="meta_keywords" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">کلمات کلیدی</label>
        <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" value="{{ old('meta_keywords', $category->meta_keywords) }}"
               required placeholder="مثل: رستوران، تخفیف، گیلاس (با ویرگول از هم جدا شوند)">
    </div>
</div>

<div class="mb-4">
    <button type="submit" class="btn btn-success font-20">{{ $buttonText ?? 'ثبت دسته بندی' }}</button>
</div>
<a href="{{ url('irenadmin/categories') }}" class="btn btn-link">صفحه قبل</a>

@include('includes.form-error')
