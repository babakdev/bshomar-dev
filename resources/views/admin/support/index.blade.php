@extends('layouts.supportLayout.support_design')

@section('content')
    <div class="container-fluid bg-secondary" style="min-height: 830px;">
        <h3 class="page-title text-light text-center pt-2">داشبورد پشتیبانی</h3>
        <hr>
        @include('includes.show-sessions')
        <article style="font-size: 16px; color: white; text-align: right" dir="rtl">
        <b>پشتیبان گرامی</b>
            <br>
        <p style="white-space: pre-line">
            دقت نمایید که تمامی تاییدها و تغییراتی که در این پنل انجام میدهید، رهگیری می شود و قابل استناد می باشد. پس با دقت و تامل کامل تاییدها را انجام دهید و مطمئن شوید که در اطلاعات متنی و تصویری کسب و کارها و کالاها، ‌مغایرتی با قوانین جمهوری اسلامی ایران، را تایید نمی کنید.

            با تشکر</p>
        </article>
    </div>

@endsection
