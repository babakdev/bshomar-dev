@extends($page)

@section('content')
    <div dir="rtl">
        @include('includes.show-sessions')
        @include('includes.form-error')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-center">تمام کالاهای سایت</h3>
            </div>
            @if(count($products)>0)
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-center table-striped">
                        <thead>
                        <tr>
                            <th>عکس</th>
                            <th>نام</th>
                            <th>فروشنده کالا</th>
                            <th class="text-right">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($products as $product)

                            <tr>
                                <td>
                                    @if(! $product->images->isEmpty())
                                        <img height="60" alt="businessPhoto" class="lazyload blur-up rounded"
                                             src="{{ asset('images/front_images/products/small/'.$product->images[0]->path) }}">
                                    @else
                                        <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
                                    @endif
                                </td>
                                <td><a href="{{ url($product->path()) }}" target="_blank">{{$product->name}}</a>
                                </td>
                                <td><a href="{{ 'https://bshomar.com/' . $product->business->id }}">{{ $product->business->name }}</a></td>
                                <td class="row mx-auto">

                                    <a href="{{ url('/product/details/'.$product->id) }}"
                                       class="btn btn-link text-success" title="مرحله بعد"><i
                                            class="fa fa-arrow-circle-left"></i> مرحله تایید
                                    </a>
                                </td>
                            </tr>
                            <div id="myModal{{ $product->id }}"
                                 class="modal fade text-right">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-header" dir="ltr">
                                            <h3>
                                                <a href="{{url ($product->path() ) }}" target="_blank"
                                                   title="رفتن به صفحه کالا">{{ $product->name}}</a>
                                            </h3>&nbsp;&nbsp;
                                            <button data-dismiss="modal" class="close" type="button">×</button>
                                        </div>

                                        <div class="modal-body" style="direction: rtl">
                                            @if(! $product->images->isEmpty())
                                                @foreach($product->images as $image)
                                                    <img height="182" alt="businessPhoto"
                                                         class="lazyload blur-up rounded"
                                                         src="{{ asset('images/front_images/products/small/'.$image->path) }}">
                                                @endforeach
                                                <hr>
                                            @else
                                                <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
                                            @endif
                                            <p><strong>متعلق به کسب و
                                                    کار:</strong> {{ $product->business->name ?? 'نامشخص' }}</p>
                                            <p><strong>دسته بندی:</strong> {{ $product->category->name ?? 'نامشخص' }}
                                            </p>
                                            <p>
                                                <strong>وضعیت:</strong> {{ $product->is_active ==1 ? 'فعال' : 'غیرفعال'  }}
                                            </p>
                                            <p><strong>قیمت اصلی:</strong> {{ $product->price ?? 'نامشخص' }}</p>
                                            <p><strong>موجودی انبار:</strong> {{ $product->stock ?? 'نامشخص' }}</p>
                                            <p><strong>واحد اندازه گیری:</strong> {{ $product->unit ?? 'نامشخص' }}</p>
                                            <p><strong>توضیحات:</strong> {{ $product->description ?? 'نامشخص' }}</p>
                                            <p><strong>کد کالا:</strong> {{ $product->code ?? 'نامشخص' }}</p>
                                            <p><strong>کشور سازنده:</strong> {{ $product->country ?? 'نامشخص' }}</p>
                                            <p><strong>کلمات کلیدی:</strong> {{ $product->meta_keywords ?? 'نامشخص' }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @else
                            <div class="col-md-12">
                                <h3 class="text-info mt-5 text-center" style="direction: rtl">کالایی موجود نمی باشد</h3>
                            </div>
                        @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="mx-auto">
                            {{$products->render()}}
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.deleteItem').click(function () {
                return confirm('آیا از حذف آیتم مورد نظر اطمینان دارید؟ با این کار ممکن است رضایت کسب و کار کاهش یابد');
            });
        });
    </script>
@endsection
