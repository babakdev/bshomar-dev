@extends($page)
@section('content')

    <div dir="rtl" class="container-fluid mt-3 text-right">
        <form method="POST" action="{{ url('/products') }}" id="productForm" enctype="multipart/form-data">
            @include ('admin.products._form')
        </form>
    </div>

@endsection