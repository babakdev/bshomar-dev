@extends($page)
@section('content')

    <div dir="rtl" class="container-fluid mt-3 text-right">
        <form method="POST" action="{{ url("/products/{$product->id}") }}" enctype="multipart/form-data">
            @method('PATCH')
            <h4>{{ $product->name }}</h4>

            @include ('admin.products._form', ['buttonText' => 'ویرایش کالا'])
        </form>
    </div>

@endsection