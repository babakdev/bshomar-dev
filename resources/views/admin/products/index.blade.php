@extends($page)

@section('content')
    <div dir="rtl">
        @include('includes.show-sessions')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title text-center">تمام کالاهای سایت</h3>
            </div>
            @if(count($products)>0)
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-center table-striped">
                        <thead>
                        <tr>
                            <th>عکس</th>
                            <th>نام</th>
                            <th>فروشنده کالا</th>
                            <th class="text-right">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($products as $product)

                            <tr>
                                <td>
                                    @if(! $product->images->isEmpty())
                                        <img height="60" alt="businessPhoto"
                                             class="lazyload blur-up rounded"
                                             src="{{ asset('images/front_images/products/small/'.$product->images[0]->path) }}">
                                    @else
                                        <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
                                    @endif
                                </td>
                                <td><a href="{{ $product->path() }}" target="_blank">{{$product->name}}</a>
                                </td>
                                <td><a href="{{ 'https://bshomar.com/' . $product->business->id }}">{{ $product->business->name }}</a>
                                </td>
                                <td class="row mx-auto">
                                    <button type="button" class="btn btn-link pt-0" data-toggle="modal"
                                            data-target="#myModal{{ $product->id }}">
                                        <i class="fa fa-eye text-success"></i>
                                    </button>

                                    <form action="{{url('/products/'. $product->id . '/edit')}}" method="GET">
                                        <input type="hidden" name="businessId" value="{{ $product->business_id }}">
                                        <button type="submit" class="btn btn-link pt-0" title="ویرایش">
                                            <i class="fa fa-edit text-orange"></i>
                                        </button>
                                    </form>
                                    @if(auth()->user()->isAdmin || auth()->user()->isSupport)
                                        <form action="{{url('/products/'. $product->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-link pt-0 deleteItem">
                                                <i class="fa fa-trash text-danger" title="حذف"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            <div id="myModal{{ $product->id }}"
                                 class="modal fade text-right">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">

                                        <div class="modal-header" dir="ltr">
                                            <h3>
                                                <a href="{{ $product->path() }}" target="_blank"
                                                   title="رفتن به صفحه کالا">{{ $product->name}}</a>
                                            </h3>&nbsp;&nbsp;
                                            <button data-dismiss="modal" class="close" type="button">×</button>
                                        </div>

                                        <div class="modal-body" style="direction: rtl">
                                            @if(! $product->images->isEmpty())
                                                <div class="text-center">
                                                    @foreach($product->images as $image)
                                                        <img height="192" alt="businessPhoto"
                                                             class="lazyload blur-up rounded"
                                                             src="{{ asset('images/front_images/products/large/'.$image->path) }}">
                                                    @endforeach
                                                </div>
                                                <hr>
                                            @else
                                                <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
                                            @endif
                                            <p><strong>متعلق به کسب و
                                                    کار:</strong> {{ $product->business->name ?? 'نامشخص' }}</p>
                                            <p><strong>دسته بندی:</strong> {{ $product->category->name ?? 'نامشخص' }}
                                            </p>
                                            <p>
                                                <strong>وضعیت:</strong> {{ $product->is_active ==1 ? 'فعال' : 'غیرفعال'  }}
                                            </p>
                                            <p><strong>قیمت اصلی:</strong> {{ $product->price ?? 'نامشخص' }}</p>
                                            <p><strong>موجودی انبار:</strong> {{ $product->stock ?? 'نامشخص' }}</p>
                                            <p><strong>واحد اندازه گیری:</strong> {{ $product->unit ?? 'نامشخص' }}</p>
                                            <p><strong>توضیحات:</strong> {{ $product->description ?? 'نامشخص' }}</p>
                                            <p><strong>کد کالا:</strong> {{ $product->code ?? 'نامشخص' }}</p>
                                            <p><strong>کشور سازنده:</strong> {{ $product->country ?? 'نامشخص' }}</p>
                                            <p><strong>کلمات کلیدی:</strong> {{ $product->meta_keywords ?? 'نامشخص' }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @else
                            <div class="col-md-8">
                                <h3 class="text-info mt-5 text-right" style="direction: rtl">کالایی موجود نمی باشد
                                    است</h3>
                            </div>
                        @endif
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="mx-auto">
                            {{$products->render()}}
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.deleteItem').click(function () {
                return confirm('آیا از حذف آیتم مورد نظر اطمینان دارید؟ با این کار ممکن است رضایت کسب و کار کاهش یابد');
            });
        });
    </script>
@endsection
