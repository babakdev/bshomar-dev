<input type="hidden" name="business_id" value="{{ $business->id }}">
<input type="hidden" name="category_id" value="{{ $business->category_id }}">

<div class="row p-4 mt-0 mb-3" style="background-color: #e5e4e5">
    <a href="{{ 'https://bshomar.com/' . $business->id}}" target="_blank" class="col-md-6"><h2>{{ $business->name }}</h2></a>
    <div class="col-md-6 text-left">
        <a href="
        {{ url('/business-information/' . $business->id) }}"
           class="btn btn-secondary">صفحه
            قبل</a>
    </div>
</div>
<div class="clearfix"></div>
@csrf
@include('includes.show-sessions')
<div class="row">
    <div class="form-group col-md-6">
        <label for="name">نام کالا
            <i class="text-danger font-20"> *</i>
        </label>
        <input type="text" class="form-control @error('name') is-danger @enderror"
               id="name" name="name" value="{{ old('name', $product->name) }}"
               required autofocus maxlength=40>
        @error('name')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="unit">واحد اندازه گیری و فروش کالا
            <small>عدد٫گرم٫کیلوگرم٫تن٫متر٫غیره</small>
            <i class="text-danger font-20"> *</i>
        </label>
        <input type="text" class="form-control @error('unit') is-danger @enderror" maxlength=20
               id="unit" name="unit" value="{{ old('unit', $product->unit) }}"
               required autofocus>
        @error('unit')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>


<div class="row py-2 mb-2" style="background-color: #e5e4e5">
    <div class="form-group col-md-6">
        <label for="price">قیمت (تومان)
            <small>اگر قیمت کالا روزانه مشخص می شود و یا خدمات ارائه می دهید ،
                <span class="text-danger">قیمت را صفر قرار دهید</span>
            </small>
        </label>
        <input type="number" class="form-control @error('price') is-danger @enderror "
               id="price" name="price" value="{{ old('price', $product->price) }}"
               required autofocus>
        @error('price')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="code">کد کالا
            <i class="text-white font-20"> &nbsp;</i>
        </label>
        <input type="text" class="form-control @error('code') is-danger @enderror" maxlength=30
               id="code" name="code" value="{{ old('code', $product->code) }}"
               autofocus>
        @error('code')
        <span class="text-danger" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        <label for="stock">موجودی انبار
            <i class="text-danger font-20"> *</i>
            <small>اگر خدمت ارائه می دهید، موجودی را عددی بالا مثل ۹۹۹۹۹۹ قرار دهید</small>
        </label>
        <input type="number" class="form-control @error('stock') is-danger @enderror"
               id="stock" name="stock" value="{{ old('stock', $product->stock) }}"
               required autofocus>
        @error('stock')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="min_stock">حداقل موجودی انبار ( با رسیدن موجودی انبار به این عدد به شما اطلاع داده میشود)
            <i class="text-danger font-20"> *</i>
        </label>
        <input type="text" class="form-control @error('min_stock') is-danger @enderror" maxlength=30
               id="min_stock" name="min_stock" value="{{ old('min_stock', $product->min_stock) }}"
               autofocus required>
        @error('min_stock')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="row py-2 mb-2">
    <div class="form-group col-md-6">
        <label for="time_to_delivery"> مدت زمان آماده سازی تا ارسال
            <i class="text-danger font-20"> *</i>
        </label>
        <input type="text" class="form-control @error('time_to_delivery') is-danger @enderror" maxlength=20
               id="time_to_delivery" name="time_to_delivery"
               value="{{ old('time_to_delivery', $product->time_to_delivery) }}"
               required autofocus>
        @error('time_to_delivery')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="delivery_type"> واحد
            <i class="text-danger font-20"> *</i>
        </label>
        <select name="delivery_type" class="form-control @error('delivery_type') is-danger @enderror">
            <option value="1" {{ old('delivery_type', $product->delivery_type) == 1 ? 'selected' : ''}}>روز</option>
            <option value="2" {{ old('delivery_type', $product->delivery_type) == 2 ? 'selected' : ''}}>ساعت
            </option>
        </select>
        @error('delivery_type')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="row py-2 mb-2" style="background-color: #e5e4e5">
    <div class="form-group col-md-6">
        <label for="product_return">از لحظه فروش کالا تا چه زمانی می توان کالا را مرجوع کرد
            <i class="text-danger font-20"> *</i>
        </label>
        <input type="text" class="form-control @error('product_return') is-danger @enderror" maxlength=20
               id="product_return" name="product_return"
               value="{{ old('product_return', $product->product_return) }}"
               required autofocus>
        @error('product_return')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="product_return_type"> واحد
            <i class="text-danger font-20"> *</i>
        </label>
        <select id="product_return_type" name="product_return_type"
                class="form-control @error('product_return_type') is-danger @enderror">
            <option value="1" {{ old('product_return_type', $product->product_return_type) == 1 ? 'selected' : ''}}>
                روز
            </option>
            <option value="2" {{ old('product_return_type', $product->product_return_type) == 2 ? 'selected' : ''}}>
                ساعت
            </option>
        </select>
        @error('product_return_type')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="row py-2 mb-2">
    <div class="form-group col-md-6">
        <label for="country">کشور سازنده یا تولید کننده
            <i class="text-danger font-20"> *</i>
        </label>
        <input type="text" class="form-control @error('country') is-danger @enderror" maxlength=20
               id="country" name="country" value="{{ old('country', $product->country) }}"
               required autofocus>
        @error('country')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group col-md-6">
        <label for="meta_keywords">کلمات کلیدی
            <i class="text-white font-20"> &nbsp;</i>
        </label>
        <input type="text" class="form-control @error('meta_keywords') is-danger @enderror" id="meta_keywords"
               name="meta_keywords"
               value="{{ old('meta_keywords', $product->meta_keywords) }}" maxlength=80
               placeholder="مثل: رستوران، تخفیف، گیلاس (با ویرگول از هم جدا شوند)">
        @error('meta_keywords')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
<div class="row py-2 mb-2" style="background-color: #e5e4e5">
    {{--<div class="form-group col-md-6">--}}
    {{--<label for="discount_cycle">مدت زمان اعتبار تخفیف (به روز)--}}
    {{--<i class="text-white font-20"> &nbsp;</i>--}}
    {{--</label>--}}
    {{--<input type="text" class="form-control @error('discount_cycle') is-danger @enderror" maxlength=20--}}
    {{--id="discount_cycle" name="discount_cycle" value="{{ old('discount_cycle', $product->discount_cycle) }}"--}}
    {{-->--}}
    {{--@error('discount_cycle')--}}
    {{--<span class="text-danger" role="alert">--}}
    {{--<strong>{{ $message }}</strong>--}}
    {{--</span>--}}
    {{--@enderror--}}
    {{--</div>--}}
</div>

<div class="row py-2 mb-2">
    <div class="form-group col-md-12">
        <label for="description">توضیح مختصر در مورد کالا / خدمت</label>
        <textarea type="text" rows="3" class="form-control @error('description') is-danger @enderror" maxlength=255
                  id="description" name="description" autofocus>{{ old('description', $product->description) }}
    </textarea>
        @error('description')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="row py-2 mb-2">
    <div class="form-group col-md-12 @error('image') border border-danger bg-warning @enderror">
        <label for="image">تصویر کالا
            <span style="font-size: 12px"> (حداکثر 4 و حداقل 1 تصویر)</span>
            <small>jpg png gif</small>
            <br><small>برای انتخاب چند تصویر بصورت همزمان کلید ctrl را پایین نگه دارید و روی تصویر کلیک
                کنید.</small>
        </label>
        <input type="file" name="image[]" multiple="multiple" class="dropify"
               data-show-loader="true" autofocus
               data-max-file-size="15000K" data-allowed-file-extensions="jpg png jpeg gif" accept="image/*"/>
        @error('image')
        <span class="text-danger text-center" role="alert">
                <strong style="font-size: 22px; text-shadow: 1px 1px 2px #222223;">{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

@if($product)
    <div class="form-group col-md-12">
        <div class="row" id="image-group">
            @foreach($product->images as $image)
                <div class="form-group col-3 mx-auto text-center" id="imageFull{{ $image->id }}">
                    <img src="{{ asset('images/front_images/products/large/'.$image->path) }}" alt="productImage"
                         class="img-fluid rounded"
                         style="width: 240px">
                    <a id="image{{ $image->id }}" class="removePicBtn btn btn-link" data-imageid="{{$image->id}}"
                       data-productid="{{$product->id}}" title="حذف تصویر">
                        <i class="fa fa-times-circle text-danger fa-2x mt-2"></i>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endif

<div class="properties_group row py-2 mb-2" style="background-color: #e5e4e5">
    <div class="form-group col-md-12">
        <b>مشخصات فنی محصول </b>
        <div id="properties" class="row py-2 mb-2">
            @if(old('property1'))
                @for($i=1; $i<=25; $i++)
                    @if(old('property'.$i))
                        <div class="main_properties row col-md-12">
                            <div class="form-group col-md-6">
                                <label for="property{{$i}}">مشخصه فنی
                                </label>
                                <select type="text" class="form-control" id="property{{$i}}" name="property{{$i}}">
                                    <option>انتخاب کنید</option>
                                    @foreach($properties as $property)
                                        <option
                                            value="{{$property->id}}" {{$property->id == old('property'.$i) ? 'selected' : ''}}>{{$property->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="amount1">مقدار
                                </label>
                                <input type="text" class="form-control" id="amount1" name="amount1"
                                       value="{{ old('amount1') }}">
                            </div>
                        </div>
                    @endif
                @endfor
            @elseif(sizeof($product->properties()->get()) > 0)
                @php $p=0; @endphp
                @foreach($product->properties()->get() as $pro)
                    @php  ++$p; @endphp
                    <div class="main_properties row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="property{{$p}}">مشخصه فنی
                            </label>
                            <select type="text" class="form-control" id="property{{$p}}" name="property{{$p}}">
                                <option value=" ">انتخاب کنید</option>
                                @foreach($properties as $property)
                                    <option
                                        value="{{$property->id}}" {{$property->id == $pro->property_id ? 'selected' : ''}}>{{$property->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="amount{{$p}}">مقدار
                            </label>
                            <input type="text" class="form-control" id="amount{{$p}}" name="amount{{$p}}"
                                   value="{{ old('amount'.$p,$pro->value) }}">
                        </div>
                    </div>
                @endforeach
            @else
                <div class="main_properties row col-md-12">
                    <div class="form-group col-md-6">
                        <label for="property1">مشخصه فنی
                        </label>
                        <select type="text" class="form-control" id="property1" name="property1">
                            <option value=" ">انتخاب کنید</option>
                            @foreach($properties as $property)
                                <option
                                    value="{{$property->id}}" {{$property->id == old('property1') ? 'selected' : ''}}>{{$property->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="amount1">مقدار
                        </label>
                        <input type="text" class="form-control" id="amount1" name="amount1"
                               value="{{ old('amount1') }}">
                    </div>
                </div>
            @endif
        </div>
        <div class="form-group col-sm-12">
            <div>
                    <span class="add-property" style="cursor: pointer; color: #23527c"><i
                            class="fa fa-plus-circle"></i><strong> افزودن مشخصات بیشتر </strong></span>
                <span class="remove-property" style="cursor: pointer; color: #c9302c; margin-right: 10px;"><i
                        class="fa fa-minus-circle"></i><strong> حذف </strong></span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="benefits_group row py-2 mb-2">
    <div class="form-group col-md-12">
        <b>مزایای محصول </b>
        <div id="benefits" class="row py-2 mb-2">
            @if(old('b_property1'))
                @for($i=1; $i<=25; $i++)
                    @if(old('b_property'.$i))
                        <div class="main_benefits row col-md-12">
                            <div class="form-group col-md-6">
                                <label for="b_property{{$i}}">مقدار
                                </label>
                                <input type="text" class="form-control" id="b_property{{$i}}"
                                       name="b_property{{$i}}"
                                       value="{{ old('b_property'.$i) }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="b_amount{{$i}}">مقدار
                                </label>
                                <input type="text" class="form-control" id="b_amount{{$i}}" name="b_amount{{$i}}"
                                       value="{{ old('b_amount'.$i) }}">
                            </div>
                        </div>
                    @endif
                @endfor
            @elseif(sizeof($product->benefitDefects()->where('type',1)->get()) > 0)
                @php $b=0; @endphp
                @foreach($product->benefitDefects()->where('type',1)->get() as $benefit)
                    @php ++$b; @endphp
                    <div class="main_benefits row col-md-12">
                        <div class="form-group col-md-6">
                            <label for="b_property{{$b}}">ویژگی
                            </label>
                            <input type="text" class="form-control" id="b_property{{$b}}" name="b_property{{$b}}"
                                   value="{{ old('b_property' .$b,$benefit->property) }}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="b_amount{{$b}}">مقدار
                            </label>
                            <input type="text" class="form-control" id="b_amount{{$b}}" name="b_amount{{$b}}"
                                   value="{{ old('b_amount' .$b,$benefit->value) }}">
                        </div>
                    </div>
                @endforeach
            @else
                <div class="main_benefits row col-md-12">
                    <div class="form-group col-md-6">
                        <label for="b_property1">ویژگی
                        </label>
                        <input type="text" class="form-control" id="b_property1" name="b_property1"
                               value="{{ old('b_property1') }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="b_amount1">مقدار
                        </label>
                        <input type="text" class="form-control" id="b_amount1" name="b_amount1"
                               value="{{ old('b_amount1') }}">
                    </div>
                </div>
            @endif
        </div>
        <div class="form-group col-sm-12">
            <div>
                    <span class="add-b-property" style="cursor: pointer; color: #23527c"><i
                            class="fa fa-plus-circle"></i><strong> افزودن مزایای بیشتر </strong></span>
                <span class="remove-b-property" style="cursor: pointer; color: #c9302c; margin-right: 10px;"><i
                        class="fa fa-minus-circle"></i><strong> حذف </strong></span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="row py-2 mb-2" style="background-color: #e5e4e5">
    <div class="form-group col-md-12">
        <label for="consider">نقد و بررسی </label>
        <textarea type="text" rows="15" class="form-control my-editor @error('consider') is-danger @enderror"
                  maxlength=255
                  id="consider" name="consider" autofocus>{{ old('consider', $product->consider) }}
    </textarea>
        @error('consider')
        <span class="text-danger" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

<div class="mb-4">
    <button type="submit" class="btn btn-success btn-block font-20 mb-3"
            autofocus>{{ $buttonText ?? 'ثبت کالا' }}</button>
</div>
</div>

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/front_js/sweetalert2.all.min.js') }}"></script>
    <script
        src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bwwivsywiq8s8c628683jhooygk3jlc9kd22wq4093syaqab"></script>

    <script>
        var editor_config = {
            path_absolute: "/",
            selector: "textarea.my-editor",
            language_url: '../../../js/backend/fa.js',
            directionality: 'rtl',
            plugins: [
                "advlist autolink lists link charmap print preview hr",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime save contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar: " undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link ",
            relative_urls: false,
            file_browser_callback: function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        };
        tinymce.init(editor_config);
    </script>

    <script>
        $('.removePicBtn').on('click', function () {
            var image = $(this).data('imageid');
            var productId = $(this).data('productid');
            axios.get('/product-image/' + image + '/' + productId)
                .then(function (response) {
                    Swal.fire({
                        title: 'حذف تصویر',
                        text: 'تصویر با موفقیت حذف شد',
                        type: 'success',
                        confirmButtonText: 'OK'
                    });
                    $("#imageFull").css({"display": "none"});
                    $("#image-group").empty().append(response.data.data);
                });
        });
    </script>

    <script>

        $('.add-property').on('click', function () {
            var num = $('.main_properties').length + 1;
            if (num < 25) {
                $('#properties').append(
                    '<div class="main_properties row col-md-12">\n' +
                    '                <div class="form-group col-md-6">\n' +
                    '                    <label for="property' + num + '">مشخصه فنی\n' +
                    '                    </label>\n' +
                    '                    <select type="text" class="form-control" id="property' + num + '" name="property' + num + '">\n' +
                    '                        <option>انتخاب کنید</option>\n' +
                    '                        @foreach($properties as $property)\n' +
                    '                            <option value="{{$property->id}}">{{$property->name}}</option>\n' +
                    '                        @endforeach\n' +
                    '                    </select>\n' +
                    '                </div>\n' +
                    '                <div class="form-group col-md-6">\n' +
                    '                    <label for="amount' + num + '">مقدار\n' +
                    '                    </label>\n' +
                    '                    <input type="text" class="form-control" id="amount' + num + '" name="amount' + num + '">\n' +
                    '                </div>\n' +
                    '            </div>'
                );
            } else {
                alert('بیش از 25 مشخصه مجاز نمی باشد.');
            }
        });
        $('.remove-property').on('click', function () {
            var num = $('.main_properties').length;
            if (num > 1) {
                $('.main_properties').last().remove();
            }
        });

        $('.add-b-property').on('click', function () {
            var num = $('.main_benefits').length + 1;
            if (num < 25) {
                $('#benefits').append('<div class="main_benefits row col-md-12">\n' +
                    '                <div class="form-group col-md-6">\n' +
                    '                    <label for="b_property' + num + '">ویژگی\n' +
                    '                    </label>\n' +
                    '                    <input type="text" class="form-control" id="b_property' + num + '" name="b_property' + num + '" >\n' +
                    '                </div>\n' +
                    '                <div class="form-group col-md-6">\n' +
                    '                    <label for="b_amount' + num + '">مقدار\n' +
                    '                    </label>\n' +
                    '                    <input type="text" class="form-control" id="b_amount' + num + '" name="b_amount' + num + '" >\n' +
                    '                </div>\n' +
                    '            </div>');
            } else {
                alert('بیش از 25 مزیت مجاز نمی باشد.');
            }
        });
        $('.remove-b-property').on('click', function () {
            var num = $('.main_benefits').length;
            if (num > 1) {
                $('.main_benefits').last().remove();
            }
        });

    </script>
@endsection
