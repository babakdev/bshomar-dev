@extends($page)

@section('content')
    @include('includes.show-sessions')
    @include('includes.form-error')
    <div class="container-fluid card text-center" style="min-height: 600px;" dir="rtl">
        <div class="card-header">
            <h3 class="card-title text-center">{{$product->name}}</h3>
        </div>
        <div class="card-body p-0">
            <b>فروشنده کالا:</b><span>{{ $product->business->name }}</span>
            <hr>

            @if(! $product->images->isEmpty())
                @foreach($product->images as $image)
                    <img height="182" alt="businessPhoto"
                         class="lazyload blur-up rounded"
                         src="{{ asset('images/front_images/products/small/'.$image->path) }}">
                @endforeach
            @else
                <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
            @endif
            <hr>

            <p><strong>دسته بندی:</strong> {{ $product->category->name ?? 'نامشخص' }}
            </p>
            <hr>

            <p><strong>قیمت:</strong> {{ number_format($product->price) ?? 'نامشخص' }} تومان </p>
            <hr>

            <p><strong>موجودی انبار:</strong> {{ $product->stock ?? 'نامشخص' }}</p>
            <hr>

            <p><strong>واحد اندازه گیری:</strong> {{ $product->unit ?? 'نامشخص' }}</p>
            <hr>

            <p><strong>توضیحات:</strong> {{ $product->description ?? 'نامشخص' }}</p>
            <hr>

            <p><strong>کد کالا:</strong> {{ $product->code ?? 'نامشخص' }}</p>
            <hr>

            <p><strong>کشور سازنده:</strong> {{ $product->country ?? 'نامشخص' }}</p>
            <hr>

            <p><strong>کلمات کلیدی:</strong> {{ $product->meta_keywords ?? 'نامشخص' }}</p>
            <hr>
            <p><strong>نقد و بررسی:</strong> {!! $product->consider ?? 'نامشخص' !!}</p>
            <hr>

        </div>

        <div class="row mx-auto mb-3">
            <span>
                @if(!auth()->user()->isAsnaf)
                    <a href="{{ url('/send-message', $product->business->user_id )}}"
                       class="btn btn-info">
                            <i class="fa fa-envelope"></i> ارسال پیام
                        </a>
                @endif
                </span>
            <form action="{{url('/products/'. $product->id . '/edit')}}"
                  method="GET">
                <input type="hidden" name="businessId"
                       value="{{ $product->business_id }}">
                <button type="submit" class="btn btn-orange" title="ویرایش">
                    <i class="fa fa-edit"></i> ویرایش
                </button>
            </form>

            @if(!auth()->user()->isAsnaf)
                <form action="{{url('/products/'. $product->id)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger deleteItem">
                        <i class="fa fa-trash" title="حذف"></i> حذف این محصول
                    </button>
                </form>
            @endif
        </div>
        <h6 dir="rtl" class="card border py-3" style="background-color: #d8d8d8">
            <b>تمامی اطلاعات محصول را مشاهده نمودم و با مسئولیت کاربری خودم تایید میکنم</b>
            <a href="{{ url('/product-is-active/'.$product->id) }}"
               class="btn btn-success mt-3" title="تایید"> تایید <i
                    class="fa fa-check-circle"></i>
            </a>
        </h6>
    </div>

@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $('.deleteItem').click(function () {
                return confirm('آیا از حذف آیتم مورد نظر اطمینان دارید؟ با این کار ممکن است رضایت کسب و کار کاهش یابد');
            });
        });
    </script>
@endsection
