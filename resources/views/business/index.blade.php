@extends('layouts.businessLayout.business_design')
@php($userId = auth()->user()->id)
@php($business = auth()->user()->business)

@section('content')
    @include('includes.show-sessions')
    @include('includes.warning-sessions')

    <div class="container-fluid">
        <!-- Sales chart -->
        <div class="row">
            <h4 class="float-left row font-18 pt-5 col-12" style="width: 400px" dir="rtl"> پنل فعال شما:&nbsp;
                <p> {{$business->panel->name}} </p>
            @if($business->panel_id != 1)
                    &nbsp;&nbsp;
                    <p class="badge badge-pill badge-warning font-18 shadow" style="margin-top: -5px;"> {{\Carbon\Carbon::parse($business->expires_at)->diffInDays()}}</p>
                    &nbsp;
                    <p class="font-16"> روز باقیمانده تا تمدید </p>
                @endif
            </h4>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="d-md-flex align-items-center text-right float-right">
                                <div>
                                    <h4 class="card-title">مدیر محترم کسب و کار خوش آمدید</h4>
                                    <div dir="rtl">
                                        <p style="text-align: justify;">در درجه اول ما مدیران بی شمار از اینکه شما را در کنار خود می بینیم بسیار شادمان هستیم و امیدواریم بتوانیم بهترین خدمات را با حداکثر سرعت در اختیار شما عزیزان قرار دهیم.</p>
                                        <p style="text-align: justify;">در بی شمار شما در اولین مراحل ورود به عنوان یک کاربر عادی امکان ثبت نام داشته و پس از دریافت کد فعال سازی پیامکی و ثبت آن در مکان مشخص شده میتوانید از امکانات بیشتر و بی شماری از محصول بهره ببرید.</p>
                                        <p style="text-align: justify;">اگر در یک شغل خاص در حال فعالیت هستید و این تقاضا را دارید که به یک بیزینس تبدیل شوید امکانات آن برای شما تعبیه شده است با دقت تمام به دکمه و توضیحاتی که تیم فنی در قسمت های مختلف سایت در اختیار شما عزیزان قرارداده اند توجه نمایید و دقت داشته باشید پس از انتخاب دکمه تغییر وضعیت به صاحب کسب و کار (درهمان مرحله اولیه پس از ورود) به این نقطه ای می رسید که در حال حاضر حضور دارید و حالا شما یکی از شرکای ما محسوب می شوید و میتوانید به راحتی از دکمه هایی که در سمت راست خود مشاهده میکنید بهره کافی را برده و کسب و کار خود را به راحتی همراه با کالا یا خدمات متنوعی که ارائه می دهید بر بستری ابری برای همگان به معرض نمایش بگذارید.</p>
                                        <p style="text-align: justify;">پیشنهاد ما این است که شما با دقت تمام کالا یا خدماتی که ارائه میدهید را در قسمت مربوطه به کالا ها قرار دهید تا در صورت نیاز مشتریان از شما خرید هایی مورد نظرشان را داشته باشند.</p>
                                        <p style="text-align: justify;"><strong> لازم میدانیم نکات ریز اما مهمی را تیتروار در ذیل این متن خدمت شما عزیزان ارائه نماییم و خواهشمندیم این نکات را با دقت فراوان مطالعه نمایید :</strong></p>
                                        <p style="text-align: justify;">&bull; تمامی اطلاعات لازم در مورد کالا ها و خدمات خود را با دقت فراوان تکمیل نمایید</p>
                                        <p style="text-align: justify;">&bull; در صورتی که کالا یا خدمتی را فقط به صورت حضوری ارائه می دهید قسمت مربوط به قیمت را صفر قرار دهید</p>
                                        <p style="text-align: justify;">&bull; دقت داشته باشید که اطلاعات کاربری خود را تکمیل نمایید تا در دید مشتریان بهتر مشاهده شده و امکان ارائه خدمات بهتری باشد</p>
                                        <p style="text-align: justify;">&bull; در صورتی که نیاز دارید کالا یا خدمات بیشتری را ارائه نمایید و از امکانات آتی ما بهره ببرید در بالای همین قسمت بر روی دکمه سفارش پنل ویژه کلیک نمایید و مراحل ساده ای را برای تبدیل شدن به کسب و کار ویژه طی کنید</p>
                                        <p style="text-align: justify;">&bull; در هر مرحله از کار در بی شمار نیاز به مشاوره و هدایت از طریق ما را داشتید سریعا در قسمت پشتیبانی آنلاین(پایین سمت راست) سوال خود را بپرسید و در سریعترین زمان ما به شما خدمت رسانی خواهیم کرد</p>
                                        <p style="text-align: justify;">&bull; همیشه این نکته را به یاد داشته باشید در صورتی که هر تغییری در کالا یا خدمات خود داشته باشید باید منتظر تایید مدیر ساید بمانید.این تایید ها جهت حفظ قوانین تجاری حاکم بر بی شمار می باشد و در صورتی که تغییرات شما با قوانین ملی ایران مغایرت نداشته باشد حتما تایید خواهد شد و در صورت عدم تایید ایراد وارده به صورت پیامی در پنل شما برای شما ارسال خواهد شد و شما می توانید با بر طرف کردن آن دوباره درخواست خود را ارسال نمایید</p>
                                        <p style="text-align: justify;">. &bull;<span style="text-decoration: underline;"> به قسمت مالی پنل خود دقت نمایید و در تمامی مراحل همیشه از راهنمای سایت کمک بگیرید ما همیشه در خدمت شما هستیم و مشتری مداری اصل اساسی بی شمار می باشد.</span></p>
                                        <p style="text-align: right;">&nbsp;</p>
                                    </div>
                                </div>
                            </div>

                            <div class="mx-auto mt-3 col-12">
                                <div class="row">
                                    <div class="col-3">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="fa fa-th-list m-b-5 font-16"></i>
                                            <h5 class="m-b-0 m-t-5">{{ App\Product::where('business_id', auth()->user()->business->id)->count() }}</h5>
                                            <small class="font-light">تعداد کالاها</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="fa fa-ban m-b-5 font-16"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\Product::whereIsActive(0)->count()}}</h5>
                                            <small class="font-light">کالاهای غیرفعال</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="fa fa-comment m-b-5 font-16"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\Comment::where('user_id',$userId)->count()}}</h5>
                                            <small class="font-light">تعداد کامنت ها</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="fas fa-shopping-basket"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\Order::where('user_id',$userId)->count()}}</h5>
                                            <small class="font-light">کل خریدها</small>
                                        </div>
                                    </div>
                                    <div class="col-3 mt-1">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="far fa-money-bill-alt"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\OrdersProduct::where('product_id',$business->product_id)->count()}}</h5>
                                            <small class="font-light">کل فروش ها</small>
                                        </div>
                                    </div>
                                    <div class="col-3 mt-1">
                                        <div class="bg-danger p-2 text-center text-light">
                                            <i class="fas fa-exclamation-circle"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\OrdersProduct::where(['product_id' => $business->product_id, 'status'=> 'پرداخت شد'])->count()}}</h5>
                                            <small class="font-light">کالاهای ارسال نشده</small>
                                        </div>
                                    </div>
                                    <div class="col-3 mt-1">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="fas fa-envelope-open"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\Inbox::where(['user_id'=>auth()->user()->id,'seen'=> 0 ])->count()}}</h5>
                                            <small class="font-light">پیام های خوانده نشده</small>
                                        </div>
                                    </div>
                                    <div class="col-3 mt-1">
                                        <div style="background-color: #1f75a5" class="text-light p-2 text-center">
                                            <i class="fas fa-shopping-cart"></i>
                                            <h5 class="m-b-0 m-t-5">{{App\Cart::where(['user_phone'=>auth()->user()->phone])->count()}}</h5>
                                            <small class="font-light">سبد خرید</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- column -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>        <!-- End Container fluid  -->

@endsection
