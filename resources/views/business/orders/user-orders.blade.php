@extends('layouts.businessLayout.business_design')
@section('content')
    <div class="container-fluid pt-3" style="min-height: 500px;direction: rtl">
        @include('includes.show-sessions')
        @include('includes.form-error')

        <h4 class="text-center text-info">خریدهای شما از سایت</h4>
        @if(count($orders)>0)
            <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center" dir="rtl">
                <thead>
                <tr class="text-light bg-megna">
                    <th>شماره فاکتور</th>
                    <th>شرح کالا</th>
                    <th>مبلغ فاکتور</th>
                    <th>وضعیت</th>
                    <th>زمان خرید</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td class="text-right pr-4">
                            @foreach($order->ordersProduct as $product)
                                {{$product->product_name}}<br>
                            @endforeach
                        </td>
                        <td>{{$order->total_amount}} تومان</td>
                        <td>{{ $product->status == 'پرداخت شد' ? 'ارسال نشده' : 'ارسال شد' }}</td>
                        <td dir="ltr">{{verta($order->created_at)->formatDate()}}</td>
                        <td class="p-0">
                            <a href="{{url('businessadmin/orders/'.$order->id)}}"
                               class="btn btn-outline-info border-0 p-2 mt-1">مشاهده جزییات
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row mt-2">
                <div class="mx-auto">
                    {{$orders->render()}}
                </div>
            </div>
        @else
            <hr>
            <h5 class="text-center text-info">شما تا کنون سفارشی انجام نداده اید</h5>
        @endif
    </div>
@endsection

