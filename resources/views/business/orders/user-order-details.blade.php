@extends('layouts.businessLayout.business_design')
@section('content')

    <div class="container-fluid pt-3" style="min-height: 500px;">

        @include('includes.show-sessions')
        @include('includes.form-error')

        <div class="breadcrumb text-info col-sm-12 p-1 m-0">
            <div class="container-fluid text-right">
                <a href="{{url('businessadmin')}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a href="{{url('profile/orders')}}">سابقه خرید </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a>جزییات سفارش </a>
            </div>
        </div> <!--END BREADCRUMB-->

        <h4 class="text-center text-info mt-2">جزییات سفارش</h4>
        <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center" dir="rtl">
            <thead>
            <tr class="text-light bg-megna">
                <th>فاکتور</th>
                <th>فروشنده</th>
                <th>کد کالا</th>
                <th>نام کالا</th>
                <th>رنگ کالا</th>
                <th>تعداد کالا</th>
                <th>قیمت کالا</th>
                <th>قیمت کل</th>
                <th>وضعیت ارسال</th>
                <th>تاریخ ارسال</th>
                <th>کد رهگیری مرسوله</th>
            </tr>
            </thead>
            <tbody>

            @foreach($orderDetails->ordersProduct as $pro)

                <tr>
                    <td>{{$pro->order_id ?? 'مشخص نشده'}}</td>
                    <td><a href="{{$pro->path()}}" title="رفتن به صفحه کالا" class="shadow p-2">
                        {{$pro->business->name ?? 'مشخص نشده'}}</a>
                    </td>
                    <td>{{$pro->product_code ?? 'مشخص نشده'}}</td>
                    <td><a href="{{ $pro->path }}" title="رفتن به صفحه کالا" class="shadow p-2">
                            {{$pro->product_name ?? 'مشخص نشده'}}</a>
                    </td>
                    <td>{{$pro->product_color ?? 'مشخص نشده'}}</td>
                    <td>{{$pro->product_qty ?? 'مشخص نشده'}}</td>
                    <td>{{$pro->product_price ?? 'مشخص نشده'}}</td>
                    <td>{{$pro->total_amount ?? 'مشخص نشده'}}</td>
                    <td>{{$pro->status== 'پرداخت شد' ? 'ارسال نشده' : 'ارسال شد'}}</td>
                    <td>{{verta($pro->sent_date)->formatDate() ?: 'ارسال نشده'}}</td>
                    <td>{{$pro->delivery_code ?? 'ارسال نشده'}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="badge badge-info badge-pill p-2" style="font-size: large" >جمع کل فاکتور: {{$order_total}} تومان</div>
    </div>
@endsection

