@extends('layouts.businessLayout.business_design')
@section('content')

    <div class="container-fluid pt-3" style="min-height: 500px;direction: rtl">
        @include('includes.show-sessions')
        @include('includes.form-error')

        <h4 class="text-center text-info">فاکتورهای فروش</h4>
        @if(count($orders)>0)
            <div class="table-responsive">
                <table id="example" class="table table-striped table-inverse table-bordered table-hover text-center" dir="rtl">
                    <thead>
                    <tr class="text-info">
                        <th>شماره فاکتور</th>
                        <th>شرح کالا</th>
                        <th>تعداد کالا</th>
                        <th>مبلغ کالا</th>
                        <th>تخفیف</th>
                        <th>مبلغ کل</th>
                        <th>مبلغ با تخفیف</th>
                        <th>نام خریدار</th>
                        <th>زمان خرید</th>
                        <th>عملیات</th>
                        <th>وضعیت</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr @if($order->status == 'تسویه شد') style="background-color: #cdffd7; color: grey" @endif>
                            <td>{{$order->order->invoice_id}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->quantity}}</td>
                            <td>{{$order->price}} تومان</td>
                            <td>
                                @php $discount_price = 0; @endphp
                                @if(isset($order->discount))
                                    @if($order->discount->amount_type == 1)
                                        @php $discount_price = $order->discount->percent * $order->total_amount / 100; @endphp
                                    @else
                                        @php $discount_price = $order->discount->amount; @endphp
                                    @endif
                                        {{$discount_price}}
                                @else
                                    0
                                @endif
                                تومان

                            </td>
                            <td>{{$order->price * $order->quantity}} تومان</td>
                            <td>{{$order->total_amount - $discount_price}} تومان</td>
                            <td>{{$order->order->user->name}}</td>
                            <td dir="ltr">{{verta($order->created_at)->formatDate()}}</td>
                            <td class="p-0">
                                <a href="#myModal{{ $order->id }}" data-toggle="modal"
                                   class="btn btn-skype border-0 p-2 mt-2">مشاهده جزییات
                                </a>
                            </td>
                            <td>{{$order->status}}</td>
                        </tr>

                        <!--MODAL-->
                        <div id="myModal{{ $order->id }}"
                             class="modal hide text-right bg-light container">
                            <div class="modal-header">
                                <button data-dismiss="modal" class="close" type="button">×</button>
                                <b>تاریخ خرید: </b>
                                <h5 dir="ltr" class="text-danger">{{verta($order->created_at)->formatDate()}}</h5>
                            </div>
                            @if($order->status == 'کنسل شد')
                                <h3 class="mt-2 mb-2 text-center">جزییات خرید کنسل شده</h3>
                            @endif
                            @if($order->status != 'کنسل شد')
                                <h3 class="mt-2 mb-2 text-center">جزییات خرید انجام شده</h3>
                            @if($order->delivery_code)
                                <h4>شماره رهگیری پست: {{$order->delivery_code}}</h4>
                            @else
                                <div class="row mx-auto">
                                    <form action="{{ url('businessadmin/sold/submit-delivery-code')}}" method="post" class="form-group">
                                        @csrf
                                        <label for="delivery_code" class="col-form-label-sm">کد رهگیری پست: </label>
                                        <input id="delivery_code" name="delivery_code" type="text"
                                               class="form-inline">
                                        <input name="user_id" type="hidden" value="{{$order->order->user->id}}">
                                        <input name="business_id" type="hidden" value="{{$order->product->business_id}}">
                                        <input name="business_phone" type="hidden"
                                               value="{{$order->product->business->phone}}">
                                        <input name="product_id" type="hidden" value="{{$order->product_id}}">
                                        <input name="product_name" type="hidden" value="{{$order->name}}">
                                        <input name="order_id" type="hidden" value="{{$order->order_id}}">
                                        <button type="submit" class="btn btn-outline-info">ثبت</button>
                                    </form>
                                </div>
                            @endif
                            @endif
                            <div class="modal-body">
                                <div style="background-color: #e4edf2" class="p-3 font-weight-bold">
                                    <div>خریدار: {{$order->order->user->name}}</div>
                                    <div>شماره تماس خریدار: {{$order->order->user->phone}}</div>
                                    <div>ایمیل خریدار: {{$order->order->address->email ?? 'ندارد'}}</div>
                                    <div>استان: {{$order->state_name}}</div>
                                    <div>شهر: {{$order->city_name}}</div>
                                    <div>آدرس: {{$order->address}}</div>
                                    <div>کدپستی: {{$order->post_code}}</div>
                                </div>
                                <hr>
                                <div style="background-color: #eee" class="p-3">
                                    <div><b>فاکتور: </b>{{$order->order->invoice_id}}</div>
                                    <div><b>کد کالا: </b>{{$order->code ?? 'بدون کد'}}</div>
                                    <div><b>نام کالا: </b>{{$order->name}}</div>
                                    <div><b>تعداد کالا: </b>{{$order->quantity}}</div>
                                    <div><b>قیمت کالا: </b>{{$order->price}} تومان</div>
                                    @if(isset($order->discount))
                                        <div><b> کدتخفیف: </b>{{$order->discount->code}} </div>
                                        <div><b>میزان تخفیف: </b>{{$order->discount->amount_type == 1 ? $order->discount->percent .'%' : $order->discount->amount . 'تومان'}} </div>
                                    @endif
                                    <div><b>جمع قیمت ( با تخفیف ): </b>{{$order->total_amount - $discount_price}}  تومان</div>
                                    <div><b>وضعیت
                                            ارسال: </b>{{$order->status == 'پرداخت شد' ? 'ارسال نشده' : $order->status}}
                                    </div>
                                    @if($order->status != 'کنسل شد')
                                    <div>
                                        <b>تاریخ ارسال: </b>
                                        <span dir="ltr">{{$order->sent_date != NULL ? verta($order->admin_checkout_date)->formatDate() : 'کد رهگیری وارد نمایید تا تاریخ ارسال به روز گردد'}}</span>
                                    </div>
                                    @endif
                                    <div>
                                        <b>{{$order->status == 'تسویه شد' ? 'تاریخ تسویه توسط بی شمار: ' : 'تاریخ کنسلی توسط مشتری: '}}</b>
                                        <span dir="ltr">
                                            {{$order->admin_checkout_date != NULL ? verta($order->admin_checkout_date)->formatDate() : 'کد رهگیری وارد نمایید و تا دریافت کالا توسط مشتری منتظر بمانید'}}
                                        </span>
                                    </div>
                                </div>
                                <br>
                                @if($order->product->status == 1)
                                    <a href="{{ url($order->product->path()) }}">رفتن به صفحه کالا</a>
                                @endif
                            </div>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row mt-2">
                <div class="mx-auto">
                    {{$orders->render()}}
                </div>
            </div>
        @else
            <hr>
            <h5 class="text-center text-info">شما تا کنون فروشی انجام نداده اید</h5>
        @endif
    </div>
@endsection

