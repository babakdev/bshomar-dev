@extends('layouts.businessLayout.business_design')

@section('content')
    <div class="container-fluid" style="min-height: 500px;">

        <div class="breadcrumb text-info col-sm-12">
            <div class="container-fluid text-right">
                <a href="{{auth()->user()->isAdmin ? '/irenadmin' : '/dashboard'}}">داشبورد </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a href="comments">نظرات </a><i class="fa fa-chevron-left breadcrumb-item"></i>
                <a>جوابها به نظرات </a>
            </div>
        </div> <!--END BREADCRUMB-->

        <h3 class="text-center pt-2 text-info">جوابها به نظرات</h3>

    <table class="table table-striped text-center" dir="rtl">
        <thead>
        <tr>
            <th>id نظر</th>
            {{--<th>نظر</th>--}} {{--todo--}}
            <th>جواب</th>
            <th>نویسنده جواب</th>
            {{--<th>حذف</th>--}}
            {{--<th>وضعیت</th>--}}
        </tr>
        </thead>

        <tbody>
        @foreach($replies as $reply)
            <tr class="text-center">
                <td>{{$reply->comment->id}}</td>
                {{--<td>--}} {{--todo--}}
                    {{--<!-- Button to Open the Modal -->--}}
                    {{--<button type="button" class="btn btn-light" data-toggle="modal" data-target="#myModal2{{ $reply->id }}">--}}
                        {{--دیدن نظر--}}
                    {{--</button>--}}

                    {{--<!-- The Modal -->--}}
                    {{--<div class="modal" id="myModal2{{ $reply->comment->id }}">--}}
                        {{--<div class="modal-dialog">--}}
                            {{--<div class="modal-content">--}}

                                {{--<!-- Modal Header -->--}}
                                {{--<div class="modal-header">--}}
                                    {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                                {{--</div>--}}

                                {{--<!-- Modal body -->--}}
                                {{--<div class="modal-body text-right">--}}
                                    {{--{{$reply->comment->body}}--}}
                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</td>--}}
                <td>
                    <!-- Button to Open the Modal -->
                    <button type="button" class="btn btn-light" data-toggle="modal" data-target="#myModal{{ $reply->id }}">
                        {{Illuminate\Support\Str::limit($reply->body,20)}}
                    </button>

                    <!-- The Modal -->
                    <div class="modal" id="myModal{{ $reply->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body" style="direction: rtl">
                                    {{$reply->body}}
                                </div>

                            </div>
                        </div>
                    </div>
                </td>
                <td>{{$reply->comment->user->name}}</td>
                {{--<td>--}}
                    {{--{!! Form::open(['method'=>'DELETE' , 'action' => ['ProductCommentRepliesController@destroy',$reply->id]]) !!}--}}
                    {{--@csrf--}}

                    {{--<div class="form-group">--}}
                        {{--{!! Form::submit('حذف', ['class'=>'btn btn-danger btn-sm rounded pl-4 pr-4']) !!}--}}
                    {{--</div>--}}

                    {{--{!! Form::close() !!}--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--@if($reply->status == 1)--}}

                        {{--{!! Form::open(['method'=>'PATCH' , 'action' => ['ProductCommentRepliesController@update',$reply->id]]) !!}--}}
                        {{--@csrf--}}
                        {{--<input type="hidden" name="status" value="0">--}}
                        {{--<div class="form-group">--}}
                            {{--{!! Form::submit('رد کردن', ['class'=>'btn btn-warning btn-sm rounded ']) !!}--}}
                        {{--</div>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--@else--}}
                        {{--{!! Form::open(['method'=>'PATCH' , 'action' => ['ProductCommentRepliesController@update',$reply->id]]) !!}--}}
                        {{--@csrf--}}
                        {{--<input type="hidden" name="status" value="1">--}}

                        {{--<div class="form-group">--}}
                            {{--{!! Form::submit('تایید', ['class'=>'btn btn-success btn-sm rounded pl-4 pr-4']) !!}--}}
                        {{--</div>--}}

                        {{--{!! Form::close() !!}--}}
                    {{--@endif--}}
                {{--</td>--}}



            </tr>
        @endforeach

        </tbody>
    </table>
</div>



@endsection
