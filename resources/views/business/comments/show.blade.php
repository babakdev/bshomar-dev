@extends('layouts.businessLayout.business_design')

@section('content')

    <h3 class="text-center pt-3">نظرات</h3>

    <table class="table table-striped">
        <thead>
        <tr>
            {{--<th>حذف</th>--}}
            {{--<th>وضعیت</th>--}}
            <th>جواب</th>
            <th>نظر</th>
            <th>لینک</th>
            <th>id</th>
        </tr>
        </thead>

        <tbody>
        @foreach($comments as $comment)
            <tr>
                {{--<td>--}}
                {{--{!! Form::open(['method'=>'DELETE' , 'action' => ['ProductCommentsController@destroy',$comment->id]]) !!}--}}
                {{--@csrf--}}
                {{--<div class="form-group">--}}
                {{--{!! Form::submit('حذف', ['class'=>'btn btn-danger btn-sm rounded']) !!}--}}
                {{--</div>--}}
                {{--{!! Form::close() !!}--}}
                {{--</td>--}}
                {{--<td>--}}
                {{--@if($comment->status == 1)--}}
                {{--{!! Form::open(['method'=>'PATCH' , 'action' => ['ProductCommentsController@update',$comment->id]]) !!}--}}
                {{--@csrf--}}
                {{--<input type="hidden" name="status" value="0">--}}

                {{--<div class="form-group">--}}
                {{--{!! Form::submit('رد کردن', ['class'=>'btn btn-warning btn-sm rounded ']) !!}--}}
                {{--</div>--}}
                {{--{!! Form::close() !!}--}}
                {{--@else--}}
                {{--{!! Form::open(['method'=>'PATCH' , 'action' => ['ProductCommentsController@update',$comment->id]]) !!}--}}
                {{--@csrf--}}
                {{--<input type="hidden" name="status" value="1">--}}

                {{--<div class="form-group">--}}
                {{--{!! Form::submit('تایید', ['class'=>'btn btn-success rounded']) !!}--}}
                {{--</div>--}}
                {{--{!! Form::close() !!}--}}
                {{--@endif--}}
                {{--</td>--}}
                <td><a href="{{url('businessadmin/comment/replies',$comment->id)}}">دیدن جواب ها</a></td>
                <td>
                    <!-- Button to Open the Modal -->
                    <button type="button" class="page-link" data-toggle="modal"
                            data-target="#myModal{{ $comment->id }}">
                        {{Illuminate\Support\Str::limit($comment->body,20)}}
                    </button>

                    <!-- The Modal -->
                    <div class="modal" id="myModal{{ $comment->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body text-right" style="direction: rtl">
                                    {{$comment->body}}
                                </div>

                            </div>
                        </div>
                    </div>
                </td>
                <td><a href="{{$comment->product->path}}">صفحه کالا</a></td>
                <td>{{$comment->id}}</td>

            </tr>
        @endforeach
        </tbody>
    </table>




@endsection
