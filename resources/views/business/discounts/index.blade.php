@extends('layouts.businessLayout.business_design')

@section('content')
    <div class="container-fluid">
        @include('includes.show-sessions')

        <div class="card">
            <div class="card-header">
                <a class="btn btn-success" href="{{ action('Business\DiscountCodeController@create') }}">
                    ساخت کد تخفیف جدید <span class="fa fa-plus"></span>
                </a>
                <h3 class="card-title text-center">لیست کدهای تخفیف</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" dir="rtl">
                @if(isset($discounts))
                    <table class="table table-striped text-right">
                        <thead>
                        <tr>
                            <th>عنوان</th>
                            <th>مقدار تخفیف</th>
                            <th>کد تخفیف</th>
                            <th>تاریخ شروع</th>
                            <th>تاریخ پایان</th>
                            <th>محصولات شامل تخفیف</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($discounts as $item)

                            <tr>
                                <td>{{$item->name}}</td>
                                <td>
                                    @if($item->amount_type == 1)
                                        {{$item->percent}} %
                                    @else
                                        {{$item->amount}} تومان
                                    @endif
                                </td>
                                <td>{{$item->code}}</td>
                                <td>{{$item->start_date}}</td>
                                <td>{{$item->end_date}}</td>
                                <td>
                                    @php $i=0; @endphp
                                    @foreach($item->products as $product)
                                        @if(++$i == sizeof($item->products))
                                            {{$product->name}}
                                        @else
                                            {{$product->name}} -
                                        @endif
                                    @endforeach
                                </td>

                                <td class="row mx-auto">
                                    <form action="{{action('Business\DiscountCodeController@destroy',$item->id)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link deleteLearn" onclick="return confirm('آیا از حذف این کد تخفیف اطمینان دارید؟');">
                                            <i class="fa fa-trash text-danger" title="حذف"></i>
                                        </button>
                                    </form>
                                    <a class="p-1" href="{{ action('Business\DiscountCodeController@edit',$item->id) }}">
                                        <i class="fa fa-edit text-info" title="ویرایش"></i>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <hr>
                    <p class="text-center">هنوز کد تخفیفی ثبت نشده است</p>
                    <div class="row">
                        <div class="col-sm-6 offset-5">
                            {{$discounts->render()}}
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
