@include('includes.show-sessions')
@section('styles')
    <link rel="stylesheet" href="/css/persianDatepicker-default.css">
    <link rel="stylesheet" href="/css/chosen.min.css">
    <style>
        #load {
            border: 4px solid #f3f3f3;
            border-top: 4px solid #da542e;
            border-radius: 50%;
            width: 8px;
            height: 8px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection
@include('includes.form-error')
@csrf

    <input type="hidden" name="id" value="{{$item->id ?? 0}}">
    <div class="row">
        <div class="form-group col-md-6">
            <label for="name" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">عنوان </label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name',$item->name ?? '') }}"
                   required>
        </div>
        <div class="form-group col-md-6">
            <label for="code" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">کد تخفیف</label>
            <input type="text" class="form-control" id="code" name="code" autocomplete="off" value="{{ old('code',$item->code ?? '') }}"
                   required>
            <span id="load" style= "display: none; position: absolute; bottom: -20px;right: 10px; width: 16px; height:16px;"></span>
            <span class="showError text-danger"></span>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-12">
            <label for="products" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">لیست محصولات</label>
            {{Form::select('products',$products,isset($item) ? $item->products->pluck('id')->toArray() : null,array('multiple'=>'multiple','name'=>'products[]','class' => 'chosen-select form-control' ,'data-placeholder' => 'انتخاب کنید'))}}
            <button type="button" class="btn btn-outline-dark btn-xs select">انتخاب همه</button>
            <button type="button" class="btn btn-outline-dark btn-xs deselect">حذف همه</button>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            <div class="col-md-2">
                <label for="amount_type" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">نوع تخفیف</label>
            </div>
            @php
                $type = old('amount_type') ? old('amount_type') == 2 ? '2' : '1' : ( isset($item) ? ($item->amount_type == 2 ? '2' : '1' ) : '1' );
            @endphp


            <div class="col-md-4">
                {{--{{Form::radio('amount_type' ,1)}} درصد--}}
                {{--{{Form::radio('amount_type' ,2)}} تومان--}}
                <input type="radio" name="amount_type" value="1" {{$type == 1 ? 'checked' : ''}}> درصد
                <input type="radio" name="amount_type" value="2" {{$type == 2 ? 'checked' : ''}}> تومان
            </div>
        </div>

        <div class="form-group col-md-6 percentage" style="{{$type == 2 ? 'display:none' : ''}}">
            <label for="percent" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">درصد تخفیف</label>
            <input type="text" class="form-control" id="percent" name="percent" value="{{ old('percent',$item->percent ?? '') }}">
        </div>
        <div class="form-group col-md-6 amount" style="{{$type == 1 ? 'display:none' : ''}}">
            <label for="amount" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">تخفیف ( تومان )</label>
            <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount',$item->amount ?? '') }}">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-md-6">
            <label for="start_date" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">تاریخ شروع تخفیف </label>
            <input type="text" class="form-control picker" id="start_date" name="start_date" value="{{ old('start_date',$item->start_date ?? '') }}"
                   required>
        </div>
        <div class="form-group col-md-6">
            <label for="end_date" class="tracking-wide uppercase text-grey-dark text-xs block pb-2">تاریخ پایان تخفیف</label>
            <input type="text" class="form-control picker" id="end_date" name="end_date" value="{{ old('end_date',$item->end_date ?? '') }}"
                   required>
        </div>
    </div>


    <div class="mb-4">
        <button type="submit" class="col-md-12 btn btn-success btn-block font-20">{{ $buttonText ?? 'ثبت کد تخفیف' }}</button>
    </div>

    <a href="{{ action('Business\DiscountCodeController@index') }}" class="btn btn-link">صفحه قبل<i class="fa fa-arrow-left"></i></a>

@section('scripts')
    <script src="{{asset('/js/persianDatepicker.min.js')}}"></script>
    <script src="{{asset('/js/chosen.jquery.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $(".picker").persianDatepicker();
            $(".chosen-select").chosen();
        });
        $('.select').click(function(){
            $('option').prop('selected', true);
            $('select').trigger('chosen:updated');
        });
        $('.deselect').click(function(){
            $('option').prop('selected', false);
            // $('select').trigger('liszt:updated');
            $('select').trigger('chosen:updated');
        });
        $("input[name='amount_type']").on('change',function () {
            var type = $("input[name='amount_type']:checked").val();
            if(type == 1){
                $(".percentage").css('display','block');
                $(".amount").css('display','none');
            }else{
                $(".percentage").css('display','none');
                $(".amount").css('display','block');
            }
        });
        $("input[name='code']").on('keyup',function(){
            var value = $(this).val();
            var id = '{{isset($item) ? $item->id : 0 }}';
            $("#load").show();
            axios.get('/businessadmin/discount/repetition/check',{
                params: {
                    value : value,
                    id : id
                    }
                })
                .then(function(response){
                    if(response.data.status == true){
                        $("span.showError").text(response.data.msg);
                    }else{
                        $("span.showError").text('');
                    }
                    $("#load").hide();
                });
        });

    </script>
@endsection
