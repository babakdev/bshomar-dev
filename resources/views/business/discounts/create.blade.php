@extends('layouts.businessLayout.business_design')

@section('content')
    <div dir="rtl" class="container text-right mt-3">
        <form method="POST" action="{{ action('Business\DiscountCodeController@store') }}">
            @include ('business.discounts._form')
        </form>
    </div>
@endsection


