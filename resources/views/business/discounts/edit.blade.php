@extends('layouts.businessLayout.business_design')

@section('content')
    <div dir="rtl" class="container text-right">

        <form method="POST" action='{{ action('Business\DiscountCodeController@update' ,$item->id) }}'>
            @method('PATCH')
            @include ('business.discounts._form' ,['buttonText' => 'ویرایش کد تخفیف'])
        </form>
    </div>
@endsection
