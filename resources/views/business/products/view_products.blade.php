@extends('layouts.businessLayout.business_design')
@section('content')
    <div class="container-fluid" dir="rtl">
        @include('includes.show-sessions')
        @include('includes.warning-sessions')

        <div class="card">
            <div class="card-header">
                <a class="btn btn-success" href="{{ url('/products/create?businessId=' . auth()->user()->business->id) }}">
                    ساخت کالای جدید <span class="fa fa-plus"></span>
                </a>
                <h3 class="card-title text-center">مشاهده کالاها</h3>
            </div>

            <br>
            <form method="get" class="row col-md-12 form-horizontal" action="{{action('ProductController@filter')}}">
                <input class="form-control col-md-2" type="text" name="name" id="name" placeholder="نام محصول" value="{{$_GET['name'] ?? ''}}">
                @if(auth()->user()->isAdmin)
                <select class="form-control col-md-4" title="دسته بندی" id="category" name="category">
                    <option value=" ">دسته بندی</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}" {{isset($_GET['category']) ? ($_GET['category'] == $category->id ? 'selected' : '') : ''}}>{{$category->name}}</option>
                    @endforeach
                </select>
                @endif
                <select class="form-control col-md-3" title="فیلتر" name="filter" id="filter">
                    <option value="1" {{isset($_GET['filter']) ? ( $_GET['filter'] == 1 ? 'selected' : '') : ''}}>کالاهای موجود</option>
                    <option value="2" {{isset($_GET['filter']) ? ( $_GET['filter'] == 2 ? 'selected' : '') : ''}}>کالاهای ناموجود</option>
                    <option value="3" {{isset($_GET['filter']) ? ( $_GET['filter'] == 3 ? 'selected' : '') : ''}}>کالاهای با موجودی هشدار</option>
                </select>
                <button class="btn btn-info col-md-1" title="فیلتر کن"><span class="fa fa-filter"></span>فیلتر کن</button>
            </form>
            <br>

            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-center table-striped">
                    <thead>
                    <tr class="text-danger bg-light text-center">
                        <th>تصویر</th>
                        <th>عنوان</th>
                        <th>دسته بندی</th>
                        <th>قیمت با تخفیف</th>
                        <th>موجودی</th>
                        <th>کد</th>
                        <th>وضعیت</th>
                        <th>عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $products as $product)
                        <tr>
                            <td>
                                @if(! $product->images->isEmpty())
                                    <img height="60" alt="businessPhoto"
                                         class="lazyload blur-up rounded"
                                         src="{{ asset('images/front_images/products/large/'.$product->images[0]->path) }}">
                                @else
                                    <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
                                @endif
                            </td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ $product->price == 0 ? 'تماس برای قیمت' : number_format($product->price) }}</td>
                            <td>
                                <span style="{{$product->min_stock >= $product->stock ? 'color:red !important;' : ''}}">{{ $product->stock }}</span>
                            </td>
                            <td>{{ $product->code }}</td>
                            <td>{{ $product->is_active ==1 ? 'فعال' : 'غیرفعال' }}</td>

                            <td class="text-center row">
                                <button type="button" class="btn btn-link pt-0" data-toggle="modal"
                                        data-target="#myModal{{ $product->id }}"><i
                                            class="fa fa-eye text-success"></i>
                                </button>
                                <form action="{{url('/products/'. $product->id . '/edit')}}" method="GET">
                                    <input type="hidden" name="businessId" value="{{ $product->business_id }}">
                                    <button type="submit" class="btn btn-link pt-0" title="ویرایش">
                                        <i class="fa fa-edit text-orange"></i>
                                    </button>
                                </form>
                                <form action="{{url('/products/'. $product->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-link pt-0 delProduct" onclick="return confirm('آیا از حذف این محصول اطمینان دارید؟')">
                                        <i class="fa fa-trash text-danger" title="حذف"></i>
                                    </button>
                                </form>
                                @if($product->stock > 0)
                                    <form action="{{action('ProductController@toggleProductStatus',$product->id)}}" method="GET">
                                        <button type="submit" class="btn btn-link pt-0" title="ناموجود کردن">
                                            <i class="fa fa-times text-danger"></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>

                        <div id="myModal{{ $product->id }}"
                             class="modal fade text-right">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header" dir="ltr">
                                        <h3>
                                            <a href="{{url ($product->path() ) }}" target="_blank"
                                               title="رفتن به صفحه کالا">{{ $product->name}}</a>
                                        </h3>&nbsp;&nbsp;
                                        <button data-dismiss="modal" class="close" type="button">×</button>
                                    </div>

                                    <div class="modal-body" style="direction: rtl">
                                        @if(! $product->images->isEmpty())
                                            <div class="text-center">
                                                @foreach($product->images as $image)
                                                    <img height="192" alt="businessPhoto"
                                                         class="lazyload blur-up rounded"
                                                         src="{{ asset('images/front_images/products/large/'.$image->path) }}">
                                                @endforeach
                                            </div>
                                            <hr>
                                        @else
                                            <p class="alert alert-danger">هشدار!!! تصویر ندارد</p>
                                        @endif
                                        <p><strong>متعلق به کسب و
                                                کار:</strong> {{ $product->business->name ?? 'نامشخص' }}</p>
                                        <p><strong>دسته بندی:</strong> {{ $product->category->name ?? 'نامشخص' }}
                                        </p>
                                        <p>
                                            <strong>وضعیت:</strong> {{ $product->is_active ==1 ? 'فعال' : 'غیرفعال'  }}
                                        </p>
                                        <p><strong>قیمت قبل تخفیف:</strong> {{ $product->fake_price ?? 'نامشخص' }}
                                        </p>
                                        <p><strong>قیمت اصلی:</strong> {{ $product->price ?? 'نامشخص' }}</p>
                                        <p><strong>موجودی انبار:</strong> {{ $product->stock ?? 'نامشخص' }}</p>
                                        <p><strong>واحد اندازه گیری:</strong> {{ $product->unit ?? 'نامشخص' }}</p>
                                        <p><strong>توضیحات:</strong> {{ $product->description ?? 'نامشخص' }}</p>
                                        <p><strong>کد کالا:</strong> {{ $product->code ?? 'نامشخص' }}</p>
                                        <p><strong>کشور سازنده:</strong> {{ $product->country ?? 'نامشخص' }}</p>
                                        <p><strong>کلمات کلیدی:</strong> {{ $product->meta_keywords ?? 'نامشخص' }}
                                        </p>
{{--                                        <p><a href="{{ url('businessadmin/comments',$product->id)}}">مشاهده نظرات مربوط به این--}}
{{--                                                کالا</a>--}}
{{--                                        </p>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach

                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-6">
                        {{$products->render()}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
