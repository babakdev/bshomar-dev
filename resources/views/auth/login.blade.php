<!DOCTYPE html>
<html lang="en">

<head>
    <title>Admin Login</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{asset('css/admin_style.min.css')}}" rel="stylesheet">


</head>
<body style="background-color: #f4f6f6">
<div id="loginbox" class="container py-5 my-5 text-center">
    @include('includes.show-sessions')
    <img src="{{asset('images/front_images/bshomar.png')}}" alt="logo" class="img-fluid ml-4 blur-up">
    <br>
    <form class="form-vertical" role="form" method="POST" action="{{ url('/') }}">
        @csrf
        <div class="control-group normal_text"><h5>ورود به قسمت پشتیبانی سایت بی‌شمار
            </h5></div>
        <br>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_lg"></span><input id="phone" type="number" required
                                                             name="phone" class="form-control col-md-3 mx-auto"
                                                             placeholder="شماره موبایل"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on bg_ly"></span><input id="password" type="password" required
                                                             name="password" class="form-control col-md-3 mx-auto mt-2"
                                                             placeholder="رمزعبور"/>
                </div>
            </div>
        </div>
        <div class="form-actions mt-3" style="cursor: pointer">
            <span class=""><input type="submit" class="btn btn-success btn-block" value="ورود"/></span>
        </div>
    </form>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/backend_js/matrix.login.js') }}"></script>
</body>

</html>
