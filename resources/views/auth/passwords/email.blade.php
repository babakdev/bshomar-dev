@extends($page)

@section('content')
<div class="container mt-5 mb-5">
    @if(Session::has('flash_error'))
        <div class="alert alert-danger alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{!! session('flash_error') !!}</strong>
        </div>
    @endif
    @if(Session::has('flash'))
        <div class="alert alert-success alert-block text-center">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{!! session('flash') !!}</strong>
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-right">{{ __('بازیابی رمز عبور') }}</div>

                <div class="card-body">

                    <form method="POST" action="/recover-password" aria-label="{{ __('ریست کردن رمز عبور') }}">
                        @csrf

                        <div class="form-group row" dir="rtl">
                            <label for="phoneRecovery" class="col-form-label">
                                <p class="text-right p-2">لطفا شماره تلفن همراه خود که به عنوان نام کاربری استفاده
                                    میکنید را در کادر زیر وارد نمایید تا کد فعالسازی جهت تغییر رمز عبور ارسال
                                    گردد</p>
                            </label>

                            <div class="col-md-6 mx-auto">
                                <input type="number" name="phoneRecovery" class="form-control" id="phoneRecovery"
                                       placeholder="مثال:09123456789" required>

                                @if ($errors->has('phoneRecovery'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phoneRecovery') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="form_1_id"></div>
                            <div id="captcha">{!!  GoogleReCaptchaV2::render('form_1_id') !!}</div>
                            @error('g-recaptcha-response')
                            <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group mb-0 float-right">
                                <button type="submit" class="btn btn-primary">ارسال
                                    درخواست
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
