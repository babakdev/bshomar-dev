@extends($page)
@section('pageTitle')تایید کد ارسالی@endsection
@section('content')
    <div class="container mt-lg-0 mt-5 mb-5">
        @if(Session::has('flash_error'))
            <div class="mx-auto text-center alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert"> ×</button>
                <strong>{!! session('flash_error') !!}</strong>
            </div>
        @endif
        @if(Session::has('flash'))
            <div class="mx-auto text-center alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert"> ×</button>
                <strong>{!! session('flash') !!}</strong>
            </div>
        @endif
        <span class="text-center" dir="rtl">@include('includes.form-error')</span>

        <div class="container pt-5 mb-5">
            <div class="row justify-content-center">
                <div class="col-md-8">
                        <div class="card">
                            <div class="card-header text-right">{{ __('تایید کد ارسالی جهت ورود کاربر') }}</div>
                                <p class="text-right small p-3">.کد تاییدیه ای تا دقایقی دیگر برای
                                    <b>{{request('phone')}}</b>، ارسال می گردد<span><br></span> کد را در کادر زیر وارد نمایید تا  بتوانید رمز عبور جدیدی انتخاب نمایید</p>
                                <form method="POST" action="/change-forgotten-password" id="change_password"
                                      aria-label="{{ __('تایید کد') }}">
                                    @csrf
                                    <input type="hidden" name="phone" value="{{request('phone')}}">
                                    <div class="form-group row" dir="rtl">

                                        <label for="code"
                                               class="col-md-4 col-form-label ">{{ __('کد تاییدیه:') }}</label>

                                        <div class="col-md-6">
                                            <input id="code" type="text"
                                                   class="form-control{{ $errors->has('code') ? ' نامعتبر' : '' }}"
                                                   name="code" value="{{ old('code') }}" required>

                                            @if ($errors->has('code'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row" dir="rtl">

                                        <label for="password"
                                               class="col-md-4 col-form-label ">{{ __('رمز عبور جدید:') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" dir="ltr"
                                                   class="form-control"
                                                   name="password" required>
                                        </div>
                                    </div>

                                    <div class="form-group text-center">
                                            <button type="submit" class="btn btn-primary rounded">
                                                {{ __(' تغییر رمز عبور') }}
                                            </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    <script>
        $().ready(function () {
            $("#change_password").validate({
                rules: {
                    password: {
                        minlength: 8
                    }
                },
                messages: {
                    password: 'رمز عبور کمتر از ۸ کاراکتر نمی تواند باشد',
                }
            });
        });
    </script>
@endsection
