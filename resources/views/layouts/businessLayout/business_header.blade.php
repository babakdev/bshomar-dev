<?php

use App\Http\Controllers\Controller;

$userDetails = auth()->user();
$business = auth()->user()->business;
?>
<header class="topbar mb-3" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header" style="background-color: #e8e8e8" data-logobg="skin5">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                    class="ti-menu ti-close text-dark"></i></a>
            <!-- Logo -->
            <a class="navbar-brand" href="/">
                <!-- Logo icon -->
                <b class="logo-icon p-l-10">
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="{{asset('images/backend_images/logo-icon.png')}}" alt="homepage"
                         class="light-logo"/>

                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="logo-text">
                             <!-- dark Logo text -->
                             <img src="{{asset('images/backend_images/logo-text.png')}}" alt="homepage"
                                  class="light-logo"/>

                </span>
            </a>

            <!-- Toggle which is visible on mobile only -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
               data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
               aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more text-dark"></i></a>
        </div>

        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <!-- toggle and nav items -->
            <ul class="navbar-nav float-left mr-auto">
                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light"
                                                          href="javascript:void(0)" data-sidebartype="mini-sidebar"><i
                            class="mdi mdi-menu font-24 text-dark"></i></a>
                </li>
                @if($business->category_id)
                    <li class="text-dark text-right row pt-2 font-20"
                        style="width: 300px;text-shadow: 1px 1px 5px cadetblue;" dir="rtl"><a
                            href="{{ url($business->path()) }}" class="nav-link text-dark mr-5">{{$business->name}}&nbsp;</a>
                    </li>
                @else
                    <p>در انتظار تایید</p>
                @endif

            <!-- Search -->
                {{--<li class="nav-item search-box"><a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i--}}
                {{--class="ti-search"></i></a>--}}
                {{--<form class="app-search position-absolute">--}}
                {{--<input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i--}}
                {{--class="ti-close"></i></a>--}}
                {{--</form>--}}
                {{--</li>--}}
            </ul>
                <a href="{{ url('businessadmin/panel')}}" class="mr-5">
                    <button class="btn btn-outline-dark mx-auto">&#x2606;&#x2606; سفارش پنل ویژه &#x2606;&#x2606;
                    </button>
                </a>
            <ul>
                <li class="list-unstyled">
                    <a href="/" class="nav-link pt-3 text-dark font-12 d-md-block d-none"> سایت</a>
                </li>
            </ul>
            <!-- Right side toggle and nav items -->
            <ul class="navbar-nav float-right">
                <!-- User profile and search -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href=""
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-caret-down"></i>
                        <img
                            src="{{ $userDetails->image ? asset($userDetails->image) : asset('/images/users/user.png')}}"
                            alt="user" class="rounded-circle" width="31">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                        {{--<a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> صندوق--}}
                        {{--پیام</a>--}}
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ url('/profile')}}"><i class="ti-settings m-r-5 m-l-5"></i>
                            تنظیمات کاربری</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="ti-power-off m-r-5 m-l-5"></i>خروج
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                <!-- User profile and search -->
            </ul>
        </div>
    </nav>
</header>
