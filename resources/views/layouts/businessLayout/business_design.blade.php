<!DOCTYPE html>
<html dir="ltr" lang="fa">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="بی‌شمار">
    <meta name="author" content="Babak Moeinifar - babakus7@gmail.com - instagram@babakmoeini">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/backend_images/favicon.png">
    <title>پنل مدیریت کسب و کار</title>
    <!-- Custom CSS -->
    <link href="/css/admin_style.min.css" rel="stylesheet">
    <link href="/css/front_css/dropify.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" rel="stylesheet">
{{--    <link href="/css/backend_css/custom.css" rel="stylesheet">--}}
    <style>
        body {
            background-image: none !important;
        }
        .inputfile {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
        input.is-danger {
            border-color: orangered;
        }
        @font-face {
            font-family: yekan;
            src: url('/css/fonts/yekan.ttf');
        }

        body {
            font-family: yekan, sans-serif;
            font-size: 12px;
        }
    </style>

@yield('styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div id="main-wrapper">
    @include('layouts.businessLayout.business_header')
    @include('layouts.businessLayout.business_sidebar')

    <div class="page-wrapper">
        <div style="min-height: 800px;">@yield('content')</div>
    </div>
</div>
{{--<script>--}}
{{--    navigator.serviceWorker.getRegistrations().then(function(registrations) {--}}
{{--        for(let registration of registrations) {--}}
{{--            registration.unregister()--}}
{{--        } })--}}
{{--</script>--}}
<!-- All Jquery -->
<script src="/js/admin_app.js"></script>
<script src="/js/backend/sidebarmenu.js"></script>
<script src="/js/backend/perfect-scrollbar.jquery.min.js"></script>
<script src="/js/backend/custom.min.js"></script>

@yield('scripts')

</body>
</html>

