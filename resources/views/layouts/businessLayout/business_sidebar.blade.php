@php $userDetails=auth()->user() @endphp

<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('businessadmin')}}" aria-expanded="false"><i
                            class="fa fa-tachometer-alt"></i><span class="hide-menu">داشبورد </span></a></li>

                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('businessadmin/products') }}" aria-expanded="false"><i
                            class="fa fa-braille"></i><span class="hide-menu">تمام کالاها </span>
                    </a>
                </li>

                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('profile/my-comments')}}" aria-expanded="false"><i
                            class="fa fa-comment"></i><span class="hide-menu">کامنت ها </span></a>
                </li>

                @php
                if(auth()->user()->isBusiness){
                    $business = \App\Business::where('user_id', auth()->id())->first();
                    $products = $business->products;
                    $orders = 0;
                    foreach ($products as $prod) {
                    $orders = \App\OrdersProduct::where('product_id', $prod->id)->count();
                }
                    }
                @endphp


                <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark"
                                            href="javascript:void(0)" aria-expanded="false"><i
                            class="fa fa-history"></i><span class="hide-menu">تاریخچه مالی </span>
                        @if( ($orders) > 0)
                                &nbsp;<span class="badge badge-pill badge-warning">{{$orders}}</span>
                        @endif
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="{{ url('businessadmin/sold')}}" class="sidebar-link"><i
                                    class="fa fa-box"></i><span class="hide-menu"> سابقه فروش ها </span>
                                @if( ($orders) > 0)
                                    &nbsp;<span class="badge badge-pill badge-warning">{{$orders}}</span>
                                @endif
                            </a>
                        </li>
                        <li class="sidebar-item"><a href="{{ url('profile/orders')}}" class="sidebar-link"><i
                                    class="fa fa-box-open"></i><span class="hide-menu">سابقه خریدها </span></a></li>
                    </ul>
                </li>

{{--                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"--}}
{{--                                            href="{{ action('Business\DiscountCodeController@index')}}"--}}
{{--                                            aria-expanded="false"><i--}}
{{--                                class="fa fa-tags"></i><span class="hide-menu"> کد تخفیف </span></a>--}}
{{--                </li>--}}

                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('/profile/wallet')}}" aria-expanded="false"><i
                            class="fa fa-wallet"></i><span class="hide-menu">کیف پول </span></a>
                </li>

                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('/profile')}}" aria-expanded="false"><i
                            class="fa fa-wrench"></i><span class="hide-menu">تنظیمات حساب کاربری </span></a>
                </li>

                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('business-information', auth()->user()->business->id)}}"
                                            aria-expanded="false"><i
                            class="fa fa-cogs"></i><span class="hide-menu">اطلاعات کسب و کار شما </span></a>
                </li>

                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('profile/inbox') }}" aria-expanded="false"><i
                            class="fa fa-envelope"></i><span class="hide-menu">صندوق پیام </span>
                        @if(\App\Inbox::where(['user_id'=> $userDetails->id,'seen'=>'0'])->count()>0)
                            &nbsp;<span
                                class="badge badge-pill badge-primary" id="msgCountBadge">{{\App\Inbox::where(['user_id'=> $userDetails->id,'seen'=>'0'])->count()}}</span>
                        @endif

                    </a>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
