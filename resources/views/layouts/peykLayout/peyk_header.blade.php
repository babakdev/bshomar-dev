<?php

use App\Http\Controllers\Controller;

if (auth()->check()) {
    $userDetails = auth()->user();
}
?>
<!-- Topbar header - style you can find in pages.scss -->
<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header" style="background-color: #e8e8e8" data-logobg="skin5">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                    class="ti-menu ti-close text-dark"></i></a>
            <!-- Logo -->
            <a class="navbar-brand" href="https://bshomar.com">
                <!-- Logo icon -->
                <b class="logo-icon p-l-10">
                    <img src="{{asset('images/backend_images/logo-icon.png')}}" alt="homepage"
                         class="light-logo"/>

                </b>
                <!-- Logo text -->
                <span class="logo-text">
                             <!-- dark Logo text -->
                             <img src="{{asset('images/backend_images/logo-text.png')}}" alt="homepage"
                                  class="light-logo"/>
                        </span>
            </a>
        </div>
        <!-- End Logo -->
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <!-- toggle and nav items -->
            <ul class="navbar-nav float-left mr-auto">
            </ul>
            <ul>
                <li class="list-unstyled">
                    <span class="nav-link text-dark"> {{ auth()->user()->name }}</span>
                </li>
            </ul>
            <!-- Right side toggle and nav items -->
            <ul class="navbar-nav float-right">

                <!-- User profile and search -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href=""
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-caret-down"></i>
                        <img
                            src="{{asset($userDetails->image ? '/images/users/'. $userDetails->image : '/images/users/user.png')}}"
                            alt="user" class="rounded-circle"
                            width="31"></a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();"><i class="ti-power-off m-r-5 m-l-5"></i>خروج
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </li>
                <!-- User profile and search -->
            </ul>
        </div>
    </nav>
</header>
<!-- End Topbar header -->
