<!DOCTYPE html>
<html dir="ltr" lang="fa">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="بی شمار">
    <meta name="author" content="Babak Moeinifar - babakus7@gmail.com - instagram@babakmoeini">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/backend_images/favicon.png')}}">
    <title>Bshomar Admin Panel</title>
    <link href="{{asset('css/admin_style.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/front_css/dropify.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" rel="stylesheet">
    <style>
        body {
            background-image: none !important;
        }

        input.is-danger {
            border-color: orangered;
        }
        @font-face {
            font-family: yekan;
            src: url('{{asset('css/fonts/yekan.ttf')}}');
        }

        body {
            font-family: yekan, sans-serif;
            font-size: 12px;
        }
    </style>

@yield('styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="main-wrapper">
    @include('layouts.adminLayout.admin_header')
    @include('layouts.adminLayout.admin_sidebar')

    <div class="page-wrapper" id="app">
        @yield('content')
    </div>
</div>


<script src="{{asset('js/admin_app.js')}}"></script>
<script src="{{asset('js/backend/sidebarmenu.js')}}"></script>
<script src="{{asset('js/backend/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('js/backend/admincustom.min.js')}}"></script>

@yield('scripts')

</body>
</html>

