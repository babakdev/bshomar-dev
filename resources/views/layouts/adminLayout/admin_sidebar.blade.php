<aside class="left-sidebar" data-sidebarbg="skin5">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ auth()->user()->isAdmin ? url('irenadmin') : url('irenadmin/supporter') }}"
                                            aria-expanded="false"><i
                            class="fa fa-tachometer-alt"></i><span class="hide-menu">داشبورد </span></a></li>
                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                            href="{{ url('/irenadmin/charts') }}"
                                            aria-expanded="false"><i class="fa fa-chart-area"></i><span
                            class="hide-menu">نمودارها </span></a>
                </li>

                <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark"
                                            href="javascript:void(0)" aria-expanded="false"><i
                            class="fa fa-user"></i><span class="hide-menu">کاربران </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark"
                                                    href="{{ url('/users') }}" aria-expanded="false"><i
                                    class="fa fa-users"></i>
                                <span class="hide-menu">مدیریت کاربران </span>
                            </a>
                        </li>
                        <li class="sidebar-item row"><a href="{{url('/disactive-users')}}"
                                                        class="sidebar-link"><i
                                    class="fa fa-check-square">
                                </i><span class="hide-menu"> کاربران غیرفعال </span>&nbsp;
                                @php($disUsers = \App\User::where('is_active',0)->count())
                                @if($disUsers > 0)
                                    <span class="badge badge-pill badge-danger">{{$disUsers}}</span>
                                @endif</a>
                        </li>
                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark"
                                                    href="{{ url('/asnaf') }}" aria-expanded="false"><i
                                    class="fa fa-user-friends"></i>
                                <span class="hide-menu">پشتیبانهای اصناف </span>
                            </a>
                        </li>

                        @if(auth()->user()->isAdmin)
                            <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark"
                                                        href="{{ url('/group-message') }}" aria-expanded="false"><i
                                        class="fa fa-newspaper"></i><span
                                        class="hide-menu">ارسال پیام گروهی </span></a>
                            </li>

                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark"
                                   href="{{ url('/sent-msgs') }}" aria-expanded="false"><i
                                        class="fas fa-backward"></i>
                                    <span class="hide-menu">پیام های ارسال شده </span></a>
                            </li>
                        @endif
                    </ul>
                </li>

                {{--Business Section--}}
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark"
                       href="javascript:void(0)" aria-expanded="false"><i
                            class="fa fa-suitcase"></i><span class="hide-menu">کسب و کارها </span>
                        &nbsp;
                        @php($disBusiness = \App\Business::where('is_active',0)->count())
                        @if($disBusiness>0)
                            <span class="badge badge-pill badge-danger">{{$disBusiness}}</span>
                        @endif
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item row"><a href="{{url('/need-approve-businesses')}}"
                                                        class="sidebar-link"><i
                                    class="fa fa-check-square">
                                </i><span class="hide-menu"> کسب وکارهای جدید یا نیازمند تایید </span>&nbsp;
                                @if($disBusiness>0)
                                    <span
                                        class="badge badge-pill badge-danger">{{$disBusiness}}</span>
                                @endif</a>
                        </li>

                        <li class="sidebar-item row"><a href="{{url('/approved-businesses')}}"
                                                        class="sidebar-link">
                                <i class="fa fa-suitcase"></i>
                                <span class="hide-menu" dir="rtl"> مدیریت کسب و کارها </span>
                            </a>
                        </li>
                    </ul>
                </li>

                {{--Products Section--}}
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark"
                       href="javascript:void(0)" aria-expanded="false"><i
                            class="fa fa-shopping-basket"></i><span class="hide-menu">کالاها </span>
                        &nbsp;
                        @php( $notActiveProds = \App\Product::where('is_active',0)->count())
                        @if($notActiveProds)
                            <span class="badge badge-pill badge-warning">{{$notActiveProds}}</span>
                        @endif
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item row"><a href="{{ url('/need-approve-products') }}"
                                                        class="sidebar-link"><i
                                    class="fa fa-check-square">
                                </i><span class="hide-menu"> کالاهای جدید </span>&nbsp;
                                @if($notActiveProds>0)
                                    <span
                                        class="badge badge-pill badge-warning">{{$notActiveProds}}</span>
                                @endif</a>
                        </li>
                        <li class="sidebar-item"><a href="{{ url('/products') }}" class="sidebar-link"><i
                                    class="fa fa-shopping-basket"></i><span
                                    class="hide-menu"> تمام کالاها </span></a>
                        </li>
                    </ul>
                </li>

                @if(auth()->user()->isAdmin || auth()->user()->isSupport)

                    <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark"
                                                href="javascript:void(0)" aria-expanded="false"><i
                                class="fa fa-history"></i><span class="hide-menu">تاریخچه فروش </span>&nbsp;
                            @php($payedOrders = \App\Order::join('order_product', 'order_product.order_id', 'orders.id')
                                ->where('orders.status', 1)
                                ->join('payments', 'payments.invoice_id', 'orders.invoice_id')
                                ->where(['payments.payed' => 1, 'payments.payment_method' => 1, 'order_product.status' => 'پرداخت شد'])
                                ->where('order_product.status', '!=', 'کنسل شد')
                                ->where('order_product.status', '!=', 'تسویه شد')
                                ->where('order_product.status', '!=', 'ارسال شد')->count())
                            @if($payedOrders)
                                <span class="badge badge-pill badge-warning">جدید</span>
                            @endif

                            @php($sentOrders = \App\OrdersProduct::where(['order_product.status'=>'ارسال شد'])
                                                    ->join('orders', 'orders.id', 'order_product.order_id')
                                                    ->join('payments', 'payments.order_id', 'orders.id')
                                                    ->where(function($query){
                                                        $query->where([['payments.payment_method', '!=', 2]])
                                                        ->orWhere([ ['order_product.delivery_code', '!=', 'با پیک ارسال شد.']]);
                                                    })->count()
                                 )
                            @if($sentOrders)
                                <span class="badge badge-pill badge-success">{{$sentOrders}}</span>
                            @endif
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="{{ url('/new-sales')}}" class="sidebar-link"><i
                                        class="fas fa-exclamation-triangle"></i><span
                                        class="hide-menu"> جدید ارسال نشده </span>
                                    @if($payedOrders>0)
                                        <span class="badge badge-pill badge-warning"
                                              dir="rtl"><strong>دارد</strong>
                                    </span>
                                    @endif
                                </a>
                            </li>
                            <li class="sidebar-item"><a href="{{ url('/sales-checkout')}}" class="sidebar-link"><i
                                        class="far fa-share-square"></i><span
                                        class="hide-menu"> ارسال شده قابل تسویه </span>&nbsp;
                                    @if($sentOrders>0)
                                        <span class="badge badge-pill badge-success"
                                              dir="rtl"><strong>{{$sentOrders}}</strong>
                                    </span>
                                    @endif
                                </a>
                            </li>
                            <li class="sidebar-item"><a href="{{ url('/sales-at-home')}}" class="sidebar-link"><i
                                        class="fa fa-home"></i><span
                                        class="hide-menu"> سابقه فروش های درب منزل </span></a>
                            </li>
                            <li class="sidebar-item"><a href="{{ url('/sales')}}" class="sidebar-link"><i
                                        class="fa fa-box"></i><span
                                        class="hide-menu"> سابقه کل فروش های نهایی شده </span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                href="{{ url('/finance') }}" aria-expanded="false">
                            <?php $newWithdrawRequests = \DB::table('transactions')->where('confirmed', 0)->count();?>
                            <i class="fa fa-wallet"></i><span class="hide-menu">مالی - کیف های پول </span>
                            @if($newWithdrawRequests > 0)
                                &nbsp;
                                <span class="badge badge-pill badge-danger border border-light"
                                      dir="rtl"><strong>{{ $newWithdrawRequests }}</strong></span>
                            @endif
                        </a>
                    </li>

                    <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                href="{{ url('/comments') }}" aria-expanded="false"><i
                                class="fa fa-comment"></i><span class="hide-menu">کامنت ها </span>
                            <?php $comments = \App\Comment::where(['is_active' => 0, 'comment_id' => null])->count(); ?>
                            @if($comments>0)
                                &nbsp;<span class="badge badge-pill badge-warning">{{ $comments }}نظر</span>
                            @endif
                            <?php $commentsAnswers = \App\Comment::where(['is_active' => 0])->where('comment_id', '!=', null)->count(); ?>
                            @if($comments>0)
                                &nbsp;<span class="badge badge-pill badge-light">{{ $commentsAnswers }}پاسخ</span>
                            @endif
                        </a>
                    </li>

                    <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                href="{{ url('/inbox') }}" aria-expanded="false"><i
                                class="fa fa-envelope"></i><span class="hide-menu">صندوق پیام </span>
                            @php($newMsgs = \App\Inbox::where(['seen'=>'0', 'user_id' => '1'])->count())
                            @if($newMsgs)
                                &nbsp;<span class="badge badge-pill badge-primary">{{$newMsgs}}</span>
                            @endif
                        </a>
                    </li>

                    <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                href="{{ url('/complaints') }}" aria-expanded="false"><i
                                class="fa fa-envelope-square"></i><span class="hide-menu">شکایات مردمی </span>
                            @php($newMsgs = \App\Complaint::where(['seen'=>'0'])->count())
                            @if($newMsgs)
                                &nbsp;<span class="badge badge-pill badge-danger">{{$newMsgs}}</span>
                            @endif
                        </a>
                    </li>

                    @if(auth()->user()->isAdmin)
                        <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark"
                                                    href="javascript:void(0)" aria-expanded="false"><i
                                    class="fa fa-images"></i><span class="hide-menu">اسلایدها و تبلیغات </span>
                            </a>
                            <ul aria-expanded="false" class="collapse  first-level">

                                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                            href="{{ url('irenadmin/slides') }}"
                                                            aria-expanded="false"><i class="fa fa-images"></i><span
                                            class="hide-menu">اسلاید اصلی </span></a>
                                </li>

                                <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark"
                                                            href="{{url('irenadmin/advertising')}}"
                                                            aria-expanded="false"><i
                                            class="fa fa-film"></i><span
                                            class="hide-menu"> مدیریت تبلیغات </span></a>
                                </li>
                            </ul>
                        </li>
                    @endif

                    @if(auth()->user()->isAdmin)
                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                    href="{{ url('irenadmin/categories') }}"
                                                    aria-expanded="false"><i class="fa fa-list"></i><span
                                    class="hide-menu">دسته بندی ها </span></a>
                        </li>

                        <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                    href="{{url('/telescope ')}}"
                                                    aria-expanded="false"><i class="fa fa-cog"></i><span
                                    class="hide-menu">گزارش خطاها </span></a>
                        </li>

                        {{--<li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"--}}
                        {{--href="{{url('sms-admin')}}"--}}
                        {{--aria-expanded="false"><i class="far fa-comment"></i><span--}}
                        {{--class="hide-menu">گزارش پیامک ها </span></a>--}}
                        {{--</li>--}}
                    @endif

                    <li class="sidebar-item"><a class="sidebar-link waves-effect waves-dark sidebar-link"
                                                href="{{ url('irenadmin/learns') }}"
                                                aria-expanded="false"><i class="fa fa-book"></i><span
                                class="hide-menu">آموزش ها </span></a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</aside>
