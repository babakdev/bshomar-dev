@if(Session::has('warning'))
    <div class="alert alert-danger alert-block alert-dismissible fade show text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong style="font-size: 18px">{!! session('warning') !!}</strong>
    </div>
@endif
