@if(count($errors) > 0)
    <div class="text-center" dir="rtl">
        <div class="alert alert-danger">
            <ul class="list-style-none">
                @foreach($errors->all() as $error)
                    <li class="list-unstyled">{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif