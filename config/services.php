<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_APP_ID','761851081560-qbo1hqvimvdp1d1bk9ui44ue80r3oubf.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_APP_SECRET','9OSDaF7nkY3cdwqauhRkSWZT'),
        'redirect' => env('GOOGLE_REDIRECT','https://bshomar.com/register/google/callback')
    ],

    'linkedin' => [
        'client_id' => env('LINKEDIN_APP_ID','77acg9akbaqgdv'),
        'client_secret' => env('LINKEDIN_APP_SECRET','zSIoEalsJvOV1HsF'),
        'redirect' => env('LINKEDIN_REDIRECT','http://bshomar.com/register/linkedin/callback')
    ]

];
