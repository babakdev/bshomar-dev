#-----Business-----
## manu store
**Url:  baseUrl + /api/business/productive/manu**
**Method: post**
## manu update
**Url:  baseUrl + /api/business/productive/manu/{id}**
**Method: patch**
## manu delete
**Url:  baseUrl + /api/business/productive/manu/{id}**
**Method: delete**

#### Fields:
token (header)
<br>
قدرت تولید
<br>
آیا تمایل به نمایش خط تولید خود دارید؟
<br>	
نام مرحله تولید	**name***
<br>
شرح این مرحله از تولید	**description*** 
<br> 
تصویر از مرحله تولید	**image***

## qc store
**Url:  baseUrl + /api/business/productive/qc**
**Method: post**
## qc update
**Url:  baseUrl + /api/business/productive/qc/{id}**
**Method: patch**
## qc delete
**Url:  baseUrl + /api/business/productive/qc/{id}**
**Method: delete**

#### Fields:
token (header)
<br>
کنترل کیفیت
<br>
آیا بخش کنترل کیفیت دارید؟	**has_qc***
<br>
نام مرحله کنترلینگ	**name***
<br>
شرح این مرحله از کنترل کیفیت	**description***
<br> 
تصویر از واحد کنترل کیفیت 	**image***

## rnd store
**Url:  baseUrl + /api/business/productive/rnd**
**Method: post**
## rnd update
**Url:  baseUrl + /api/business/productive/rnd/{id}**
**Method: patch**
## rnd delete
**Url:  baseUrl + /api/business/productive/rnd/{id}**
**Method: delete**

#### Fields:
token (header)
<br>
تحقیق و توسعه
<br>
آیا مرحله تحقیق و توسعه دارید؟	**has_rnd*** 
<br>
نام مرحله تحقیق و توسعه	**name*** 
<br>
شرح این مرحله از تحقیق و توسعه	**description***
<br>
تصویر از واحد تحقیق و توسعه	**image***


## certificate store
**Url:  baseUrl + /api/business/productive/certificate**
**Method: post**
## certificate update
**Url:  baseUrl + /api/business/productive/certificate/{id}**
**Method: patch**
## certificate delete
**Url:  baseUrl + /api/business/productive/certificate/{id}**
**Method: delete**

#### Fields:
token (header)
<br>
گواهینامه ها
<br>
آیا گواهینامه دارید؟	**has_cert***
<br>
صادر شده به نام	**cert_owner_name***
<br>
نوع گواهینامه	**cert_type***
<br>
شماره گواهینامه	**cert_num***
<br>
عنوان گواهینامه	**cert_name***
<br>
صادرکننده گواهینامه	**cert_exporter***
<br>
تاریخ شروع دوره	**cert_start_date***
<br>
تاریخ پایان دوره	**cert_end_date**
<br>
محدوده اعتبار گواهینامه (کدام کشورها)	**cert_scop***
<br>
تصویر گواهینامه	**cert_image***



## export store
**Url:  baseUrl + /api/business/productive/export**
**Method: post**
## export update
**Url:  baseUrl + /api/business/productive/export/{id}**
**Method: patch**
## export delete
**Url:  baseUrl + /api/business/productive/export/{id}**
**Method: delete**

#### Fields:
token (header)
<br>
صادرات
<br>
آیا صادرات دارید؟	**has_export***
<br>
ارزش صادرات	**ex_value***
<br>
چند درصد از تولید صادر میشود؟	**ex_of_products**
<br>
بازارهای هدف	**target_market***
<br>
سال شروع صادرات	**ex_start_year**
<br>
روش صادرات	**ex_type**
<br>
میانگین زمان تحویل محموله صادراتی	**ex_avg**
<br>
قوانین بین المللی صادرات که در شرکت ما اجرایی هستند	**ex_rules**
<br>
زبان هایی که برای مشتریان، پشتیبانی می کنیم	**langs**

## customer export store
**Url:  baseUrl + /api/business/productive/customer/export**
**Method: post**
## customer export update
**Url:  baseUrl + /api/business/productive/customer/export/{id}**
**Method: patch**
## customer export delete
**Url:  baseUrl + /api/business/productive/customer/export/{id}**
**Method: delete**

#### Fields:
token (header)
<br>
در صورت تمایل می توانید سابقه ای از مشتریان مقصد خود در کشورهای دیگر وارد نمایید	**has_cust***
نام شرکت واردکننده محصول ما	**cust_name***
<br>
کشور مقصد صادرات	**cust_country***
<br>
ایالت / استان مقصد صادرات	**cust_state***
<br>
محصول صادراتی به این شرکت	**cust_product_name***
<br>
ترن اور سالیانه با این شرکت	**cust_turnover**
<br>
تصویر شرکت واردکننده محصول ما و جلسه عقد قرارداد	**cust_image***


## Url:  baseUrl + /api/business/distributive/update
#### Method: post
#### Fields:
token (header)
<br>
نمایندگی ها
<br>
آیا تمایل به نمایش نمایندگی های خود دارید؟	 **has_agency***
<br>
اطلاعات نمایندگی ها	**agencies***


## Url:  baseUrl + /api/business/service/update
#### Method: post
#### Fields: 
token (header)
<br>
خدمات
<br>
آیا تمایل به نمایش اطلاعات خدمات خود را دارید؟	**has_service***
<br>
نوع خدمت؟ (حضوری غیرحضوری)	**service_type*** 
<br>
نحوه ارائه خدمت	**service_info***

#-----Product-----

#index
## Url:  baseUrl + /api/business/products
#### Method: get
#### Fields:
**token**
**i**


#store
## Url:  baseUrl + /api/business/products
#### Method: post
#### Fields:
**token**
**name***
**unit***
**price**
**discount**
**discount_cycle**
**stock**
**min_stock**
**code**
**time_to_delivery***
**country***
**meta_keywords**
**description**
**consider**
**image***
**properties**
**benefits**

#update
## Url:  baseUrl + /api/business/products/update/{id}
#### Method: post
#### Fields:
**token**

#delete
## Url:  baseUrl + /api/business/products/{id}
#### Method: delete
#### Fields:
**token**

#deleteImage
## Url:  baseUrl + /api/business/product/image/delete/{id}
#### Method: delete
#### Fields:
**token**

#filter
## Url:  baseUrl + /api/business/products/filter
#### Method: post
#### Fields:
**i***
**filter***
**name**


#productUnavailable
## Url:  baseUrl + /api/business/make/product/unavailable
#### Method: post
#### Fields:
**id***
**token***


#------------Client----------
#splash
## Url:  baseUrl + /api/client/splash
#### Method: get

#search
## Url:  baseUrl + /api/client/search
#### Method: post
#### Fields:
**text**
**type** 
(1: search in products , 2: search in businesses)
**i**

#newsletter
## Url:  baseUrl + /api/client/newsletter
#### Method: post
#### Fields:
**email**

#get sub categories
## Url:  baseUrl + /api/client/get/subcategoriesh
#### Method: get
#### Fields:
**cat_id**
**i**

#all categories
## Url:  baseUrl + /api/client/get/allcategories
#### Method: get
#### Fields:
**cat_id**
**i**

#best business in index
## Url:  baseUrl + /api/client/get/best/business
#### Method: get
#### Fields:
**cat_id**
**i**

#best seller in index
## Url:  baseUrl + /api/client/get/best/seller
#### Method: get
#### Fields:
**cat_id**
**i**

#best products in index
## Url:  baseUrl + /api/client/get/best/product
#### Method: get
#### Fields:
**cat_id**
**i**

#best selling in index
## Url:  baseUrl + /api/client/get/best/selling
#### Method: get
#### Fields:
**cat_id**
**i**

#filter
## Url:  baseUrl + /api/client/filter
#### Method: get
#### Fields:
**cat_id**
**type**
(type = 1 => products , type = 2 => businesses)
**i**
######for type = 1
**is_available**
**has_discount**
**country**
**min_price**
**max_price**
**state_id**
**city_id**

######for type = 2
**business_type**
**state_id**
**city_id**
**b2b**
**b2c**
**is_seller**
**has_export**

#single product
## Url:  baseUrl + /api/client/get/product/details
#### Method: get
#### Fields:
**id**

# toggle favorite product
## Url:  baseUrl + /api/client/toggle/favorite/product
#### Method: post
#### Fields:
**token**
**product_id**

# get products by business
## Url:  baseUrl + /api/client/toggle/favorite/product
#### Method: get
#### Fields:
**i**
**business_id**

# single business
## Url:  baseUrl + /api/client/get/business/details
#### Method: get
#### Fields:
**id**

# newest businesses
## Url:  baseUrl + /api/client/get/newest/businesses
#### Method: get
#### Fields:
**i**

# most visited businesses
## Url:  baseUrl + /api/client/get/most/visited/businesses
#### Method: get
#### Fields:
**i**

# categories of learning
## Url:  baseUrl + /api/client/get/categories/learning
#### Method: get

# learning by category
## Url:  baseUrl + /api/client/get/learning
#### Method: get
#### Fields:
**category_id**


# get cities
## Url:  baseUrl + /api/client/get/cities
#### Method: get
#### Fields:
**state_id**


