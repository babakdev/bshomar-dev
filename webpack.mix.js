const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/js/admin_app.js', 'public/js');

mix.js('resources/js/jquery.custom-file-input.js', 'public/js/front_js');
mix.js('resources/js/jquery.validate.js', 'public/js/front_js');
mix.js('resources/js/scripts.js', 'public/js/front_js');

mix.styles('resources/sass/persianDatepicker-default.css', 'public/css/persianDatepicker-default.css');
