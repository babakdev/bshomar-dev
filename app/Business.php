<?php

namespace App;

use App\BusinessModels\Certificate;
use App\BusinessModels\CustomerExport;
use App\BusinessModels\Export;
use App\BusinessModels\ManufacturingLine;
use App\BusinessModels\Qc;
use App\BusinessModels\ResearchDevelop;
use App\BusinessModels\Service;
use DateTimeInterface;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic;

class Business extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id', 'category_id', 'city_id', 'name', 'address', 'email', 'is_seller', 'expires_at',
        'latitude', 'longitude', 'phone', 'b2b', 'b2c', 'description', 'sheba', 'feature',
        'has_qc', 'product_lines', 'header_image', 'logo', 'advantages', 'tax', 'is_active',
        'website', 'found_date', 'business_type', 'agencies', 'panel_id', 'min_shipping_cost', 'shipping_cost'
    ];

    protected $casts = ['expires_at' => 'date:Y-m-d'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function path()
    {
        return "/{$this->id}/" . str_replace(' ', '-', $this->name);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

    public function customerExports()
    {
        return $this->hasMany(CustomerExport::class);
    }

    public function export()
    {
        return $this->hasOne(Export::class);
    }

    public function manufacturingLines()
    {
        return $this->hasMany(ManufacturingLine::class);
    }

    public function qc()
    {
        return $this->hasMany(Qc::class);
    }

    public function researchDevelop()
    {
        return $this->hasMany(ResearchDevelop::class);
    }

    public function service()
    {
        return $this->hasOne(Service::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }

    public function visits()
    {
        return new Visits($this);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function panel()
    {
        return $this->belongsTo(Panel::class);
    }

    public function image()
    {
        if (count($this->images) > 0) {
            $image = asset('images/front_images/businesses/small/' . $this->images[0]->path);
        } elseif ($this->category) {
            $image = asset('/images/front_images/cats/' . $this->category->image);
        } else {
            $image = asset('images/front_images/NoImageAvailable.webp');
        }
        return $image;
    }

    public function checkPanelProducts($panel, $productsLimit)
    {
        if ($this->panel_id == $panel && $this->products()->count() >= $productsLimit) {
            return redirect('businessadmin/products')->with('flash_error', 'تعداد کالای مجاز برای شما در این پنل به پایان رسید، می توانید کالای را ویرایش یا حذف کنید و کالایی جایگزین کنید، و یا به پنل مجهزتری ارتقا پیدا کنید');
        }
    }

    public static function updateInfo($request, $business, $user)
    {
        $business->category_id = $request->category_id ?? $business->category_id;
        $business->city_id = $request->city_id ?? $business->city_id;
        $business->name = $request->name;
        $business->address = $request->address;
        $business->phone = $request->phone;
        $business->is_seller = 1;
        $business->sheba = $request->sheba;
        $business->email = $request->email;
        $business->is_active = 0;
        $business->latitude = $request->lat ?? $business->latitude;
        $business->longitude = $request->lng ?? $business->longitude;
//        $business->b2b = $request->b2b ?? 0;
//        $business->b2c = $request->b2c ?? 0;
        $business->description = $request->description;
//        $business->has_qc = $request->has_qc ?? $business->has_qc;
//        $business->product_lines = $request->product_lines ?? $business->product_lines;
        $business->advantages = $request->advantages ?? $business->advantages;
        $business->website = $request->website ?? $business->website;
        $business->found_date = null;
        $business->business_type = $request->business_type ?? $business->business_type;
        $business->kasb_number = $request->kasb_number;
        $business->min_shipping_cost = $request->min_shipping_cost;
        $business->shipping_cost = $request->shipping_cost;
        $business->rules = $request->rules;

        if ($request->hasFile('header_image')) {
            if ($business->header_image) {
                if (file_exists(public_path('/images/front_images/businesses/headers/' . $business->header_image))) {
                    unlink(public_path('/images/front_images/businesses/headers/' . $business->header_image));
                }
            }
            $file = $request->file('header_image');
            $filename = "h-{$business->id}-" . rand('111', '99999') . '.webp';
            $image_path = 'images/front_images/businesses/headers/' . $filename;
            ImageManagerStatic::make($file)->resize(825, 339)->encode('webp', 100)->save($image_path);

            $business->header_image = $filename;
        }

        if ($request->hasFile('logo')) {
            if ($business->logo) {
                if (file_exists(public_path('/images/front_images/businesses/logos/' . $business->logo))) {
                    unlink(public_path('/images/front_images/businesses/logos/' . $business->logo));
                }
            }
            $file = $request->file('logo');
            $filename = "l-{$business->id}-" . rand('111', '99999') . '.webp';
            $image_path = 'images/front_images/businesses/logos/' . $filename;
            ImageManagerStatic::make($file)->resize(150, 150)->encode('webp', 100)->save($image_path);

            $business->logo = $filename;
        }

        $business->save();

        // Save Image Section
        if ($request->hasFile('image') && count($business->images) + count($request->file('image')) > 4) {
            $response = ['message' => 'حداقل یک تصویر و حداکثر چهار تصویر برای کسب و کار مجاز است.'];
            return response()->json($response, 400);
        }
        if ($request->hasFile('image') && count($request->file('image')) > 4) {
            $response = ['message' => 'حداقل یک تصویر و حداکثر چهار تصویر برای کسب و کار مجاز است.'];
            return response()->json($response, 400);
        }
        if (count($business->images) == 0 && !$request->hasFile('image')) {
            $response = ['message' => 'حداقل یک تصویر و حداکثر چهار تصویر برای کسب و کار مجاز است.'];
            return response()->json($response, 400);
        }

        $business->saveImage($request, $business);

        $business_info = null;
        if ($user != null) {
            $business_info = self::where(['businesses.user_id' => $user->id])
                ->join('categories', 'categories.id', 'businesses.category_id')
                ->join('cities', 'cities.id', 'businesses.city_id')
                ->join('cities as states', 'cities.parent_id', 'states.id')
                ->select('businesses.*', 'businesses.category_id as sub_category_id', 'categories.parent_id as category_id', 'states.id as state_id',
                    DB::raw('CONCAT("/images/front_images/businesses/headers/",businesses.header_image) as header_image'),
                    DB::raw('CONCAT("/images/front_images/businesses/logos/",businesses.logo) as logo'))
                ->first();
            $business_info->images = Image::where(['imageable_type' => Business::class, 'imageable_id' => $business->id])
                ->select('id', DB::raw('CONCAT("/images/front_images/businesses/large/",images.path) as path_large'),
                    DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->get();
        }
        if ($request->wantsJson()) {
            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'information' => $business_info], 200);
        }
    }

    public static function saveImage(Request $request, $business)
    {
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file) {
                $filename = rand(111, 99999) . '.webp';
                $image_path_small = 'images/front_images/businesses/small/' . $filename;
                $image_path_large = 'images/front_images/businesses/large/' . $filename;
                \Intervention\Image\ImageManagerStatic::make($file)->resize(180, 160)->encode('webp', 100)->save($image_path_small);
                \Intervention\Image\ImageManagerStatic::make($file)->resize(800, 711)->encode('webp', 100)->save($image_path_large);

                Image::create([
                    'imageable_id' => $business->id,
                    'imageable_type' => get_class($business),
                    'path' => $filename
                ]);
            }
        }
    }

}
