<?php

use App\Inbox;
use App\Invoice;
use App\Sms\FastSms;
use App\SmsIR_VerificationCode;
use App\User;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

function convertPersianNumsToEng($string)
{
    return str_replace(
        ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'],
        ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        $string);
}

function sendToInbox($user_id, $subject, $sent_by, $message, $sender_id = 1, $message_id = null)
{
    Inbox::create([
        'user_id' => $user_id,
        'message_id' => $message_id,
        'subject' => $subject,
        'sent_by' => $sent_by,
        'sender_id' => $sender_id,
        'message' => $message
    ]);
}

function saveImageFile($file, array $input, $with, $height, $path, $field = 'image'): array
{
    $filename = rand(111, 99999) . '.webp';
    $image_path = $path . $filename;
    Image::make($file)->resize($with, $height)->encode('webp', 100)->save($image_path);
    $input[$field] = $filename;
    return $input;
}

function saveMorphImageFile($file, $with, $height, $path, $model, $input): array
{
    $filename = rand(111, 99999) . '.webp';
    $image_path = $path . $filename;
    Image::make($file)->resize($with, $height)->encode('webp', 100)->save($image_path);

    $model::create([
        'imageable_id' => $input->id,
        'imageable_type' => get_class($input),
        'path' => $image_path
    ]);
}

function increaseUserRateWithNotify($userId, $unit)
{
    $user = User::findOrFail($userId);
    $user->score += $unit;
    if ($user->save()) {
        $msg = $user->name . ' گرامی , امتیاز شما ' . $unit . ' واحد افزایش یافت.';
        sendToInbox($userId, 'افزایش امتیاز', 'info@bshomar.com', $msg);
    }
}

function generateInvoiceId()
{
    $last = Invoice::first()->last_invoice_number;
    $string = generateRandomString();
    $result = $string . str_pad($last, 6, '0', STR_PAD_LEFT);
    Invoice::first()->update([
        'last_invoice_number' => \DB::raw('last_invoice_number+1'),
        'date_update' => Carbon::now()
    ]);
    return $result;
}

function generateRandomString($length = 4)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function convertPersianDateToStandardFormat($date)
{
    $ex_start = explode('/', $date);

    $day = sprintf("%02d", $ex_start[2]);
    $month = sprintf("%02d", $ex_start[1]);

    return implode('/', [$ex_start[0], $month, $day]);
}

//for one parameters
function sendSmsWithTemplate($templateId, $mobile, $parameter, $value)
{
    $smsData = [
        "ParameterArray" => [
            [
                "Parameter" => $parameter,
                "ParameterValue" => $value
            ]
        ],
        "Mobile" => $mobile,
        "TemplateId" => $templateId
    ];
    $fastSms = new FastSms();
    $fastSms->sendMessage($smsData);
}

/**
 * @param array $data
 * @param $user
 */
function GenCodeForUser(array $data, $user): void
{
    $code = rand(1111, 9999);

    $user->code = $code;

    User::where('phone', $data['phone'])->update(['code' => $code]);
    try {
        $APIKey = '7d8f414d3ca0120b76fd414d';
        $SecretKey = 'bshomar@iren32n53e';
        $APIURL = 'https://ws.sms.ir/';
        $MobileNumber = $data['phone'];

        $SmsIR_VerificationCode = new SmsIR_VerificationCode($APIKey, $SecretKey, $APIURL);
        $VerificationCode = $SmsIR_VerificationCode->verificationCode($code, $MobileNumber);
        var_dump($VerificationCode);

    } catch (Exeption $exept) {
        echo 'Error VerificationCode : ' . $exept->getMessage();
    }
    $user->save();
}

function SiteUrl()
{
    return 'https://bshomar.com';
}

function ServerUrl()
{
    return 'https://server.bshomar.com';
}

function checkDiscountExpireDate($date)
{
    $now = Carbon::now()->format('Y-m-d H:i:s');
    if ($date = !null && $date >= $now) {
        return true;
    }
    return false;
}
