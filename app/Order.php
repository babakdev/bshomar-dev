<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    public function ordersProduct()
    {
        return $this->hasMany(OrdersProduct::class, 'order_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function address()
    {
        return $this->belongsTo(DeliveryAddress::class, 'address_id');
    }

    public function discount()
    {
        return $this->belongsTo(DiscountCode::class, 'discount_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
