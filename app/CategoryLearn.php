<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLearn extends Model
{
    public $timestamps = false;

    protected $table = 'category_learns';

    protected $fillable = ['title', 'image'];

    public function learns()
    {
        return $this->hasMany(LearnPage::class);
    }
}
