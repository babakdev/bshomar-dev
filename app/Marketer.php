<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Marketer extends Model
{
    protected $fillable = ['business_id', 'marketer_id'];
    protected $softDelete = true;

    public function businessUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function marketerUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

}
