<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $subject;
    protected $message;

    /**
     * Create a new job instance.
     *
     * @param $id
     * @param $subject
     * @param $message
     */
    public function __construct($id, $subject, $message)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->message = $message;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        sendToInbox($this->id, $this->subject, 'info@bshomar.com', $this->message);
    }
}
