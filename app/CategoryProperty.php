<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class CategoryProperty extends Model
{
    protected $guarded = 'id';

    public function properties()
    {
        return $this->belongsTo(Property::class);
    }

    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
