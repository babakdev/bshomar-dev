<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'business_id','category_id', 'name', 'code', 'country', 'meta_keywords', 'feature',
         'unit', 'description', 'price', 'fake_price', 'is_active', 'stock' ,'min_stock', 'time_to_delivery' ,
        'consider' ,'status' ,'discount', 'discount_cycle','discount_expire_date','delivery_type','product_return','product_return_type'
    ];

    protected $dates = ['deleted_at'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function path()
    {
        return  'https://bshomar.com/product?id=' . $this->id;
    }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function visits()
    {
        return new Visits($this);
    }


    public function properties()
    {
        return $this->hasMany(ProductProperty::class);
    }

    public function benefitDefects()
    {
        return $this->hasMany(BenefitDefect::class);
    }

    public function saveImage(Request $request, $product)
    {
        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file) {
                $filename = rand(111, 99999) . '.webp';
                $image_path_small = 'images/front_images/products/small/' . $filename;
                $image_path_large = 'images/front_images/products/large/' . $filename;
                \Intervention\Image\ImageManagerStatic::make($file)->resize(180, 160)->encode('webp', 100)->save($image_path_small);
                \Intervention\Image\ImageManagerStatic::make($file)->resize(800, 711)->encode('webp', 100)->save($image_path_large);

                \App\Image::create([
                    'imageable_id' => $product->id,
                    'imageable_type' => get_class($product),
                    'path' => $filename
                ]);
            }
        }
    }

}
