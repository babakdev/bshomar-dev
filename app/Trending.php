<?php

namespace App;

use Illuminate\Support\Facades\Redis;

class Trending
{
    public function getProducts()
    {
        $trending = array_map('json_decode', Redis::zrevrange($this->productCacheKey(), 0, -1));
        return $trending;
    }

    public function getbusinesses()
    {
        return array_map('json_decode', Redis::zrevrange($this->businessCacheKey(), 0, -1));
    }

    // for asnaf - most visited businesses
    public function getBestBusinessesScore($items)
    {
        return array_map('json_decode', Redis::zrevrange($this->businessCacheKey(), 0, $items, 'WITHSCORES'));
    }

    public function pushProduct($product)
    {
        Redis::zincrby($this->productCacheKey(), 1, json_encode([
            'id' => $product->id,
//            'ip' => request()->getClientIp()
        ], JSON_THROW_ON_ERROR, 512));
    }

    public function pushBusiness($business)
    {
        Redis::zincrby($this->businessCacheKey(), 1, json_encode([
            'id' => $business->id,
//            'ip' => request()->getClientIp(),
        ], JSON_THROW_ON_ERROR, 512));
    }

    protected function productCacheKey()
    {
        return 'trending_products';
    }

    protected function businessCacheKey()
    {
        return 'trending_businesses';
    }

    public function getSellbusinesses($offset=0, $limit=-1)
    {
        return array_map('json_decode', Redis::zrevrange($this->businessSellCacheKey(), $offset, $limit));
    }

    public function pushSellBusiness($business)
    {
        Redis::zincrby($this->businessSellCacheKey(), 1, json_encode([
            'business' => $business,
            'category_id' => $business->category_id,
            'image' => $business->images[0]->path ?? '',
            'ip' => request()->getClientIp(),
        ]));
    }

    protected function businessSellCacheKey()
    {
        return 'trending_sell_businesses';
    }
}
