<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{
    protected $guarded = ['id'];

    protected $table = 'product_properties';

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function properties()
    {
        return $this->belongsTo(Property::class);
    }

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
