<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearnPage extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'category_id', 'body'];

    public function path()
    {
        return "https://bshomar.com/learning/{$this->id}/";
    }

    public function category()
    {
        return $this->belongsTo(CategoryLearn::class);
    }
}
