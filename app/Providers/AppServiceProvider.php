<?php

namespace App\Providers;

use App\Category;
use App\City;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') === 'production') {
            \URL::forceScheme('https');
        }

        Schema::defaultStringLength(191);

        \View::composer('*', function ($view) {
            $categories = Cache::rememberForever('categories', function () {
                return Category::all();
            });

            $view->with(['categories' => collect($categories)]);
        });


        \View::composer('*', function ($view) {
            $cities = Cache::rememberForever('cities', function () {
                return City::all();
            });
            $view->with(['cities' => collect($cities)]);

        });

        \View::composer('*', function ($view) {
            if (auth()->check() && auth()->user()->isAdmin) {
                $page = ('layouts.adminLayout.admin_design');
            } else {
                $page = ('layouts.supportLayout.support_design');
            }
            $view->with(['page' => $page]);
        });

        \Validator::extend('spamfree', 'App\Rules\SpamFree@passes');

//        Relation::morphMap([
//            'product' => Product::class,
//        ]);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
