<?php

namespace App;

use Illuminate\Support\Facades\Redis;

class Visits
{
    protected $relation;


    /**
     * Visits constructor.
     *
     * @param $relation
     */
    public function __construct($relation)
    {
        $this->relation = $relation;
    }

    public function reset()
    {
        Redis::del($this->cacheKey());
        return $this;
    }

    public function count()
    {
        return Redis::get($this->cacheKey()) ?? 0;
    }

    public function record()
    {
            Redis::incr($this->cacheKey());
            return $this;
    }

    public function cacheKey()
    {
        return get_class($this->relation).'.'."{$this->relation->id}.visits";
    }
}
