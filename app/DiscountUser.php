<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class DiscountUser extends Model
{
    protected $fillable = ['discount_id' ,'user_id','cart_id','used' ,'date_used'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
