<?php

namespace App;

use Bavix\Wallet\Interfaces\Confirmable;
use Bavix\Wallet\Interfaces\Wallet;
use Bavix\Wallet\Traits\CanConfirm;
use Bavix\Wallet\Traits\HasWallet;
use DateTimeInterface;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements Wallet, Confirmable, JWTSubject
{
    use Notifiable, HasWallet, CanConfirm;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password', 'image', 'is_active',
        'code', 'city_id', 'nationalcode', 'role_id', 'score',
        'last_login_at', 'last_login_ip', 'address', 'category_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $appends = ['isClient', 'isBusiness', 'isAdmin', 'isSupport', 'isAsnaf', 'isPeyk'];
//    protected $with = 'wallet';

    protected $casts = [
        'category_id' => 'array',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function order()
    {
        return $this->hasOne(Order::class);
    }

    public function isAdmin()
    {
        return $this->role_id === 1;
    }

    public function getIsAdminAttribute()
    {
        return $this->isAdmin();
    }

    public function isSupport()
    {
        return $this->role_id === 2;
    }

    public function getIsSupportAttribute()
    {
        return $this->isSupport();
    }

    public function isBusiness()
    {
        return $this->role_id === 3;
    }

    public function getIsBusinessAttribute()
    {
        return $this->isBusiness();
    }

    public function isClient()
    {
        return $this->role_id === 4;
    }

    public function getIsClientAttribute()
    {
        return $this->isClient();
    }

    public function isAsnaf()
    {
        return $this->role_id === 5;
    }

    public function getIsAsnafAttribute()
    {
        return $this->isAsnaf();
    }

    public function isPeyk()
    {
        return $this->role_id === 6;
    }

    public function getIsPeykAttribute()
    {
        return $this->isPeyk();
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function business()
    {
        return $this->hasOne(Business::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
