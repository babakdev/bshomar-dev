<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $fillable = ['subject','message','user_id','sent_by','sender_id','message_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
