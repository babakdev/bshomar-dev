<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['path', 'imageable_id', 'imageable_type'];

    // foreign keys should be here at boot method

    public function imageable()
    {
        return $this->morphTo();
    }
}
