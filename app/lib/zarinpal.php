<?php

namespace App\lib;

use Carbon\Carbon;
use DB;
use nusoap_client;

/*require_once 'nusoap.php';*/

class zarinpal
{
    protected $MerchantID;

    public function __construct()
    {
        $this->MerchantID = "e55ffa32-13ee-11e9-9579-005056a205be";
    }

    public function pay($order,$user_id, $total, $description, $phone, $date_create, $callback, $invoice_id, $type)
    {
        $client = new nusoap_client('https://www.zarinpal.com/pg/services/WebGate/wsdl', 'wsdl');
        $client->soap_defencoding = 'UTF-8';
        $result = $client->call('PaymentRequest', [
            [
                'MerchantID' => $this->MerchantID,
                'Amount' => $total,
                'Description' => $description,
                'Email' => '',
                'Mobile' => $phone,
                'CallbackURL' => $callback,
            ],
        ]);

        //Redirect to URL You can do it also by creating a form
        if ($result['Status'] == 100) {
            $authority = $result['Authority'];
            $this->InsertToDatabase($order,$user_id, $authority, $total, $invoice_id, $date_create, $type);
            return ('https://www.zarinpal.com/pg/StartPay/' . $result['Authority']);
        } else {
            return false;
        }
    }

    public function InsertToDatabase($order,$user_id, $authority, $amount, $invoice_id, $date_create, $type)
    {

        \DB::table('payments')->insert([
            'invoice_id' => $invoice_id,
            'order_id' => $order->id,
            'user_id' => $user_id,
            'price' => $amount,
            'authority' => $authority,
            'date_create' => $date_create,
            'payed' => 0,
            'reference' => 0,
            'payment_method' => 1,
            'gateway' => 'zarinpal',
            'type' => $type,
            'created_at' => Carbon::now('Asia/Tehran'),
        ]);
    }

}
