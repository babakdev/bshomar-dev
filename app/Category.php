<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'parent_id',
        'name',
        'is_active',
        'image',
        'slug',
        'meta_description',
        'meta_keywords'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function path()
    {
        return "categories/{$this->slug}";
    }

    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }


    /**
     * @param $url
     * @return Category|Model|object|null
     */
    public static function mainCat($url)
    {
        return self::where(['slug' => $url])->first();
    }

    /**
     * @param $main_cat
     * @return
     */
    public static function lev_cat2($main_cat)
    {
        return self::where(['parent_id' => $main_cat->id])->get();
    }
}
