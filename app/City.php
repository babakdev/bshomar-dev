<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;

    public function state() {
        return $this->belongsTo(City::class,'parent_id','id') ;
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }

}
