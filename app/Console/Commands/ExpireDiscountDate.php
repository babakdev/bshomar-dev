<?php

namespace App\Console\Commands;

use App\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireDiscountDate extends Command
{
    protected $products;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expireDiscountDate:null';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'if discount date will be expired this command run and discount value became null';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->products = Product::where('discount_expire_date', '!=', null)
            ->where('discount_expire_date', '<=', Carbon::now())
            ->get();

        foreach ($this->products as $product) {
            $product->discount_expire_date = null;
            $product->discount = 0;
            $product->discount_cycle = 0;
            $product->save();
        }
        error_log('Command successfully ran.');
    }
}
