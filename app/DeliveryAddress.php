<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryAddress extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public $timestamps = false;
    protected $fillable = ['name', 'user_id', 'city_id', 'address', 'post_code', 'phone', 'email'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
