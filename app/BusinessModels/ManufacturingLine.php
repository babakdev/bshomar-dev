<?php

namespace App\BusinessModels;

use App\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;

class ManufacturingLine extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public static function persist($data, $business)
    {
        $businessManus = self::where('business_id', $business->id)->count();

        if ($businessManus < 4) {
            $rules = [
                'name' => 'required | min:3 | max:150',
                'image' => 'required | image | mimes:jpeg,png,jpg,gif | max:15000',
                'description' => 'required | sometimes'
            ];
            $validator = Validator::make($data, $rules);
            if($validator->fails()){
                $response = ['message' => $validator->errors()->first()];
                return response()->json($response,400);
            }

            if (request()->hasFile('image')) {
                $file = request()->file('image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/manu/');
                }
            }

            $data['business_id'] = $business->id;
            self::create($data);

            $manus = ManufacturingLine::where('business_id', $business->id)
                ->select('id','name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/manu/",image) as image'))
                ->get();
            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'manufacturing' => $manus], 200);
        }
        else {
            $response = ['message' => 'حداکثر 4 آیتم به عنوان خط تولید میتوانید ثبت کنید.'];
            return response()->json($response,400);
        }
    }

    public static function updateItem($data,$id, $business)
    {
        $manu = ManufacturingLine::where('id', $id)->first();

        if(!$manu){
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        $rules = [
            'name' => 'required | sometimes | min:3 | max:150',
            'image' => 'nullable | image | max:15000',
            'description' => 'required | sometimes'
        ];
        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response,400);
        }

        if (request()->hasFile('image')) {
            if($manu->image){
                if (file_exists(public_path('/images/front_images/businesses/manu/' . $manu->image))) {
                    unlink(public_path('/images/front_images/businesses/manu/' . $manu->image));
                }
            }

            $file = request()->file('image');
            if ($file->isValid()) {
                $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/manu/');
            }
        }

        $data['business_id'] = $business->id;
        self::where('id',$id)->update($data);

        $manus = ManufacturingLine::where('business_id', $business->id)
            ->select('id','name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/manu/",image) as image'))
            ->get();
        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'manufacturing' => $manus], 200);
    }

    public static function adminPersist($data, $business)
    {
        $businessManus = self::where('business_id', $business->id)->count();

        if ($businessManus < 4) {
            request()->validate([
                'manu_name' => 'required | min:3 | max:150',
                'manu_image' => 'required | sometimes',
                'manu_image.*' => 'image|max:15000',
                'manu_desc' => 'required | sometimes'
            ]);

            if (request()->hasFile('manu_image')) {
                $file = request()->file('manu_image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/manu/');
                }
            }

            self::create([
                'business_id' => $business->id,
                'name' => $data['manu_name'],
                'description' => $data['manu_desc'],
                'image' => $data['image']
            ]);
        }
        else {
            return back()->with('flash_error', 'حداکثر 4 آیتم به عنوان خط تولید میتوانید ثبت نمایید');
        }
    }
}
