<?php

namespace App\BusinessModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Service extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public static function persist($data, $business)
    {
        $businessServices = self::where('business_id', $business->id)->count();
        $rules = [
            'service_type' => 'required | integer',
            'service_info' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if (request()->wantsJson()) {
            if ($validator->fails()) {
                $response = ['message' => $validator->errors()->first()];
                return response()->json($response, 400);
            }
        }
        self::updateOrCreate([
            'business_id' => $business->id,
            'type' => $data['service_type'],
            'service_info' => $data['service_info'],
        ]);

        if (request()->wantsJson()) {
            $service = Service::where('business_id', $business->id)->first();
            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'service' => $service], 200);
        }
    }

}
