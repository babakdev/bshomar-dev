<?php

namespace App\BusinessModels;

use App\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Export extends Model
{
    protected $fillable = ['business_id', 'ex_value', 'ex_of_products', 'target_market', 'ex_start_year', 'ex_type', 'ex_avg', 'ex_rules', 'langs'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public static function persist($data, $business)
    {
        $rules = [
            'ex_value' => 'required',
            'ex_of_products' => 'min:1 | max:100',
            'target_market' => 'required',
        ];
        $validator = Validator::make($data, $rules);
        if (request()->wantsJson()) {
            if($validator->fails()){
                $response = ['message' => $validator->errors()->first()];
                return response()->json($response,400);
            }
        }

        $ex = Export::where('business_id' , $business->id)->count();
        if($ex == 0){
            $data['business_id'] = $business->id;
            self::create($data);
        }else{
            $data['business_id'] = $business->id;
            self::where('business_id', $business->id)->update($data);
        }

        if (request()->wantsJson()) {
            $exports = Export::where('business_id', $business->id)
                ->select('id', 'ex_value', 'ex_of_products', 'target_market', 'ex_start_year', 'ex_type', 'ex_avg', 'ex_rules', 'langs')
                ->first();

            return response()->json(['message' => 'اطلاعات صادرات شما با موفقیت ثبت شد.', 'exports' => $exports], 200);
        } else {
            return back()->with('flash', 'اطلاعات صادرات شما با موفقیت ثبت شد.');
        }
    }

    public static function updateItem($data,$id,$business)
    {
        $ex = self::where('id', $id)->first();

        if(!$ex){
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        $rules = [
            'ex_value' => 'required | sometimes',
            'ex_of_products' => 'min:1 | max:100',
            'target_market' => 'required | sometimes',
        ];

        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response,400);
        }

        $data['business_id'] = $business->id;
        self::where('id', $id)->update($data);

        $exports = Export::where('business_id', $business->id)
            ->select('id','ex_value', 'ex_of_products', 'target_market', 'ex_start_year', 'ex_type', 'ex_avg', 'ex_rules', 'langs')
            ->get();


        return response()->json(['message' => 'اطلاعات صادرات شما با موفقیت ثبت شد.', 'exports' => $exports], 200);
    }
}
