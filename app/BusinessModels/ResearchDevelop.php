<?php

namespace App\BusinessModels;

use App\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class ResearchDevelop extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public static function persist($data, $business)
    {
        $businessRnds = self::where('business_id', $business->id)->count();

        if ($businessRnds < 4) {
            $rules = [
                'name' => 'required | min:3 | max:150',
                'image' => 'required | image | mimes:jpeg,png,jpg,gif | max:15000',
                'description' => 'required | sometimes'
            ];

            $validator = Validator::make($data, $rules);
            if (request()->wantsJson()) {
                if ($validator->fails()) {
                    $response = ['message' => $validator->errors()->first()];
                    return response()->json($response, 400);
                }
            }

            if (request()->hasFile('image')) {
                $file = request()->file('image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/rnd/');
                }
            }

            self::create([
                'business_id' => $business->id,
                'name' => $data['name'],
                'description' => $data['description'],
                'image' => $data['image']
            ]);

            if (request()->wantsJson()) {
                $rnd = ResearchDevelop::where('business_id', $business->id)
                    ->select('id', 'name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/rnd/",image) as image'))
                    ->get();
                return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'rnd' => $rnd], 200);

            } else {
                $response = ['message' => 'حداکثر 4 آیتم به عنوان واحد تحقیق و توسعه میتوانید ثبت کنید.'];
                return response()->json($response, 400);
            }
        }
    }

    public static function updateItem($data, $id, $business)
    {
        $rnd = ResearchDevelop::where('id', $id)->first();

        if (!$rnd) {
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        $rules = [
            'name' => 'required | sometimes | min:3 | max:150',
            'image' => 'nullable | image | mimes:jpeg,png,jpg,gif | max:15000',
            'description' => 'required | sometimes'
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        if (request()->hasFile('image')) {
            if ($rnd->image) {
                if (file_exists(public_path('/images/front_images/businesses/rnd/' . $rnd->image))) {
                    unlink(public_path('/images/front_images/businesses/rnd/' . $rnd->image));
                }
            }

            $file = request()->file('image');
            if ($file->isValid()) {
                $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/rnd/');
            }
        }

        $data['business_id'] = $business->id;
        self::where('id', $id)->update($data);

        $rnds = ResearchDevelop::where('business_id', $business->id)
            ->select('id', 'name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/rnd/",image) as image'))
            ->get();
        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'rnd' => $rnds], 200);
    }

    public static function adminPersist($data, $business)
    {
        $businessRnds = self::where('business_id', $business->id)->count();

        if ($businessRnds < 4) {
            request()->validate([
                'rnd_name' => 'required | min:3 | max:150',
                'rnd_image' => 'required | sometimes',
                'rnd_image.*' => 'image|max:15000',
                'rnd_desc' => 'required | sometimes'
            ]);

            if (request()->hasFile('rnd_image')) {
                $file = request()->file('rnd_image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/rnd/');
                }
            }

            self::create([
                'business_id' => $business->id,
                'name' => $data['rnd_name'],
                'description' => $data['rnd_desc'],
                'image' => $data['image']
            ]);
        } else {
            return back()->with('flash_error', 'حداکثر 4 آیتم به عنوان کنترل کیفیت میتوانید ثبت نمایید');
        }
    }
}
