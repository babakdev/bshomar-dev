<?php

namespace App\BusinessModels;

use App\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class CustomerExport extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public static function persist($data, $business)
    {
        $businessCusts = self::where('business_id', $business->id)->count();

        if ($businessCusts < 4) {
            $rules = [
                'cust_name' => 'required | min:3 | max:150',
                'cust_country' => 'required ',
                'cust_state' => 'required ',
                'cust_product_name' => 'required ',
                'cust_image' => 'required | image | mimes:jpeg,png,jpg,gif | max:15000',
            ];

            $validator = Validator::make($data, $rules);
            if (request()->wantsJson()) {
                if ($validator->fails()) {
                    $response = ['message' => $validator->errors()->first()];
                    return response()->json($response, 400);
                }
            }

            if (request()->hasFile('cust_image')) {
                $file = request()->file('cust_image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/cust/');
                }
            }

            self::create([
                'business_id' => $business->id,
                'name' => $data['cust_name'],
                'country' => $data['cust_country'],
                'state' => $data['cust_state'],
                'product_name' => $data['cust_product_name'],
                'turn_over_year' => $data['cust_turnover'] ?? '',
                'image' => $data['image']
            ]);
            if (request()->wantsJson()) {
                $custs = CustomerExport::where('business_id', $business->id)
                    ->select('id', 'name', 'country', 'state', 'product_name', 'turn_over_year',
                        \DB::raw('CONCAT("/images/front_images/businesses/cust/",image) as image'))
                    ->get();
                return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'customers' => $custs], 200);

            } else {
                $response = ['message' => 'حداکثر 4 آیتم به عنوان مشتریان واردات میتوانید ثبت کنید.'];
                return response()->json($response, 404);
            }
        }
    }

    public static function updateItem($data, $id, $business)
    {
        $cust = CustomerExport::where('id', $id)->first();

        if (!$cust) {
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        $rules = [
            'cust_name' => 'required | sometimes | min:3 | max:150',
            'cust_country' => 'required | sometimes',
            'cust_state' => 'required | sometimes',
            'cust_product_name' => 'required | sometimes',
            'cust_image' => 'nullable | image | mimes:jpeg,png,jpg,gif | max:15000',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 404);
        }


        if (request()->hasFile('cust_image')) {
            if ($cust->image) {
                if (file_exists(public_path('/images/front_images/businesses/cust/' . $cust->image))) {
                    unlink(public_path('/images/front_images/businesses/cust/' . $cust->image));
                }
            }

            $file = request()->file('cust_image');
            if ($file->isValid()) {
                $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/cust/');
            }
        }

        self::where('id', $id)->update([
            'business_id' => $business->id,
            'name' => $data['cust_name'] ?? $cust->name,
            'country' => $data['cust_country'] ?? $cust->country,
            'state' => $data['cust_state'] ?? $cust->state,
            'product_name' => $data['cust_product_name'] ?? $cust->product_name,
            'turn_over_year' => $data['cust_turnover'] ?? 0,
            'image' => $data['image'] ?? $cust->image
        ]);

        $custs = CustomerExport::where('business_id', $business->id)
            ->select('id', 'name', 'country', 'state', 'product_name', 'turn_over_year',
                \DB::raw('CONCAT("/images/front_images/businesses/cust/",image) as image'))
            ->get();
        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'customers' => $custs], 200);
    }

}
