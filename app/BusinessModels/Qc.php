<?php

namespace App\BusinessModels;

use App\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Qc extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public static function persist($data, $business)
    {
        $businessQs = self::where('business_id', $business->id)->count();

        if ($businessQs < 4) {
            $rules = [
                'name' => 'required | min:3 | max:150',
                'image' => 'required | image | mimes:jpeg,png,jpg,gif | max:15000',
                'description' => 'required | sometimes'
            ];
            $validator = Validator::make($data, $rules);
            if($validator->fails()){
                $response = ['message' => $validator->errors()->first()];
                return response()->json($response,400);
            }

            if (request()->hasFile('image')) {
                $file = request()->file('image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/qc/');
                }
            }

            self::create([
                'business_id' => $business->id,
                'name' => $data['name'],
                'description' => $data['description'],
                'image' => $data['image']
            ]);
            $qcs = Qc::where('business_id', $business->id)
                ->select('id','name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/qc/",image) as image'))
                ->get();
            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'qcs' => $qcs], 200);

        }
        else {
            $response = ['message' => 'حداکثر 4 آیتم کنترل کیفیت میتوانید ثبت کنید.'];
            return response()->json($response,400);
        }
    }

    public static function updateItem($data,$id, $business)
    {
        $qc = Qc::where('id', $id)->first();

        if(!$qc){
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        $rules = [
            'name' => 'required | sometimes | min:3 | max:150',
            'image' => 'nullable | image | mimes:jpeg,png,jpg,gif | max:15000',
            'description' => 'required | sometimes'
        ];
        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response,400);
        }

        if (request()->hasFile('image')) {
            if($qc->image){
                if (file_exists(public_path('/images/front_images/businesses/qc/' . $qc->image))) {
                    unlink(public_path('/images/front_images/businesses/qc/' . $qc->image));
                }
            }

            $file = request()->file('image');
            if ($file->isValid()) {
                $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/qc/');
            }
        }

        $data['business_id'] = $business->id;
        self::where('id',$id)->update($data);

        $qcs = Qc::where('business_id', $business->id)
            ->select('id','name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/qc/",image) as image'))
            ->get();
        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'qcs' => $qcs], 200);
    }

    public static function adminPersist($data, $business)
    {
        $businessQs = self::where('business_id', $business->id)->count();

        if ($businessQs < 4) {
            request()->validate([
                'qc_name' => 'required | min:3 | max:150',
                'qc_image' => 'required | sometimes',
                'qc_image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000',
                'qc_desc' => 'required | sometimes'
            ]);

            if (request()->hasFile('qc_image')) {
                $file = request()->file('qc_image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/qc/');
                }
            }

            self::create([
                'business_id' => $business->id,
                'name' => $data['qc_name'],
                'description' => $data['qc_desc'],
                'image' => $data['image']
            ]);
        }
        else {
            return back()->with('flash_error', 'حداکثر 4 آیتم به عنوان کنترل کیفیت میتوانید ثبت نمایید');
        }
    }
}
