<?php

namespace App\BusinessModels;

use App\Business;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Certificate extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public static function persist($data, $business)
    {
        $businessCerts = self::where('business_id', $business->id)->count();

        if ($businessCerts < 4) {
            $rules = [
                'cert_owner_name' => 'required | min:3 | max:150',
                'cert_type' => 'required',
                'cert_num' => 'required',
                'cert_name' => 'required | min:3 | max:150',
                'cert_exporter' => 'required | min:3 | max:150',
                'cert_start_date' => 'required',
                'cert_end_date' => 'nullable',
                'cert_scope' => 'required',
                'cert_image' => 'required | image | mimes:jpeg,png,jpg,gif | max:15000',
            ];

            $validator = Validator::make($data, $rules);
            if (request()->wantsJson()) {
                if ($validator->fails()) {
                    $response = ['message' => $validator->errors()->first()];
                    return response()->json($response, 400);
                }
            }

            if (request()->hasFile('cert_image')) {
                $file = request()->file('cert_image');
                if ($file->isValid()) {
                    $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/cert/');
                }
            }

            (new Certificate())->_make($data, $business);

            if (request()->wantsJson()) {
                $certs = Certificate::where('business_id', $business->id)
                    ->select('id', 'owner_name', 'certificate_type', 'certificate_number', 'certificate_name', 'exporter',
                        'start_date', 'end_date', 'scope',
                        \DB::raw('CONCAT("/images/front_images/businesses/cert/",image) as image'))
                    ->get();
                return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'certificates' => $certs], 200);
            } else {
                $response = ['message' => 'حداکثر 4 آیتم به عنوان گواهینامه میتوانید ثبت کنید.'];
                return response()->json($response, 404);
            }
        }
    }

    public static function updateItem($data, $id, $business)
    {
        $cert = Certificate::where('id', $id)->first();

        if (!$cert) {
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        $rules = [
            'cert_owner_name' => 'required | sometimes |  min:3 | max:150',
            'cert_type' => 'required | sometimes | string',
            'cert_num' => 'required | sometimes | numeric',
            'cert_name' => 'required | sometimes | min:3 | max:150',
            'cert_exporter' => 'required | sometimes | min:3 | max:150',
            'cert_start_date' => 'required | sometimes | string',
            'cert_end_date' => 'nullable | string',
            'cert_scope' => 'required | sometimes | string',
            'cert_image' => 'nullable | image | mimes:jpeg,png,jpg,gif | max:15000',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        if (request()->hasFile('cert_image')) {
            if ($cert->image) {
                if (file_exists(public_path('/images/front_images/businesses/cert/' . $cert->image))) {
                    unlink(public_path('/images/front_images/businesses/cert/' . $cert->image));
                }
            }

            $file = request()->file('cert_image');
            if ($file->isValid()) {
                $data = saveImageFile($file, $data, 400, 400, 'images/front_images/businesses/cert/');
            }
        }

        self::where('id', $id)->update([
            'business_id' => $business->id,
            'owner_name' => $data['cert_owner_name'] ?? $cert->owner_name,
            'certificate_type' => $data['cert_type'] ?? $cert->certificate_type,
            'certificate_number' => $data['cert_num'] ?? $cert->certificate_number,
            'certificate_name' => $data['cert_name'] ?? $cert->certificate_name,
            'exporter' => $data['cert_exporter'] ?? $cert->exporter,
            'start_date' => $data['cert_start_date'] ?? $cert->start_date,
            'end_date' => $data['cert_end_date'] ?? $cert->end_date,
            'scope' => $data['cert_scope'] ?? $cert->scope,
            'image' => $data['image'] ?? $cert->image
        ]);

        $certs = Certificate::where('business_id', $business->id)
            ->select('id', 'owner_name', 'certificate_type', 'certificate_number', 'certificate_name', 'exporter',
                'start_date', 'end_date', 'scope',
                \DB::raw('CONCAT("/images/front_images/businesses/cert/",image) as image'))
            ->get();
        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'certificates' => $certs], 200);
    }

    protected function _make($data, $business): void
    {
        self::create([
            'business_id' => $business->id,
            'owner_name' => $data['cert_owner_name'],
            'certificate_type' => $data['cert_type'],
            'certificate_number' => $data['cert_num'],
            'certificate_name' => $data['cert_name'],
            'exporter' => $data['cert_exporter'],
            'start_date' => $data['cert_start_date'],
            'end_date' => $data['cert_end_date'] ?? null,
            'scope' => $data['cert_scope'],
            'image' => $data['image']
        ]);
    }
}
