<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class BenefitDefect extends Model
{
    protected $guarded = ['id'];

    protected $table = 'benefit_defects';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
