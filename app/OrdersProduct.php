<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class OrdersProduct extends Model
{
    protected $table = 'order_product';

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

}
