<?php


namespace App\Filters;


use App\User;

class ProductFilters extends Filters
{
    protected $filters = ['by', 'byCategory'];

    /**
     * Filter the query by a given username
     * @param string $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->builder->where('user_id', $user->id);
    }

    /**
     * Filter the query by most popular threads by replies.
     * @return $this
     */
//    protected function popular()
//    {
//        /*for disabling latest() in $this->getThreads() in @index of ThreadsController*/
//        $this->builder->getQuery()->orders =[];
//
//        return $this->builder->orderBy('replies_count', 'desc');
//    }

    protected function byCategory($id)
    {
        return $this->builder->category->where('id', $id);
    }

}