<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = 'id';

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
