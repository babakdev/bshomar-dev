<?php

namespace App;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class DiscountProduct extends Model
{
    protected $fillable = ['discount_id', 'product_id'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
