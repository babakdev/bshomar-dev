<?php


namespace App\Inspections;
use Exception;

class InvalidKeywords
{
    protected $keywords = [
        'sex','lesbian', 'gay', 'pron', 'porno', 'vpn', 'sexual', 'cunt', 'priapus', 'cock', 'dick', 'ass', 'vagina', 'sexy',
        'cum', 'adult videos', 'panties', 'babes', 'breast', 'breasts', 'bitch', 'bitches', 'c*o*c*k*s', 'c*u*m', 'c0ck',
        'christian', 'play boy', 'lick', 'licking', 'filtershecan', 'fuck', 'fucker', ' fu­cks', 'fukking', 'fucking',
        'gambling', 'horny', 'hot ladies', 'hot boys', 'hot moms', 'hot mothers', 'hot married women', 'hot teens', 'hot housewives',
        'hot girls', 'hot girl', 'orgasm', 'pussy', 'penis', 'playboy', 'pornography', 'p.o.r.n', 'drugs', 'weed', 'rape', 'milf',
        'sekx', 'sekz', 'seks', 'schoolgirls', 'sex­crazed', 'sexual', 'spice girls', 'v­a­l­i­u­m', 'v­i­a­g­r­a','violence',
        'whores', 'whore', 'XXX', 'X X X', 'سکس','کس','کیر','دول','کون','سکسی','واژن','سینه ۸۵','والیوم','ویاگرا','پورن','پورنو',
        'پورنوگرافی','آب منی','جنده','فاحشه','پلی بوی','مسیحی','مسیحیت','لیس','فاک','وی پی ان','فیلترشکن','فیلتر شکن','فاکینگ','قمار',
        'حشیش','هشیش','تریاک','هرویین','هروئین','قرص اکس','قرص ایکس','گل کشیدن','لزبین','لز','گی','همجنس گرایی','همجنسگرایی','ممه','خشونت',
        'رهبری','خامنه ای', 'روحانی','سیاست','سیاسی','دولت', 'شرط بندی'
    ];
    public function detect($body)
    {
        foreach ($this->keywords as $keyword) {
            if (stripos($body, $keyword) != false) {
                throw new Exception('متن ورودی شامل کلمات غیرمجاز می باشد');
            }
        }
    }
}
