<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class SharedAdmins
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->isAdmin) {
            return $next($request);
        }
        if (auth()->check() && auth()->user()->isBusiness) {
            return $next($request);
        }
        if (auth()->check() && auth()->user()->isSupport) {
            return $next($request);
        }
        if (auth()->check() && auth()->user()->isAsnaf) {
            return $next($request);
        }
        return redirect('/');
    }

}
