<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBusinessDiscountCode extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255|unique:discount_codes',
            'amount' => 'nullable:percent|integer',
            'percent' => 'nullable|integer|between:1,99',
            'products' => 'required',
            'amount_type' => 'required|integer|between:1,2',
            'start_date' => 'required|string|max:255',
            'end_date' => 'required|string|max:255',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'عنوان تخفیف را وارد کنید.',
            'name.string' => 'عنوان وارد شده معتبر نمی باشد.',
            'name.max' => 'عنوان وارد شده معتبر نمی باشد.',
            'code.required' => 'کد تخفیف را وارد کنید.',
            'code.string' => 'کد تخفیف وارد شده معتب نمی باشد.',
            'code.max' => 'کد تخفیف وارد شده معتب نمی باشد.',
            'code.unique' => 'کد تخفیف وارد شده تکراری است.',
            'products.required' => 'محصول/محصولات مورد نظر را انتخاب کنید.',
//            'amount.required_without' => 'شما باید یکی از انواع تخفیف را وارد کنید.',
            'amount.integer' => 'مبلغ تخفیف وارد شده معتبر نمی باشد.',
//            'percent.required_without' => 'شما باید یکی از انواع تخفیف را وارد کنید.',
            'percent.integer' => 'درصد تخفیف وارد شده معتبر نمی باشد.',
            'percent.between' => 'درصد تخفیف وارد شده معتبر نمی باشد.',
            'amount_type.required' => 'نوع تخفیف را انتخاب کنید.',
            'amount_type.integer' => 'نوع تخفیف معتبر نمی باشد.',
            'amount_type.between' => 'نوع تخفیف معتبر نمی باشد.',
            'start_date.required' => 'تاریخ شروع تخفیف را وارد کنید.',
            'start_date.string' => 'تاریخ وارد شده معتبر نمی باشد.',
            'start_date.max' => 'تاریخ وارد شده معتبر نمی باشد.',
            'end_date.required' => 'تاریخ پایان تخفیف را وارد کنید.',
            'end_date.string' => 'تاریخ وارد شده معتبر نمی باشد.',
            'end_date.max' => 'تاریخ وارد شده معتبر نمی باشد.',
        ];
    }
}
