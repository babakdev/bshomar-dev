<?php

namespace App\Http\Controllers;

use App\BenefitDefect;
use App\Business;
use App\Cart;
use App\Category;
use App\City;
use App\Comment;
use App\Image;
use App\OrdersProduct;
use App\Product;
use App\ProductProperty;
use App\Trending;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index(): void
    {
        abort(404);
    }

    public function create()
    {
        $business = Business::where('id', request('businessId'))->first();
        if ($business->category_id == null) {
            return back()->with('flash_error', 'ابتدا اطلاعات کسب و کار خود را تکمیل کنید سپس محصول موردنظر خود را ثبت کنید.');
        }

        $parent = Category::where('id', $business->category_id)->first()->parent_id;

        $properties = \DB::table('category_properties')->where('category_properties.category_id', $parent)
            ->join('properties', 'category_properties.property_id', 'properties.id')
            ->select('properties.id', 'properties.name')->get();

        if ($business->checkPanelProducts(1, 10)) {
            // Free panel
            $business->checkPanelProducts(1, 10);
            return back();
        }
        if ($business->checkPanelProducts(2, 30)) {
            // Business panel
            $business->checkPanelProducts(2, 30);
            return back();
        }

        if ($business->checkPanelProducts(3, 50)) {
            // Commerce panel
            $business->checkPanelProducts(3, 50);
            return back();
        }

        return view('admin.products.create', ['product' => new Product()]
            , compact('business', 'properties'));

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $businessId = Business::where('id', $data['business_id'])->value('id');
        $request->validate([
            'name' => 'required | string | min:3 | max:40',
            'unit' => 'required | string | min:2 | max:40',
            'stock' => 'required | numeric',
            'country' => 'required | string',
            'time_to_delivery' => 'required',
            'delivery_type' => 'required',
            'product_return' => 'required',
            'product_return_type' => 'required',
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000'
        ]);
        $data['price'] = convertPersianNumsToEng($data['price']) ?? 0;
        $data['stock'] = convertPersianNumsToEng($data['stock']) ?? 0;
        $data['is_active'] = auth()->user()->isAdmin || auth()->user()->isSupport ? 1 : 0;
        $data['discount_cycle'] = $data['discount_cycle'] ?? 0;

        $product = Product::create($data);

        if ($request->hasFile('image') && count($request->file('image')) > 4) {
            return back()->with('flash_error', 'لطفا بیشتر از ۴ تصویر انتخاب نکنید');
        }

        $product->saveImage($request, $product);

        $this->storePropertiesInDB($data, $product->id);

        return redirect("/business-information/{$businessId}")->with('flash', 'کالا ایجاد شد');
    }

    public function edit(Product $product)
    {
        $business = Business::where('id', request('businessId'))->first();
        $parent = Category::where('id', $business->category_id)->first()->parent_id;
        $properties = \DB::table('category_properties')->where('category_properties.category_id', $parent)
            ->join('properties', 'category_properties.property_id', 'properties.id')
            ->select('properties.id', 'properties.name')->get();

        return view('admin.products.edit', compact('product', 'properties', 'business'));
    }

    public function update(Request $request, Product $product)
    {
        $data = $request->all();
        $businessId = Business::where('id', $data['business_id'])->value('id');

        $request->validate([
            'name' => 'required | string | min:3 | max:40',
            'unit' => 'required | string | min:2 | max:40',
            'stock' => 'required | numeric',
            'country' => 'required | string',
            'time_to_delivery' => 'required',
            'delivery_type' => 'required',
            'product_return' => 'required',
            'product_return_type' => 'required',
            'image' => 'required | sometimes',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000',
            'min_stock' => 'required'
        ]);

        $data['price'] = convertPersianNumsToEng($data['price']) ?? $product->price;
        $data['stock'] = convertPersianNumsToEng($data['stock']) ?? 0;
        $data['is_active'] = auth()->user()->isAdmin || auth()->user()->isSupport ? 1 : 0;
        $data['discount_cycle'] = $data['discount_cycle'] ?? 0;

        $product->update($data);

        $editedProductInCart = Cart::where('product_id', $product->id)->get();
        $message = "خریدار گرامی ، با توجه به ویرایش محصول {$product->name} توسط تامین کننده، محصول مورد نظر از سبد خرید شما حذف شد. لطفا دوباره به ثبت این محصول اقدام نمایید.";
        foreach ($editedProductInCart as $pro) {
            $pro->delete();
            sendToInbox(User::where('phone', $pro->user_phone)->value('id'), 'تغییر مشخصات کالا', 'info@bshomar.com', $message);
        }

        if (count($product->images) > 4) {
            return back()->with('flash_error', 'تعداد عکس مجاز به پایان رسید. تصویری را حذف کنید و یک تصویر را دوباره اضافه نمایید');
        }
        if ($request->hasFile('image') && count($product->images) + count($request->file('image')) > 4) {
            return back()->withInput($request->all())->with('flash_error', 'تعداد عکس مجاز فقط ۴ عدد می باشد لطفا دقت کنید');
        }
        if ($request->hasFile('image') && count($request->file('image')) > 4) {
            return back()->withInput($request->all())->with('flash_error', 'لطفا بیشتر از ۴ تصویر انتخاب نکنید');
        }
        if (count($product->images) == 0 && !$request->hasFile('image')) {
            return back()->withInput($request->all())->with('flash_error', 'حداقل یک تصویر برای کالا باید انتخاب شود');
        }

        $product->saveImage($request, $product);

        ProductProperty::where('product_id', $product->id)->delete();
        BenefitDefect::where('product_id', $product->id)->delete();

        $this->storePropertiesInDB($data, $product->id);
        return redirect("/business-information/{$businessId}")->with('flash', 'کالا ویرایش شد');
    }

    private function storePropertiesInDB($data, $productId)
    {
        for ($p = 1; $p < 25; $p++) {
            if (isset($data['amount' . $p]) && $data['amount' . $p] != null) {
                if (isset($data['property' . $p])) {
                    ProductProperty::create([
                        'product_id' => $productId,
                        'property_id' => $data['property' . $p],
                        'value' => $data['amount' . $p],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }
            }
        }


        for ($b = 1; $b < 25; $b++) {
            if (isset($data['b_property' . $b]) && $data['b_property' . $b] != null) {
                BenefitDefect::create([
                    'product_id' => $productId,
                    'property' => $data['b_property' . $b],
                    'value' => $data['b_amount' . $b],
                    'type' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
        for ($d = 1; $d < 25; $d++) {
            if (isset($data['d_property' . $d]) && $data['d_property' . $d] != null) {
                BenefitDefect::create([
                    'product_id' => $productId,
                    'property' => $data['d_property' . $d],
                    'value' => $data['d_amount' . $d],
                    'type' => 2,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }

    }

    public function destroy(Product $product)
    {
        $removedProductInCart = Cart::where('product_id', $product->id)->get();
        $message = "خریدار گرامی ، با توجه به حذف محصول {$product->name} توسط تامین کننده، محصول مورد نظر از سبد خرید شما حذف شد. ";
        foreach ($removedProductInCart as $pro) {
            $pro->delete();
            sendToInbox(User::where('phone', $pro->user_phone)->value('id'), 'حذف کالا', 'info@bshomar.com', $message);
        }
        if (count($product->images) > 0) {
            foreach ($product->images as $image) {
                if ($image->path && file_exists('/images/front_images/products/small/' . $image->path)) {
                    unlink('images/front_images/products/small/' . $image->path);
                    unlink('images/front_images/products/large/' . $image->path);
                }
                $image->delete();
            }
        }

        $product->delete();
        return back()->with('flash', 'کالا حذف شد');
    }

    public function destroyImage($id, $productId)
    {
        $prePhoto = Image::where(['id' => $id])->first();
        if ($prePhoto && file_exists('/images/front_images/products/small/' . $prePhoto->path)) {
            unlink('images/front_images/products/small/' . $prePhoto->path);
            unlink('images/front_images/products/large/' . $prePhoto->path);
        }

        if ($prePhoto) {
            $prePhoto->delete();
            $product = Product::find($productId);
            $data = '';

            foreach ($product->images as $image) {
                $data .= '
                <div class="form-group col-3 mx-auto text-center" id="imageFull{{ $image->id }}">
                    <img src="/images/front_images/products/large/' . $image->path . '" alt="productImage" class="img-fluid rounded"
                         style="width: 240px">
                        <a id="image' . $image->id . '" class="removePicBtn btn btn-link" data-imageid="' . $image->id . '" data-productId="' . $product->id . '" title="حذف تصویر">
                            <i class="fa fa-times-circle text-danger fa-2x mt-2"></i>
                        </a>
                </div>
            ';
            }
            return response()->json(['msg' => 'تصویر با موفقیت حذف شد.', 'data' => $data], 200);
        }

        return response()->json(['msg' => 'تصویر موجود نیست.', 'data' => ''], 404);
    }

    public function toggleProductStatus($productId)
    {
        $product = Product::where('id', $productId)->first();
        $product->stock = 0;
        $product->save();
        return back();
    }

    public function show()
    {
        return;
    }
}
