<?php

namespace App\Http\Controllers\API\Client;

use App\Cart;
use App\DiscountCode;
use App\DiscountProduct;
use App\DiscountUser;
use App\Http\Controllers\Controller;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class DiscountController extends Controller
{
    public function checkDiscountCode(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $code = $request->code;
        $discount = DiscountCode::where('code', $code)->first();
        if (!$discount) {
            $msg = 'کد تخفیف وارد شده معتبر نیست.';
            return response()->json(['message' => $msg], 400);
        }

        $userCart = Cart::where(['user_phone' => $user->phone])->get();

        $result = $this->checkDiscountProduct($userCart, $discount->id);
        if (!$result) {
            $msg = 'این کد تخفیف شامل سبد خرید شما نمی شود.';
            return response()->json(['message' => $msg], 400);
        }

        $discount_user = DiscountUser::where('user_id', $user->id)
            ->where('discount_id', $discount->id)
            ->first();
        if (!$discount_user) {
            DiscountUser::create([
                'discount_id' => $discount->id,
                'user_id' => $user->id,
                'cart_id' => $userCart->first()->id,
                'used' => 0,
                'date_used' => ''
            ]);
        } elseif ($discount_user->used == 1) {
            $discount_id = null;
            $response = ['message' => 'شما قبلا از این کوپن تخفیف استفاده کرده اید.'];
            return response()->json($response, 400);
        }

        $now = Verta::now()->format('Y/m/d');
        $start = convertPersianDateToStandardFormat($discount->start_date);
        $end = convertPersianDateToStandardFormat($discount->end_date);

        if (!($start <= $now && $now <= $end)) {
            $msg = 'اعتبار کد تخفیف به اتمام رسیده است.';
            return response()->json(['message' => $msg], 400);
        }

        $discount_price = 0;
        $businessesInCart = Cart::where(['user_phone' => $user->phone])
            ->join('businesses', 'businesses.id', 'carts.business_id')
            ->select(\DB::raw('SUM(price*quantity) as total_price'), 'businesses.id')
            ->groupBy('business_id')->get();
        foreach ($businessesInCart as $business) {
            if ($discount->business_id == $business->id) {
                $discount_price += $this->computeDiscountPrice($discount->id, $business->total_price);
            }
        }

        return response()->json(['discount_price' => $discount_price, 'message' => 'کد تخفیف اعمال شد.'], 200);

    }

    public function computeDiscountPrice($discount_id, $total)
    {
        $discount_price = 0;
        if ($discount_id) {
            $discount = DiscountCode::where('id', $discount_id)->first();
            if ($discount) {
                if ($discount->amount_type == 1) {
                    $discount_price = $total * $discount->percent / 100;
                    return $discount_price;
                } else {
                    $discount_price = $discount->amount;
                    return $discount_price;
                }
            }
            return $discount_price;
        }
        return $discount_price;
    }

    public function checkDiscountProduct($userCart, $discount_id)
    {
        $discount_p_ids = DiscountProduct::where('discount_id', $discount_id)->pluck('product_id')->toArray();
        foreach ($userCart as $cart) {
            $cart_p_id = $cart->product_id;
            $result = in_array($cart_p_id, (array)$discount_p_ids);
            if ($result) {
                return true;
            }
        }
        return false;
    }
}
