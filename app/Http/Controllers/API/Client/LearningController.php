<?php

namespace App\Http\Controllers\API\Client;

use App\CategoryLearn;
use App\Http\Controllers\Controller;
use App\LearnPage;
use Illuminate\Http\Request;

class LearningController extends Controller
{
    public function categories()
    {
        // TODO : Cache this
        $cats = CategoryLearn::select('id', 'title', 'icon')->get();

        return response()->json($cats, 200);
    }

    public function learningByCategory(Request $request)
    {
        $cat = $request->category_id;
        if (!$cat) {
            return response()->json(['message' => 'دسته بندی را انتخاب کنید.'], 400);
        }

        // TODO : Cache this
        $learning = LearnPage::where('category_id', $cat)->select('title', 'body')->get();

        return response()->json($learning, 200);

    }

    public function learningWithoutCategory()
    {
        // TODO : Cache this
        $learning = LearnPage::select('id','title', 'body')->get();
        foreach($learning as $item){
            $item->body = str_replace('/images/','https://server.bshomar.com/images/',$item->body);
        }

        return response()->json($learning, 200);
    }
}
