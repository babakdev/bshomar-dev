<?php

namespace App\Http\Controllers\API\Client;

use App\Advertising;
use App\Business;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Image;
use App\NewsLetter;
use App\Product;
use App\ProductProperty;
use App\Slide;
use App\Trending;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function splash(Trending $trending)
    {
        $sliders = Cache::remember('splashSlides', 60, fn() => Slide::select(\DB::raw('CONCAT("/images/front_images/slides/" ,image) AS image'), 'business_id', 'title', 'text', 'url')->limit(5)->get());

        $ads = Advertising::where('page', 1)->select('id', 'link', 'href', 'type')->orderBy('id', 'desc')->limit(2)->get();

        foreach ($ads as $ad) {
            $ad->type_name = $ad->type == 1 ? 'auto' : 'manual';
            if ($ad->type == 2) {
                $ad->link = '/images/front_images/ads/' . $ad->link;
            }
        }

        $categories = Cache::remember('splashRandomCats', 120, fn() => Category::where(['is_active' => 1, 'parent_id' => 0])
        ->select('id', 'name', 'slug', \DB::raw('CONCAT("/images/front_images/cats/" ,image) AS image'))
        ->get()->random(3));

        foreach ($categories as $category) {
            $count = Category::where('is_active', 1)->where('parent_id', $category->id)
                ->select('id', 'name', 'slug', \DB::raw('CONCAT("/images/front_images/cats/" ,image) AS image'))->count();
            if ($count >= 2) {
                $category->sub_categories = Category::where('is_active', 1)->where('parent_id', $category->id)
                    ->select('id', 'name', 'slug', \DB::raw('CONCAT("/images/front_images/cats/" ,image) AS image'))->get()->random(2);
            } else {
                $category->sub_categories = Category::where('is_active', 1)->where('parent_id', $category->id)
                    ->select('id', 'name', 'slug', \DB::raw('CONCAT("/images/front_images/cats/" ,image) AS image'))->get()->random(1);
            }
        }

        $states = Cache::remember('splashStates', 300, fn() => City::where(['parent_id' => 0])->select('id', 'title')->get());

        $suggestion_products = $this->suggestion_products();
        $newest_business = $this->newest_businesses();
        $most_visited_business = $this->most_visited_business($trending);
        $newest_discounts = $this->newest_discount_products();


        $data = [
            'slider' => $sliders, 'random_categories' => $categories, 'index_ads' => $ads, 'suggestion_products' => $suggestion_products,
            'newest_business' => $newest_business, 'most_visited_business' => $most_visited_business, 'newest_discounts' => $newest_discounts,
            'states' => $states
        ];

        return response()->json($data, 200);
    }

    protected function suggestion_products()
    {
        $products = Cache::remember('splashSuggestionProducts', 60, fn() => Product::where('products.is_active', 1)
        ->join('categories', 'categories.id', 'products.category_id')
        ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
        ->join('businesses', 'businesses.id', 'products.business_id')
        ->select('products.id as product_id', 'products.name', 'products.is_active',
            'products.stock', 'products.price', 'products.discount', 'products.discount_expire_date',
            'categories.id as category_id', 'categories.name as category_name', 'categories.name as cat_name',
            'businesses.name as business_name', 'businesses.id as business_id', 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
        ->inRandomOrder()->limit(12)->get());

        foreach ($products as $item) {
            $properyCount = ProductProperty::where('product_id', $item->product_id)->get()->count();
            $item->has_properties = ($properyCount > 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $item->product_id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->image = $img->path_small ?? null;

            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;
        }

        return $products;
    }

    protected function newest_businesses()
    {
        $businesses = Cache::remember('splashNewestBusinesses', 120, fn() => Business::where(['businesses.is_active' => 1])
        ->join('categories', 'categories.id', 'businesses.category_id')
        ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
        ->join('cities', 'cities.id', 'businesses.city_id')
        ->join('cities as states', 'cities.parent_id', 'states.id')
        ->select('businesses.id as id', 'cities.title as city_name', 'businesses.is_active',
            'states.title as state', 'businesses.name', 'businesses.category_id', 'categories.name as cat_name',
            'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
        ->orderBy('businesses.created_at', 'desc')
        ->limit(12)->get());

        foreach ($businesses as $item) {
            $item->logo = '/images/front_images/businesses/logos/' . ($item->logo ?? 'logo-default.png');
            $img = Image::where(['imageable_type' => Business::class, 'imageable_id' => $item->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;
            $count = Product::where('business_id', $item->business_id)->count();
            if ($count >= 2) {
                $count = 2;
            }
            $item->products = Product::where('business_id', $item->business_id)
                ->where('products.is_active', 1)
                ->join('categories', 'categories.id', 'products.category_id')
                ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
                ->join('businesses', 'businesses.id', 'products.business_id')
                ->join('cities', 'cities.id', 'businesses.city_id')
                ->join('cities as states', 'cities.parent_id', 'states.id')
                ->select('products.id', 'products.name', 'products.price', 'products.meta_keywords', 'products.is_active',
                    'products.stock', 'products.min_product', 'products.discount', 'products.discount_expire_date', 'products.category_id', 'products.country',
                    'businesses.city_id', 'states.id as state_id', 'cities.title as city_name', 'states.title as state_name', 'categories.name as category_name',
                    'businesses.name as business_name', 'businesses.id as business_id', 'categories.name as cat_name', 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
                ->get()->random($count);

            foreach ($item->products as $product) {
                $properyCount = ProductProperty::where('product_id', $product->id)->get()->count();
                $product->has_properties = ($properyCount > 0) ? 1 : 0;

                $product->discount_expire_date = checkDiscountExpireDate($product->discount_expire_date) ? $product->discount_expire_date : null;
                $product->discount = $product->discount_expire_date != null ? $product->discount : 0;

                $img = Image::where(['imageable_type' => get_class($product), 'imageable_id' => $product->id])
                    ->select(
                        \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                    ->first();
                $product->img = $img->path_small ?? null;
            }
        }

        return $businesses;
    }

    protected function most_visited_business($trending)
    {
        $businessKeys = [];

        $trends = $trending->getBestBusinessesScore(11);
        foreach (collect($trends) as $key => $item) {
            $key = json_decode($key, true);
            $businessKeys[] = Business::where('businesses.id', $key)
                ->join('categories', 'categories.id', 'businesses.category_id')
                ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
                ->join('cities', 'cities.id', 'businesses.city_id')
                ->join('cities as states', 'cities.parent_id', 'states.id')
                ->select('businesses.id', 'businesses.name', 'businesses.category_id', 'categories.name as cat_name',
                    'cities.title as city_name', 'states.title as state', 'businesses.is_active'
                    , 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
                ->first();
        }

        foreach ($businessKeys as $item) {
            if ($item) {
                $item->logo = '/images/front_images/businesses/logos/' . ($item->logo ?? 'logo-default.png');
                $img = Image::where(['imageable_type' => Business::class, 'imageable_id' => $item->id])
                    ->select(
                        \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                    ->first();
                $item->image = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;

                $count = Product::where('business_id', $item->id)->count();
                if ($count >= 2) {
                    $count = 2;
                }
                $item->products = Product::where('business_id', $item->id)
                    ->where('products.is_active', 1)
                    ->join('categories', 'categories.id', 'products.category_id')
                    ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
                    ->join('businesses', 'businesses.id', 'products.business_id')
                    ->join('cities', 'cities.id', 'businesses.city_id')
                    ->join('cities as states', 'cities.parent_id', 'states.id')
                    ->select('products.id', 'products.name', 'products.price', 'products.meta_keywords', 'products.is_active',
                        'products.stock', 'products.min_product', 'products.discount', 'products.discount_expire_date', 'products.category_id', 'products.country',
                        'businesses.city_id', 'businesses.name as business_name', 'businesses.id as business_id', 'states.id as state_id', 'cities.title as city_name', 'states.title as state_name', 'categories.name as category_name',
                        'categories.name as cat_name', 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
                    ->get()->random($count);

                foreach ($item->products as $product) {
                    $properyCount = ProductProperty::where('product_id', $product->id)->get()->count();
                    $product->has_properties = ($properyCount > 0) ? 1 : 0;

                    $product->discount_expire_date = checkDiscountExpireDate($product->discount_expire_date) ? $product->discount_expire_date : null;
                    $product->discount = $product->discount_expire_date != null ? $product->discount : 0;

                    $img = Image::where(['imageable_type' => get_class($product), 'imageable_id' => $product->id])
                        ->select(
                            \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                        ->first();
                    $product->img = $img->path_small ?? null;
                }
            }
        }

        return $businessKeys;
    }

    protected function newest_discount_products()
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        // TODO : Cache this
        $products = Cache::remember('splashNewestDiscountProducts', 180, fn() => Product::where('products.is_active', 1)
        ->join('categories', 'categories.id', 'products.category_id')
        ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
        ->join('businesses', 'businesses.id', 'products.business_id')
//            ->orderBY('products.discount_expire_date', 'ASC')
        ->select('products.id as product_id', 'products.name', 'products.is_active',
            'products.stock', 'products.price', 'products.discount', 'products.discount_expire_date',
            'categories.id as category_id', 'categories.name as category_name', 'categories.name as cat_name',
            'businesses.name as business_name', 'businesses.id as business_id', 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
        ->where('products.discount', '!=', 0)
        ->where('products.discount_expire_date', '!=', null)
        ->where('products.discount_expire_date', '>=', $now)
        ->inRandomOrder()->limit(12)->get());

        foreach ($products as $item) {
            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;

            $properyCount = ProductProperty::where('product_id', $item->id)->get()->count();
            $item->has_properties = ($properyCount > 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $item->product_id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->image = $img->path_small ?? null;
        }

        return $products;

    }

    public function searchByProductOrBusiness(Request $request)
    {
        $text = $request->text;
        if (trim($text) == '') {
            return response()->json(['count' => 0, 'data' => []], 200);
        }
        // type = 1 search in products , type = 2 search in businesses
        $type = $request->type;
        $i = $request->i ?? 1;

        if ($type == 1) {
            $data = Product::where('products.name', 'LIKE', '%' . $text . '%')
                ->where('products.is_active', 1)
                ->join('businesses', 'businesses.id', 'products.business_id')
                ->join('categories', 'categories.id', 'products.category_id')
                ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
                ->select('products.id', 'products.name', 'products.code', 'products.unit', 'products.price', 'products.is_active',
                    'products.stock', 'products.discount', 'products.discount_expire_date', 'categories.id as category_id', 'categories.name as category_name',
                    'categories.name as cat_name', 'businesses.name as business_name', 'businesses.id as business_id', 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name')
                ->offset(($i - 1) * 24)->limit(24)->get();

            $count = Product::where('products.name', 'LIKE', '%' . $text . '%')->count();

            foreach ($data as $item) {
                $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
                $item->discount = $item->discount_expire_date != null ? $item->discount : 0;
                $properyCount = ProductProperty::where('product_id', $item->id)->get()->count();
                $item->has_properties = ($properyCount > 0) ? 1 : 0;
                $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $item->id])
                    ->select(
//                        \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'),
                        \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                    ->first();
                $item->image = $img->path_small ?? null;
            }
        } else {
            $data = Business::where('businesses.name', 'LIKE', '%' . $text . '%')
                ->where('businesses.is_active', 1)
                ->join('categories', 'categories.id', 'businesses.category_id')
                ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
                ->join('cities', 'cities.id', 'businesses.city_id')
                ->select('businesses.id', 'businesses.category_id', 'businesses.name', 'businesses.is_active', 'cities.title as city_name',
                    'categories.name as cat_name', 'categories.parent_id as cat_parent_id', 'catparent.name as cat_parent_name',
                    \DB::raw('CONCAT("/images/front_images/businesses/logos/",businesses.logo) as logo'))
                ->offset(($i - 1) * 24)->limit(24)->get();

            $count = Business::where(['businesses.is_active' => 1])
                ->where('businesses.name', 'LIKE', '%' . $text . '%')->count();

            foreach ($data as $item) {
                $img = Image::where(['imageable_type' => Business::class, 'imageable_id' => $item->id])
                    ->select(
//                        \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'),
                        \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                    ->first();
                $item->image = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;
            }
        }

        return response()->json(['count' => $count, 'data' => $data], 200);
    }

    public function newsLetters(Request $request)
    {
        $data = $request->all();

        $rules = [
            'email' => 'required|email|unique:news_letters',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        NewsLetter::create(['email' => $data['email']]);

        $response = ['message' => 'شما عضو خبرنامه ما شدید.'];
        return response()->json($response, 200);
    }
}
