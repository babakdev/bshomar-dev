<?php

namespace App\Http\Controllers\API\Client;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{
    public function getSubCategories(Request $request)
    {
        // TODO : Cache this
        $category_id = $request->id;
        if(!$category_id){
            return response()->json(['message' => 'نامعتبر!'], 400);
        }
        $i = $request->i ?? 1;

        $sub_categories = Category::where('categories.is_active', 1)
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->where('categories.parent_id', $category_id)
            ->select('categories.id', 'categories.name', 'categories.slug', \DB::raw('CONCAT("/images/front_images/cats/" ,categories.image) AS image'),
                'categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        $count = Category::where('is_active', 1)->where('parent_id', $category_id)->count();

        $data = ['count' => $count, 'sub_categories' => $sub_categories];

        return response()->json($data, 200);
    }

    public function getAllCategories()
    {
        $all_categories = $this->all_category_with_sub();

        return response()->json(['all_categories' => $all_categories], 200);
    }

    protected function all_category_with_sub()
    {
        $cats = Cache::remember('all_categories_with_sub', 86400, function () {
            return Category::where(['parent_id' => 0, 'is_active' => 1])
                ->select('id', 'name', 'slug')
                ->get();
        });

        foreach ($cats as $cat) {
            $cat->sub_categories = Cache::remember($cat->slug, 86400, function () use ($cat) {
                return Category::where(['parent_id' => $cat->id, 'is_active' => 1])
                    ->select('id', 'name', 'slug')
                    ->get();
            });
        }
        return $cats;
    }

}
