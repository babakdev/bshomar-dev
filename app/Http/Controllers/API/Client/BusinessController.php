<?php

namespace App\Http\Controllers\API\Client;

use App\Business;
use App\BusinessModels\Certificate;
use App\BusinessModels\CustomerExport;
use App\BusinessModels\Export;
use App\BusinessModels\ManufacturingLine;
use App\BusinessModels\Qc;
use App\BusinessModels\ResearchDevelop;
use App\BusinessModels\Service;
use App\Category;
use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\ProductProperty;
use App\Property;
use App\Trending;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    public function getBusinessDetailsById(Request $request)
    {
        $id = $request->id;
        if (!$id) {
            return response()->json(['message' => 'کسب و کار نامعتبر!'], 404);
        }

        $business = Business::where(['businesses.id' => $id])
            ->join('users', 'users.id', 'businesses.user_id')
            ->join('categories', 'categories.id', 'businesses.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->select('users.name as user_name', 'categories.name as cat_name', 'cities.title as city_name',
                'states.title as state', 'businesses.id as id', 'businesses.category_id', 'businesses.is_active',
                'businesses.name', 'businesses.address', 'businesses.phone', 'businesses.email', 'businesses.latitude',
                'businesses.longitude', 'businesses.description','businesses.logo','businesses.header_image',
                'businesses.product_lines', 'businesses.advantages', 'businesses.website', 'businesses.found_date',
                'businesses.b2b', 'businesses.b2c', 'businesses.has_qc', 'businesses.is_seller', 'businesses.rules',
                'categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->first();
        if (!$business) {
            return response()->json(['message' => 'کسب و کار یافت نشد!'], 404);
        }
        $trending = new Trending();
//        if ($business->is_seller == 1) {
            $trending->pushSellBusiness($business);
//        }
        $trending->pushBusiness($business);
        $business->visits()->record();

        if ($business) {

            $business->header_image = "/images/front_images/businesses/headers/" . ($business->header_image ?? 'header-default.jpg');
            $business->logo = "/images/front_images/businesses/logos/" . ($business->logo ?? 'logo-default.png');

            $imgs = Image::where(['imageable_type' => Business::class, 'imageable_id' => $id])
                ->select('id', \DB::raw('CONCAT("/images/front_images/businesses/large/",images.path) as path_large'),
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->get();

            $catImage = Category::find($business->category_id)->image;
            $img_default[0] = [
                'id' => null,
                'path_small' => "/images/front_images/cats/" . $catImage,
                'path_large' => "/images/front_images/cats/" . $catImage,
            ];
            $business->imgs = sizeof($imgs) ? $imgs : $img_default;
            $business->allProducts = Product::where(['products.is_active' => 1, 'business_id' => $business->id])->select('id', 'name')->get();

            $business->products = Product::where(['products.is_active' => 1, 'business_id' => $business->id])
                ->join('businesses','businesses.id','products.business_id')
                ->join('categories','categories.id','products.category_id')
                ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
                ->select('products.id as product_id', 'products.name',
                    'products.price', 'products.discount', 'products.discount_expire_date', 'products.stock',
                    'categories.id as category_id','categories.name as category_name','businesses.name as business_name','businesses.id as business_id',
                    'categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
                ->limit(6)->get();
            foreach($business->products as $product){
                $product->discount_expire_date = checkDiscountExpireDate($product->discount_expire_date) ? $product->discount_expire_date : null;
                $product->discount = $product->discount_expire_date != null ? $product->discount : 0;
                $properyCount = ProductProperty::where('product_id', $product->product_id)->get()->count();
                $product->has_properties = ($properyCount > 0) ? 1 : 0;
            }

            foreach ($business->products as $product) {
                $image = Image::where(['imageable_type' => Product::class, 'imageable_id' => $product->product_id])
                    ->select(\DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                    ->first();
                $product->image = $image->path_small ?? null;
            }

            $business->certificates = Certificate::where('business_id', $id)
                ->select('owner_name', 'certificate_type', 'certificate_number', 'certificate_name', 'exporter',
                    'start_date', 'end_date', 'scope',
                    \DB::raw('CONCAT("/images/front_images/businesses/cert/",image) as image'))
                ->get();

            $business->resume_export = CustomerExport::where('business_id', $id)
                ->select('name', 'country', 'state', 'product_name', 'turn_over_year',

                    \DB::raw('CONCAT("/images/front_images/businesses/cust/",image) as image'))
                ->get();

            $exports = Export::where('business_id', $id)
                ->select('ex_value', 'ex_of_products', 'target_market', 'ex_start_year', 'ex_type', 'ex_avg', 'ex_rules', 'langs')
                ->get();

            foreach ($exports as $export) {
                switch ($export->ex_value) {
                    case 1:
                        $export->ex_value_text = 'زیر یک میلیون دلار';
                        break;
                    case 2:
                        $export->ex_value_text = 'بین 1 تا 3 میلیون دلار';
                        break;
                    case 3:
                        $export->ex_value_text = 'بین 3 تا 5 میلیون دلار';
                        break;
                    case 4:
                        $export->ex_value_text = 'بین 5 تا 10 میلیون دلار';
                        break;
                    case 5:
                        $export->ex_value_text = 'بین 10 تا 100 میلیون دلار';
                        break;
                    case 6:
                        $export->ex_value_text = 'بالاتر از 100 میلیون دلار';
                        break;
                }
            }

            $business->exports = $exports;

            $business->manufacturing_line = ManufacturingLine::where('business_id', $id)
                ->select('name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/manu/",image) as image'))
                ->get();

            $business->qc = Qc::where('business_id', $id)
                ->select('name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/qc/",image) as image'))
                ->get();

            $business->r_and_d = ResearchDevelop::where('business_id', $id)
                ->select('name', 'description', \DB::raw('CONCAT("/images/front_images/businesses/rnd/",image) as image'))
                ->get();

            $business->sevice = Service::where('business_id', $id)
                ->select('type', 'service_info')->get();

            $business['agency'] = $business->agencies;
            unset($business['images']);

            //todo hard code
            $business['view'] = $business->visits()->count();

            return response()->json($business, 200);
        }

        return response()->json([], 404);

    }

    public function newestBusinesses(Request $request)
    {
        $i = $request->i ?? 1;

        // TODO : Cache this
        //todo remove condition is_seller = 0
        $businesses = Business::where(['businesses.is_active' => 1])
            ->join('categories', 'categories.id', 'businesses.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->select('businesses.id as business_id', 'businesses.category_id', 'businesses.is_active',
                'categories.name as cat_name', 'cities.title as city_name',
                'states.title as state', 'businesses.name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->orderBy('businesses.created_at', 'desc')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        $count = Business::where(['is_active' => 1])->count();

        foreach ($businesses as $item) {
            $item->logo = "/images/front_images/businesses/logos/" . ($item->logo ?? 'logo-default.png');
            $img = Image::where(['imageable_type' => get_class($item), 'imageable_id' => $item->business_id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;
        }

        return response()->json(['count' => $count, 'businesses' => $businesses], 200);
    }

    public function mostVisitedBusinesses(Request $request)
    {
        $i = $request->i ?? 1;

        // TODO : Cache this
        $businesses = Business::all();

        $businessKeys = [];
        foreach ($businesses as $business) {
            $businessKeys[] = $business->visits()->cacheKey();
        }
        $businessKeys = str_replace(['App\\Business.', '.visits'], '', $businessKeys);

        $data = Business::whereIn('businesses.id', $businessKeys)
            ->where('businesses.is_active' ,1)
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('categories', 'categories.id', 'businesses.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->select('businesses.id', 'businesses.category_id', 'businesses.is_active', 'businesses.name as name',
                'cities.title as city_name', 'categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        $count = Business::whereIn('businesses.id', $businessKeys)->count();

        foreach ($data as $item) {
            $item->logo = "/images/front_images/businesses/logos/" . ($item->logo ?? 'logo-default.png');
            $img = Image::where(['imageable_type' => get_class($item), 'imageable_id' => $item->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;
        }

        return response()->json(['count' => $count, 'businesses' => $data], 200);
    }
}
