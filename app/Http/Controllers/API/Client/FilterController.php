<?php

namespace App\Http\Controllers\API\Client;

use App\Business;
use App\Category;
use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\ProductProperty;
use App\Trending;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function getBestBusinessesByCategory(Request $request)
    {
        $id = $request->cat_id;
        if (!$id) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }
        $cat = Category::find($id);
        if (!$cat) {
            return response()->json(['message' => 'دسته بندی یافت نشد.'], 404);
        }

        $data = $this->get_filter_businesses_by_id($id);

        return response()->json(['data' => $data], 200);
    }

    protected function get_filter_businesses_by_id($id)
    {
        // TODO : Cache this
        $data = Business::where('businesses.is_active', 1)
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->join('categories', 'categories.id', 'businesses.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->where('categories.parent_id' , $id)
            ->select('businesses.id', 'businesses.name', 'businesses.business_type', 'businesses.category_id', 'businesses.is_active',
            'cities.title as city_name', 'categories.name as cat_name', 'businesses.city_id', 'states.id as state_id', 'states.title as state_name',
            'businesses.b2b', 'businesses.b2c', 'businesses.is_seller','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->inRandomOrder()->limit(12)->get();

        foreach ($data as $item) {
            $item->logo = "/images/front_images/businesses/logos/" . ($item->logo ?? 'logo-default.png');
            $img = Image::where(['imageable_type' => get_class($item), 'imageable_id' => $item->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;
            $count = Product::where('business_id', $item->id)->count();

            $item->has_product = $count ? 1 : 0;
            $item->products = [];
        }
        return $data;
    }

    protected function get_filter_products_by_id($id)
    {
        // TODO : Cache this
        $data = Product::where('products.is_active',1)
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->where('categories.parent_id',$id)
            ->select('products.id', 'products.name', 'products.price', 'products.meta_keywords', 'products.is_active',
            'products.stock', 'products.min_product', 'products.discount', 'products.discount_expire_date', 'products.category_id', 'products.country',
            'businesses.city_id', 'states.id as state_id', 'cities.title as city_name', 'states.title as state_name','categories.name as category_name',
            'businesses.name as business_name','businesses.id as business_id','categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->inRandomOrder()->limit(12)->get();

        foreach ($data as $item) {
            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;
            $properyCount = ProductProperty::where('product_id', $item->id)->get()->count();
            $item->has_properties = ($properyCount> 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => get_class($item), 'imageable_id' => $item->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->img = $img->path_small ?? null;
        }
        return $data;
    }

    public function getBestProductsByCategory(Request $request)
    {
        $id = $request->cat_id;
        if (!$id) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }
        $cat = Category::find($id);
        if (!$cat) {
            return response()->json(['message' => 'دسته بندی یافت نشد.'], 404);
        }

        $data = $this->get_filter_products_by_id($id);

        return response()->json(['data' => $data], 200);
    }

    public function filterByCategories(Request $request)
    {
        $catId = $request->cat_id;
        // type = 1 => products , type = 2 => businesses
        $type = $request->type;
        $i = $request->i;

        if (!$catId) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }

        if (!$type) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }

        if ($type == 1) {
            $is_available = isset($request->is_available) ? $request->is_available : null;
            $has_discount = isset($request->has_discount) ? $request->has_discount : null;
            $country = isset($request->country) ? $request->country : null;
            $min_price = isset($request->min_price) ? $request->min_price : null;
            $max_price = isset($request->max_price) ? $request->max_price : null;
            $state_id = isset($request->state_id) ? $request->state_id : null;
            $city_id = isset($request->city_id) ? $request->city_id : null;

            $data = $this->get_filter_products($i, $catId, $is_available, $has_discount, $country, $min_price, $max_price, $state_id, $city_id);

        } else {

            $business_type = isset($request->business_type) ? $request->business_type : null;
            $state_id = isset($request->state_id) ? $request->state_id : null;
            $city_id = isset($request->city_id) ? $request->city_id : null;

            $data = $this->get_filter_businesses($i, $catId, $business_type, $state_id, $city_id);
        }

        return response()->json(['date' => $data['data'] , 'count' => $data['count']], 200);
    }

    protected function get_filter_products($i, $catId, $is_available, $has_discount, $country, $min_price, $max_price, $state_id, $city_id)
    {
        $opr1 = $is_available ? '>' : '=';
        $opr2 = $has_discount ? '>' : '=';

        // TODO : Cache this
        $data = Product::where(['products.category_id' => $catId])
            ->where('products.is_active',1)
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id');
        if ($is_available != null) {
            $data = $data->where('products.stock', $opr1, 0);
        }
        if ($has_discount != null) {
            $data = $data->where('products.discount', $opr2, 0);
        }
        if ($country != null) {
            $data = $data->where('products.country', 'LIKE', "%{$country}%");
        }
        if ($min_price != null) {
            $data = $data->where('products.price', '>=', (int)$min_price);
        }
        if ($max_price != null) {
            $data = $data->where('products.price', '<=', (int)$max_price);
        }
        if ($city_id != null) {
            $data = $data->where('businesses.city_id', $city_id);
        }
        if ($state_id != null) {
            $data = $data->where('states.id', $state_id);
        }
        $count = $data->get()->count();
        $data = $data->select('products.id', 'products.name', 'products.price', 'products.meta_keywords', 'products.is_active',
            'products.stock', 'products.min_product', 'products.discount', 'products.discount_expire_date', 'products.category_id', 'products.country',
            'businesses.city_id', 'states.id as state_id', 'cities.title as city_name', 'states.title as state_name','categories.name as category_name'
            ,'businesses.name as business_name','businesses.id as business_id','categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->offset(($i - 1) * 24)->limit(24)->get();

        foreach ($data as $item) {
            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;
            $properyCount = ProductProperty::where('product_id', $item->id)->get()->count();
            $item->has_properties = ($properyCount> 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => get_class($item), 'imageable_id' => $item->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->img = $img->path_small ?? null;
        }

        $res = [
            'data' => $data,
            'count' => $count
        ];
        return $res;
    }

    protected function get_filter_businesses($i, $catId, $business_type, $state_id, $city_id)
    {
        // TODO : Cache this
        $data = Business::where(['businesses.category_id' => $catId])
            ->where('businesses.is_active',1)
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->join('categories', 'categories.id', 'businesses.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id');

        if ($business_type != null) {
            $data = $data->where('businesses.business_type', $business_type);
        }
        if ($city_id != null) {
            $data = $data->where('businesses.city_id', $city_id);
        }
        if ($state_id != null) {
            $data = $data->where('states.id', $state_id);
        }

        $count = $data->get()->count();
        $data = $data->select('businesses.id', 'businesses.name as name', 'businesses.business_type', 'businesses.category_id', 'businesses.is_active',
            'cities.title as city_name', 'categories.name as cat_name', 'businesses.city_id', 'states.id as state_id', 'states.title as state_name',
            'categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->offset(($i - 1) * 24)->limit(24)->get();

        foreach ($data as $item) {
            $item->logo = "/images/front_images/businesses/logos/" . ($item->logo ?? 'logo-default.png');
            $img = Image::where(['imageable_type' => get_class($item), 'imageable_id' => $item->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : '/images/front_images/cats/' . Category::where('id', $item->category_id)->first()->image;
            $countPro = Product::where('business_id', $item->id)->where('products.is_active',1)->count();

            $item->has_product = $countPro ? 1 : 0;
            $item->products = [];
        }

        $res = [
            'data' => $data,
            'count' => $count
        ];
        return $res;
    }

    public function businessFilterParameters()
    {
        // TODO : Cache this
        $states = Business::distinct()
            ->where('businesses.is_active',1)
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->select('states.id', 'states.title as state_name')
            ->orderBy('states.id', 'ASC')
            ->get();

        $business_type = [
            [
                'id' => 1,
                'name' => 'تولیدی'
            ],
            [
                'id' => 2,
                'name' => 'توزیعی'
            ],
            [
                'id' => 3,
                'name' => 'خدماتی'
            ],
            [
                'id' => 4,
                'name' => 'صنایع دستی'
            ]
        ];

        $data = ['states' => $states, 'business_type' => $business_type];
        return response()->json($data, 200);
    }

    public function productFilterParameters()
    {
        // TODO : Cache this
        $states = Product::distinct()
            ->where('products.is_active',1)
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->select('states.id', 'states.title as state_name')
            ->orderBy('states.id', 'ASC')
            ->get();

        $max_price = Product::select(\DB::raw('MAX(price) as max'))->first()->max;

        $data = ['states' => $states, 'max_price' => $max_price];
        return response()->json($data, 200);
    }
}
