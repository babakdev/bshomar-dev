<?php

namespace App\Http\Controllers\API\Client;

use App\City;
use App\Complaint;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function citiesByState(Request $request)
    {
        // TODO : Cache this
        $state_id = $request->state_id;

        if (!$state_id) {
            return response()->json(['message' => 'استان را انتخاب کنید.'], 400);
        }
        $cities = City::where('parent_id', $state_id)->select('id', 'title')->get();

        return response()->json($cities, 200);
    }

    public function allStates()
    {
        // TODO : Cache this
        $states = City::where('parent_id', 0)->select('id', 'title')->get();

        return response()->json($states, 200);
    }

    // complaint form
    public function contactUsForm(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name' => 'required | string',
            'phone' => 'required | numeric',
            'email' => 'nullable | email',
            'message' => 'required | max:1024'
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        $complaint = Complaint::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'email' => $data['email'] ?? null,
            'message' => $data['message'],
        ]);

        return response()->json(['message' => 'پیام شما با موفقیت ثبت شد.'], 200);
    }
}
