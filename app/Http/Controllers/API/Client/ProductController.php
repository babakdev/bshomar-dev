<?php

namespace App\Http\Controllers\API\Client;

use App\BenefitDefect;
use App\Business;
use App\Comment;
use App\Favorite;
use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\ProductProperty;
use App\Trending;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    public function getProductDetailsById(Request $request)
    {
        $id = $request->id;
        if (!$id) {
            return response()->json(['message' => 'محصول نامعتبر!'], 400);
        }

        $product = Product::where(['products.id' => $id])
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->select('products.id as product_id', 'products.is_active', 'products.name', 'products.code', 'products.unit', 'products.country',
                'products.description', 'products.price', 'products.stock', 'products.status', 'products.meta_keywords',
                'products.min_product', 'products.discount', 'products.discount_expire_date', 'products.time_to_delivery','products.delivery_type',
                'products.consider', 'products.business_id', 'categories.id as category_id','categories.name as category_name', 'businesses.name as business_name',
                'products.product_return','products.product_return_type','businesses.id as business_id','businesses.name as business_name','categories.name as cat_name',
                'categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->first();

        if (!$product) {
            return response()->json(['message' => 'محصول نامعتبر!'], 404);
        }
        $trendProduct = Product::find($product->product_id);
        $trending = new Trending();
        $trending->pushProduct($trendProduct);
        $trendProduct->visits()->record();
        if ($product) {
            $business = Business::where('id',$product->business_id)->first();
            $product->shipping_cost = $business->shipping_cost;
            $product->min_shipping_cost = $business->min_shipping_cost;

            $product->discount_expire_date = checkDiscountExpireDate($product->discount_expire_date) ? $product->discount_expire_date : null;
            $product->discount = $product->discount_expire_date != null ? $product->discount : 0;

            $properyCount = ProductProperty::where('product_id', $product->id)->get()->count();
            $product->has_properties = ($properyCount> 0) ? 1 : 0;
            $product->imgs = Image::where(['imageable_type' => Product::class, 'imageable_id' => $id])
                ->select(\DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'),
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->get();

            $product->properties = ProductProperty::where('product_id', $id)
                ->join('properties', 'properties.id', 'product_properties.property_id')
                ->select('properties.name', 'product_properties.value')
                ->get();

            $product->benefits = BenefitDefect::where('product_id', $id)
                ->select('property', 'value')
                ->get();

            $product->defects = BenefitDefect::where('product_id', $id)->where('type', 2)
                ->select('property', 'value')
                ->get();

            $product->main_benefit = BenefitDefect::where('product_id', $id)->where('type', 3)
                ->select('main_benefit')
                ->first();

            $product->main_defect = BenefitDefect::where('product_id', $id)->where('type', 3)
                ->select('main_defect')
                ->first();

            $product->rules = Business::where('id', $product->business_id)->first()->rules;

            $product->available = $product->stock > 0 ? 'موجود' : 'ناموجود';

            $product->comments = Comment::where('comments.product_id', $product->product_id)
                ->where('comments.comment_id', null)->where('comments.is_active', 1)
                ->join('users', 'users.id', 'comments.user_id')
                ->select('comments.id', 'users.name as user_name', 'comments.body as comment', 'comments.created_at as date')
                ->get();

            foreach ($product->comments as $comment) {
                $comment->date = verta($comment->date)->format('Y/m/d');
                $comment->reply = Comment::where('product_id', $product->product_id)
                    ->join('users', 'users.id', 'comments.user_id')
                    ->where('comment_id', $comment->id)->where('comments.is_active', 1)
                    ->select('comments.id', 'users.name as user_name', 'comments.body as comment', 'comments.created_at as date')
                    ->get();
                foreach ($comment->reply as $reply) {
                    $reply->date = verta($reply->date)->format('Y/m/d');
                }
            }

            return response()->json($product, 200);
        }

        return response()->json([], 404);

    }

    public function newestDiscountProducts(Request $request)
    {
        $i = $request->i ?? 1;

        // TODO : Cache this
        $products = Product::where(['products.is_active' => 1])
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->orderBY('products.discount_expire_date', 'ASC')
            ->select('products.id as product_id', 'products.name', 'products.unit',
                'products.price', 'products.stock', 'products.min_product', 'products.is_active',
                'products.discount', 'products.discount_expire_date','categories.id as category_id',
                'categories.name as category_name', 'businesses.name as business_name','businesses.id as business_id',
                'categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        $count = Product::where(['products.status' => 1])->count();


        foreach ($products as $item) {
            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;
            $properyCount = ProductProperty::where('product_id', $item->product_id)->get()->count();
            $item->has_properties = ($properyCount> 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $item->product_id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : null;
        }

        return response()->json(['count' => $count, 'products' => $products], 200);
    }

    public function toggleFavoriteProduct(Request $request)
    {
        $token = JWTAuth::getToken();
        if (!JWTAuth::parseToken()->check()) {
            $response = ['message' => 'لطفا مجددا وارد شوید.'];
            return response()->json($response, 401);
        }
        $user = JWTAuth::toUser($token);
        $product_id = $request->product_id;

        $fav = Favorite::where(['product_id' => $product_id, 'user_id' => $user->id])->first();

        if ($fav) {
            $fav->delete();
            $response = ['message' => 'این محصول از لیست علاقمندی ها حذف شد.'];
        } else {
            Favorite::create([
                'user_id' => $user->id,
                'product_id' => $product_id
            ]);
            $response = ['message' => 'این محصول به لیست علاقمندی ها اضافه شد.'];
        }
        return response()->json($response, 200);
    }

    public function getProductsByBusiness(Request $request)
    {
        $i = $request->i ?? 1;
        $business_id = $request->business_id;
        if (!$business_id) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }

        // TODO : Cache this
        $products = Product::where(['products.is_active' => 1, 'products.business_id' => $business_id])
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->orderBY('products.discount_expire_date', 'ASC')
            ->select('products.id as product_id', 'products.name', 'products.is_active',
                'products.stock', 'products.min_product', 'products.price', 'products.status', 'products.discount',
                'products.discount_expire_date','categories.id as category_id','categories.name as category_name',
                'businesses.name as business_name','businesses.id as business_id',
                'categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->offset(($i - 1) * 24)->limit(24)->get();

        $count = Product::where(['products.is_active' => 1, 'products.business_id' => $business_id])->count();


        foreach ($products as $item) {
            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;

            $properyCount = ProductProperty::where('product_id', $item->id)->get()->count();
            $item->has_properties = ($properyCount> 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $item->product_id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->img = $img ? $img->path_small : null;
        }

        return response()->json(['count' => $count, 'products' => $products], 200);
    }

    public function relationProducts(Request $request)
    {
        $productId = $request->id;
        if (!$productId) {
            return response()->json(['message' => 'محصولی یافت نشد!'], 400);
        }

        $product = Product::where('id', $productId)->first();
        if (!$product) {
            return response()->json(['message' => 'محصولی یافت نشد!'], 404);
        }
        $catId = $product->category_id;

        // TODO : Cache this
        $products = Product::where('products.is_active',1)
            ->where('products.category_id', $catId)
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->where('products.id', '!=', $productId)
            ->select('products.id as product_id', 'products.name', 'products.is_active',
            'products.price', 'products.discount', 'products.discount_expire_date', 'products.stock',
            'categories.id as category_id','categories.name as category_name','businesses.name as business_name','businesses.id as business_id',
                'categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->orderBy('products.id','DESC')
            ->limit(5)->get();
        foreach ($products as $product) {
            $properyCount = ProductProperty::where('product_id', $product->product_id)->get()->count();
            $product->has_properties = ($properyCount> 0) ? 1 : 0;

            $product->discount_expire_date = checkDiscountExpireDate($product->discount_expire_date) ? $product->discount_expire_date : null;
            $product->discount = $product->discount_expire_date != null ? $product->discount : 0;

            $image = Image::where(['imageable_type' => Product::class, 'imageable_id' => $product->product_id])
                ->select(\DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $product->image = $image->path_small ?? null;
        }

        return response()->json(['relation' => $products], 200);
    }

    public function allDiscountProducts(Request $request)
    {
        $i = $request->i ?? 1;
        $now = Carbon::now()->format('Y-m-d H:i:s');
        // TODO : Cache this
        $products = Product::where('products.is_active', 1)
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->orderBY('products.discount_expire_date', 'ASC')
            ->select('products.id as product_id', 'products.name', 'products.is_active',
                'products.stock', 'products.price', 'products.discount', 'products.discount_expire_date',
                'categories.id as category_id', 'categories.name as category_name','categories.name as cat_name',
                'businesses.name as business_name','businesses.id as business_id','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->where('products.discount', '!=', 0)
            ->where('products.discount_expire_date', '!=', null)
            ->where('products.discount_expire_date' ,'>=', $now)
            ->offset(($i - 1) * 12)->limit(12)->get();

        $count = Product::where('products.is_active', 1)
            ->where('products.discount', '!=', 0)
            ->where('products.discount_expire_date', '!=', null)
            ->where('products.discount_expire_date' ,'>=', $now)
            ->get()->count();

        foreach ($products as $item) {
            $item->discount_expire_date = checkDiscountExpireDate($item->discount_expire_date) ? $item->discount_expire_date : null;
            $item->discount = $item->discount_expire_date != null ? $item->discount : 0;

            $properyCount = ProductProperty::where('product_id', $item->id)->get()->count();
            $item->has_properties = ($properyCount> 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $item->product_id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $item->image = $img->path_small ?? null;
        }

        return response()->json(['count' => $count, 'data' => $products], 200);

    }
}
