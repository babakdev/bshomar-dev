<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\DiscountCode;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersProduct;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class FinanceController extends Controller
{
    public function salesHistory(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();

        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        // TODO : Cache this
        $orders = OrdersProduct::join('payments', 'payments.order_id', 'order_product.order_id')
            ->where('order_product.business_id',$business->id)
            ->join('orders', 'orders.id', 'payments.order_id')
            ->leftJoin('delivery_addresses', 'orders.address_id', 'delivery_addresses.id')
            ->leftJoin('cities', 'cities.id', 'delivery_addresses.city_id')
            ->leftJoin('cities as state', 'cities.parent_id', 'state.id')
            ->join('users', 'users.id', 'orders.user_id')
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->groupBy('order_product.order_id')
            ->select('orders.id', 'users.name as buyer_name', 'delivery_addresses.phone as buyer_phone', 'delivery_addresses.email as buyer_email',
                'state.title as buyer_state', 'cities.title as buyer_city', 'delivery_addresses.address as buyer_address',
                'delivery_addresses.post_code as buyer_postal_code', 'orders.discount_id as discount_id', 'orders.invoice_id as factor_number',
                'orders.created_at as buy_date', 'payments.payed', 'payments.payment_method','orders.coupon_price')
            ->orderBy('orders.id', 'DESC')
//            ->groupBy('orders.id')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();
        foreach ($orders as $order) {
            $order->buy_date = verta($order->buy_date)->format('Y/m/d H:i:s');
            $order->details = OrdersProduct::where('order_product.order_id', $order->id)
                ->where('order_product.business_id', $business->id)
                ->join('orders', 'orders.id', 'order_product.order_id')
                ->select('order_product.id as id', 'order_product.name as product_name', 'order_product.code as product_code',
                    'order_product.price as price', 'orders.description', 'order_product.quantity', 'order_product.status', 'order_product.delivery_code',
                    'order_product.sent_date', 'order_product.admin_checkout_date as cancel_date','order_product.discount_price','order_product.shipping_cost')
                ->orderByDesc('order_product.order_id')->get();

            $price = 0;
            $discount_price = 0;
            $coupon_price = 0;
            $post = 0;

            if ($order->discount_id != null) {
                $discount = DiscountCode::where('id', $order->discount_id)->first();
                if($discount){
                    if($discount->business_id == $business->id){
                        $coupon_price += $order->coupon_price;
                    }
                }
                $order->coupon_price = 0;
            }
            $flag = 0;
            foreach ($order->details as $detail) {
                if($detail->status != 'ارسال شد' && $detail->status != 'تسویه شد'){
                    $flag += 1;
                }
                $detail->discount_price = (int) $detail->discount_price ;
                $price += $detail->price * $detail->quantity;
                $detail->cancel_date = $detail->cancel_date ? verta($detail->cancel_date)->format('Y/m/d') : null;
                $detail->sent_date = $detail->sent_date ? verta($detail->sent_date)->format('Y/m/d') : null;
                $discount_price += $detail->discount_price;
                $post += $detail->shipping_cost;

            }
            $order->unread = $flag > 0 ? true : false;
            $order->total_without_discount = $price + $discount_price + $post;
            $order->total_with_discount = $price + $post - $coupon_price;

        }

        $count = OrdersProduct::join('payments', 'payments.order_id', 'order_product.order_id')
            ->where('order_product.business_id',$business->id)
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->groupBy('order_product.order_id')->get()->count();

        return response()->json(['count' => $count, 'sales_history' => $orders], 200);
    }

    public function submitDeliveryCode(Request $request)
    {
        $order_product_id = $request->id;
        $code = $request->delivery_code;
        if (!$code) {
            return response()->json(['message' => 'کد رهگیری را وارد کنید.'], 404);
        }
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();
        if ($code == 'delivery') {
            $code = 'با پیک ارسال شد.';
        }
        OrdersProduct::where('id', $order_product_id)
            ->update(['delivery_code' => $code, 'status' => 'ارسال شد', 'sent_date' => now()]);

        $order_product = OrdersProduct::where('order_product.id', $order_product_id)
            ->join('orders', 'orders.id', 'order_product.order_id')
            ->join('products', 'products.id', 'order_product.product_id')
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->select('orders.user_id as user_id','orders.id as order_id', 'orders.invoice_id', 'products.name as product_name', 'order_product.delivery_code', 'businesses.id as business_id', 'businesses.phone as business_phone')
            ->first();

        $this->changeStatusForSend($order_product->order_id);

        $message = $order_product->product_name . '-' . $order_product->delivery_code;
        sendToInbox($order_product->user_id, $order_product->business_id, $order_product->business_phone, $message);

        $user = User::find($order_product->user_id);
        sendSmsWithTemplate(19969, $user->phone, 'InvoiceId', $order_product->invoice_id);

        $unreadSell = $this->computeSellBadge($business);

        return response()->json(['message' => 'کد رهگیری ارسال کالا ثبت شد و وضعیت کالا به ارسال شده تغییر کرد.', 'sellBadge' => $unreadSell], 200);
    }

    public function submitDeliveryAtHome(Request $request)
    {
        $order_product_id = $request->id;

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        OrdersProduct::where('id', $order_product_id)
            ->update(['status' => 'ارسال شد', 'sent_date' => now()]);

        $order_product = OrdersProduct::where('order_product.id', $order_product_id)
            ->join('orders', 'orders.id', 'order_product.order_id')
            ->join('products', 'products.id', 'order_product.product_id')
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->select('orders.user_id as user_id','orders.id as order_id', 'orders.invoice_id', 'products.name as product_name', 'order_product.delivery_code', 'businesses.id as business_id', 'businesses.phone as business_phone')
            ->first();

        $this->changeStatusForSend($order_product->order_id);

        $message = 'کالای ' . $order_product->product_name . 'ارسال شد.';
        sendToInbox($order_product->user_id, $order_product->business_id, $order_product->business_phone, $message);

        $user = User::find($order_product->user_id);
        sendSmsWithTemplate(19969, $user->phone, 'InvoiceId', $order_product->invoice_id);

        $unreadSell = $this->computeSellBadge($business);

        return response()->json(['message' => 'وضعیت کالا به ارسال شده تغییر کرد.', 'sellBadge' => $unreadSell], 200);
    }

    protected function changeStatusForSend($order_id)
    {
        $order = Order::find($order_id);
        $order_product_count = OrdersProduct::where('order_id',$order_id)->count();
        $order_product_sent_count = OrdersProduct::where('order_id',$order_id)->where('status','ارسال شد')->count();
        if($order_product_count == $order_product_sent_count){
            $order->status = 3;
            $order->save();
            return;
        }
        return;
    }

    public function buyHistory(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        // TODO : Cache this
        $orders = Order::where('orders.user_id', $user->id)
            ->join('payments', 'payments.order_id', 'orders.id')
            ->where('payments.type', '!=', 1)
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->select('orders.id', 'orders.description', 'orders.invoice_id', 'orders.created_at as buy_date',
                'orders.price', 'orders.discount_price', 'payments.payed', 'payments.payment_method','orders.coupon_price')
            ->orderBy('orders.id', 'DESC')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        $count = Order::where('orders.user_id', $user->id)
            ->join('payments', 'payments.order_id', 'orders.id')
            ->where('payments.type', '!=', 1)
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })->get()->count();

        foreach ($orders as $order) {
            $order->buy_date = verta($order->buy_date)->format('Y/m/d H:i:s');
            $order->payment_status = ($order->payed == 0 && $order->payment_method == 2) ? 'پرداخت نشده درب منزل' : ($order->payed == 1 ? 'پرداخت شده' : 'پرداخت ناموفق');
            $order->total_without_discount = (int)$order->price + (int)$order->discount_price + (int) $order->coupon_price;
            $order->total_with_discount = (int)$order->price;
            $order->status_name = $order->status == 2 ? 'کنسل شد' : '';

            $order->details = OrdersProduct::where('order_id', $order->id)
                ->select('name as product_name', 'code as product_code', 'price', 'quantity as count',
                    'status', 'delivery_code', 'sent_date', 'admin_checkout_date as cancel_date','discount_price','order_product.shipping_cost')
                ->get();

            $flag = 0;
            foreach ($order->details as $detail) {
                if($detail->status != 'ارسال شد' && $detail->status != 'تسویه شد'){
                    $flag += 1;
                }
                $detail->cancel_date = $detail->cancel_date ? verta($detail->cancel_date)->format('Y/m/d') : null;
                $detail->sent_date = $detail->sent_date ? verta($detail->sent_date)->format('Y/m/d') : null;
            }
            $order->unread = $flag > 0 ? true : false;
        }

        return response()->json(['count' => $count, 'buy_history' => $orders], 200);
    }

    public function computeSellBadge($business)
    {
        $unreadSell = OrdersProduct::join('payments', 'payments.order_id', 'order_product.order_id')
            ->where('order_product.business_id',$business->id)
            ->join('orders', 'orders.id', 'payments.order_id')
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->where(function ($query) {
                $query->where('order_product.status', '=', 'پرداخت شد')
                    ->orWhere('order_product.status', '=', 'در انتظار پرداخت');
            })
            ->groupBy('order_product.order_id')
            ->get()->count();

        return $unreadSell;
    }
}
