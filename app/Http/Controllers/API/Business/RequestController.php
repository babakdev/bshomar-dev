<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function requestsForBusiness(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }
        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'کسب و کار وجود ندارد.'], 400);
        }

        $count = \App\Request::where('requests.business_id', $business->id)->get()->count();
        $reqs = \App\Request::where('requests.business_id', $business->id)
            ->join('users','users.id','requests.user_id')
            ->join('products','products.id','requests.product_id')
            ->select('requests.id','users.name as sender_name','users.phone as sender_phone','requests.message',
                'products.name as product_name','requests.seen','requests.quantity','requests.created_at as date')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        foreach($reqs as $req){
            $req->update(['seen' => 1]);
            $req->date = verta($req->date)->format('Y/m/d H:i:s');
        }
        $countBadge = \App\Request::where(['requests.business_id'=> $business->id , 'seen' => 0])->get()->count();

        return response()->json(['requests' => $reqs , 'count' => $count ,'requestBadge' => $countBadge], 200);
    }
}
