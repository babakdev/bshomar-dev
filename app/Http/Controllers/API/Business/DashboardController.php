<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\BusinessModels\Certificate;
use App\BusinessModels\CustomerExport;
use App\BusinessModels\Export;
use App\BusinessModels\ManufacturingLine;
use App\BusinessModels\Qc;
use App\BusinessModels\ResearchDevelop;
use App\BusinessModels\Service;
use App\Cart;
use App\Category;
use App\City;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Image;
use App\Inbox;
use App\Order;
use App\OrdersProduct;
use App\Product;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class DashboardController extends Controller
{
    public function dashboard()
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $bus = Business::where('user_id' , $user->id)->first();
        if (!$bus) {
            return response()->json(['message' => 'کسب و کار وجود ندارد.'], 400);
        }

        $info = Business::where(['businesses.user_id' => $user->id])
            ->join('users', 'users.id', 'businesses.user_id')
            ->leftJoin('categories', 'categories.id', 'businesses.category_id')
            ->leftJoin('cities', 'cities.id', 'businesses.city_id')
            ->leftJoin('cities as states', 'cities.parent_id', 'states.id')
            ->select('businesses.id as business_id', 'businesses.*', 'cities.id as city_id', 'states.id as state_id',
                'categories.id as sub_category_id', 'categories.parent_id as category_id', 'businesses.panel_id',
                DB::raw('CONCAT("/images/front_images/businesses/headers/",businesses.header_image) as header_image'),
                DB::raw('CONCAT("/images/front_images/businesses/logos/",businesses.logo) as logo'))
            ->first();
        $business = [
            'information' => $info
        ];


        $business['information']['images'] = Image::where(['imageable_type' => Business::class, 'imageable_id' => $info->business_id])
            ->select('id', DB::raw('CONCAT("/images/front_images/businesses/large/",images.path) as path_large'),
                DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
            ->get();

        // TODO : Cache this
        $business['certificates'] = Certificate::where('business_id', $info->business_id)
            ->select('id', 'owner_name', 'certificate_type', 'certificate_number', 'certificate_name', 'exporter',
                'start_date', 'end_date', 'scope',
                DB::raw('CONCAT("/images/front_images/businesses/cert/",image) as image'))
            ->get();

        // TODO : Cache this
        $business['customer_export'] = CustomerExport::where('business_id', $info->business_id)
            ->select('id', 'name', 'country', 'state', 'product_name', 'turn_over_year',
                DB::raw('CONCAT("/images/front_images/businesses/cust/",image) as image'))
            ->get();

        $business['exports'] = Export::where('business_id', $info->business_id)
            ->select('id', 'ex_value', 'ex_of_products', 'target_market', 'ex_start_year', 'ex_type', 'ex_avg', 'ex_rules', 'langs')
            ->first();

        // TODO : Cache this
        $business['manu'] = ManufacturingLine::where('business_id', $info->business_id)
            ->select('id', 'name', 'description', DB::raw('CONCAT("/images/front_images/businesses/manu/",image) as image'))
            ->get();

        // TODO : Cache this
        $business['qc'] = Qc::where('business_id', $info->business_id)
            ->select('id', 'name', 'description', DB::raw('CONCAT("/images/front_images/businesses/qc/",image) as image'))
            ->get();

        // TODO : Cache this
        $business['rnd'] = ResearchDevelop::where('business_id', $info->business_id)
            ->select('id', 'name', 'description', DB::raw('CONCAT("/images/front_images/businesses/rnd/",image) as image'))
            ->get();

        // TODO : Cache this
        $business['service'] = Service::where('business_id', $info->business_id)
            ->select('id', 'type', 'service_info')->get();

        $business['agency'] = $info->agencies;

        $bus_panel = \App\Panel::where('id', $info->panel_id)->first();
        $panel = [
            'name' => $bus_panel->name,
            'type' => $bus_panel->type
        ];
        if ($bus_panel->type == 1) {
            $panel['expire_at'] = null;
        } else {
            $now = now();
            $panel['expire_at'] = $info->expires_at->diffIndays($now);
        }

        if($info->sub_category_id){
            // TODO : Cache this
            $parent = Category::where('id', $info->sub_category_id)->first()->parent_id;

            $business_properties = DB::table('category_properties')->where('category_properties.category_id', $parent)
                ->join('properties', 'category_properties.property_id', 'properties.id')
                ->select('properties.id', 'properties.name')->get();
        }

        $productIds = Product::where('business_id', $info->business_id)->pluck('id')->toArray();
        $products = sizeof($productIds);
        $notAvailableProducts = Product::where('business_id', $info->business_id)->where('stock',0)->count();
        $comments = Comment::where('user_id', $user->id)->count();
        $buys = Order::where('orders.user_id', $user->id)
            ->join('payments', 'payments.order_id', 'orders.id')
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })->count();

        $sells = OrdersProduct::join('payments', 'payments.order_id', 'order_product.order_id')
            ->where('order_product.business_id',$info->business_id)
            ->where('order_product.status','!=', 'کنسل شد')
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })->groupBy('order_product.order_id')->get()->count();

        $unreadMsg = Inbox::where(['user_id' => $user->id, 'seen' => 0])->count();
        $cart = Cart::where(['user_phone' => $user->phone])->count();

        $city_name = $user->city_id != null ? City::where('id', $user->city_id)->first()->title : null;
        $state_id = $user->city_id ? City::where('id', $user->city_id)->first()->parent_id : null;
        $user_customized = [
            'id' => $user->id,
            'token' => $token,
            'name' => $user->name,
            'role_id' => $user->role_id,
            'city_id' => $user->city_id,
            'state_id' => $state_id,
            'city_name' => $city_name,
            'img' => $user->image != null ? '/images/users/' . $user->image : null,
            'phone' => $user->phone,
            'email' => $user->email,
            'national_code' => $user->nationalcode,
            'score' => $user->score,
            'address' => $user->address,
        ];

        // TODO : Cache this
        $categories = Category::where(['parent_id' => 0, 'is_active' => 1])
            ->select('id', 'name')
            ->get();

        foreach ($categories as $category) {
            $category->sub_categories = Category::where(['parent_id' => $category->id, 'is_active' => 1])
                ->select('id', 'name')
                ->get();
        }

        // TODO : Cache this
        $states = City::where(['parent_id' => 0])->select('id', 'title')->get();

        $unreadSell = OrdersProduct::join('payments', 'payments.order_id', 'order_product.order_id')
            ->where('order_product.business_id',$info->business_id)
            ->join('orders', 'orders.id', 'payments.order_id')
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->where(function ($query) {
                $query->where('order_product.status', '=', 'پرداخت شد')
                    ->orWhere('order_product.status', '=', 'در انتظار پرداخت');
            })
            ->groupBy('order_product.order_id')
            ->get()->count();

        $unreadBuy =  Order::where('orders.user_id', $user->id)
            ->join('order_product', 'order_product.order_id', 'orders.id')
            ->join('payments','payments.order_id','orders.id')
            ->where('payments.type' ,2)
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->where(function ($query) {
                $query->where('order_product.status', '=', 'پرداخت شد')
                    ->orWhere('order_product.status', '=', 'در انتظار پرداخت');
            })
            ->groupBy('order_product.order_id')->count();

        $reqBadge = \App\Request::where(['requests.business_id'=> $info->business_id , 'requests.seen' => 0])->get()->count();

        $statistics = [
            'products' => $products,
            'notAvailableProducts' => $notAvailableProducts,
            'comments' => $comments,
            'buy' => $buys,
            'sell' => $sells,
            'notSend' => $unreadSell,
            'unreadMsg' => $unreadMsg,
            'cart' => $cart,
            'sellBadge' => $unreadSell,
            'buyBadge' => $unreadBuy,
            'requestBadge' => $reqBadge,
        ];

        $response = [
            'panel' => $panel,
            'statistics' => $statistics,
            'business' => $business,
            'categories' => $categories,
            'properties' => $business_properties ?? null,
            'states' => $states,
            'wallet_balance' => $user->balance,
            'user' => $user_customized
        ];

        return response()->json($response, 200);
    }
}
