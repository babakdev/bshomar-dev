<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class DistributiveController extends Controller
{
    public function update(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $rules = [
            'agencies' => 'required',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'کسب و کار وجود ندارد.'], 400);
        }
        $business->business_type = 2;
        $business->agencies = $data['agencies'];
        $business->save();

        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'agencies' => $business->agencies], 200);
    }
}
