<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class CommentController extends Controller
{
    public function commentsByUserId(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $i = $request->i ?? 1;

        // TODO : Cache this
        $comments = Comment::where(['comments.user_id' => $user->id, 'comments.is_active' => 1, 'comments.comment_id' => null])
            ->join('products', 'products.id', 'comments.product_id')
            ->select('comments.id as id', 'products.id as product_id', 'products.name as product_name', 'comments.body', 'comments.created_at as date')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();


        foreach ($comments as $comment) {
            $comment->date = verta($comment->date)->format('Y/m/d');
            $comment->reply = Comment::where(['comments.comment_id' => $comment->id, 'comments.is_active' => 1])
                ->join('users', 'users.id', 'comments.user_id')
                ->join('products', 'products.id', 'comments.product_id')
                ->select('comments.id as comment_id', 'products.id as product_id', 'products.name as product_name', 'users.name as user_name', 'comments.body', 'comments.created_at as date')
                ->get();
            foreach ($comment->reply as $reply) {
                $reply->date = verta($reply->date)->format('Y/m/d');
            }
        }

        $count = Comment::where(['comments.user_id' => $user->id, 'comments.is_active' => 1, 'comments.comment_id' => null])->count();

        return response()->json(['count' => $count, 'comments' => $comments], 200);
    }

    public function productsComments(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        $count = Comment::where(['products.business_id' => $business->id, 'comments.is_active' => 1, 'comments.comment_id' => null])
            ->join('products', 'products.id', 'comments.product_id')->count();

        // TODO : Cache this
        $comments = Comment::where(['products.business_id' => $business->id, 'comments.is_active' => 1, 'comments.comment_id' => null])
            ->join('products', 'products.id', 'comments.product_id')
            ->select('comments.id as id', 'products.id as product_id', 'products.name as product_name', 'comments.body', 'comments.created_at as date')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

        foreach ($comments as $comment) {
            $comment->date = verta($comment->date)->format('Y/m/d');
            $comment->reply = Comment::where(['comments.comment_id' => $comment->id, 'comments.is_active' => 1])
                ->join('users', 'users.id', 'comments.user_id')
                ->join('products', 'products.id', 'comments.product_id')
                ->select('comments.id as id', 'products.id as product_id', 'products.name as product_name', 'users.name as user_name', 'comments.body', 'comments.created_at as date')
                ->get();
            foreach ($comment->reply as $reply) {
                $reply->date = verta($reply->date)->format('Y/m/d');
            }
        }

        return response()->json(['count' => $count, 'comments' => $comments], 200);

    }
}
