<?php

namespace App\Http\Controllers\API\Business;

use App\BenefitDefect;
use App\Business;
use App\Cart;
use App\Http\Controllers\Controller;
use App\Image;
use App\Panel;
use App\Product;
use App\ProductProperty;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();

        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $name = $request->name;
        $filter = $request->filter;

        if ($request->name || $request->filter) {
            switch ($filter) {
                case 1:
                    $condition = 'stock';
                    $operation = '>';
                    $value = '0';
                    break;
                case 2:
                    $condition = 'stock';
                    $operation = '<=';
                    $value = '0';
                    break;
                case 3:
                    $condition = 'stock';
                    $operation = '<=';
                    $value = 'min_stock';
                    break;
                default:
                    $condition = 'category_id';
                    $operation = '>';
                    $value = '0';
            }
            if ($filter == 3) {
                // TODO : Cache this
                $products = Product::where('business_id', $business->id)
                    ->where(function ($query) {
                        $query->select()
                            ->from('products')
                            ->whereRaw('products.stock <= products.min_stock');
                    })
                    ->where('name', 'LIKE', '%' . $name . '%')
                    ->select('products.*')
                    ->orderBy('products.updated_at','DESC')
                    ->orderBy('products.id','DESC')
                    ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

                $count = Product::where('business_id', $business->id)
                    ->where(function ($query) {
                        $query->select()
                            ->from('products')
                            ->whereRaw('products.stock <= products.min_stock');
                    })
                    ->where('name', 'LIKE', '%' . $name . '%')->count();
            } else {
                // TODO : Cache this
                $products = Product::where('business_id', $business->id)
                    ->where($condition, $operation, $value)
                    ->where('name', 'LIKE', '%' . $name . '%')
                    ->select('products.*')
                    ->orderBy('products.updated_at','DESC')
                    ->orderBy('products.id','DESC')
                    ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

                $count = Product::where('business_id', $business->id)
                    ->where($condition, $operation, $value)
                    ->where('name', 'LIKE', '%' . $name . '%')->count();

            }
        } else {
            // TODO : Cache this
            $products = Product::where('products.business_id', $business->id)
                ->join('categories', 'categories.id', 'products.category_id')
                ->select('products.id', 'products.category_id', 'categories.name as cat_name', 'products.name',
                    'products.code', 'products.country', 'products.unit', 'products.min_product','products.is_active',
                    'products.description', 'products.price', 'products.stock', 'products.min_stock', 'products.meta_keywords',
                    'products.discount', 'products.discount_cycle', 'products.discount_expire_date', 'products.product_return', 'products.product_return_type',
                    'products.delivery_type', 'products.time_to_delivery', 'products.consider', 'products.created_at')
                ->orderBy('products.updated_at','DESC')
                ->orderBy('products.id','DESC')
                ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

            $count = Product::where('products.business_id', $business->id)->count();
        }

        foreach ($products as $product) {
            // TODO : Cache this
            $product->images = Image::where(['imageable_type' => Product::class, 'imageable_id' => $product->id])
                ->select('id',
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'),
                    \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'))
                ->get();

            $product->discount_expire_date_persian = ($product->discount_expire_date && $product->discount_expire_date != 'null') ? verta($product->discount_expire_date)->format('Y/m/d') : null;
            $product->created_at_persian = verta($product->created_at)->format('Y/m/d');

            // TODO : Cache this
            $product->properties = ProductProperty::where('product_id', $product->id)
                ->join('properties', 'properties.id', 'product_properties.property_id')
                ->select('properties.name', 'product_properties.value')
                ->get();

            // TODO : Cache this
            $product->benefits = BenefitDefect::where('product_id', $product->id)
                ->select('property', 'value')
                ->get();

        }

        $warning = 0;
        if ($products) {
            foreach ($products as $product) {
                if ($product->stock <= $product->min_stock) {
                    $warning++;
                    break;
                }
            }
        }

        if ($warning > 0) {
            $msg1 = 'شما تعدادی محصول با موجودی پایین دارید.در صورتی که میخواهید موجودی را افزایش دهید به قسمت ویرایش کالاها مراجعه کنید.';
        }

        $allowCreate = Business::where('id', $business->id)
            ->where('city_id', '!=', null)
            ->where('category_id', '!=', null)->count();


        if ($allowCreate == 0) {
            $allow = 0;
            $type = 'create';
            $msg = 'برای ثبت کالا ابتدا باید اطلاعات کسب و کار خود را تکمیل کنید.';
        }

        $panel = Panel::where('id', $business->panel_id)->first();
        if ($business->products()->count() >= $panel->product_limit) {
            $type = 'limit';
            $allow = 0;
            $msg = 'تعداد کالای مجاز برای شما در این پنل به پایان رسید، برای ثبت کالاهای بیشتر پنل خود را ارتقا دهید.';
        }

        $response = [
            'count' => $count,
            'products' => $products,
            'warning' => [
                'warning' => $warning,
                'message' => $msg1 ?? ''
            ],
            'allow_create' => [
                'type' => $type ?? 'default',
                'allow_create' => $allow ?? 1,
                'message' => $msg ?? ''
            ],
        ];

        return response()->json($response, 200);
    }

    public function filter(Request $request)
    {
        $i = $request->i ?? 1;
        $name = $request->name;
        $filter = $request->filter;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        switch ($filter) {
            case 1:
                $condition = 'stock';
                $operation = '>';
                $value = '0';
                break;
            case 2:
                $condition = 'stock';
                $operation = '<=';
                $value = '0';
                break;
            case 3:
                $condition = 'stock';
                $operation = '<=';
                $value = 'min_stock';
                break;
            default:
                $condition = 'category_id';
                $operation = '>';
                $value = '0';
        }
        if ($filter == 3) {
            // TODO : Cache this
            $products = Product::where('business_id', $business->id)
                ->where(function ($query) {
                    $query->select()
                        ->from('products')
                        ->whereRaw('products.stock <= products.min_stock');
                })
                ->where('name', 'LIKE', '%' . $name . '%')
                ->select('products.*')
                ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();

            // TODO : Cache this
            $count = Product::where('business_id', $business->id)
                ->where(function ($query) {
                    $query->select()
                        ->from('products')
                        ->whereRaw('products.stock <= products.min_stock');
                })
                ->where('name', 'LIKE', '%' . $name . '%')->count();
        } else {
            // TODO : Cache this
            $products = Product::where('business_id', $business->id)
                ->where($condition, $operation, $value)
                ->where('name', 'LIKE', '%' . $name . '%')
                ->select('products.*')
                ->offset(($i - 1) * $this->item_count)->limit($this->item_count)->get();
            // TODO : Cache this
            $count = Product::where('business_id', $business->id)
                ->where($condition, $operation, $value)
                ->where('name', 'LIKE', '%' . $name . '%')->count();

        }

        foreach ($products as $product) {
            $product->images = Image::where(['imageable_type' => Product::class, 'imageable_id' => $product->id])
                ->select(
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'),
                    \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'))
                ->get();
            $product->discount_expire_date_persian = verta($product->discount_expire_date)->format('Y/m/d');
            $product->created_at_persian = verta($product->created_at)->format('Y/m/d');

            // TODO : Cache this
            $product->properties = ProductProperty::where('product_id', $product->id)
                ->join('properties', 'properties.id', 'product_properties.property_id')
                ->select('properties.name', 'product_properties.value')
                ->get();

            // TODO : Cache this
            $product->benefits = BenefitDefect::where('product_id', $product->id)
                ->select('property', 'value')
                ->get();

        }

        return response()->json(['products' => $products, 'count' => $count], 200);

    }

    public function makeProductUnavailable(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $product = Product::where('id', $request->id)->first();
        if (!$product) {
            return response()->json(['message' => 'موردی یافت نشد!'], 404);
        }

        if ($product->business->id != $business->id) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $product->stock = 0;
        $product->save();

        $editedProductInCart = Cart::where('product_id', $product->id)->get();
        $message = "خریدار گرامی ، با توجه به ناموجود شدن محصول {$product->name} توسط تامین کننده، محصول مورد نظر از سبد خرید شما حذف شد.";
        foreach ($editedProductInCart as $pro) {
            $pro->delete();
            sendToInbox(User::where('phone', $pro->user_phone)->value('id'), 'ناموجود شدن کالا', 'info@bshomar.com', $message);
        }

        return response()->json(['message' => 'کالای موردنظر ناموجود شد.'], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $rules = [
            'name' => 'required | string | min:3 | max:40',
            'unit' => 'required | string | min:2 | max:40',
            'time_to_delivery' => 'required | string',
            'delivery_type' => 'required | integer',
            'product_return' => 'required | integer',
            'product_return_type' => 'required | integer',
            'country' => 'required | string',
            'description' => 'nullable | max:10000',
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000'
        ];

        $validator = Validator::make($data, $rules);
        if (!$validator->fails()) {
            if (isset($data['discount'],$data['discount_cycle']) && $data['discount'] !=0 && $data['discount_cycle'] != 0) {
                $data['discount_expire_date'] = Carbon::now()->addDay($data['discount_cycle']);
            }

            $data['business_id'] = $business->id;
            $data['category_id'] = $business->category_id;
            $data['price'] = isset($data['price']) ? (int) convertPersianNumsToEng($data['price']) : 0;
            $data['stock'] = isset($data['stock']) ? (int) convertPersianNumsToEng($data['stock']) : 0;
            $data['is_active'] = 0;
            $data['discount_cycle'] = $data['discount_cycle'] ?? 0;
            $data['discount'] = $data['discount'] ?? 0;
            $data['min_stock'] = $data['min_stock'] ?? 0;

            if( (int)$data['price'] == 0 && (isset($data['discount']) || isset($data['discount_cycle']))){
                $response = ['message' => 'برای قیمت صفر نمی توان تخفیف تعیین کرد.'];
                return response()->json($response, 400);
            }

            $product = Product::create($data);

            if (!$request->hasFile('image') || count($request->file('image')) > 4) {
                $response = ['message', 'حداقل یک تصویر و حداکثر چهار تصویر برای کالا مجاز است.'];
                return response()->json($response, 404);
            }

            $product->saveImage($request, $product);
            $this->storePropertiesInDB($data, $product->id);

            $response = ['message' => 'محصول با موفقیت ثبت شد.'];
            return response()->json($response, 200);
        } else {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }
        $product = Product::find($id);
        if (!$product) {
            return response()->json(['message' => 'محصول یافت نشد!'], 404);
        }

//        if ($product->business_id != $business->id) {
//            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
//        }

        $rules = [
            'name' => 'required | string | min:3 | max:40',
            'unit' => 'required | string | min:2 | max:40',
            'time_to_delivery' => 'required | string',
            'delivery_type' => 'required | integer',
            'product_return' => 'required | integer',
            'product_return_type' => 'required | integer',
            'country' => 'required | string',
            'description' => 'nullable | max:10000',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000'
        ];

        $validator = Validator::make($data, $rules);
        if (!$validator->fails()) {
            if (isset($data['discount'],$data['discount_cycle']) && $data['discount'] !=0 && $data['discount_cycle'] != 0) {
                $data['discount_expire_date'] = Carbon::now()->addDay($data['discount_cycle']);
            }

            $data['price'] = convertPersianNumsToEng($data['price']) ?? 0;
            $data['stock'] = convertPersianNumsToEng($data['stock']) ?? 0;
            $data['is_active'] = 0;
            $data['discount_cycle'] = $data['discount_cycle'] ?? 0;

            $product->update($data);

            $editedProductInCart = Cart::where('product_id', $product->id)->get();
            $message = "خریدار گرامی ، با توجه به ویرایش محصول {$product->name} توسط تامین کننده، محصول مورد نظر از سبد خرید شما حذف شد. لطفا دوباره به ثبت این محصول اقدام نمایید.";
            foreach ($editedProductInCart as $pro) {
                $pro->delete();
                sendToInbox(User::where('phone', $pro->user_phone)->value('id'), 'تغییر مشخصات کالا', 'info@bshomar.com', $message);
            }


            if ($request->hasFile('image') && count($product->images) + count($request->file('image')) > 4) {
                $response = ['message' => 'حداقل یک تصویر و حداکثر چهار تصویر برای کالا مجاز است.'];
                return response()->json($response, 400);
            }
            if ($request->hasFile('image') && count($request->file('image')) > 4) {
                $response = ['message' => 'حداقل یک تصویر و حداکثر چهار تصویر برای کالا مجاز است.'];
                return response()->json($response, 400);
            }
            if (count($product->images) == 0 && !$request->hasFile('image')) {
                $response = ['message' => 'حداقل یک تصویر و حداکثر چهار تصویر برای کالا مجاز است.'];
                return response()->json($response, 400);
            }

            $product->saveImage($request, $product);

            ProductProperty::where('product_id', $product->id)->delete();
            BenefitDefect::where('product_id', $product->id)->delete();

            $this->storePropertiesInDB($data, $product->id);

            $response = ['message' => 'محصول با موفقیت ویرایش شد.'];
            return response()->json($response, 200);
        } else {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }
    }

    protected function storePropertiesInDB($data, $productId)
    {
        foreach (json_decode($data['properties']) as $property) {
            ProductProperty::create([
                'product_id' => $productId,
                'property_id' => (int)$property->label,
                'value' => $property->value,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        foreach (json_decode($data['benefits']) as $b_property) {
            BenefitDefect::create([
                'product_id' => $productId,
                'property' => $b_property->label,
                'value' => $b_property->value,
                'type' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

    }

    public function destroy($id)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }
        $product = Product::find($id);
        if (!$product) {
            return response()->json(['message' => 'محصول یافت نشد!'], 404);
        }

        if ($product->business_id != $business->id) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        if (count($product->images) > 0) {
            foreach ($product->images as $image) {
                $this->checksHasImage($image);
                $image->delete();
            }
        }

        $editedProductInCart = Cart::where('product_id', $product->id)->get();
        $message = "خریدار گرامی ، با توجه به ویرایش محصول {$product->name} توسط تامین کننده، محصول مورد نظر از سبد خرید شما حذف شد. لطفا دوباره به ثبت این محصول اقدام نمایید.";
        foreach ($editedProductInCart as $pro) {
            $pro->delete();
            sendToInbox(User::where('phone', $pro->user_phone)->value('id'), 'تغییر مشخصات کالا', 'info@bshomar.com', $message);
        }

        $product->delete();

        $response = ['message' => 'محصول با موفقیت حذف شد.'];
        return response()->json($response, 200);
    }

    public function destroyImage($id)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $prePhoto = Image::where(['id' => $id])->first();

        if ($prePhoto) {
            $product = Product::find($prePhoto->imageable_id);
            if (!$product) {
                return response()->json(['message' => 'محصول یافت نشد!'], 404);
            }

            if ($product->business_id != $business->id) {
                return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
            }

            $old_product_images = Image::where(['imageable_type' => Product::class, 'imageable_id' => $product->id])
                ->select('id', \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'),
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->get()->count();

            if($old_product_images <= 1){
                return response()->json(['message' => 'محصول باید حداقل یک تصویر داشته باشد.شما مجاز به حذف این تصویر نیستید.'], 400);
            }

            if (file_exists(public_path('/images/front_images/products/small/' . $prePhoto->path))) {
                unlink('images/front_images/products/small/' . $prePhoto->path);
                unlink('images/front_images/products/large/' . $prePhoto->path);
            }
            $prePhoto->delete();
            $product = Product::find($prePhoto->imageable_id);
            if (!$product) {
                return response()->json(['message' => 'موردی یافت نشد!'], 404);
            }
            $product->images = Image::where(['imageable_type' => Product::class, 'imageable_id' => $product->id])
                ->select('id', \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'),
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->get();
            return response()->json(['message' => 'تصویر با موفقیت حذف شد.', 'images' => $product->images], 200);
        }
        return response()->json(['message' => 'تصویر موجود نیست.'], 404);
    }

    protected function checksHasImage($image): void
    {
        if (file_exists(public_path('/images/front_images/products/small/' . $image->path))) {
            unlink('images/front_images/products/small/' . $image->path);
            unlink('images/front_images/products/large/' . $image->path);
        }
    }

}
