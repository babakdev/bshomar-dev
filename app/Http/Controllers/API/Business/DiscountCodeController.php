<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\DiscountCode;
use App\DiscountProduct;
use App\DiscountUser;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class DiscountCodeController extends Controller
{
    public function allProducts()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $products = Product::where('business_id', $business->id)->select('id', 'name')->get();

        return response()->json(['products' => $products], 200);

    }

    // get all discounts and search into it
    public function index()
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $discounts = DiscountCode::where('business_id', $business->id)
            ->select('id', 'name', 'code', 'amount_type', 'amount', 'percent', 'start_date', 'end_date')
            ->get();

        foreach ($discounts as $discount) {
            $discount->is_expire = $discount->end_date < verta()->now()->format('Y/m/d') ? 1 : 0;
            $discount->products = DiscountProduct::where('discount_id', $discount->id)
                ->join('products', 'products.id', 'discount_products.product_id')
                ->select('products.id', 'products.name')->get();
        }

        return response()->json(['discounts' => $discounts], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }
        $rules = [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255|unique:discount_codes',
            'amount_type' => 'required|integer|between:1,2',
            'amount' => 'nullable:percent|integer',
            'percent' => 'nullable|integer|between:1,99',
            'products' => 'required',
            'start_date' => 'required|string|max:255',
            'end_date' => 'required|string|max:255',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        if ($request->start_date > $request->end_date) {
            $response = ['message' => 'تاریخ شروع نمی تواند بعد از تاریخ پایان باشد.'];
            return response()->json($response, 400);
        }

        if ($request->amount_type == 1 && !$request->percent) {
            $response = ['message' => 'درصد تخفیف را وارد کنید.'];
            return response()->json($response, 400);
        }

        if ($request->amount_type == 2 && !$request->amount) {
            $response = ['message' => 'مبلغ تخفیف را وارد کنید.'];
            return response()->json($response, 400);
        }

        $data['business_id'] = $business->id;
        if ($request->products) {
            $discount = DiscountCode::create($data);
            $discount_products = [];
            if ($discount) {
                foreach ($request->products as $product) {
                    $discount_products[] = [
                        'product_id' => $product,
                        'discount_id' => $discount->id
                    ];
                }
                \DB::table('discount_products')->insert($discount_products);
            } else {
                $response = ['message' => 'مشکلی در ثبت بوجود آمده است لطفا مجددا تلاش کنید.'];
                return response()->json($response, 400);
            }

        } else {
            $response = ['message' => 'محصول/محصولات موردنظر را انتخاب کنید.'];
            return response()->json($response, 400);
        }
        return response()->json(['message' => 'کد تخفیف با موفقیت ایجاد شد.'], 200);
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }
        $rules = [
            'name' => 'required|string|max:255',
            'code' => ['required', 'string', 'max:255', Rule::unique('discount_codes')->ignore($id)],
            'amount' => 'nullable:percent|integer',
            'percent' => 'nullable|integer|between:1,99',
            'products' => 'required',
            'amount_type' => 'required|integer|between:1,2',
            'start_date' => 'required|string|max:255',
            'end_date' => 'required|string|max:255',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        if ($request->start_date > $request->end_date) {
            $response = ['message' => 'تاریخ شروع نمی تواند بعد از تاریخ پایان باشد.'];
            return response()->json($response, 400);
        }

        if ($request->amount_type == 1 && !$request->percent) {
            $response = ['message' => 'درصد تخفیف را وارد کنید.'];
            return response()->json($response, 400);
        }

        if ($request->amount_type == 2 && !$request->amount) {
            $response = ['message' => 'مبلغ تخفیف را وارد کنید.'];
            return response()->json($response, 400);
        }

        $discount = DiscountCode::find($id);
        if ($request->amount_type == 1) {
            $data['amount'] = null;
            $data['percent'] = $request->percent;
        } else {
            $data['amount'] = $request->amount;
            $data['percent'] = null;
        }
        $update = $discount->update($data);
        if ($update) {
//            DiscountProduct::where('discount_id',$discount->id)->delete();
//            foreach($request->products as $product){
//                DiscountProduct::create(['discount_id' => $discount->id , 'product_id' => $product]);
//            }
            $discount->products()->sync($request->products);

            $response = ['message' => 'کد تخفیف با موفقیت ویرایش شد.'];
            return response()->json($response, 200);
        } else {
            $response = ['message' => 'مشکلی در ویرایش بوجود آمده است لطفا مجددا تلاش کنید.'];
            return response()->json($response, 400);
        }
    }

    public function destroy($id)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }
        $item = DiscountCode::find($id);
        if (!$item) {
            return response()->json(['message' => 'تخفیف وجود ندارد.'], 400);
        }
        if ($item->delete()) {
            DiscountUser::where('discount_id',$id)->where('used',0)->delete();
            $response = ['message' => 'کد تخفیف با موفقیت حذف شد.'];
            return response()->json($response, 200);
        }
    }

    public function checkForRepeatCode(Request $request)
    {
        $code = $request->code;
        $id = $request->id;
        if ($id) {
            $discountCodes = DiscountCode::where('code', $code)->where('id', '!=', $id)->first();
        } else {
            $discountCodes = DiscountCode::where('code', $code)->first();
        }
        if ($discountCodes) {
            $response = ['message' => 'کد تخفیف وارد شده تکراری است.'];
            return response()->json($response, 400);
        }

        $response = ['message' => 'ok!'];
        return response()->json($response, 200);
    }

    public function update()
    {
        return;
    }

    public function show()
    {
        return;
    }

    public function edit()
    {
        return;
    }

    public function create()
    {
        return;
    }
}
