<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class BusinessController extends Controller
{
    public function updateInfo(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        $rules = [
            'name' => 'required|min:3|max:40',
            'category_id' => 'required | sometimes',
            'city_id' => 'required | sometimes',
            'image' => 'required | sometimes',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000',
            'business_type' => 'required',
            'description' => 'nullable | max:10000',
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        return Business::updateInfo($request, $business, $user);
    }

    public function destroyImage(Request $request)
    {
        $token = JWTAuth::getToken();
        if (!$token || !$request->id) {
            return response()->json(['message' => 'درخواست نامعتبر!'], 400);
        }
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        $prePhoto = Image::where(['id' => $request->id])->first();

        if (!$prePhoto) {
            return response()->json(['message' => 'تصویر وجود ندارد.'], 404);
        }
        $old_business_images = Image::where(['imageable_type' => Business::class, 'imageable_id' => $business->id])->get()->count();
        if($old_business_images <= 1){
            return response()->json(['message' => 'کسب و کار باید حداقل یک تصویر داشته باشد.شما مجاز به حذف این تصویر نیستید.'], 400);
        }

        if (file_exists(public_path('/images/front_images/businesses/small/' . $prePhoto->path))) {
            unlink(public_path('/images/front_images/businesses/small/' . $prePhoto->path));
            unlink(public_path('/images/front_images/businesses/large/' . $prePhoto->path));
        }

        if ($prePhoto) {
            $prePhoto->delete();
            $images = Image::where(['imageable_type' => Business::class, 'imageable_id' => $business->id])
                ->select('id', \DB::raw('CONCAT("/images/front_images/businesses/large/",images.path) as path_large'),
                    \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
                ->get();
            return response()->json(['message' => 'تصویر با موفقیت حذف شد.', 'images' => $images], 200);
        }

        return response()->json(['message' => 'تصویر موجود نیست.'], 404);
    }

    public function destroyLogoOrHeader(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        if (!$request->type || !$token) {
            return response()->json(['message' => 'درخواست نامعتبر!'], 400);
        }

        if ($request->type == 1) {
            if ($business->logo) {
                $this->checkHasLogo($business);
                $business->logo = null;
            } else {
                return response()->json(['message' => 'لوگو وجود ندارد.'], 404);
            }

        } else {
            if ($business->header_image) {
                $this->checkHasHeader($business);
                $business->header_image = null;
            } else {
                return response()->json(['message' => 'هدر وجود ندارد.'], 404);
            }
        }
        $business->save();
        $business_info = Business::where(['businesses.user_id' => $user->id])
            ->join('categories', 'categories.id', 'businesses.category_id')
            ->join('cities', 'cities.id', 'businesses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->select('businesses.*', 'businesses.category_id as sub_category_id', 'categories.parent_id as category_id', 'states.id as state_id',
                \DB::raw('CONCAT("/images/front_images/businesses/headers/",businesses.header_image) as header_image'),
                \DB::raw('CONCAT("/images/front_images/businesses/logos/",businesses.logo) as logo'))
            ->first();
        $business_info->images = Image::where(['imageable_type' => Business::class, 'imageable_id' => $business->id])
            ->select('id', \DB::raw('CONCAT("/images/front_images/businesses/large/",images.path) as path_large'),
                \DB::raw('CONCAT("/images/front_images/businesses/small/",images.path) as path_small'))
            ->get();

        return response()->json(['message' => 'تصویر' . ($request->type == 1 ? ' لوگو' : ' هدر') . ' با موفقیت حذف شد.', 'information' => $business_info], 200);
    }

    protected function checkHasLogo($business): void
    {
        if (file_exists(public_path('/images/front_images/businesses/logos/' . $business->logo))) {
            unlink(public_path('/images/front_images/businesses/logos/' . $business->logo));
        }
    }

    protected function checkHasHeader($business): void
    {
        if (file_exists(public_path('/images/front_images/businesses/headers/' . $business->header_image))) {
            unlink(public_path('/images/front_images/businesses/headers/' . $business->header_image));
        }
    }
}
