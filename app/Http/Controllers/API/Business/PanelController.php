<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\Http\Controllers\Controller;
use App\lib\zarinpal;
use App\Marketer;
use App\Order;
use App\OrdersProduct;
use App\Panel;
use App\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class PanelController extends Controller
{
    public function index()
    {
        // TODO : Cache this
        $panels = Panel::where('type', '!=', 1)
            ->select('id', 'name', 'amount', 'time', 'type', 'product_limit', 'description')
            ->get();
        return response()->json(['panels' => $panels]);
    }

    public function displayFinalPriceOfPanel()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        $old_panel = Panel::where('id', $business->panel_id)->first();
        if ($old_panel->type != 1 && $business->expires_at > Carbon::now()) {
            $remind_price = $this->computeRemindPriceOfOldPanel($business);
            $msg = 'شما درحال حاضر یک پنل فعال دارید که تا تاریخ ' .
                verta($business->expires_at)->formatDate() .
                ' اعتبار دارد در صورت سفارش پنل جدید مبلغ ' .
                number_format($remind_price) .
                ' تومان ' . 'از مبلغ پنل جدید شما کسر خواهد شد.';

            return response()->json(['message' => $msg], 200);
        }
        return response()->json(['message' => ''], 400);

    }

    public function order(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        if (!$request->id) {
            return response()->json(['message' => 'یک پنل را انتخاب کنید.'], 400);
        }

        $panel = Panel::where('id', $request->id)->first();
        $remind_price = $this->computeRemindPriceOfOldPanel($business);
        if($remind_price > $panel->amount){
            return response()->json(['message' => 'امکان ارتقا به این پنل وجود ندارد.'], 400);
        }
        $total_price = $panel->amount - $remind_price;

        $order = Order::create([
            'user_id' => $user->id,
            'description' => 'ارتقا پنل',
            'status' => 1,
            'invoice_id' => generateInvoiceId(),
            'date_create' => verta(Carbon::now())->formatDate(),
            'price' => $total_price,
            'panel_id' => $panel->id,
        ]);

        if ($order) {
            $zarinpal = new zarinpal();

            $callback = ServerUrl() . '/api/business/payment/panel';
            $date = verta(Carbon::now())->formatDate();
            $res = $zarinpal->pay($order,$user->id, $total_price, 'خرید پنل از سایت بی شمار', $user->phone, $date, $callback, $order->invoice_id, 1);
            $response = ['message' => 'به درگاه بانک متصل شد', 'url' => $res];
            return response()->json($response, 200);
        }
        return response()->json(['message' => 'سفارش شما ثبت نشد لطفا مجددا امتحان کنید'], 400);
    }

    public function payment(Request $request)
    {
        $MerchantID = 'e55ffa32-13ee-11e9-9579-005056a205be';
        $Authority = $_GET['Authority'];

        $payment = Payment::where('authority',$Authority)->first();

        if (!isset($Authority)) {
            $response = ['message' => 'شما به صورت غیر مجاز به این صفحه وارد شدید.'];
            return response()->json($response, 404);
        }
        $user_id = $payment->user_id;
        $invoiceId = $payment->invoice_id;
        $order = Order::where(['user_id' => $user_id ,'invoice_id'=> $invoiceId])->first();
        if (!$order) {
            $url = SiteUrl() . '/failed-order';
            return redirect($url);
        }
        $Amount = $order->price;
        if ($request->get('Status') === 'OK') {
            $client = new \nusoap_client('https://www.zarinpal.com/pg/services/WebGate/wsdl', 'wsdl');
            $client->soap_defencoding = 'UTF-8';
            $result = $client->call('PaymentVerification', [
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ],
            ]);

            if ($result['Status'] == 100) {
                $refid = $result['RefID'];

                $payment->payed = 1;
                $payment->reference = $refid;
                $payment->save();

                $bsn = Business::where(['user_id' => $order->user_id])->first();
                // temp = old panel_id
                $temp = $bsn->panel_id;

                Business::where(['user_id' => $order->user_id])->update(['panel_id' => $order->panel_id, 'expires_at' => now()->addYear()]);

                $business = Business::where('user_id', $order->user_id)->first();
                $panel = Panel::where('id', $business->panel_id)->first();

                if ($panel->type != 1) {
                    $message = 'همکار گرامی پرداخت شما با موفقیت انجام شد و پنل کاربری شما به پنل ' . $panel->name . ' ارتقا یافت.';
                    sendToInbox($order->user_id, 'بخش مالی سایت بی شمار', 'financial@bshomar.com', $message);
                }

                \Log::info('پنل ' . $panel->name . ' فروخته شد');

                $user = User::find($order->user_id);
                $msg = 'پنل کسب و کار به مبلغ ' . $Amount . ' تومان فروخته شد به کاربر با یوزرنیم ' . $user->phone;
                sendToInbox('1', 'financial@bshomar.com', 'financial@bshomar.com', $msg);

                // send gift for business and marketer with notification
                $this->giftForBusinessUserAndMarketerWithNotify($order->user_id, $panel->type, $temp);

                $url = SiteUrl() . '/success-order?invoice=' . $refid . '&amount=' . $Amount;
                return redirect($url);

            } else {
                $this->removeFromDB($order,$payment);
                $url = SiteUrl() . '/failed-order';
                return redirect($url);
            }
        } else {
            $this->removeFromDB($order,$payment);
            $url = SiteUrl() . '/failed-order';
            return redirect($url);
        }

    }

    protected function giftForBusinessUserAndMarketerWithNotify($businessUserId, $type, $pldPanelId)
    {
        $marketer_business = Marketer::where('business_id', $businessUserId)->first();
        $businessUser = User::findOrFail($businessUserId);

        if ($pldPanelId == 1 && $marketer_business) {
            $business = Business::where('id', $businessUserId)->first();
            $panel = Panel::where('id', $business->panel_id)->first();

            // 10% gift for business user
            $giftForBusiness = $panel->amount * 10 / 100;
            $businessUser->deposit($giftForBusiness, ['واریز ده درصد مبلغ ارتقا پنل ']);
            $msg1 = $businessUser->name . ' گرامی , مبلغ ' . $giftForBusiness . 'تومان به عنوان پورسانت به کیف پول شما واریز شد.';
            sendToInbox($businessUser->id, 'واریز ده درصد مبلغ ارتقا پنل ', 'financial@bshomar.com', $msg1);

            // 30% gift for marketer
            if ($marketer_business) {
                $gift = ($panel->amount - 3000) * 30 / 100;
                $marketer = User::findOrFail($marketer_business->marketer_id);

                if ($type == 2) {
                    $score = 10;
                } elseif ($type == 3) {
                    $score = 20;
                } elseif ($type == 4) {
                    $score = 30;
                } else {
                    $score = 0;
                }

                if ($score != 0) {
                    $marketer->deposit($gift, ['واریز پورسانت ارتقا پنل']);
                    increaseUserRateWithNotify($marketer->id, $score);
                    increaseUserRateWithNotify($businessUserId, $score);
                }

                $msg1 = $marketer->name . ' گرامی , مبلغ ' . $gift . 'تومان به عنوان پورسانت به کیف پول شما واریز شد.';
                sendToInbox($marketer->id, 'واریز پورسانت ارتقا پنل', 'finance@bshomar.com', $msg1);

                // delete record from marketers table
                Marketer::where(['business_id' => $businessUserId, 'marketer_id' => $marketer->id])->delete();
            }
        }
    }

    protected function computeRemindPriceOfOldPanel($business)
    {
        $old_panel = Panel::where('id', $business->panel_id)->first();
        if ($business->panel_id != 1) {
            $remind_days = Carbon::make($business->expires_at)->diffInDays(now());
            $remind_price = ($old_panel->amount / 365) * $remind_days;

            return round($remind_price);
        }
        return 0;

    }

    protected function removeFromDB($order,$payment)
    {
        $payment->delete();
        $order->delete();
    }
}
