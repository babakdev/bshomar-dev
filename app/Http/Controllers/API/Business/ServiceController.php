<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\BusinessModels\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ServiceController extends Controller
{
    public function update(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 3;
        $business->save();

        $res = Service::persist($data, $business);
        return $res;
    }
}
