<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\ResearchDevelop;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class RndController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = ResearchDevelop::persist($data, $business);
        return $res;
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = ResearchDevelop::updateItem($data, $id, $business);
        return $res;
    }

    public function destroy($id)
    {
        $rnd = ResearchDevelop::where('id', $id)->first();
        if ($rnd) {
            if (file_exists(public_path('/images/front_images/businesses/rnd/' . $rnd->image))) {
                unlink(public_path('/images/front_images/businesses/rnd/' . $rnd->image));
            }
            $rnd->delete();
        }

        if (!request('admin')) {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            $business = Business::where('user_id', $user->id)->first();

            $rnds = ResearchDevelop::where('business_id', $business->id)
                ->select('id', 'name', 'description')
                ->get();
            foreach ($rnds as $rnd) {
                $rnd->image = "/images/front_images/businesses/rnd/" . $rnd->image;
            }
            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'rnd' => $rnds], 200);
        }
    }
}
