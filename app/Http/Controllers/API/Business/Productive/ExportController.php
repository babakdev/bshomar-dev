<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\CustomerExport;
use App\BusinessModels\Export;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ExportController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = Export::persist($data, $business);
        return $res;
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = Export::updateItem($data, $id, $business);
        return $res;
    }

    public function destroy($id)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();

        $ex = Export::where('id', $id)->first();
        if ($ex) {
            $ex->delete();
            CustomerExport::where('business_id', $business->id)->delete();
        }

        $exports = Export::where('business_id', $business->id)
            ->select('ex_value', 'ex_of_products', 'target_market', 'ex_start_year', 'ex_type', 'ex_avg', 'ex_rules', 'langs')
            ->get();
        return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ویرایش شد.', 'exports' => $exports], 200);
    }
}
