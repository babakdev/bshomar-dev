<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\ManufacturingLine;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ManufacturingController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = ManufacturingLine::persist($data, $business);
        return $res;
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربری یافت نشد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();

        if (!$business) {
            return response()->json(['message' => 'کسب و کاری یافت نشد.'], 404);
        }
        $business->business_type = 1;
        $business->save();

        $res = ManufacturingLine::updateItem($data, $id, $business);
        return $res;
    }

    public function destroy($id)
    {
        $manu = ManufacturingLine::where('id', $id)->first();
        if ($manu) {
            if (file_exists(public_path('/images/front_images/businesses/manu/' . $manu->image))) {
                unlink(public_path('/images/front_images/businesses/manu/' . $manu->image));
            }
            $manu->delete();
        }
        if (!request('admin')) {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            $business = Business::where('user_id', $user->id)->first();

            $manus = ManufacturingLine::where('business_id', $business->id)
                ->select('id', 'name', 'description')
                ->get();
            foreach ($manus as $manu) {
                $manu->image = "/images/front_images/businesses/manu/" . $manu->image;
            }

            return response()->json(['message' => 'خط تولید با موفقیت حذف شد.', 'manufacturing' => $manus], 200);
        }
    }
}
