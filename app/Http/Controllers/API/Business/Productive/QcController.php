<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\Qc;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class QcController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = Qc::persist($data, $business);
        return $res;
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = Qc::updateItem($data, $id, $business);
        return $res;
    }

    public function destroy($id)
    {
        $qcItem = Qc::where('id', $id)->first();
        if ($qcItem) {
            if (file_exists(public_path('/images/front_images/businesses/qc/' . $qcItem->image))) {
                unlink(public_path('/images/front_images/businesses/qc/' . $qcItem->image));
            }
            $qcItem->delete();
        }

        if (!request('admin')) {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            $business = Business::where('user_id', $user->id)->first();
            $qcs = Qc::where('business_id', $business->id)
                ->select('id', 'name', 'description')
                ->get();
            foreach ($qcs as $qc) {
                $qc->image = "/images/front_images/businesses/qc/" . $qc->image;
            }
            return response()->json(['message' => 'واحد کنترل کیفیت با موفقیت حذف شد.', 'qcs' => $qcs], 200);
        }
    }
}
