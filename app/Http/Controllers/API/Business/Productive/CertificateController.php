<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\Certificate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class CertificateController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = Certificate::persist($data, $business);
        return $res;
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = Certificate::updateItem($data, $id, $business);
        return $res;
    }

    public function destroy($id)
    {
        $cert = Certificate::where('id', $id)->first();
        if ($cert) {
            if (file_exists(public_path('/images/front_images/businesses/cert/' . $cert->image))) {
                unlink(public_path('/images/front_images/businesses/cert/' . $cert->image));
            }
            $cert->delete();
        }

        if (!request('admin')) {

            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            $business = Business::where('user_id', $user->id)->first();

            $certs = Certificate::where('business_id', $business->id)
                ->select('id', 'owner_name', 'certificate_type', 'certificate_number', 'certificate_name', 'exporter',
                    'start_date', 'end_date', 'scope')
                ->get();
            foreach ($certs as $cert) {
                $cert->image = '/images/front_images/businesses/cert/' . $cert->image;
            }

            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت ثبت شد.', 'certificates' => $certs], 200);
        }
    }
}
