<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\CustomerExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class CustomerExportController extends Controller
{
    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = CustomerExport::persist($data, $business);
        return $res;
    }

    public function updateItem(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $business = Business::where('user_id', $user->id)->first();
        $business->business_type = 1;
        $business->save();

        $res = CustomerExport::updateItem($data, $id, $business);
        return $res;
    }

    public function destroy($id)
    {
        $cust = CustomerExport::where('id', $id)->first();
        if ($cust) {
            if (file_exists(public_path('/images/front_images/businesses/cust/' . $cust->image))) {
                unlink(public_path('/images/front_images/businesses/cust/' . $cust->image));
            }
            $cust->delete();
        }

        if (!request('admin')) {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);

            $business = Business::where('user_id', $user->id)->first();

            $custs = CustomerExport::where('business_id', $business->id)
                ->select('id', 'name', 'country', 'state', 'product_name', 'turn_over_year')
                ->get();
            foreach ($custs as $cust) {
                $cust->image = '/images/front_images/businesses/cust/' . $cust->image;
            }

            return response()->json(['message' => 'اطلاعات کسب و کار شما با موفقیت حذف شد.', 'customers' => $custs], 200);
        }
    }
}
