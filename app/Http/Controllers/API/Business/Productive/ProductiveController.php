<?php

namespace App\Http\Controllers\API\Business\Productive;

use App\Business;
use App\BusinessModels\Certificate;
use App\BusinessModels\CustomerExport;
use App\BusinessModels\Export;
use App\BusinessModels\ManufacturingLine;
use App\BusinessModels\Qc;
use App\BusinessModels\ResearchDevelop;
use App\BusinessModels\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductiveController extends Controller
{
    public function deleteAllItem(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $business = Business::where('user_id', $user->id)->first();

        $type = $request->type;
        switch ($type) {
            case 1:
                ManufacturingLine::where('business_id', $business->id)->delete();
                break;
            case 2:
                Qc::where('business_id', $business->id)->delete();
                break;
            case 3:
                ResearchDevelop::where('business_id', $business->id)->delete();
                break;
            case 4:
                Certificate::where('business_id', $business->id)->delete();
                break;
            case 5:
                Export::where('business_id', $business->id)->delete();
                break;
            case 6:
                CustomerExport::where('business_id', $business->id)->delete();
                break;
            case 7:
                Service::where('business_id', $business->id)->delete();
                break;
            case 8:
                $business->agencies = null;
                break;
        }

        $response = ['message' => 'با موفقیت حذف شد.'];
        return response()->json($response, 200);
    }
}
