<?php

namespace App\Http\Controllers\API\Business;

use App\Business;
use App\Http\Controllers\Controller;
use App\OrdersProduct;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ChartController extends Controller
{
    public function statistics(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }
        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }
        if (!$request->type || !$request->sub || !$request->from || !$request->to || !$request->date) {
            return response()->json(['message' => 'لطفا تمام فیلد ها را پر کنید.'], 400);
        }
        $type = $request->type;  // sale,buy
        /*
            type = sale
            1: Sale order by product
            2: Count of sale order by product
            3: Sale order by month
            4: Count of sale order by month

            type = buy
            1: Buy
            2: Count of buy

        */
        $sub = $request->sub;
        $from = $request->from;
        $to = $request->to;
        $date = $request->date ?? null; // now,month(this month),year(this year)

        $now = verta()->now()->format('Y/m/d');
        $month = verta()->now()->month;
        $year = verta()->now()->year;

        if ($type == 'sale') {
            $data = $this->get_sale_chart($business, $sub, $from, $to, $date, $now, $month, $year);
        } elseif ($type == 'buy') {
            $data = $this->get_buy_chart($user->id, $sub, $from, $to, $date, $now, $month, $year);
        }
        return response()->json($data, 200);

    }

    protected function get_sale_chart($business, $sub, $from, $to, $date, $now, $month, $year)
    {
        $data = OrdersProduct::where('products.business_id', $business->id)
            ->join('products', 'products.id', 'order_product.product_id')
            ->join('orders', 'orders.id', 'order_product.order_id')
            ->where('order_product.status', 'پرداخت شد');
        if ($sub == 1 || $sub == 2) {
            $data = $data->groupBy('order_product.product_id');
        }

        if ($date != null) {
            switch ($date) {
                case 'now':
                    $data = $data->where('orders.date_create', $now);
                    break;
                case 'month':
                    $start = $year . '/' . $month . '/01';
                    $end = $year . '/' . $month . '/30';
                    $data = $data->whereBetween('orders.date_create', [$start, $end]);
                    break;
                case 'year':
                    $start = $year . '/01/01';
                    $end = $year . '/12/30';
                    $data = $data->whereBetween('orders.date_create', [$start, $end])
                        ->orderBy('orders.date_create', 'ASC');
                    break;
            }
        } else {
            if ($from && $to) {
                $data = $data->whereBetween('orders.date_create', [$from, $to]);
            }
        }

        switch ($sub) {
            case 1:
                $data = $data->select('products.name as product_name', \DB::raw('SUM(order_product.price) as value'))
                    ->get();
                break;
            case 2:
                $data = $data->select('products.name as product_name', \DB::raw('SUM(order_product.quantity) as value'))
                    ->get();
                break;
            case 3:
                $data = $data->select('orders.date_create', 'order_product.quantity', 'order_product.price')
                    ->get()->groupBy(function ($item) {
                        return Verta::parse($item->date_create)->format('m');
                    });

                $data = $data->values();
                $data = $this->create_result_array($data, 'price');
                break;
            case 4:
                $data = $data->select('orders.date_create', 'order_product.quantity', 'order_product.price')
                    ->get()->groupBy(function ($item) {
                        return Verta::parse($item->date_create)->format('m');
                    });
                $data = $data->values();
                $data = $this->create_result_array($data, 'quantity');
                break;
        }
        return $data;
    }

    protected function create_result_array($data, $field)
    {
        $result = [];
        $dataSize = sizeof($data);
        for ($i = 0; $i < $dataSize; $i++) {
            $temp = 0;
            foreach ($data[$i] as $value) {
                $temp += $value->{$field};
            }
            $result[$i]['month'] = Verta::parse($data[$i]->first()->date_create)->format('m');
            $result[$i]['month_name'] = Verta::parse($data[$i]->first()->date_create)->format('F');
            $result[$i]['value'] = $temp;
        }
        return $result;
    }

    protected function get_buy_chart($userId, $sub, $from, $to, $date, $now, $month, $year)
    {
        $data = OrdersProduct::where('orders.user_id', $userId)
            ->join('orders', 'orders.id', 'order_product.order_id')
            ->where('order_product.status', 'پرداخت شد');


        if ($date != null) {
            switch ($date) {
                case 'now':
                    $data = $data->where('orders.date_create', $now);
                    break;
                case 'month':
                    $start = $year . '/' . $month . '/01';
                    $end = $year . '/' . $month . '/30';
                    $data = $data->whereBetween('orders.date_create', [$start, $end]);
                    break;
                case 'year':
                    $start = $year . '/01/01';
                    $end = $year . '/12/30';
                    $data = $data->whereBetween('orders.date_create', [$start, $end])
                        ->orderBy('orders.date_create', 'ASC');
                    break;
            }
        } else {
            if ($from && $to) {
                $data = $data->whereBetween('orders.date_create', [$from, $to]);
            }
        }

        switch ($sub) {
            case 1:
                $data = $data->select('orders.date_create', 'order_product.quantity', 'order_product.price')
                    ->get()->groupBy(function ($item) {
                        return Verta::parse($item->date_create)->format('m');
                    });

                $data = $data->values();
                $data = $this->create_result_array($data, 'price');
                break;
            case 2:
                $data = $data->select('orders.date_create', 'order_product.quantity', 'order_product.price')
                    ->get()->groupBy(function ($item) {
                        return Verta::parse($item->date_create)->format('m');
                    });
                $data = $data->values();
                $data = $this->create_result_array($data, 'quantity');
                break;
        }
        return $data;
    }
}
