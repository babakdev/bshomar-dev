<?php

namespace App\Http\Controllers\API\Auth;

use App\City;
use App\Http\Controllers\Auth\LoginController;
use App\SmsIR_VerificationCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthLoginController extends LoginController
{
    public function __construct()
    {
        $this->guard();
    }

    public function api_login(Request $request)
    {
        if (app(LoginController::class)->hasTooManyLoginAttempts($request)) {
            app(LoginController::class)->fireLockoutEvent($request);
            $response = ['message' => 'ورود ناموفق! لطفا چند دقیقه دیگر مجددا تلاش کنید.'];
            return response()->json($response, 400);
        }

        $credentials = $request->only('phone', 'password' ,'type');
        if (!isset($credentials['phone']) || empty($credentials['phone'])) {
            $response = ['message' => 'شماره تلفن همراه را وارد کنید.'];
            return response()->json($response, 400);
        }
        if (!isset($request->password) || empty($request->password)) {
            $response = ['message' => 'رمزعبور را وارد کنید.'];
            return response()->json($response, 400);
        }

        $phone = convertPersianNumsToEng($credentials['phone']);
        $password = $request->password;

        // attempt to verify the credentials and create a token for the user
        $user = User::where('phone', $phone)->first();
        if (!$user) {
            $response = ['message' => 'کاربری با این مشخصات وجود ندارد.'];
            return response()->json($response, 400);
        }
        if ($user->isAdmin) {
            $response = ['message' => 'کاربری با این مشخصات وجود ندارد.'];
            return response()->json($response, 400);
        }
        if( ($request->type == 'panel') && ($user->role_id != 3) ){
            $response = ['message' => 'کسب و کاری با این مشخصات وجود ندارد.'];
            return response()->json($response, 400);
        }
        if( ($request->type == 'site') && ($user->role_id == 5) ){
            $response = ['message' => 'کاربری با این مشخصات وجود ندارد.'];
            return response()->json($response, 400);
        }
        if ($user && Hash::check($password, $user->password)) {
            if ($user->is_active && app(LoginController::class)->attemptLogin($request)) {
                $user->update([
                    'last_login_at' => Carbon::now()->toDateTimeString(),
                    'last_login_ip' => $request->getClientIp()
                ]);
                return $this->SuccessLogin($user);
            } else {
                app(LoginController::class)->incrementLoginAttempts($request);

                $code = rand(1111, 9999);
                $user->update(['code' => $code]);
                sendSmsWithTemplate(25140,$user->phone,'VerificationCode', $code);
                $response = ['message' => 'کدفعالسازی برای شما ارسال شد.','type' => 'verify'];
                return response()->json($response, 200);
            }
        } else {
            app(LoginController::class)->incrementLoginAttempts($request);
            $response = ['message' => 'کاربری با این مشخصات وجود ندارد.'];
            return response()->json($response, 400);
        }
    }

    protected function SuccessLogin($user): \Illuminate\Http\JsonResponse
    {
        Auth::login($user);
        $token = JWTAuth::fromUser($user);

        $city_name = $user->city_id != null ? City::where('id', $user->city_id)->first()->title : null;
        $user_customized = [
            'id' => $user->id,
            'token' => $token,
            'name' => $user->name,
            'role_id' => $user->role_id,
            'city_id' => $user->city_id,
            'city_name' => $city_name,
            'img' => $user->image != null ? '/images/users/' . $user->image : null,
            'phone' => $user->phone,
            'email' => $user->email,
            'national_code' => $user->nationalcode,
            'score' => $user->score,
            'address' => $user->address,
            'supplier' => $user->role_id == 3 ? true : false
        ];
        $response = ['message' => 'شما با موفقیت وارد شدید.', 'user' => $user_customized,'type' => 'login'];
        return response()->json($response, 200);
    }

    public function api_logout()
    {
        $token = JWTAuth::getToken();
        if (!$token) {
            return response()->json(['message' => 'درخواست نامعتبر!'], 401);
        }
        Auth::guard('api')->logout();

        return response()->json(['message' => 'logout successfully!'], 200);
    }

}
