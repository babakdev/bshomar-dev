<?php

namespace App\Http\Controllers\API\Auth;

use App\Business;
use App\City;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    public function api_register(Request $request)
    {
        $credentials = $request->only('name', 'phone', 'password', 'password_confirmation', 'type', 'marketer','business_type');

        if (!isset($credentials['name']) or empty($credentials['name'])) {
            $response = ['message' => 'نام را وارد کنید.'];
            return response()->json($response, 400);
        }

        if (!isset($credentials['phone']) or empty($credentials['phone'])) {
            $response = ['message' => 'شماره تلفن همراه را وارد کنید.'];
            return response()->json($response, 400);
        }

        $user = User::where('phone', $credentials['phone'])->first();
        if ($user) {
            $response = ['message' => 'شماره تلفن همراه تکراری است.'];
            return response()->json($response, 400);
        }

        if (!isset($request->password) or empty($request->password)) {
            $response = ['message' => 'رمزعبور را وارد کنید.'];
            return response()->json($response, 400);
        }

        if (!isset($request->password_confirmation) or empty($request->password_confirmation)) {
            $response = ['message' => 'تکرار رمزعبور را وارد کنید.'];
            return response()->json($response, 400);
        }

        if ($request->password != $request->password_confirmation) {
            $response = ['message' => 'تکرار رمزعبور مطابقت ندارد.'];
            return response()->json($response, 400);
        }

        if (isset($request->marketer) && !empty($request->marketer)) {
            $marketerUser = User::where('phone', trim($request->marketer))->first();
            if (!$marketerUser) {
                $response = ['message' => 'کد معرف شما معتبر نیست در صورتی که کد معرف ندارید این گزینه را خالی بگذارید.'];
                return response()->json($response, 400);
            }
        }

        $phone = convertPersianNumsToEng($credentials['phone']);
        $name = $credentials['name'];
        $password = $request->password;
        $type = $credentials['type'] ?? 1;

        $user = new User();
        $user->name = $name;
        $user->phone = $phone;
        $user->password = bcrypt($password);
        $user->role_id = $type == 1 ? 4 : 3;


        if ($user->save()) {
            $code = rand(1111, 9999);
            $user->update(['code' => $code]);
            sendSmsWithTemplate(25140,$user->phone,'VerificationCode', $code);
            if ($type == 2) {
                Business::create([
                    'name' => $name,
                    'phone' => convertPersianNumsToEng($phone),
                    'user_id' => $user->id,
                    'business_type' => $credentials['business_type'] ?? 1,
                ]);
            }

            $response = ['message' => 'کدفعال سازی ارسال شد.'];
            return response()->json($response, 200);
        } else {
            $response = ['message' => 'ثبت نام با مشکل مواجه شد.'];
            return response()->json($response, 400);
        }
    }

    public function verifyCode(Request $request)
    {
        $type = $request->type ?? 'register';
        if (!$request->verify_code) {
            $response = ['message' => 'اطلاعات نامعتبر!'];
            return response()->json($response, 400);
        }
        $verify_code = convertPersianNumsToEng($request->verify_code);
        $phone = convertPersianNumsToEng(trim($request->phone));
        $userByMobile = User::where('users.phone', $phone)->first();

        if (!$userByMobile) {
            $response = ['message' => 'کاربری با این مشخصات یافت نشد.'];
            return response()->json($response, 404);
        }

        $user = User::where(['code' => $verify_code, 'phone' => $phone])->first();
        if (!$user) {
            $response = ['message' => 'کد فعال سازی صحیح نمی باشد.'];
            return response()->json($response, 400);
        } else {
            $user->update(['code' => null, 'is_active' => 1]);
            $token = JWTAuth::fromUser($user);

            if ($type == 'register' && $user->isBusiness) {
                $mPhone = $request->marketer;
                if ($mPhone) {
                    $this->sendGiftForMarketerAndBusiness($mPhone, $user->id);
                }
            }
            $message = 'با سلام و خوش آمد خدمت شما کاربر گرامی
            ورود شما به پلتفرم اقتصادی بی شمار مایه فخر و مباهات ما می باشد، باشد که همه بتوانیم با یاری یکدیگر قدم استوار و موثری در جهت پیشرفت کشور عزیزمان ایران برداریم. حال می تواند محصولات خود را بارگذاری نمایید.
            همراه همیشگی اقتصاد ایران ، بی شمار';
            sendToInbox($user->id, 'مدیریت سایت بی شمار', 'admin@bshomar.com', $message);

            auth()->loginUsingId($user->id);
            $city_name = $user->city_id != null ? City::where('id', $user->city_id)->first()->title : null;
            $user_customized = [
                'id' => $user->id,
                'token' => $token,
                'name' => $user->name,
                'role_id' => $user->role_id,
                'supplier' => $user->role_id == 3 ? true : false,
                'city_name' => $city_name,
                'img' => $user->image != null ? '/images/users/' . $user->image : null,
                'phone' => $user->phone,
                'email' => $user->email,
                'national_code' => $user->nationalcode,
                'score' => $user->score,
                'address' => $user->address,
            ];

            if ($type == 'forget') {
                $response = ['message' => 'کد صحیح است!', 'user' => $user_customized];
            } else {
                $response = ['message' => 'شما وارد حساب کاربری خود شدید.', 'user' => $user_customized];
            }
            $user->update(['code' => null]);
            return response()->json($response, 200);
        }
    }

    public function sendGiftForMarketerAndBusiness($marketerPhone, $businessUserId)
    {
        //marketer
        $marketer = User::where('phone', $marketerPhone)->first();
        if ($marketer) {
            $marketer->deposit(3000, ['واریز پورسانت 3 هزار تومانی']);
            $msg1 = $marketer->name . ' گرامی , مبلغ 3000 تومان به عنوان پورسانت به کیف پول شما واریز شد.';
            sendToInbox($marketer->id, 'واریز پورسانت', 'finance@bshomar.com', $msg1);

            increaseUserRateWithNotify($marketer->id, 1);

            \DB::table('marketers')->insert([
                'marketer_id' => $marketer->id,
                'business_id' => $businessUserId
            ]);

            $business = User::where('id', $businessUserId)->first();
            if ($business->isBusiness) {
                increaseUserRateWithNotify($businessUserId, 1);
            }


        }
    }
}
