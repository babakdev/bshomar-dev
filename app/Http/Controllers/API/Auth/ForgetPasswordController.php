<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ForgetPasswordController extends Controller
{
    public function forgetPassword(Request $request)
    {
        $phone = $request->phone;
        if (!$phone) {
            return response()->json(['message' => 'درخواست نامعتبر!'], 400);
        }
        $user = User::where('phone', $phone)->first();
        if (!$user) {
            return response()->json(['message' => 'کاربری با این مشخصات وجود ندارد.'], 400);
        }
        $code = rand(1111, 9999);
        $user->update(['code' => $code]);
        sendSmsWithTemplate(25140,$user->phone,'VerificationCode', $code);
        $response = ['message' => 'کد برای شما ارسال شد.'];
        return response()->json($response, 200);

    }

    public function changePassword(Request $request)
    {
        $token = JWTAuth::getToken();
        if (!$token) {
            return response()->json(['message' => 'درخواست نامعتبر!'], 401);
        }
        $user = JWTAuth::toUser($token);

        if (!isset($request->password) or empty($request->password)) {
            $response = ['message' => 'رمزعبور را وارد کنید.', 'status' => false];
            return response()->json($response, 400);
        }

        if (!isset($request->password_confirmation) or empty($request->password_confirmation)) {
            $response = ['message' => 'تکرار رمزعبور را وارد کنید.', 'status' => false];
            return response()->json($response, 400);
        }

        if ($request->password != $request->password_confirmation) {
            $response = ['message' => 'تکرار رمزعبور مطابقت ندارد.', 'status' => false];
            return response()->json($response, 400);
        }

        $user->update(['code' => null, 'password' => bcrypt($request->password)]);
        $response = ['message' => 'رمزعبور شما با موفقیت تغییر یافت.'];
        return response()->json($response, 200);
    }
}
