<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersProduct;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class FinanceController extends Controller
{
    public function buyHistory(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        // TODO : Cache this
        $orders = Order::where('orders.user_id', $user->id)
            ->join('payments', 'payments.order_id', 'orders.id')
            ->where('payments.type', '!=', 1)
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })
            ->select('orders.id', 'orders.description', 'orders.invoice_id', 'orders.created_at as buy_date',
                'orders.price', 'orders.discount_price', 'payments.payed', 'payments.payment_method','orders.coupon_price')
            ->orderBy('orders.id', 'DESC')
            ->offset(($i - 1) * 15)->limit(15)->get();

        $count = Order::where('orders.user_id', $user->id)
            ->join('payments', 'payments.order_id', 'orders.id')
            ->where('payments.type', '!=', 1)
            ->where(function ($query) {
                $query->where('payed', '=', 1)
                    ->where('payment_method','=',1)
                    ->orWhere('payed', '=', 0)
                    ->where('payment_method', '=', 2);
            })->get()->count();

        foreach ($orders as $order) {
            $order->buy_date = verta($order->buy_date)->format('Y/m/d H:i:s');
            $order->payment_status = ($order->payed == 0 && $order->payment_method == 2) ? 'پرداخت نشده درب منزل' : ($order->payed == 1 ? 'پرداخت شده' : 'پرداخت ناموفق');
            $order->total_without_discount = (int)$order->price + (int)$order->discount_price + (int) $order->coupon_price;
            $order->total_with_discount = (int)$order->price;
            $order->status_name = $order->status == 2 ? 'کنسل شد' : '';

            $order->details = OrdersProduct::where('order_id', $order->id)
                ->select('name as product_name', 'code as product_code', 'price', 'quantity as count',
                    'status', 'delivery_code', 'sent_date', 'admin_checkout_date as cancel_date','discount_price','order_product.shipping_cost')
                ->get();

            foreach ($order->details as $detail) {
                $detail->cancel_date = $detail->cancel_date ? verta($detail->cancel_date)->format('Y/m/d') : null;
                $detail->sent_date = $detail->sent_date ? verta($detail->sent_date)->format('Y/m/d') : null;
            }
        }

        return response()->json(['count' => $count, 'buy_history' => $orders], 200);
    }
}
