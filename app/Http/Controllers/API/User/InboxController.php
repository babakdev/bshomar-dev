<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Inbox;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class InboxController extends Controller
{
    public function inbox(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $contacts = Inbox::where('user_id', $user->id)->groupBy('sent_by')
            ->select('sent_by as contact', 'sender_id as id')
            ->orderBy('created_at', 'DESC')
            ->offset(($i - 1) * $this->item_count)->limit($this->item_count)
            ->get();

        $count = Inbox::where('user_id', $user->id)->groupBy('sent_by')->get()->count();

        $user_id = $user->id;
        foreach ($contacts as $contact) {
            $sent_by = $contact->contact;
            $contact->last_message = Inbox::where(['sent_by' => $sent_by, 'user_id' => $user_id])
                ->join('users', 'users.id', 'inboxes.sender_id')
                ->select('inboxes.id', 'subject', 'sent_by', 'message', 'seen', 'sender_id', 'message_id', 'inboxes.created_at as date', 'users.name as sender_name')
                ->orderBy('date', 'DESC')
                ->first();

            $contact->last_message->date = verta($contact->last_message->date)->format('Y/m/d H:i:s');
            $contact->last_message->allow_reply = $this->checkAllowReply($contact->last_message->sender_id);
            $contact->unread = Inbox::where(['user_id' => $user->id, 'sent_by' => $contact->contact, 'seen' => 0])->get()->count();
        }

        return response()->json(['count' => $count, 'contacts' => $contacts], 200);

    }

    public function seen(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $sent_by = $request->sent_by;

        $unread = Inbox::where(['user_id' => $user->id, 'sent_by' => $sent_by])->get();
        foreach ($unread as $msg) {
            $msg->seen = 1;
            $msg->save();
        }

        $messages = $this->getMessagesByContactId($sent_by, $user->id);

        return response()->json(['message' => 'seen!', 'messages' => $messages], 200);
    }

    /*    public function reply(Request $request)
        {
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);
            if (!$user) {
                return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
            }

    //        $business = Business::where('user_id', $user->id)->first();
    //        if (!$business) {
    //            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
    //        }

            $message = Inbox::find($request->message_id);
            if(!$message){
                return response()->json([ 'message' => 'پیامی وجود ندارد!' ], 404);
            }
            if(!$this->checkAllowReply($message->sender_id)){
                return response()->json([ 'message' => 'شما نمی توانید به پیام های سیستمی پاسخ ارسال کنید.' ], 400);
            }

            $data = $request->all();
            $sent_by = $request->sent_by;

            $rules = [
                'subject' => 'required | string',
                'message' => 'required | string',
            ];

            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response = ['message' => $validator->errors()->first()];
                return response()->json($response, 400);
            }

            sendToInbox($contact_id, $request->subject, $user->name, $request->message,$user->id,$request->message_id);

            $messages = $this->getMessagesByContactId($sent_by,$user->id);

            return response()->json(['message' => 'پیام شما با موفقیت ارسال شد.' , 'messages' => $messages] ,200);
        }*/

    protected function checkAllowReply($sender_id)
    {
        $user = User::find($sender_id);
        if ($user->role_id == 1 || $user->role_id == 2) {
            return 0;
        }
        return 1;
    }

    public function allMessageByContact(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $contact_id = $request->sent_by;
        $messages = $this->getMessagesByContactId($contact_id, $user->id);

        return response()->json(['messages' => $messages], 200);
    }

    protected function getMessagesByContactId($sent_by, $user_id)
    {
        $messages = Inbox::where(['sent_by' => $sent_by, 'user_id' => $user_id])
            ->join('users', 'users.id', 'inboxes.sender_id')
            ->select('inboxes.id', 'subject', 'sent_by', 'message', 'seen', 'sender_id', 'message_id', 'inboxes.created_at as date', 'users.name as sender_name')
            ->orderBy('id', 'DESC')
            ->get();

        foreach ($messages as $message) {
            $message->date = verta($message->date)->format('Y/m/d H:i:s');
            $message->allow_reply = $this->checkAllowReply($message->sender_id);
        }

        return $messages;
    }

    public function unreadMsgCountByToken()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }
        $count = Inbox::where(['seen' => 0, 'user_id' => $user->id])->count();
        return response()->json(['count' => $count], 200);
    }
}
