<?php

namespace App\Http\Controllers\API\User;

use App\Business;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class RequestController extends Controller
{
    public function requestInPage(Request $request)
    {
        $token = JWTAuth::getToken();

        $data = $request->all();
        $rules = [
            'product_id' => 'required',
            'quantity' => 'required',
            'message' => 'required | max:1024'
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }
        if (!JWTAuth::parseToken()->check()) {
            $response = ['message' => 'لطفا مجددا وارد شوید.'];
            return response()->json($response, 401);
        }
        $user = JWTAuth::toUser($token);
        if (!$user or !isset($user)) {
            $response = ['message' => 'کاربر وجود ندارد.'];
            return response()->json($response, 404);
        }

        $product = Product::find($request->product_id);
        $req = \App\Request::create([
            'user_id' => $user->id,
            'product_id' => $request->product_id,
            'business_id' => $product->business_id,
            'quantity' => $request->quantity,
            'message' => $request->message,
            'unit' => null,
            'type' => 1,
        ]);
        return response()->json(['message' => 'درخواست شما با موفقیت برای کسب وکار مذکور ارسال شد.'], 200);
    }
}
