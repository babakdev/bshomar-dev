<?php

namespace App\Http\Controllers\API\User;

use App\DeliveryAddress;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class AddressController extends Controller
{
    public function index()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $address = DeliveryAddress::join('cities', 'cities.id', 'delivery_addresses.city_id')
            ->join('cities as states', 'cities.parent_id', 'states.id')
            ->where('user_id', $user->id)
            ->select('delivery_addresses.*', 'cities.title as city_name', 'states.id as state_id', 'states.title as state_name')
            ->get();
        return response()->json(['address' => $address], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $rules = [
            'name' => 'required',
            'email' => 'nullable | email',
            'city_id' => 'required',
            'address' => 'required | string',
            'post_code' => 'required|digits:10',
            'phone' => 'required'
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        DeliveryAddress::create([
            'user_id' => $user->id,
            'name' => $request->name,
            'email' => $request->email,
            'city_id' => $request->city_id,
            'address' => $request->address,
            'post_code' => $request->post_code,
            'phone' => $request->phone,
        ]);
        return response()->json(['message' => 'آدرس با موفقیت ثبت شد.'], 200);

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $rules = [
            'name' => 'required',
            'email' => 'nullable | email',
            'city_id' => 'required',
            'address' => 'required | string',
            'post_code' => 'required|digits:10',
            'phone' => 'required'
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        DeliveryAddress::where('id', $id)->update([
            'user_id' => $user->id,
            'name' => $request->name,
            'email' => $request->email,
            'city_id' => $request->city_id,
            'address' => $request->address,
            'post_code' => $request->post_code,
            'phone' => $request->phone,
        ]);
        return response()->json(['message' => 'آدرس با موفقیت ویرایش شد.'], 200);

    }


}
