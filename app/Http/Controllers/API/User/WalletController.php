<?php

namespace App\Http\Controllers\API\User;

use App\Business;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class WalletController extends Controller
{
    public function index(Request $request)
    {
        $i = $request->i ?? 1;
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $balance = $user->balance;
        $removeable_balance = $user->balance > 50000 ? $user->balance : 0;
        $invite_code = "bshomar.com/register?marketer=" . $user->phone;

        $confirmed_trans = $user->transactions->where('confirmed', 1)->slice(($i - 1) * $this->item_count, $this->item_count)->values();
        $confirmed_count = sizeof($user->transactions->where('confirmed', 1));
        foreach ($confirmed_trans as $trans) {
            $trans['type'] = $trans['type'] == 'deposit' ? 'واریز به کیف پول' : 'برداشت از کیف پول';
            $trans['description'] = $trans['meta'][0];
            $trans['created_at'] = $trans['created_at']->format('Y-m-d H:i:s');
        }
        $response = [
            'invite_code' => $invite_code,
            'balance' => $balance,
            'removeable_balance' => $removeable_balance,
            'confirmed_count' => $confirmed_count,
            'confirmed_trans' => $confirmed_trans,
        ];
        return response()->json($response, 200);
    }

    public function settle(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $business = Business::where('user_id', $user->id)->first();
        if (!$business) {
            return response()->json(['message' => 'دسترسی غیرمجاز!'], 400);
        }

        $balance = $user->balance;

        if ($balance < 50000) {
            return response()->json(['message' => 'حداقل درآمد قابل برداشت شما در هنگام درخواست واریز وجه باید 50,000 تومان باشد.'], 400);
        }

        if ($request->amount > $balance) {
            return response()->json(['message' => 'میزان مبلغ درخواست شده از موجودی کیف پول شما بیشتر است.'], 400);
        }

        $rules = [
            'amount' => 'required | numeric | min:50000',
            'bank_name' => 'required | string',
            'bank_card' => 'required | numeric | digits:16',
            'sheba' => 'required | digits:24'
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        $user->withdraw($request->amount,
            [
                'درخواست برداشت از کیف پول',
                $request->bank_name,
                $request->bank_card,
                $request->sheba
            ]
            , false); // false is not confirmed

        return response()->json(['message' => 'درخواست واریز برای بخش مالی ارسال شد. به زودی مبلغ به حساب شما واریز می گردد. با تشکر'], 200);
    }
}
