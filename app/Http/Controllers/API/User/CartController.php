<?php

namespace App\Http\Controllers\API\User;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class CartController extends Controller
{
    protected function getUserCart($user)
    {
        $userCart = [];
        if($user) {
            $userCart = Cart::where('user_phone' , $user->phone)->get();

            //Gain list of products in carts table for auth()->user()
            foreach ($userCart as $key => $cart) {
                $product = Product::where('id', $cart->product_id)->first();
                $userCart[$key]->image = $product->images->first() ? '/images/front_images/products/small/' . $product->images->first()->path : null;
            }
        }

        return $userCart;
    }

    protected function getPostPrice($user)
    {
        $businessesInCart = Cart::where(['user_phone' => $user->phone])
            ->join('businesses', 'businesses.id', 'carts.business_id')
            ->select(\DB::raw('SUM(price*quantity) as total_price'),
                'businesses.id', 'businesses.name as business_name', 'businesses.min_shipping_cost',
                'businesses.shipping_cost')
            ->groupBy('business_id')->get();
        foreach ($businessesInCart as $business) {
            $business->post_price = 0;
            $business->min_shipping_cost = $business->min_shipping_cost != null ? $business->min_shipping_cost : 0;
            $business->shipping_cost = $business->shipping_cost != null ? $business->shipping_cost : 0;
            if ($business->min_shipping_cost && ($business->total_price <= $business->min_shipping_cost)) {
                $business->post_price = $business->shipping_cost;
            }
        }

        return $businessesInCart;
    }

    protected function getDiscountPrice($user)
    {
        $userCart = Cart::where(['user_phone' => $user->phone])->get();

        $discount_price = 0;
        foreach ($userCart as $cart) {
            $product = Product::where('id',$cart->product_id)->first();
            $now = Carbon::now()->format('Y-m-d H:i:s');

            if($product->discount != 0 && $product->discount_expire_date != null){
                if($product->discount_expire_date >= $now){
                    $discount_price += $product->price * $cart->quantity * ( $product->discount / 100 );
                }
            }
        }

        return $discount_price;
    }

    public function index()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $userCart = $this->getUserCart($user);
        $businessesInCart = $this->getPostPrice($user);
        $discountPrice = $this->getDiscountPrice($user);

        return response()->json(['user_cart' => $userCart, 'post_price' => $businessesInCart,'discount_price' => $discountPrice], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }
        if (!$request->product_id || !$request->quantity) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }

        // Check Product stock is available or not
        $getProductStock = Product::where('id', $data['product_id'])->first();
        if (!$getProductStock) {
            return response()->json(['message' => 'نامعتبر!'], 400);
        }
        if($getProductStock->price == 0){
            return response()->json(['message' => 'قیمت محصول مشخص نیست لطفا برای اطلاع از قیمت با فروشنده تماس بگیرید.'], 400);
        }
        $data['product_name'] = $getProductStock->name;
        $data['product_code'] = $getProductStock->code;
        // compute price with discount
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $data['price'] = $getProductStock->price;
        if($getProductStock->discount != 0 && $getProductStock->discount_expire_date != null){
            if($getProductStock->discount_expire_date >= $now){
                $data['price'] = $getProductStock->price * ( 100 - $getProductStock->discount) / 100 ;
            }
        }
        $data['business_id'] = $getProductStock->business_id;
        $data['user_phone'] = $user->phone;

        //Check we have product in Cart or not
        $countProducts = Cart::where([
            'product_id' => $data['product_id'],
            'user_phone' => $data['user_phone']
        ])->count();

        $getCartDetails = Cart::where([
            'product_id' => $data['product_id'],
            'user_phone' => $data['user_phone']
        ])->first();

        if ($countProducts > 0) {
            $updated_quantity = $getCartDetails->quantity + $request->quantity;
            if ($this->stockAvailable($getProductStock, $updated_quantity)) {
                return $this->increaseProductQty($getProductStock, $data['quantity'], $user);
            }
            return response()->json(['message' => 'تعداد محصول مورد نظر بیشتر از موجودی انبار می باشد.'], 400);
        }

        if ($getProductStock->stock < $data['quantity']) {
            return response()->json(['message' => 'میزان تقاضای شما برای این کالا از موجودی انبار بیشتر است.'], 400);
        }
        $session_id = Str::random(40);
        $this->createCartDetails($data, $session_id);
        $userCart = $this->getUserCart($user);
        $businessesInCart = $this->getPostPrice($user);
        $discountPrice = $this->getDiscountPrice($user);

        return response()->json([
            'message' => 'محصول به سبد خرید اضافه شد.',
            'user_cart' => $userCart ,
            'post_price' => $businessesInCart,
            'discount_price' => $discountPrice,
        ], 200);
    }

    public function destroy($id)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }
        $cart = Cart::where('id', $id)->first();
        if (!$cart) {
            return response()->json(['message' => 'کارت نامعتبر!'], 400);
        }
        if ($user->phone != $cart->user_phone) {
            return response()->json(['message' => 'دسترسی نامعتبر!'], 400);
        }
        Cart::where('id', $id)->delete();
        $userCart = $this->getUserCart($user);
        $businessesInCart = $this->getPostPrice($user);
        $discountPrice = $this->getDiscountPrice($user);

        return response()->json(['message' => 'محصول از سبد خرید حذف شد.', 'user_cart' => $userCart, 'post_price' => $businessesInCart,'discount_price' => $discountPrice], 200);
    }

    public function destroyAllItems()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }
        Cart::where('user_phone', $user->phone)->delete();
        return response()->json(['message' => 'سبد خرید شما خالی است!'], 200);
    }

    protected function createCartDetails($data, $session_id)
    {
        \DB::table('carts')->insert([
            'product_id' => $data['product_id'],
            'business_id' => $data['business_id'],
            'product_name' => $data['product_name'],
            'product_code' => $data['product_code'],
            'price' => $data['price'],
            'quantity' => $data['quantity'],
            'user_phone' => $data['user_phone'],
            'session_id' => $session_id
        ]);
    }

    protected function stockAvailable($getStock, $updated_quantity): bool
    {
        return $getStock->stock >= $updated_quantity;
    }

    protected function increaseProductQty($getProductStock, $quantity, $user)
    {
        $cart = Cart::where(['product_id' => $getProductStock->id, 'user_phone' => $user->phone])->first();

        $updated_quantity = $cart->quantity + $quantity;
        if ($updated_quantity <= 0) {
            Cart::where(['product_id' => $getProductStock->id, 'user_phone' => $user->phone])->delete();
        }
        Cart::where(['product_id' => $getProductStock->id, 'user_phone' => $user->phone])->increment('quantity', $quantity);

        $userCart = $this->getUserCart($user);
        $businessesInCart = $this->getPostPrice($user);
        $discountPrice = $this->getDiscountPrice($user);
        return response()->json(['message' => 'سبد خرید به روز شد.', 'user_cart' => $userCart, 'post_price' => $businessesInCart,'discount_price' => $discountPrice], 200);
    }

    public function update()
    {
        return;
    }

    public function edit()
    {
        return;
    }

    public function create()
    {
        return;
    }

    public function show()
    {
        return;
    }
}
