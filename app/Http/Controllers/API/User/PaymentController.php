<?php

namespace App\Http\Controllers\API\User;

use App\Business;
use App\Cart;
use App\DiscountUser;
use App\Http\Controllers\Controller;
use App\lib\zarinpal;
use App\Order;
use App\OrdersProduct;
use App\Payment;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Bus;
use nusoap_client;

class PaymentController extends Controller
{
    public function onlinePayment($order,$user_id, $total, $date_create, $invoice_id, $phone, $type)
    {
        $description = 'خرید محصول از اصناف در سامانه بی شمار';

        $callback = ServerUrl() . '/api/user/payment/verify';
        $zarinpal = new ZarinPal();

        return $zarinpal->pay($order,$user_id, $total, $description, $phone, $date_create, $callback, $invoice_id, $type);
    }

    public function paymentAtHome($order,$user_id, $total, $date_create, $invoice_id, $phone, $type)
    {
        $payment = \DB::table('payments')->insert([
            'invoice_id' => $invoice_id,
            'order_id' => $order->id,
            'user_id' => $user_id,
            'price' => $total,
            'authority' => null,
            'date_create' => $date_create,
            'payment_method' => 2,
            'gateway' => 'at home',
            'payed' => 0,
            'type' => $type,
            'reference' => null,
            'created_at' => Carbon::now()
        ]);
        Cart::where('user_phone', $phone)->delete();
        $order_discount = Order::where(['user_id' => $user_id , 'invoice_id' => $invoice_id])->first();
        /*If a Coupon used, Then expires it*/
        if ($order_discount->discount_id != null) {
            DiscountUser::where('user_id', $user_id)
                ->where('discount_id', $order_discount->discount_id)->update(['used' => 1, 'cart_id' => null,'date_used' => verta(Carbon::now())->format('Y-m-d H:i:s')]);
        }
        $orderProduct = OrdersProduct::where('order_id' , $order->id)->get();
        $businesses = OrdersProduct::where('order_id' , $order->id)->groupBy('business_id')->select('id','business_id')->get();

        foreach($businesses as $bus){

            /*Seller*/
                $business = Business::join('users','users.id','businesses.user_id')->where('businesses.id',$bus->business_id)
                    ->select('users.name as user_name','users.phone as phone','businesses.user_id')->first();

                sendSmsWithTemplate(23758, $business->phone, 'UserName', $business->user_name);

                $message = 'کالایی از فروشگاه شما بصورت پرداخت در محل (درب منزل) سفارش داده شده است ،‌به پنل خود مراجعه کنید و از قسمت تاریخچه خرید و فروش ،
                        بخش سابقه فروش ها را بازکنید و از صفحه باز شده روی مشاهده جزییات کلیک کنید ، تا اطلاعات ارسال شامل نوع کالا و تعداد و آدرس گیرنده را مشاهده کنید.
                         همچنین اگر کالا در ساعت اداری سفارش داده شده است ، موظف هستید در همان روز کالا را ارسال نمایید در غیر این صورت اول روز کاری بعد باید کالا ارسال شود و
                         کد رهگیری دریافت شده از پست در قسمت بالای صفحه جزییات وارد شود تا وضعیت کالا به ارسال شده تغییر کند .';
                sendToInbox($business->user_id, 'مدیر بخش کسب و کارهای بی‌شمار', 'info@bshomar.com', $message);

                if($order->delivery_time != null || $order->delivery_date != null) {
                    $msg2 = 'شماره فاکتور '. $invoice_id . 'مایل است زمان ارسال بدین صورت باشد: '.$order->delivery_date.' - ' . $order->delivery_time;
                    sendToInbox($business->user_id, 'سفارش پرداخت در محل', 'info@bshomar.com', $msg2);
                }

        }

        foreach ($orderProduct as $item) {
            $item->update(['status' => 'در انتظار پرداخت']);

            /* Update stock & sales_number */
            $updateQuantity = Product::where('id', $item->product_id)->first();
            $updateQuantity->stock -= $item->quantity;
            $updateQuantity->sales_number += $item->quantity;
            $updateQuantity->save();
        }

        /*Buyer*/
        sendSmsWithTemplate(19590, $phone, 'InvoiceId', $invoice_id);
        $message2 = 'فاکتور شما به شماره ' . $order->invoice_id . ' صادر شد در تاریخ: ' . verta($order->created_at)->formatDate();
        sendToInbox($user_id, 'ثبت سفارش بصورت پرداخت در محل ', 'info@bshomar.com', $message2);

        $Amount = $total;
        $refid = $invoice_id;
        $url =  SiteUrl() . '/success-order?invoice='.$refid.'&amount='.$Amount;
        $response = ['message' => 'سفارش درب منزل شما ثبت شد.', 'url' => $url];
        return response()->json($response, 200);
    }

    public function verify()
    {
        $MerchantID = 'e55ffa32-13ee-11e9-9579-005056a205be';
        $Authority = $_GET['Authority'];

        $payment = Payment::where('authority',$Authority)->first();
        $user_id = $payment->user_id;
        $user = User::find($user_id);
        $order = Order::where(['user_id'=> $user_id , 'invoice_id' => $payment->invoice_id])->first();
        $Amount = $order->price;

        if (!isset($Authority)) {
            $response = ['message' => 'شما به صورت غیر مجاز به این صفحه وارد شدید.'];
            return response()->json($response, 404);
        }

        if ($_GET['Status'] == 'OK') {

            $client = new nusoap_client('https://www.zarinpal.com/pg/services/WebGate/wsdl', 'wsl');
            $client->soap_defencoding = 'UTF-8';
            $result = $client->call('PaymentVerification', [
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ],
            ]);

            if ($result['Status'] == 100) {
                $refid = $result['RefID'];

                $this->updateDB($refid, $Authority, $order,$user);
                $this->sendNotifyAndSms($order,$user);

                $url = SiteUrl() .'/success-order?invoice='.$refid.'&amount='.$Amount;
                return redirect($url);

            } else {
                $this->removeFromDB($order,$payment);

                $url = SiteUrl() .'/failed-order';
                return redirect($url);
            }
        } else {
            $this->removeFromDB($order,$payment);

            //todo
            $url = SiteUrl() . '/failed-order';
            return redirect($url);
        }
    }

    protected function updateDB($refid, $Authority, $order,$user)
    {
        $payment = Payment::where('authority', $Authority)->first();
        $payment->payed = 1;
        $payment->reference = $refid;
        $payment->save();

        $orderProduct = OrdersProduct::where('order_id', $order->id)->get();
        foreach ($orderProduct as $item) {
            $item->update(['status' => 'پرداخت شد']);
            Product::where('id', $item->product_id)->increment('sales_number', $item->quantity);
        }

        Cart::where('user_phone', $user->phone)->delete();
        if ($order->discount_id != null) {
            DiscountUser::where('user_id', $user->id)
                ->where('discount_id', $order->discount_id)->update(['used' => 1, 'cart_id' => null,'date_used' => verta(Carbon::now())->format('Y-m-d H:i:s')]);
        }
        return;
    }

    protected function sendNotifyAndSms($order,$user)
    {
        sendSmsWithTemplate(19590, $user->phone, 'InvoiceId', $order->invoice_id);

        $businesses = OrdersProduct::where('order_id' , $order->id)->groupBy('business_id')->select('id','business_id')->get();

        foreach($businesses as $bus){

            /*Seller*/
            $business = Business::join('users','users.id','businesses.user_id')->where('businesses.id',$bus->business_id)
                ->select('users.name as user_name','users.phone as phone','businesses.user_id')->first();

            sendSmsWithTemplate(19592, $business->phone, 'UserName', $business->user_name);

            $message = 'کالایی از فروشگاه شما فروش رفته است ،‌به پنل خود مراجعه کنید و از قسمت تاریخچه خرید و فروش ، بخش سابقه فروش ها را بازکنید و از صفحه باز شده روی مشاهده جزییات کلیک کنید ، تا اطلاعات ارسال شامل نوع کالا و تعداد و آدرس گیرنده را مشاهده کنید. همچنین اگر کالا در ساعت اداری سفارش داده شده است ، موظف هستید در همان روز کالا را ارسال نمایید در غیر این صورت اول روز کاری بعد باید کالا ارسال شود و کد رهگیری دریافت شده از پست در قسمت بالای صفحه جزییات وارد شود تا وضعیت کالا به ارسال شده تغییر کند و پس از دریافت کالا توسط خریدار ، وجه شما پرداخت گردد.';
            sendToInbox($business->user_id, 'مدیر بخش کسب و کارهای بی‌شمار', 'info@bshomar.com', $message);

            if($order->delivery_time != null || $order->delivery_date != null) {
                $msg2 = 'شماره فاکتور '. $order->invoice_id . 'مایل است زمان ارسال بدین صورت باشد: '.$order->delivery_date.' - ' . $order->delivery_time;
                sendToInbox($business->user_id, 'سفارش پرداخت در محل', 'info@bshomar.com', $msg2);
            }

        }

        $message2 = 'فاکتور شما به شماره ' . $order->invoice_id . ' پرداخت شد در تاریخ: ' . verta($order->created_at)->formatDate();
        sendToInbox($user->id, 'مدیر بخش کسب و کارهای بی‌شمار', 'info@bshomar.com', $message2);
    }

    protected function removeFromDB($order,$payment)
    {
        $orderProduct = OrdersProduct::where(['order_id' => $order->id])->get();
        foreach ($orderProduct as $item) {
            $item->delete();
        }
        $payment->delete();
        $order->delete();
    }

}
