<?php

namespace App\Http\Controllers\API\User;

use App\Cart;
use App\DeliveryAddress;
use App\DiscountCode;
use App\DiscountProduct;
use App\DiscountUser;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersProduct;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\API\Client\DiscountController;
use App\Http\Controllers\API\User\PaymentController;

class OrderController extends Controller
{
    public function order(Request $request)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user or !isset($user)) {
            $response = ['message' => 'چنین کاربری وجود ندارد.'];
            return response()->json($response, 404);
        }

        $userCart = Cart::where('user_phone', $user->phone)->get();
        if (count($userCart) < 1) {
            $response = ['message' => 'محصولی که در سبدخرید داشتید، اکنون در دسترس نمی باشد', 'url' => ''];
            return response()->json($response, 301);
        }

        $invoice_id = generateInvoiceId();
        $user_id = $user->id;
        $description = $request->description ?? null;
        $status = 1;
        $date_create = verta(Carbon::now('Asia/Tehran'))->format('Y-m-d');
        $discount_code = $request->discount_code ?? null;
        if ($discount_code) {
            $discount = DiscountCode::where('code', $discount_code)->first();
            $discount_id = $discount ? $discount->id : null;

        } else {
            $discount_id = null;
        }
        $payment_method = $request->payment_method;
        $address_id = $request->address_id;
        $validAddress = DeliveryAddress::find($address_id);
        if (!$validAddress) {
            $response = ['message' => 'ادرس نامعتبر!'];
            return response()->json($response, 404);
        }

        $total_price = 0;
        if ($discount_id != null) {
            $discount_user = DiscountUser::where('user_id', $user_id)
                ->where('discount_id', $discount_id)
                ->first();
            if (!$discount_user) {
                $result = app(DiscountController::class)->checkDiscountProduct($userCart, $discount_id);
                if ($result) {
                    DiscountUser::create([
                        'discount_id' => $discount_id,
                        'user_id' => $user_id,
                        'cart_id' => $userCart->first()->id,
                        'used' => 0,
                        'date_used' => ''
                    ]);
                }
            } elseif ($discount_user->used == 1) {
                $discount_id = null;
                $response = ['message' => 'شما قبلا از این کوپن تخفیف استفاده کرده اید.'];
                return response()->json($response, 400);
            }
        }

        $now = Carbon::now()->format('Y-m-d H:i:s');

        // sum of all discount prices
        $discount_price = 0;

        // product discount price
        $product_discount_price = [];

        foreach ($userCart as $cart) {
            $product = Product::where('id', $cart->product_id)->first();
            if($cart->quantity < $product->min_product){
                $response = ['message' => 'حداقل تعداد سفارش برای کالای ' . $product->name . ' ' . $product->min_product . ' می باشد.'];
                return response()->json($response, 400);
            }
            $priceOfProduct = $product->price;
            if($product->discount != 0 && $product->discount_expire_date != null){
                if($product->discount_expire_date >= $now){
                    $priceOfProduct = $product->price * ( 100 - $product->discount) / 100 ;
                    $product_discount_price[$product->id] = $cart->quantity * $product->price * ($product->discount / 100);
                    $discount_price += $product_discount_price[$product->id];
                }
            }
            if($priceOfProduct != $cart->price){
                Cart::where('id',$cart->id)->delete();
                $response = ['message' => 'قیمت و تخفیف این محصول تغییر کرده است لطفا این محصول را مجددا به سبد خرید خود اضافه کنید.'];
                return response()->json($response, 400);
            }
            $total_price += $product->price * $cart->quantity;
            if ($product->stock < $cart->quantity) {
                $response = ['message' => 'سفارش شما بیشتر از موجودی انبار است.'];
                return response()->json($response, 400);
            }
        }

        // compute post price in all business
        $post_price = 0;
        $coupon_price = 0;
        $businessesInCart = Cart::where(['user_phone' => $user->phone])
            ->join('businesses', 'businesses.id', 'carts.business_id')
            ->select(\DB::raw('SUM(price*quantity) as total_price'),
                'businesses.id','businesses.name as business_name', 'businesses.min_shipping_cost',
                'businesses.shipping_cost')
            ->groupBy('business_id')->get();
        foreach($businessesInCart as $business) {
            $business->min_shipping_cost = $business->min_shipping_cost != null ? $business->min_shipping_cost : 0;
            $business->shipping_cost = $business->shipping_cost != null ? $business->shipping_cost : 0;
            if ( $business->min_shipping_cost && ( $business->total_price <= $business->min_shipping_cost) ) {
                $post_price += $business->shipping_cost;
            }
            if(isset($discount)){
                if($discount->business_id == $business->id){
                    $coupon_price += app(DiscountController::class)->computeDiscountPrice($discount_id, $business->total_price);
                }
            }
        }

        $total_amount_with_discount = $total_price + $post_price - $discount_price - $coupon_price;

        if($total_amount_with_discount < 1000 && $payment_method == 1){
            $response = ['message' => 'برای پرداخت آنلاین مبلغ باید بیشتر از 1000 تومان باشد.'];
            return response()->json($response, 404);
        }

        // Submit new Order
        $order = new Order();
        $order->user_id = $user_id;
        $order->description = $description;
        $order->status = $status;
        $order->invoice_id = $invoice_id;
        $order->date_create = $date_create;
        $order->price = $total_amount_with_discount;
        $order->discount_price = $discount_price;
        $order->coupon_price = $coupon_price;
        $order->discount_id = $discount_id;
        $order->shipping_charges = $post_price;
        $order->address_id = $address_id;
        $order->delivery_time = $request->delivery_time;
        $order->delivery_date = $request->delivery_date;
        $order->save();

        // Gain order_id in from orders table after save
        $order_id = \DB::getPdo()->lastInsertId();

        // Inserting Cart Details to orders_products table
        foreach($businessesInCart as $business){
            $post = 0;
            $business->min_shipping_cost = $business->min_shipping_cost != null ? $business->min_shipping_cost : 0;
            $business->shipping_cost = $business->shipping_cost != null ? $business->shipping_cost : 0;
            if ( $business->min_shipping_cost && ( $business->total_price <= $business->min_shipping_cost) ) {
                $post += $business->shipping_cost;
            }
            foreach ($userCart as $product) {
                $pro = Product::where('id', $product->product_id)->first();
                if ($pro->business_id == $business->id) {
                    $order_pro_count = OrdersProduct::where('order_id',$order_id)->where('business_id',$pro->business_id)->get()->count();
                    $cartPro = new OrdersProduct();
                    $cartPro->order_id = $order_id;
                    $cartPro->product_id = $product->product_id;
                    $cartPro->business_id = $product->business_id;
                    $cartPro->discount_price = isset($product_discount_price[$product->product_id]) ? $product_discount_price[$product->product_id] : 0;
                    $cartPro->shipping_cost = $order_pro_count > 0 ? 0 : $post;
                    $cartPro->name = $product->product_name;
                    $cartPro->code = $product->product_code;
                    $cartPro->price = $product->price;
                    $cartPro->quantity = $product->quantity;
                    $cartPro->status = 'در انتظار پرداخت';
                    $cartPro->save();
                }
            }
        }

        if ($payment_method == 2) {
            return app(PaymentController::class)->paymentAtHome($order, $user_id, $total_amount_with_discount, $date_create, $invoice_id, $user->phone, 2);
        }else{
            $bank_url = app(PaymentController::class)->onlinePayment($order,$user_id, $total_amount_with_discount, $date_create, $invoice_id, $user->phone, 2);
            if ($bank_url) {
                $response = ['message' => 'به درگاه بانک متصل شد', 'url' => $bank_url];
                return response()->json($response, 200);
            } else {
                $response = ['message' => 'خطا در برقراری ارتباط با بانک', 'url' => ''];
                return response()->json($response, 400);
            }
        }
    }

}
