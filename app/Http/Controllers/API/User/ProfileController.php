<?php

namespace App\Http\Controllers\API\User;

use App\City;
use App\Http\Controllers\Controller;
use App\Rules\NationalCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends Controller
{
    public function updateProfile(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $rules = [
            'name' => ['required', 'string', 'min:3', 'max:191'],
            'email' => ['nullable', 'max:191', Rule::unique('users')->ignore($user->id)],
//            'phone' => [ Rule::requiredIf($user->isBusiness), Rule::unique('users')->ignore($user->id)],
            'national_code' => [new NationalCode()],
            'address' => 'nullable | string',
            'city_id' => 'nullable | integer',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }


        if (trim($request->password) == '') {
            $input = $request->except('password');
        } else {
            $input = $request->all();
            $input['password'] = bcrypt($request->password);
        }

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            if ($file->isValid()) {
                $prePhoto = User::where('id', $user->id)->value('image');
                if ($prePhoto) {
                    if (file_exists(public_path('/images/users/' . $prePhoto))) {
                        unlink('images/users/' . $prePhoto);
                    }
                }
                $input = saveImageFile($file, $input, 100, 100, 'images/users/');

            }
        }

        $user->update($input);
        $city_name = $user->city_id != null ? City::where('id', $user->city_id)->first()->title : null;
        $state_id = $user->city_id ? City::where('id', $user->city_id)->first()->parent_id : null;
        $user_customized = [
            'id' => $user->id,
            'token' => $token,
            'name' => $user->name,
            'role_id' => $user->role_id,
            'city_id' => $user->city_id,
            'state_id' => $state_id,
            'city_name' => $city_name,
            'img' => $user->image != null ? '/images/users/' . $user->image : null,
            'phone' => $user->phone,
            'email' => $user->email,
            'national_code' => $user->nationalcode,
            'score' => $user->score,
            'address' => $user->address,
        ];

        $response = ['message' => 'اطلاعات کاربری شما ویرایش شد.', 'user' => $user_customized];
        return response()->json($response, 200);
    }

    public function updatePassword(Request $request)
    {
        $data = $request->all();
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        $rules = [
            'current_password' => 'required',
            'password' => 'required | string | min:8 '
        ];
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $response = ['message' => $validator->errors()->first()];
            return response()->json($response, 400);
        }

        $currentPassword = $request->current_password;

        if (Hash::check($currentPassword, $user->password)) {
            $password = $request->password;
            User::find($user->id)->update(['password' => bcrypt($password)]);
            $response = ['message' => 'رمز عبور با موفقیت تغییر یافت.'];
            return response()->json($response, 200);
        } else {
            $response = ['message' => 'رمز عبور فعلی صحیح نیست.'];
            return response()->json($response, 400);
        }
    }

    public function removeProfilePhoto()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        if (!$user) {
            return response()->json(['message' => 'کاربر وجود ندارد.'], 404);
        }

        if ($user->image) {
            if (file_exists(public_path('/images/users/' . $user->image))) {
                unlink(public_path('/images/users/' . $user->image));
            }
            $user->image = null;
            $user->save();
            return response()->json(['message' => 'تصویر با موفقیت حذف شد.'], 200);
        }
        return response()->json(['message' => 'شما تصویر پروفایل ندارید.'], 400);
    }
}
