<?php

namespace App\Http\Controllers\API\User;

use App\Favorite;
use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\ProductProperty;
use Tymon\JWTAuth\Facades\JWTAuth;

class FavoriteController extends Controller
{
    public function getAllFavoriteProducts()
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        $favs = Favorite::where('favorites.user_id', $user->id)
            ->join('products', 'products.id', 'favorites.product_id')
            ->join('businesses', 'businesses.id', 'products.business_id')
            ->join('categories', 'categories.id', 'products.category_id')
            ->join('categories as catparent', 'categories.parent_id', 'catparent.id')
            ->select('favorites.id as fav_id','products.id', 'products.name', 'products.code', 'products.unit', 'products.price', 'products.is_active',
                'products.stock', 'products.discount', 'products.discount_expire_date','categories.id as category_id', 'categories.name as category_name',
                'businesses.name as business_name','businesses.id as business_id','categories.name as cat_name','categories.parent_id as cat_parent_id','catparent.name as cat_parent_name')
            ->get();

        foreach ($favs as $fav) {
            $fav->discount_expire_date = checkDiscountExpireDate($fav->discount_expire_date);
            $fav->discount = $fav->discount_expire_date != null ? $fav->discount : 0;
            $properyCount = ProductProperty::where('product_id', $fav->id)->get()->count();
            $fav->has_properties = ($properyCount> 0) ? 1 : 0;
            $img = Image::where(['imageable_type' => Product::class, 'imageable_id' => $fav->id])
                ->select(
//                        \DB::raw('CONCAT("/images/front_images/products/large/",images.path) as path_large'),
                    \DB::raw('CONCAT("/images/front_images/products/small/",images.path) as path_small'))
                ->first();
            $fav->image = $img->path_small ?? null;
        }

        return response()->json(['favorites' => $favs], 200);
    }
}
