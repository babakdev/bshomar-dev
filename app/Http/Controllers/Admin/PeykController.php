<?php

namespace App\Http\Controllers\Admin;

use App\DeliveryAddress;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersProduct;
use Illuminate\Http\Request;

class PeykController extends Controller
{
    public function index()
    {
        $salesNew = Order::join('order_product', 'order_product.order_id', 'orders.id')
            ->where('orders.status', 1)
            ->join('payments', 'payments.invoice_id', 'orders.invoice_id')
            ->where(['payments.payed' => 1, 'payments.payment_method' => 1, 'order_product.status' => 'پرداخت شد'])
            ->where('order_product.status', '!=', 'کنسل شد')
            ->where('order_product.status', '!=', 'تسویه شد')
            ->where('order_product.status', '!=', 'ارسال شد')
            ->join('users', 'orders.user_id', 'users.id')
            ->select('orders.invoice_id', 'orders.user_id',
                'payments.price', 'payments.date_create', 'payments.payment_method', 'payments.payed',
                'order_product.order_id',
                'users.name as user_name', 'users.phone'
            )
            ->orderBy('orders.created_at', 'DESC')
            ->groupBy('orders.invoice_id')
            ->paginate(20);

        return view('admin.peyk.index', compact('salesNew'));
    }

    public function saleDetails($order_id)
    {
        $orderCustomerDetails = Order::where('id', $order_id)->select('orders.user_id', 'address_id')->first();
        $address = DeliveryAddress::where('id', $orderCustomerDetails->address_id)->first();
        $orderDetails = OrdersProduct::where(['order_id' => $order_id])->get();

        return view('admin.peyk.details', compact('orderDetails', 'orderCustomerDetails', 'address'));
    }
}
