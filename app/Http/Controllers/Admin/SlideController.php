<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Slide;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Rule;

class SlideController extends Controller
{
    public function index()
    {
        $slides = Cache::remember('allSlides', 30, fn() => Slide::paginate());
        return view('admin.slides.index', compact('slides'));
    }

    public function create()
    {
        return view('admin.slides.create', ['slide' => new Slide()]);
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'image' => 'image | required | max:15000'
        ]);

        $input = request()->all();

        if (request()->hasFile('image')) {
            $image_tmp = request()->file('image');
            if ($image_tmp->isValid()) {
                $input = saveImageFile($image_tmp, $input, 1400, 600, 'images/front_images/slides/');
            }
        }
        $input['url'] = request('url') ?? null;
        Slide::create($input);

        return redirect(url('irenadmin/slides'))->with('flash', 'اسلاید جدید ایجاد شد');
    }

    public function edit(Slide $slide)
    {
        return view('admin.slides.edit', compact('slide'));
    }

    public function update(Slide $slide)
    {
        request()->validate([
            'title' => 'required',
            'image' => 'image | max:15000'
        ]);

        $input = request()->all();

        if (request()->hasFile('image')) {
            $image_tmp = request()->file('image');
            if ($image_tmp->isValid()) {
                $input = saveImageFile($image_tmp, $input, 1400, 600, 'images/front_images/slides/');
                if ($slide->image && file_exists('images/front_images/slides/' . $slide->image)) {
                    unlink('images/front_images/slides/' . $slide->image);
                }
            }
        } else {
            $input['image'] = $slide->image;
        }

        $slide->update($input);
        return redirect(url('irenadmin/slides'))
            ->with('flash', 'اسلاید ویرایش شد');
    }

    public function destroy(Slide $slide)
    {
        if (file_exists('images/front_images/slides/' . $slide->image)) {
            unlink('images/front_images/slides/' . $slide->image);
        }
        $slide->delete();

        return back()->with('flash', 'اسلاید حذف شد');
    }

    public function show()
    {
        return;
    }
}
