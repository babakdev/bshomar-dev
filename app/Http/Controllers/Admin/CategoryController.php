<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create', ['category' => new Category()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     * @throws Exception
     */
    public function store()
    {
        Category::create(
            request()->validate([
                'name' => 'required|unique:categories',
                'slug' => 'required|unique:categories',
                'parent_id' => 'required',
                'is_active' => 'required | boolean',
            ])
        );

        cache()->forget('categories');

        return redirect(url('irenadmin/categories'))
            ->with('flash', 'دسته بندی جدید ایجاد شد');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Category $category
     * @return Response
     * @throws Exception
     */
    public function update(Category $category)
    {
        $category->update(
            request()->validate([
                'name' => 'required | unique:categories,id,' . $category->id,
                'slug' => ['required', Rule::unique('categories')->ignore($category->id)],
                'meta_description' => 'required',
                'meta_keywords' => 'required',
            ])
        );

        cache()->forget('categories');

        return redirect(url('irenadmin/categories'))
            ->with('flash', 'دسته بندی ویرایش شد');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        cache()->forget('categories');

        return back()->with('flash', 'دسته بندی حذف شد');
    }

    public function show()
    {
        return;
    }
}
