<?php

namespace App\Http\Controllers\Admin;

use App\Advertising;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdvertiseController extends Controller
{
    public function index()
    {
        $advertises = Advertising::paginate(10);
        return view('admin.advertises.index', compact('advertises'));
    }

    public function create()
    {
        return view('admin.advertises.create', ['adv' => new Advertising()]);
    }

    public function store(Request $request)
    {
        request()->validate([
            'type' => 'required | integer',
        ]);

        $input = request()->all();

        $count_page_one = Advertising::where('page',1)->get()->count();
        if($count_page_one >= 2 && $request->page == 1){
            return back()->with('flash', 'تبلیغات صفحه اصلی بیشتر از دو تبلیغ مجاز نیست.');
        }

        if($request->file('image')){
            $image_tmp = $request->file('image');
            $input = saveImageFile($image_tmp, $input, 525, 192, 'images/front_images/ads/', 'link');
        } else {
            return back()->with('flash_error', 'فیلد تصویر نمی تواند برای تبلیغات داخل سایت خالی باشد');
        }

        $input['page'] = 1;
        unset($input['image']);

        if ($input['type'] === 2 && !$request->file('image')) {
            return back()->with('flash_error', 'فیلد تصویر نمی تواند برای تبلیغات داخل سایت خالی باشد');
        }

        Advertising::create($input);
        return back()->with('flash', 'عملیات با موفقیت انجام شد');
    }

    public function edit($id)
    {
        $adv = Advertising::findOrFail($id);
        return view('admin.advertises.edit', compact('adv'));
    }

    public function update(Request $request, $id)
    {
        request()->validate([
            'type' => 'required | integer',
        ]);
        $adv = Advertising::find($id);
        $input = $request->all();
        $count_page_one = Advertising::where('page',1)->get()->count();
        if($count_page_one >= 2 && $request->page == 1){
            return back()->with('flash', 'تبلیغات صفحه اصلی بیشتر از دو تبلیغ مجاز نیست.');
        }

        if($request->file('image')){
            if($adv->type == 2){
                $this->removeOldImageFile($adv);
            }
            $image_tmp = $request->file('image');
            $input = saveImageFile($image_tmp, $input, 525, 192, 'images/front_images/ads/', 'link');
        }

        $input['page'] = 1;
        unset($input['image']);
        $adv->update($input);
        return redirect(url('irenadmin/advertising'))
            ->with('flash', 'تبلیغ ویرایش شد');
    }

    public function destroy($id)
    {
        $countAdvertising = Advertising::count();
        if ($countAdvertising <= 2) {
            return back()->with('flash_error', 'حداقل تعداد تبلیغ ۲ عدد باید باشد');
        }
        $adv = Advertising::findOrFail($id);
        if($adv->type == 2){
            $this->removeOldImageFile($adv);
        }
        $adv->delete();
        return back()->with('flash', 'تبلیغ حذف شد');
    }

    protected function removeOldImageFile($adv): void
    {
        if ($adv->link && file_exists(public_path('/images/front_images/ads/' . $adv->link))) {
            unlink(public_path('/images/front_images/ads/' . $adv->link));
        }
    }

    public function show()
    {
        return;
    }
}
