<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\BusinessModels\Certificate;
use App\BusinessModels\CustomerExport;
use App\BusinessModels\Export;
use App\BusinessModels\ManufacturingLine;
use App\BusinessModels\Qc;
use App\BusinessModels\ResearchDevelop;
use App\BusinessModels\Service;
use App\Http\Controllers\Controller;
use App\Image;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\ImageManagerStatic;

class BusinessController extends Controller
{
    public function editBusiness($id, Request $request)
    {
        $business = Business::where('id', $id)->first();

        if ($request->isMethod('post')) {
            $data = $request->all();

            $request->validate([
                'name' => 'required|min:3|max:40',
                'category_id' => 'required | sometimes',
                'city_id' => 'required | sometimes',
                'image' => 'required | sometimes',
                'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:15000',
                'business_type' => 'required | sometimes',
                'description' => 'nullable | max:10000',
            ]);

            $this->updateInfo($data, $business, $request);

            if ($data['has_manu']) {
                ManufacturingLine::adminPersist($data, $business);
            }
            if ($data['has_qc']) {
                Qc::adminPersist($data, $business);
            }
            if ($data['has_rnd']) {
                ResearchDevelop::adminPersist($data, $business);
            }
            if ($data['has_cert']) {
                Certificate::persist($data, $business);
            }
            if ($data['has_export']) {
                Export::persist($data, $business);
            }
            if ($data['has_cust']) {
                CustomerExport::persist($data, $business);
            }
            if ($data['has_service']) {
                Service::persist($data, $business);
            }

            $business->business_type = $data['business_type'];
            if ($data['business_type'] == 2) {
                $business->update(['agencies' => $data['agency_info']]);
            }

            return back()->with('flash', 'اطلاعات کسب و کار ثبت شد');
        }

        return view('admin.businesses.manage', compact('business'));
    }

    public function needToApproveBusinesses()
    {
        $businesses = null;
        $asnafUser = auth()->user();
        if ($asnafUser->isAsnaf) {
            $businesses = Business::with(['user'])->where(['is_active' => 0])
                ->whereIn('category_id', $asnafUser->category_id ?? ['0'])
                ->latest('updated_at')->paginate();
        } else {
            $businesses = Business::with(['user', 'city', 'images'])->where('is_active', 0)->latest()->paginate();
        }

        return view('admin.businesses.needApproveBusinesses', compact('businesses'));
    }

    public function approvedBusinesses()
    {
        $currentPage = request()->get('page', 1);
        $businesses =
            Cache::remember('adminApprovedBusinesses-' . $currentPage, 30, fn() => Business::with(['user', 'city', 'category', 'panel', 'images'])
                ->where('is_active', 1)->latest()->paginate()
            );

        return view('admin.businesses.listBusinesses', compact('businesses'));
    }

    public function destroy(Business $business)
    {
        $user = User::where('id', $business->user_id)->first();
        $products = Product::where('business_id', $business->id)->get();

        if (count($products) > 0) {
            foreach ($products as $product) {

                if (count($product->images) > 0) {
                    foreach ($product->images as $image) {
                        if ($image && file_exists('/images/front_images/products/small/' . $image)) {
                            unlink('/images/front_images/products/small/' . $image);
                            unlink('/images/front_images/products/large/' . $image);
                        }
                    }
                }
                $product->delete();
            }
        }

        if ($user->image && file_exists('/images/users/' . $user->image)) {
            unlink('/images/users/' . $user->image);
        }

        if (count($business->images) > 0) {
            foreach ($business->images as $image) {
                if ($image && file_exists('/images/front_images/businesses/small/' . $image->path)) {
                    unlink('images/front_images/businesses/small/' . $image->path);
                    unlink('images/front_images/businesses/large/' . $image->path);
                }
            }
        }

        $user->delete();
        $business->delete();

        return back()->with('flash', 'کسب و کار حذف شد');
    }

    public function businessIsActive($id)
    {
        $business = Business::findOrFail($id);
        if ($business->is_active == 1) {
            $business->is_active = 0;
            $business->update();
            return redirect('/need-approve-businesses')->with('flash', 'کسب و کار مورد نظر غیرفعال شد');
        }

        if (!$business->category_id) {
            return back()->with('flash_error', 'دسته بندی انتخاب نشده است');
        }
        if (!$business->city_id) {
            return back()->with('flash_error', 'شهر انتخاب نشده است');
        }

        $business->is_active = 1;
        $business->update();
        return redirect('/need-approve-businesses')->with('flash', 'کسب و کار مورد نظر فعال شد');
    }

    // inline method
    protected function updateInfo(array $data, $business, $request)
    {
        if (auth()->user()->isAdmin || auth()->user()->isSupport) {
            $category = $data['category_id'];
            if ($category != null) {
                $business->category_id = $category;
                $products = Product::where('business_id', $business->id)->get();
                if (count($products) > 0) {
                    foreach ($products as $item) {
                        $item->category_id = $category;
                        $item->save();
                    }
                }
            }
        }

        $business->name = $data['name'];
        $business->kasb_number = $data['kasb_number'];
        $business->phone = $data['phone'];
        $business->city_id = $data['city_id'] ?? $business->city_id;
        $business->address = $data['address'];
        $business->sheba = $data['sheba'];
        $business->email = $data['email'];
        $business->is_active = auth()->user()->isAdmin || auth()->user()->isSupport || auth()->user()->isAsnaf ? $data['is_active'] : 0;
        $business->panel_id = auth()->user()->isAdmin || auth()->user()->isSupport ? $data['panel'] : $business->panel_id;
        if (isset($data['panel'])) {
            if (auth()->user()->isAdmin || auth()->user()->isSupport) {
                if ($data['panel'] != 1) {
                    $business->expires_at = now()->addYear();
                }
            }
        }
        $business->latitude = $data['lat'] ?? $business->latitude;
        $business->longitude = $data['lng'] ?? $business->longitude;
        $business->b2b = $data['b2b'] ?? '0';
        $business->b2c = $data['b2c'] ?? '0';
        $business->description = $data['description'];
        $business->tax = 0;
        $business->feature = $data['feature'] ?? $business->feature;
        $business->min_shipping_cost = $data['min_shipping_cost'];
        $business->shipping_cost = $data['shipping_cost'];
//        $business->product_lines = $request->product_lines ?? $business->product_lines;
        $business->advantages = $request->advantages ?? $business->advantages;
        $business->website = $request->website ?? $business->website;
//        $business->found_date = $request->found_date ?? $business->found_date;
        $business->business_type = $request->business_type ?? $business->business_type;
        $business->rules = $request->rules;
        $business->is_seller = 1;

        if ($request->hasFile('header_image')) {
            if ($business->header_image && file_exists(public_path('/images/front_images/businesses/headers/' . $business->header_image))) {
                unlink(public_path('/images/front_images/businesses/headers/' . $business->header_image));
            }
            $file = $request->file('header_image');
            $filename = "h-{$business->id}-" . rand('111', '99999') . '.jpg';
            $image_path = 'images/front_images/businesses/headers/' . $filename;
            ImageManagerStatic::make($file)->resize(820, 512)->save($image_path);

            $business->header_image = $filename;
        }

        if ($request->hasFile('logo')) {
            if ($business->logo && file_exists(public_path('/images/front_images/businesses/logos/' . $business->logo))) {
                unlink(public_path('/images/front_images/businesses/logos/' . $business->logo));
            }
            $file = $request->file('logo');
            $filename = "l-{$business->id}-" . rand('111', '99999') . '.jpg';
            $image_path = 'images/front_images/businesses/logos/' . $filename;
            ImageManagerStatic::make($file)->resize(100, 100)->save($image_path);

            $business->logo = $filename;
        }
        $business->save();

        // Save Image Section
        if ($request->hasFile('image') && count($business->images) + count($request->file('image')) > 4) {
            return back()->with('flash_error', 'حداقل یک تصویر و حداکثر چهار تصویر برای کسب و کار مجاز است.');
        }
        if ($request->hasFile('image') && count($request->file('image')) > 4) {
            return back()->with('flash_error', 'حداقل یک تصویر و حداکثر چهار تصویر برای کسب و کار مجاز است.');
        }
        if (count($business->images) == 0 && !$request->hasFile('image')) {
            return back()->with('flash_error', 'حداقل یک تصویر و حداکثر چهار تصویر برای کسب و کار مجاز است.');
        }

        Business::saveImage($request, $business);

    }

    public function destroyImage($id, $businessId)
    {
        $prePhoto = Image::where(['id' => $id])->first();
        if ($prePhoto && file_exists('/images/front_images/businesses/small/' . $prePhoto->path)) {
            unlink('images/front_images/businesses/small/' . $prePhoto->path);
            unlink('images/front_images/businesses/large/' . $prePhoto->path);
        }

        if ($prePhoto) {
            $prePhoto->delete();
            $product = Business::find($businessId);
            $data = '';

            foreach ($product->images as $image) {
                $data .= '
                <div class="form-group col-3 mx-auto text-center" id="imageFull{{ $image->id }}">
                    <img src="/images/front_images/businesses/large/' . $image->path . '" alt="businessImage" class="img-fluid rounded"
                         style="width: 240px">
                        <a id="image' . $image->id . '" class="removePicBtn btn btn-link" data-imageid="' . $image->id . '" data-productId="' . $product->id . '" title="حذف تصویر">
                            <i class="fa fa-times-circle text-danger fa-2x mt-2"></i>
                        </a>
                </div>
            ';
            }
            return response()->json(['msg' => 'تصویر با موفقیت حذف شد.', 'data' => $data], 200);
        }

        return response()->json(['msg' => 'تصویر موجود نیست.', 'data' => ''], 404);
    }

    public function business($id)
    {
        $business = Business::findOrFail($id);
        return view('admin.businesses.business', compact('business'));
    }
}
