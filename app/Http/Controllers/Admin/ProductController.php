<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::latest()->paginate();
        return view('admin.products.index', compact('products'));
    }

    public function needToApproveProducts()
    {
        $asnafUser = auth()->user();
        if ($asnafUser->isAsnaf) {
            $products = Product::where(['is_active' => 0])
                ->whereIn('category_id', $asnafUser->category_id ?? ['0'])
                ->latest()->paginate();
        } else {
            $products = Product::where('is_active', 0)->latest()->paginate();
        }
        return view('admin.products.need_approve', compact('products'));
    }

    public function productIsActive($id)
    {
        $item = Product::findOrFail($id);

        if(count($item->images) < 1) {
            return back()->with('flash_error', 'کالا تصویر ندارد');
        }

        $item->is_active = 1;
        $item->update();
        return redirect('need-approve-products')->with('flash', 'کالای مورد نظر فعال شد');
    }

    public function product($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.products.product', compact('product'));
    }

}
