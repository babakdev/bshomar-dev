<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\DeliveryAddress;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersProduct;
use App\Payment;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OrderController extends Controller
{
    public function orderDetails($order_id)
    {
        $orderCustomerDetails = Order::where('id', $order_id)->select('orders.user_id', 'address_id', 'orders.invoice_id')->first();
        $address = DeliveryAddress::where('id', $orderCustomerDetails->address_id)->first();

        $orderDetails = OrdersProduct::where(['order_id' => $order_id])->get();

        return view('admin.orders.order-details', compact('orderDetails', 'orderCustomerDetails', 'address'));
    }

    public function sales()
    {
        $salesProduct = OrdersProduct::whereIn('status', ['کنسل شد', 'تسویه شد'])->latest()->paginate(20);
        return view('admin.orders.sales', compact('salesProduct'));
    }

    public function newSales()
    {
        $salesNew = Order::join('order_product', 'order_product.order_id', 'orders.id')
            ->where('orders.status', 1)
            ->join('payments', 'payments.invoice_id', 'orders.invoice_id')
            ->where(['payments.payed' => 1, 'payments.payment_method' => 1, 'order_product.status' => 'پرداخت شد'])
            ->where('order_product.status', '!=', 'کنسل شد')
            ->where('order_product.status', '!=', 'تسویه شد')
            ->where('order_product.status', '!=', 'ارسال شد')
            ->join('users', 'orders.user_id', 'users.id')
            ->select('orders.invoice_id', 'orders.user_id',
                'payments.price', 'payments.date_create', 'payments.payment_method', 'payments.payed',
                'order_product.order_id',
                'users.name as user_name', 'users.phone'
            )
            ->orderBy('orders.created_at', 'DESC')
            ->groupBy('orders.invoice_id')
            ->paginate(20);

        return view('admin.orders.sales_new', compact('salesNew'));
    }

    public function salesCheckout()
    {
        $factorNumbers = OrdersProduct::join('orders', 'orders.id', 'order_product.order_id')
            ->where('order_product.status', 'ارسال شد')
            ->join('payments', 'payments.order_id', 'orders.id')
            ->where(function($query){
                $query->where([['payments.payment_method', '!=', 2]])
                ->orWhere([ ['order_product.delivery_code', '!=', 'با پیک ارسال شد.']]);
            })
            ->select('order_product.order_id', 'order_product.created_at', 'order_product.status', 'orders.id', 'order_product.delivery_code', 'payments.payment_method')
            ->orderBy('orders.invoice_id', 'DESC')
            ->groupBy('orders.invoice_id')
            ->latest()->paginate(20);
        return view('admin.orders.sales_checkout', compact('factorNumbers'));
    }

    public function sendCheckoutMsg(Request $request)
    {
        $ordersP = OrdersProduct::where(['order_id' => $request->orderId, 'business_id' => $request->businessId])->get();
        foreach ($ordersP as $product) {
            $product->update(['status' => 'تسویه شد', 'admin_checkout_date' => now()]);
        }
        $msg = "تامین کننده گرامی، مبلغ {$request->total} تومان
            بابت فاکتور {$request->orderInvoiceId}
            به کیف پول شما واریز شد.";
        sendToInbox($request->businessUserId, 'مدیر مالی بی‌شمار', 'info@bshomar.com', $msg);

        $businessUser = User::where('id', $request->businessUserId)->first();
        $businessUser->deposit($request->total, ["واریز بابت فاکتور {$request->orderInvoiceId}"]);

        if ($request->shippingPerFacore > 0) {
            if ($request->isForBusiness == 1) {
                // shipping cost is for business
                $businessUser->deposit($request->shippingPerFacore, ["واریز هزینه ارسال بابت فاکتور {$request->orderInvoiceId}"]);
            } else {
                $user = User::findOrFail(1014);
                $user->deposit($request->shippingPerFacore, ["واریز هزینه ارسال بابت فاکتور {$request->orderInvoiceId}"]);
            }
        }

        return back()->with('flash', 'فاکتور تسویه شد و مبلغ فاکتور به کیف پول تامین کننده واریز شد');
    }

    public function cancelBuy(Request $request)
    {
        $orderPro = OrdersProduct::where('id', $request->orderProdId)->first();
        $factorNumber = $orderPro->order->invoice_id;
        $orderPro->update(['status' => 'کنسل شد', 'admin_checkout_date' => now()]);

        $rizFactors = OrdersProduct::where(['order_id' => $request->orderId])->whereIn('status', ['پرداخت شد', 'در انتظار پرداخت'])->count();
        if ($rizFactors == 0) {
            Order::where('id', $request->orderId)->update(['status' => 3]);
        }

        $factors = OrdersProduct::where(['order_id' => $request->orderId])->count();
        $cancelledFactors = OrdersProduct::where(['order_id' => $request->orderId])->where('status', 'کنسل شد')->count();
        if ($cancelledFactors === $factors) {
            Order::where('id', $request->orderId)->update(['status' => 2]);
        }

        $msg = 'مدیر محترم کسب و کار فاکتور شماره ' . $factorNumber . ' توسط خریدار کنسل شد';
        sendToInbox($request->businessUserId, 'فاکتور کنسل شد', 'info@bshomar.com', $msg);
        $businessUserPhone = User::where('id', $request->businessUserId)->value('phone');
        sendSmsWithTemplate(24063, $businessUserPhone, 'InvoiceId', $factorNumber);

        $msg2 = 'مدیر محترم مبلغ ' . $request->factorPrice . ' تومان را به حساب مشتری با یوزرنیم ' . $request->userPhone . ' بازگشت دهید بابت کنسلی';
        sendToInbox(auth()->user()->id == 1, 'فاکتور کنسل شد', 'info@bshomar.com', $msg2);

        return back()->with('flash', 'وضعیت فاکتور به کنسل شد تغییر کرد و پیامی حاوی شماره فاکتور کنسلی برای کسب و کار ارسال شد');
    }

    public function alertToBusiness(Request $request)
    {
        $msg = 'مدیر محترم کسب وکار با توجه به تاخیر در ارسال کالای ' . $request->name . ' لطفا هرچه سریعتر و امروز نسبت به ارسال کالای فروش رفته اقدام نمایید ، تا مشمول قوانین سلبی سایت نشوید';
        sendToInbox($request->user_id, 'مدیر بخش کسب و کارهای بی‌شمار', 'info@bshomar.com', $msg);
        return back()->with('flash', 'پیامی حاوی اخطار به کسب و کار مورد نظر جهت ارسال سریعتر کالا ارسال شد');
    }

    public function destroyFailedSell()
    {
        $sell = OrdersProduct::where('id', request('sellId'))->first();
        $sellsCount = OrdersProduct::where(['order_id' => $sell->order_id, 'payment_method' => 2])->count();
        if ($sellsCount == 1) {
            $order = Order::where('id', $sell->order->id)->first();
            Payment::where(['invoice_id' => $order->invoice_id, 'payed' => 0])->delete();
            $order->delete();
        }
        $sell->delete();
        return back()->with('flash', 'فاکتور مورد نظر حذف شد');
    }

    public function salesAtHome()
    {
        $salesNew = Order::join('order_product', 'order_product.order_id', 'orders.id')
            ->where('orders.status', 1)
            ->join('payments', 'payments.invoice_id', 'orders.invoice_id')
            ->where(['payments.payment_method' => 2])
            ->join('users', 'orders.user_id', 'users.id')
            ->select('orders.invoice_id', 'orders.user_id',
                'payments.price', 'payments.date_create', 'payments.payment_method', 'payments.payed',
                'order_product.order_id',
                'users.name as user_name', 'users.phone'
            )
            ->orderBy('orders.created_at', 'DESC')
            ->groupBy('orders.invoice_id')
            ->paginate(20);
        return view('admin.orders.sales_at_home', compact('salesNew'));
    }
}
