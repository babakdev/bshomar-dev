<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\Http\Controllers\Controller;
use App\Inbox;
use App\Jobs\SendMessageJob;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class InboxController extends Controller
{
    public function sendMsgToUser(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        if ($request->isMethod('post')) {
            $data = $request->all();
            $request->validate([
                'subject' => 'required',
                'message' => 'required',
            ]);
            sendToInbox($id, $data['subject'], 'info@bshomar.com', $data['message']);

            return back()->with('flash', 'ارسال پیام با موفقیت انجام شد');
        }
        return view('admin.users.messaging.sendMsgToAUser', compact('user'));
    }

    public function sentMessages()
    {
        $allMsgs = Inbox::orderByDesc('id')->paginate();
        return view('admin.users.messaging.sent_messages', compact('allMsgs'));
    }

    public function groupMessage()
    {
        if (request()->isMethod('post')) {
            request()->validate([
                'msg' => 'required'
            ]);

            if (request('msg') == 0) {
                $users = User::all();
                foreach ($users as $user) {
                    SendMessageJob::dispatch($user->id, request('subject'), request('message'));
                }
            } elseif(request('msg') == 1) {
                $businesses = Business::all();
                foreach ($businesses as $business) {
                    SendMessageJob::dispatch($business->user_id, request('subject'), request('message'));
                }
            } elseif(request('msg') == 2) {
                $businesses = Business::where('is_active', 0)->get();
                foreach ($businesses as $business) {
                    SendMessageJob::dispatch($business->user_id, request('subject'), request('message'));
                }
            }


            return back()->with('flash', 'ارسال پیام با موفقیت انجام شد');
        }
        return view('admin.users.messaging.group_message');
    }

    public function inbox()
    {
        $data = Inbox::where(['user_id' => '1'])->latest()->paginate();
        return view('admin.users.messaging.inbox', compact('data'));
    }

    public function updateInbox(Request $request)
    {
        $mId = $request->msgId;
        Inbox::where('id', $mId)->update([
            'seen' => 1
        ]);
        $count = \DB::table('inboxes')->where(['user_id' => auth()->user()->id, 'seen' => 0])->count();
        $item = \DB::table('inboxes')->where('id', $mId)->first();
        $msg = str_limit($item->message, 80);
        $subj = str_limit($item->subject, 30);
        $message = '<span class="small font-italic">' . $msg . '</span>';
        $subject = '<span class="small font-italic">' . $subj . '</span>';
        return response()->json(['count' => $count, 'message' => $message, 'subject' => $subject]);
    }
}
