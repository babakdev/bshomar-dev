<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Cache;

class FinanceController extends Controller
{
    public function index()
    {
        $newWithdrawRequests = Cache::remember('newWithdrawRequests', 300, fn() => \DB::table('transactions')->where('confirmed', 0)->count());
        return view('admin.finance.index', compact('newWithdrawRequests'));
    }

    public function debts()
    {
        // TODO paginate
        $users = Cache::remember('debtsToUsers', 300, fn() => User::get()->where('balance', '>', 0));
        return view('admin.finance.debts', compact('users'));
    }

    public function withdrawRequests()
    {
        $notConfirmeds = \DB::table('transactions')->where('confirmed', 0)->paginate();
        return view('admin.finance.withdraw-requests', compact('notConfirmeds'));
    }

    public function settleUser($transactionId)
    {
        $trans = \DB::table('transactions')->where('id', $transactionId)->first();
        $marketer = User::where('id', $trans->payable_id)->first();
        \DB::table('transactions')->where('id', $transactionId)->update(['confirmed' => 1]);
        $marketer->withdraw(-($trans->amount), ['واریز به حساب بانکی']);

        $admin = User::find(1);
        $admin->deposit(-($trans->amount), [$marketer->name, $marketer->phone]);

        $message = "کاربر گرامی، مبلغ {$trans->amount} به حساب بانکی شما واریز شد. با تشکر واحد مالی بی شمار";
        sendToInbox($trans->payable_id, 'واریز انجام شد', 'finance@bshomar.com', $message);

        $message2 = "مبلغ {$trans->amount} تومان برای کاربر به نام، {$marketer->name} و شماره موبایل {$marketer->phone} واریز شد.";
        sendToInbox($admin->id, 'واریز انجام شد', 'finance@bshomar.com', $message2);

        return back()->with('flash', 'تایید واریزی انجام شد');
    }

    public function dontSettleUser($transactionId)
    {
        $trans = \DB::table('transactions')->where('id', $transactionId)->first();
        $marketer = User::where('id', $trans->payable_id)->first();
        \DB::table('transactions')->where('id', $transactionId)->delete();


        $message = 'کاربر گرامی، برای دریافت مبلغ پورسانت، باید حداقل یک پنل غیر رایگان کسب و کار را به فروش رسانید. با تشکر واحد مالی بی شمار';
        sendToInbox($trans->payable_id, 'واریز انجام نشد', 'finance@bshomar.com', $message);

        return back()->with('flash', 'رد واریزی انجام شد');
    }

    public function allPaids()
    {
        $admin = User::find(1);
        // TODO paginate
        $allPaids = $admin->transactions;
        return view('admin.finance.all-paids', compact('allPaids'));
    }

    public function wallet($userPhone = null)
    {

        if (request('phone')) {
            $user = User::where('phone', request('phone'))->first();
            if (!$user) {
                return back()->with('flash_error', 'شماره موبایل کاربر یافت نشد');
            }
        } else {
            return back()->with('flash_error', 'شماره موبایل کاربر یافت نشد');
        }
        return view('admin.users.wallet', compact('user'));
    }
}
