<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Inbox;
use App\Product;
use App\Role;
use App\Rules\NationalCode;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\Rule;


class UserController extends Controller
{
    public function index()
    {
        $currentPage = request()->get('page', 1);
        if (auth()->user()->isSupport) {
            $users = Cache::remember('adminUsersIndexSupport-'.$currentPage, 30, fn() =>
            User::with(['role', 'city'])->orderByDesc('id')->where('role_id', '!=', 1)->where('role_id', '!=', 2)->paginate(20));
        } else {
            $users = Cache::remember('adminUsersIndex-'.$currentPage, 30, fn() =>
            User::with(['role', 'city'])->orderByDesc('id')->paginate(20));
        }

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Cache::remember('roles', 10000, fn() => Role::orderBy('id')->get());
        return view('admin.users.create', compact('roles'));
    }

    public function store()
    {
        request()->validate([
            'name' => ['required', 'string', 'min:3', 'max:191'],
            'email' => 'nullable | max:191 | unique:users,email',
            'phone' => 'required | unique:users,phone',
            'nationalcode' => [new NationalCode()]
        ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'phone' => convertPersianNumsToEng(request('phone')),
            'role_id' => request('role_id'),
            'is_active' => request('is_active'),
            'city_id' => request('city_id'),
            'nationalcode' => request('nationalcode'),
            'address' => request('address'),
            'password' => bcrypt(request('phone')),
            'category_id' => request('category_id'),
        ]);


        if (request('role_id') == 3) {
            $business = Business::create([
                'user_id' => $user->id,
                'name' => $user->name
            ]);
            return redirect('/business-information/' . $business->id)->with('flash', 'کاربر جدید ایجاد شد، اطلاعات مربوط به کسب و کار را تکمیل کنید');

        }
        return redirect('/users')->with('flash', 'کاربر جدید ایجاد شد');
    }

    public function edit(User $user)
    {
        $roles = Cache::remember('roles', 10000, fn() => Role::orderBy('id')->get());
        return view('admin.users.edit', compact('user', 'roles'));
    }

    public function update(User $user)
    {
        request()->validate([
            'name' => ['required', 'string', 'min:3', 'max:191'],
            'email' => ['nullable', 'max:191', Rule::unique('users')->ignore($user->id)],
            'phone' => [Rule::unique('users')->ignore($user->id)],
            'nationalcode' => [new NationalCode()]
        ]);

        if (!auth()->user()->isAsnaf){
            $user->phone = convertPersianNumsToEng(request('phone')) ?? $user->phone;
        }

        if (trim(request('password')) == '') {
            $input = request()->except('password');
        } else {
            $input = request()->all();
            $input['password'] = bcrypt(request('password'));
        }

        if (request()->hasFile('image')) {
            $image_tmp = request()->file('image');
            if ($image_tmp->isValid()) {
                $input = saveImageFile($image_tmp, $input, 100, 100, 'images/users/');
                $this->removeOldImageFile($user);
            }
        }

        $user->update($input);
        return redirect('/users')->with('flash', 'کاربر موردنظر ویرایش شد');
    }

    public function destroy(User $user)
    {
        if (!auth()->user()->isAsnaf) {
        $business = Business::where('user_id', $user->id)->first();

        if ($business) {
            $products = Product::where('business_id', $business->id)->get();
            if (count($products) > 0) {
                foreach ($products as $product) {
                    $comments = Comment::where('product_id', $product->id)->get();
                    if (count($comments) > 0) {
                        foreach ($comments as $comment) {
                            $comment->delete();
                        }
                    }
                    if (count($product->images) > 0) {
                        foreach ($product->images as $image) {
                            if (file_exists('/images/front_images/products/small/' . $image)) {
                                unlink('/images/front_images/products/small/' . $image);
                                unlink('/images/front_images/products/large/' . $image);
                            }
                            $image->delete();
                        }
                    }
                    $product->delete();
                }
            }
            $business->delete();
        }

        $inboxes = Inbox::where('user_id', $user->id)->get();
        if (count($inboxes) > 0) {
            foreach ($inboxes as $message) {
                $message->delete();
            }
        }

        if (file_exists($user->image)) {
            unlink($user->image);
        }
        $user->delete();
        return back()->with('flash', 'کاربر موردنظر حذف شد');
    }
        return back()->with('flash_error', 'Not Access');
    }

    // inline method
    protected function removeOldImageFile($user): void
    {
        $prePhoto = User::where('id', $user->id)->value('image');
        if (file_exists($prePhoto)) {
            unlink($prePhoto);
        }
    }

    public function disactiveUsers()
    {
        $users = User::where('is_active', 0)->latest()->paginate();
        return view('admin.users.disactives', compact('users'));
    }

    public function userIsActive($id)
    {
        $user = User::findOrFail($id);
        $user->is_active = 1;
        $user->update();
        return back()->with('flash', 'کاربر مورد نظر فعال شد');
    }

    public function asnaf()
    {
        $users = User::where('role_id', 5)->latest()->paginate(20);

        return view('admin.users.asnaf', compact('users'));
    }

    public function show()
    {
        return;
    }
}
