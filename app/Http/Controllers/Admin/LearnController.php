<?php

namespace App\Http\Controllers\Admin;

use App\CategoryLearn;
use App\Http\Controllers\Controller;
use App\LearnPage;

class LearnController extends Controller
{
    public function index()
    {
        $learns = LearnPage::orderByDesc('id')->paginate(20);
        return view('admin.learns.index', compact('learns'));
    }

    public function create()
    {
        $cats = CategoryLearn::pluck('title','id');
        return view('admin.learns.create', ['learn' => new LearnPage(),'cats'=>$cats]);
    }

    public function store()
    {
        $validated = request()->validate(
            [
                'title' => 'required',
                'category_id' => 'required',
                'body' => 'required'
            ]);
        LearnPage::create($validated);
        return redirect('irenadmin/learns')->with('flash', 'آموزش با موفقیت ایجاد شد');
    }

    public function edit(LearnPage $learn)
    {
        $cats = CategoryLearn::pluck('title','id');
        return view('admin.learns.edit', compact('learn','cats'));
    }

    public function update($id)
    {
        $validated = request()->validate(
            [
                'title' => 'required',
                'category_id' => 'required',
                'body' => 'required'
            ]);

        LearnPage::where('id', $id)->update($validated);
        return redirect('irenadmin/learns')->with('flash', 'آموزش با موفقیت ویرایش شد');
    }

    public function destroy($id)
    {
        LearnPage::where('id', $id)->delete();
        return redirect('irenadmin/learns')->with('flash', 'آموزش با موفقیت حذف شد');
    }

    public function show($id)
    {
    }
}
