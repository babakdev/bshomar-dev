<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use Carbon\Carbon;

class ChartController extends Controller
{
    public function index()
    {
        return view('admin.orders.charts.chart');
    }
    public function sellsChart()
    {
        $month_array = $this->getAllMonths(Order::class, request('year'));
        if (!empty($month_array)) {

            switch (request('on')) {
                case 'all':
                    foreach ($month_array as $month_no => $month_name) {
                        $monthly_order_count[] = Order::whereMonth('created_at', $month_no)->get()->count();
                        $labels[] = $month_name;
                    }
                    $title = 'نمودار فروش کلی';
                    break;

                case 'business':
                    foreach ($month_array as $month_no => $month_name) {
                        $monthly_order_count[] = Business::whereMonth('created_at', $month_no)
//                            ->orderBy('business_id')
                            ->get()->count();
                        $labels[] = $month_name;
                    }
                    $title = 'نمودار فروش برحسب پذیرنده';
                    break;
            }

        }
        return view('admin.orders.charts.sells_chart', [
            'labels' => collect($labels),
            'orders' => collect($monthly_order_count),
            'max' => max($monthly_order_count),
            'type' => request('type'),
            'title' => $title
        ]);
    }

    public function allSellsChart()
    {
        $month_array = $this->getAllMonths(Order::class, 2020);
        if (!empty($month_array)) {
            foreach ($month_array as $month_no => $month_name) {
                $monthly_count[] = Order::whereMonth('created_at', $month_no)->get()->count();
                $labels[] = $month_name;
            }
        }

        return [
            'labels' => json_encode($labels),
            'orders' => json_encode($monthly_count),
            'max' => max($monthly_count),
        ];
    }

    public function usersChart()
    {
        $month_array = $this->getAllMonths(User::class, 2020);
        if (!empty($month_array)) {
            $length = sizeof($month_array);
            $i = 0;
            foreach ($month_array as $month_no => $month_name) {
                if (++$i === $length) {
                    break;
                }
                $monthly_count[] = User::whereMonth('created_at', $month_no)->get()->count();
                $labels[] = $month_name;
            }
        }

        return [
            'labels' => json_encode($labels),
            'users' => json_encode($monthly_count),
            'max' => max($monthly_count),
        ];
    }

    public function businessChart()
    {
        $month_array = $this->getAllMonths(Business::class, 2020);
        if (!empty($month_array)) {
            $length = sizeof($month_array);
            $i = 0;
            foreach ($month_array as $month_no => $month_name) {
                if (++$i === $length) {
                    break;
                }
                $monthly_count[] = Business::whereMonth('created_at', $month_no)->get()->count();
                $labels[] = $month_name;
            }
        }

        return [
            'labels' => json_encode($labels),
            'businesses' => json_encode($monthly_count),
            'max' => max($monthly_count),
        ];
    }

    public function getAllMonths($model, $year = 2020)
    {
        $firstOfYear = $year . '-03-21';
        $lastOfYear = ($year + 1) . '-03-20';
        $dates = $model::whereBetween('created_at', [$firstOfYear, $lastOfYear])
            ->orderBy('created_at', 'ASC')->pluck('created_at');

        $month_array = [];
        $month_name = null;

        if (count($dates) == 0) {
            $month_array[0] = 'بدون رکورد';
        } else {
            foreach ($dates as $unformatted_date) {
                $date = new Carbon($unformatted_date);
                $month_no = $date->format('m');
                $month_name = verta($date)->format('F');
                $month_array[$month_no] = $month_name;
            }
        }
        return $month_array;
    }

}
