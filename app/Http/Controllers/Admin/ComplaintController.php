<?php

namespace App\Http\Controllers\Admin;

use App\Complaint;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    public function index()
    {
        $data = Complaint::latest()->paginate(20);
        return view('admin.complaints.index', compact('data'));
    }

    public function update(Request $request)
    {
        $mId = $request->msgId;
        Complaint::where('id', $mId)->update([
            'seen' => 1
        ]);
        $count = Complaint::where(['seen' => 0])->count();
        $item = Complaint::where('id', $mId)->first();
        $msg = str_limit($item->message, 80);
        $subj = str_limit($item->name, 30);
        $message = '<span class="small font-italic">' . $msg . '</span>';
        $subject = '<span class="small font-italic">' . $subj . '</span>';
        return response()->json(['count' => $count, 'message' => $message, 'subject' => $subject]);
    }
}
