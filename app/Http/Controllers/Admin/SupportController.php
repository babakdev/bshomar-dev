<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupportController extends Controller
{
    public function adminLogin(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['phone' => $data['phone'], 'password' => $data['password']])) {
                if (auth()->user()->isAdmin){
                    return redirect('irenadmin');
                }
                if (auth()->user()->isPeyk) {
                    return redirect('peyk');
                }
                return redirect('dashboard');
            }
            return redirect('/')->with('flash_error', 'اطلاعات ورودی اشتباه است');
        }
        if (auth()->check()) {
            return auth()->user()->isAdmin ? redirect('irenadmin') : redirect('dashboard');
        }
        return view('admin.admin_login');
    }
    public function dashboard()
    {
        return view('admin.support.index');
    }
}
