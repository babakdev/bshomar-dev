<?php

namespace App\Http\Controllers\Admin;

use App\Business;
use App\Http\Controllers\Controller;
use App\OrdersProduct;
use App\Payment;
use App\Product;
use App\Trending;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $ordersProducts = Cache::remember('allPaidProductsCount', 300, fn() => OrdersProduct::where('status', 'پرداخت شد')->count());
        $sentProducts = Cache::remember('allSentProductsCount', 300, fn() => OrdersProduct::where('status', 'ارسال شد')->count());

        if (\request()->wantsJson()) {
            return [
                'users' => Cache::remember('allUsersCount', 300, fn() => User::count()),
                'businesses' => Cache::remember('allBusinessesCount', 300, fn() => Business::count())
            ];
        }
        if (!auth()->user()->isAdmin) {
            return redirect('dashboard');
        }

        return view('admin.index', compact('ordersProducts', 'sentProducts'));
    }

    public function charts(Trending $trending)
    {
        // ttl 300 -> 5 minutes
        $users = Cache::remember('allUsersCount', 300, fn() => User::count());
        $deactiveUsers = Cache::remember('deactiveUsersCount', 300, fn() => User::whereIsActive(0)->count());

        $business = Cache::remember('allBusinessesCount', 300, fn() => Business::count());
        $products = Cache::remember('allProductsCount', 300, fn() => Product::count());
        $ordersProducts = Cache::remember('allPaidProductsCount', 300, fn() => OrdersProduct::where('status', 'پرداخت شد')->count());
        $sentProducts = Cache::remember('allSentProductsCount', 300, fn() => OrdersProduct::where('status', 'ارسال شد')->count());
        $payedOnline = Cache::remember('allOnlineSoldCount', 300, fn() => Payment::where(['payed' => 1])->where('payment_method', 1)->count());
        $payedHome = Cache::remember('allHomeSoldCount', 300, fn() => Payment::where(['payed' => 1])->where('payment_method', 2)->count());

        $businessesByCat = Cache::remember('allBusinessByCatCount', 300, fn() => Business::groupBy('category_id')->select('businesses.category_id', \DB::raw('COUNT(*) as cnt'))->get());
        $catNames = [];
        $catScores = [];
        foreach ($businessesByCat as $item) {
            $catNames[] = $item->category ? $item->category->name : 'بدون دسته بندی';
            $catScores[] = $item->cnt;
        }

        /* chart of businesses by score from redis */
        $trends = $trending->getBestBusinessesScore(24);
        $businessesTrendingCharts = [];
        $scores = [];
        foreach (collect($trends) as $key => $item) {
            $key = json_decode($key, true);
            $businessesTrendingCharts[] = Business::where('id', $key)->value('name');
            $scores[] = $item;
        }

        return view('admin.charts', [
            'businessesByCat' => $businessesByCat,
            'users' => $users,
            'deactiveUsers' => $deactiveUsers,
            'business' => $business,
            'products' => $products,
            'ordersProducts' => $ordersProducts,
            'sentProducts' => $sentProducts,
            'payedOnline' => $payedOnline,
            'payedHome' => $payedHome,
            'trending' => collect($businessesTrendingCharts),
            'trendingScore' => collect($scores),
            'catNames' => collect($catNames),
            'catScores' => collect($catScores),
        ]);
    }

    public function searchUser(Request $request)
    {
        $searchData = $request->search;
        $user = User::where('phone', $searchData)->first();
        $business = null;
        if ($user) {
            $business = Business::where('user_id', $user->id)->first();
        }

        $dataB = Business::where('name', 'like', '%' . $searchData . '%')
            ->paginate(30)
            ->appends('name', $searchData);

        return view('admin.users.user_search', compact('user', 'business', 'dataB'));
    }

    public function dashmin(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->input();
            if (Auth::attempt(['phone' => $data['phone'], 'password' => $data['password'], 'role_id' => '1'])) {
                return redirect('irenadmin');
            }
            return redirect('/dashmin')->with('flash_error', 'Invalid Username or Password');
        }
        if (auth()->check()) {
            return redirect('irenadmin');
        }
        return view('admin.admin_login');
    }

}
