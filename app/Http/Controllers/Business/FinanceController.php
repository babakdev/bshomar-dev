<?php

namespace App\Http\Controllers\Business;

use App\DiscountCode;
use App\Http\Controllers\Controller;
use App\OrdersProduct;
use App\Product;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function businessSold()
    {
        $product_id = Product::where('business_id', auth()->user()->business->id)->pluck('id');
        $orders = OrdersProduct::whereIn('product_id', $product_id)
            ->join('orders','orders.id','order_product.order_id')
            ->join('delivery_addresses','orders.address_id','delivery_addresses.id')
            ->join('cities','cities.id','delivery_addresses.city_id')
            ->join('cities as state','cities.parent_id','state.id')
            ->leftJoin('discount_codes','discount_codes.id','orders.discount_id')
            ->select('state.title as state_name','order_product.*','cities.title as city_name','delivery_addresses.*','orders.discount_id as discount_id','orders.price as total_amount')
            ->orderByDesc('order_id')
            ->paginate(20);
        foreach ($orders as $order){
            if($order->discount_id != null){
                $order->discount = DiscountCode::find($order->discount_id);
            }
        }

        return view('business.orders.business-products-order', compact('orders'));
    }

    public function submitDeliveryCode(Request $request)
    {
        $data = $request->all();
        OrdersProduct::where(['product_id' => $data['product_id'], 'order_id' => $data['order_id']])
            ->update(['delivery_code' => $data['delivery_code'], 'status' => 'ارسال شد', 'sent_date' => now()]);

        $message = $data['product_name'] . '-' . $data['delivery_code'];
        sendToInbox($data['user_id'], $data['business_id'], $data['business_phone'], $message);

        return redirect('businessadmin/sold')->with('flash', 'کد رهگیری ارسال کالا ثبت شد و وضعیت کالا به ارسال شده تغییر کرد');
    }
}
