<?php

namespace App\Http\Controllers\Business;

use App\Business;
use App\Category;
use App\City;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Product;
use App\Trending;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $business = Business::where('user_id', $user->id)->first();
        $products = Product::where('business_id', $business->id)->get();

        $warning = 0;
        if ($products) {
            foreach ($products as $product) {
                if ($product->stock <= $product->min_stock) {
                    $warning++;
                    break;
                }
            }
        }

        if ($warning > 0) {
            session()->put('warning', 'شما تعدادی محصول با موجودی پایین دارید برای مشاهده محصولات <a href="/businessadmin/product/filter?name=&category=&filter=3">اینجا</a>کلیک کنید');
        }
        return view('business.index');

    }

    public function products()
    {
        $user = auth()->user();
        $business = Business::where('user_id', $user->id)->first();
        $products = Product::where('business_id', $business->id)->get();

        $warning = 0;
        if ($products) {
            foreach ($products as $product) {
                if ($product->stock <= $product->min_stock) {
                    $warning++;
                    break;
                }
            }
        }

        if ($warning > 0) {
            session()->put('warning', 'شما تعدادی محصول با موجودی پایین دارید برای مشاهده محصولات <a href="/businessadmin/product/filter?name=&category=&filter=3">اینجا</a>کلیک کنید');
        }
        $allow = Business::where('user_id', auth()->id())
            ->where('city_id', '!=', null)
            ->where('category_id', '!=', null)->count();
        if ($allow == 0) {
            return redirect()->back()->with('flash_error', 'برای ثبت کالا ابتدا باید اطلاعات کسب و کار خود را تکمیل کنید.');
        }
        $products = Product::where('business_id', auth()->user()->business->id)->orderBy('id', 'DESC')->paginate(10);
        return view('business.products.view_products', compact('products'));
    }

    public function comments($productId)
    {
        $comments = Comment::where('product_id', $productId)->get();
        return view('business.comments.index', compact('comments'));
    }
}
