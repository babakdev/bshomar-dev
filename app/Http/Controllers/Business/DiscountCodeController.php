<?php

namespace App\Http\Controllers\Business;

use App\Business;
use App\DiscountCode;
use App\DiscountProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBusinessDiscountCode;
use App\Http\Requests\UpdateBusinessDiscountCode;
use App\Product;
use Illuminate\Http\Request;

class DiscountCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business_id = Business::where('user_id',auth()->user()->id)->first()->id;
        $discounts = DiscountCode::where('business_id',$business_id)->paginate(20);
        return view('business.discounts.index',compact('discounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $business_id = Business::where('user_id',auth()->user()->id)->first()->id;
        $products = Product::where('business_id',$business_id)->pluck('name','id');
        return view('business.discounts.create',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBusinessDiscountCode $request)
    {
        $validated = $request->validated();
        if($request->start_date > $request->end_date){
            return back()->withInput($request->all())->with('flash_error','تاریخ شروع نمی تواند بعد از تاریخ پایان باشد.');
        }
        if($request->amount_type == 1 && !$request->percent){
            return back()->withInput($request->all())->with('flash_error','درصد تخفیف را وارد کنید.');
        }
        if($request->amount_type == 2 && !$request->amount){
            return back()->withInput($request->all())->with('flash_error','مبلغ تخفیف را وارد کنید.');
        }

        $business_id = Business::where('user_id',auth()->user()->id)->first()->id;
        $validated['business_id'] = $business_id;
        if($request->products){
            $discount = DiscountCode::create($validated);
            if($discount){
                $data = [];
                foreach($request->products as $product){
                   $data[] = [
                        'product_id' => $product,
                        'discount_id' => $discount->id
                    ];
                }
                \DB::table('discount_products')->insert($data);
            }else{
                return back()->withInput($request->all())->with('flash_error','مشکلی در ثبت بوجود آمده است لطفا مجددا تلاش کنید.');
            }

        }else{
            return back()->withInput($request->all())->with('flash_error','محصول/محصولات موردنظر را انتخاب کنید.');
        }
        return back()->with('flash','کد تخفیف با موفقیت ایجاد شد.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business_id = Business::where('user_id',auth()->user()->id)->first()->id;
        $products = Product::where('business_id',$business_id)->pluck('name','id');
        $item = DiscountCode::find($id);
        return view('business.discounts.edit',compact('products','item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBusinessDiscountCode $request, $id)
    {
        $validated = $request->validated();
        if($request->start_date > $request->end_date){
            return back()->withInput($request->all())->with('flash_error','تاریخ شروع نمی تواند بعد از تاریخ پایان باشد.');
        }
        if($request->amount_type == 1 && !$request->percent){
            return back()->withInput($request->all())->with('flash_error','درصد تخفیف را وارد کنید.');
        }
        if($request->amount_type == 2 && !$request->amount){
            return back()->withInput($request->all())->with('flash_error','مبلغ تخفیف را وارد کنید.');
        }
        $discount = DiscountCode::find($id);
        if($request->amount_type == 1){
            $validated['amount']=null;
            $validated['percent'] = $request->percent;
        }else{
            $validated['amount'] = $request->amount;
            $validated['percent'] = null;
        }
        $update = $discount->update($validated);
        if($update){
            $discount->products()->sync($request->products);
            return back()->with('flash','کد تخفیف با موفقیت ویرایش شد.');
        }else{
            return back()->withInput($request->all())->with('flash_error','مشکلی در ثبت بوجود آمده است لطفا مجددا تلاش کنید.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = DiscountCode::find($id);

        //check for in use this discount code

        if($item->delete()){
            return redirect('/businessadmin/discount/codes')->with('flash','کد تخفیف با موفقیت حذف شد.');
        }
    }

    public function checkForRepeatCode(Request $request)
    {
        $code = $request->get('value');
        $id = $request->get('id');
        if($id){
            $discountCodes = \DB::table('discount_codes')->where('code',$code)->where('id','!=' ,$id)->first();
        }else{
            $discountCodes = \DB::table('discount_codes')->where('code',$code)->first();
        }
        if($discountCodes){
            $msg = 'کد تخفیف وارد شده تکراری است.';
            return response()->json(['status'=>true ,'msg' => $msg]);
        }
        return response()->json(['status'=>false ,'msg' => '']);
    }
}
