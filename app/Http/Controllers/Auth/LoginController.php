<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SmsIR_VerificationCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'phone';
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        //--------------------------------------------------
        $phone = convertPersianNumsToEng($request->phone);
        $userLogin = User::where('phone', $phone)->first();

        if ($userLogin && \Hash::check($request->password, $userLogin->password) && ($userLogin->phone === $phone)) {

            if ($userLogin->is_active && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
                $this->incrementLoginAttempts($request);

                $code = rand(1111, 9999);
                $user = User::where('phone', $phone)->first();
                $user->update(['code' => $code]);
                try {
                    $APIKey = '7d8f414d3ca0120b76fd414d';
                    $SecretKey = 'bshomar@iren32n53e';
                    $APIURL = 'https://ws.sms.ir/';
                    $MobileNumber = $user->phone;

                    $SmsIR_VerificationCode = new SmsIR_VerificationCode($APIKey, $SecretKey, $APIURL);
                    $VerificationCode = $SmsIR_VerificationCode->verificationCode($code, $MobileNumber);
                    var_dump($VerificationCode);

                } catch (Exeption $e) {
                    echo 'Error VerificationCode : ' . $e->getMessage();
                }

                return redirect('/verify?phone='.$user->phone)->with('phone', $user->phone);
            }
        }
        //--------------------------------------------------
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => $request->getClientIp()
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect('/');
    }

}
