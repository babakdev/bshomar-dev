<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SmsIR_VerificationCode;
use App\User;
use Illuminate\Http\Request;
use TimeHunter\LaravelGoogleReCaptchaV2\Validations\GoogleReCaptchaV2ValidationRule;

class VerifyController extends Controller
{
    public function postVerify(Request $request)
    {
        // TODO
//        if ($request->changePhone) {
//        }

        if ($user = User::where('code', convertPersianNumsToEng($request->code))->first()) {
            $user->is_active = 1;
            $user->code = null;
            $user->save();
            $userId = $user->id;
            auth()->loginUsingId($userId);

            $message = 'با سلام و خوش آمد خدمت شما کاربر گرامی
            ورود شما به پلتفرم اقتصادی بی شمار مایه فخر و مباهات ما می باشد، باشد که همه بتوانیم با یاری یکدیگر قدم استوار و موثری در جهت پیشرفت کشور عزیزمان ایران برداریم.
همراه همیشگی اقتصاد ایران ، بی شمار';
            sendToInbox($userId, 'مدیریت سایت بی شمار', 'admin@bshomar.com', $message);

            if ($user->isBusiness) {
                $mPhone = session()->get('marketerPhone');
                if ($mPhone) {
                    $this->sendGiftForMarketerAndBusiness($mPhone, $userId);
                }
                return redirect("/business-information/{$user->business->id}")->with('flash', 'حساب کاربری شما با موفقیت فعال شد، به بی شمار خوش آمدید. اطلاعات کسب و کار خود را تکمیل کنید ');
            }
            return redirect('profile')->with('flash', 'حساب کاربری شما با موفقیت فعال شد، به پلتفرم عظیم بی شمار خوش آمدید.');
        } else {
            return back()->with('flash_error', 'کد فعال سازی صحیح نیست دوباره وارد کنید');

        }
    }

    public function resendCode(Request $request)
    {
        $request->validate(['g-recaptcha-response' => [new GoogleReCaptchaV2ValidationRule()]]);
        $user = User::where('phone', $request->phone)->first();
        try {
            $code = $user->code;
            $APIKey = '7d8f414d3ca0120b76fd414d';
            $SecretKey = 'bshomar@iren32n53e';
            $APIURL = 'https://ws.sms.ir/';
            $MobileNumber = $user->phone;

            $SmsIR_VerificationCode = new SmsIR_VerificationCode($APIKey, $SecretKey, $APIURL);
            $VerificationCode = $SmsIR_VerificationCode->verificationCode($code, $MobileNumber);
            var_dump($VerificationCode);

        } catch (Exeption $e) {
            echo 'Error VerificationCode : ' . $e->getMessage();
        }

        return redirect('/verify?phone='.$user->phone)->with('flash','کد فعال سازی مجدد برای شما ارسال شد');
    }

    public function sendGiftForMarketerAndBusiness($marketerPhone,$businessUserId)
    {
        //marketer
        $marketer = User::where('phone',$marketerPhone)->first();
        if($marketer){
            $marketer->deposit(3000,['واریز پورسانت 3 هزار تومانی']);
            $msg1 = $marketer->name.' گرامی , مبلغ 3000 تومان به عنوان پورسانت به کیف پول شما واریز شد.';
            sendToInbox($marketer->id , 'واریز پورسانت' , 'finance@bshomar.com' , $msg1);

            increaseUserRateWithNotify($marketer->id,1);

            \DB::table('marketers')->insert([
                'marketer_id' => $marketer->id,
                'business_id' => $businessUserId
            ]);

            increaseUserRateWithNotify($businessUserId,1);

        }
    }
}
