<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SmsIR_VerificationCode;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use TimeHunter\LaravelGoogleReCaptchaV2\Validations\GoogleReCaptchaV2ValidationRule;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function recoverPassword()
    {
        \request()->validate(['g-recaptcha-response' => [new GoogleReCaptchaV2ValidationRule()]]);

        $user = User::where('phone', request('phoneRecovery'))->first();
        if($user) {
            $code = rand(1111, 9999);
            $user->code = $code;
            $user->update(['code' => $code]);


        } else {
            return back()->with('flash_error', 'شماره ورودی، از اعضای سایت نمی باشد، لطفا با دقت بیشتری شماره موبایل را وارد نمایید');
        }

        try {
            $APIKey = '7d8f414d3ca0120b76fd414d';
            $SecretKey = 'bshomar@iren32n53e';
            $APIURL = 'https://ws.sms.ir/';
            $MobileNumber = request('phoneRecovery');

            $SmsIR_VerificationCode = new SmsIR_VerificationCode($APIKey, $SecretKey, $APIURL);
            $VerificationCode = $SmsIR_VerificationCode->verificationCode($code, $MobileNumber);
            var_dump($VerificationCode);

        } catch (Exeption $e) {
            echo 'Error VerificationCode : ' . $e->getMessage();
        }
        return redirect('/verify-for-password?phone=' . $user->phone)->with('phone', $user->phone);
    }

    public function getVerify()
    {
        $phone = session('phoneRecovery');
        return view('auth.passwords.reset', compact('phone'));
    }
}
