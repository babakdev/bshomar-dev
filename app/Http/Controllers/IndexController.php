<?php

namespace App\Http\Controllers;

use App\Business;
use App\Category;
use App\City;
use App\Product;
use App\Slide;
use Illuminate\Http\Request;


class IndexController extends Controller
{
    public function ajaxCities(Request $request)
    {
        $state_id = $request->state_id;
        $cities = City::where('parent_id', '=', $state_id)->get();
        return response()->json($cities);
    }

    public function ajaxCategories()
    {
        $catId = request('cat_id');
        $subcategory = Category::where('parent_id', $catId)->get();
        if ($subcategory->isEmpty()) {
            $subcategory = Category::where('id', $catId)->get();
        }
        return response()->json($subcategory);
    }
}
