$().ready(function () {
    $('#mainCat').on('change', function (e) {
        let category_id = e.target.value;
        $.get('/ajax-subcat?cat_id=' + category_id, function (data) {
            $('#category').empty();
            $.each(data, function (index, categoryObj) {
                $('#category').append('<option value="' + categoryObj.id + '">' + categoryObj.name + '</option>')
            });
        });
    });
});
