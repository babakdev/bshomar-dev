<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('ajax-subcat', 'IndexController@ajaxCategories');
Route::get('ajax-city/{state_id}', 'IndexController@ajaxCities')->where('state_id', '[0-9]+');

Auth::routes();

Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::match(['get', 'post'], '/', 'Admin\SupportController@adminLogin');

// ====================================================sharedAdmins=============================================
Route::group(['middleware' => 'sharedAdmins'], function () {
    Route::match(['get', 'post'], 'business-information/{id}', 'Admin\BusinessController@editBusiness');
    Route::resource('/products', 'ProductController');
    Route::get('/business-image/{id}/{businessId}', 'Admin\BusinessController@destroyImage');
    Route::get('/product-image/{id}/{productId}', 'ProductController@destroyImage');
    Route::get('/toggle/product/{productId}/status', 'ProductController@toggleProductStatus');
});

Route::group(['middleware' => 'support'], function (){
    Route::get('dashboard', 'Admin\SupportController@dashboard');
    Route::resource('/users', 'Admin\UserController');
    Route::match(['get', 'post'], 'search-user', 'Admin\DashboardController@searchUser');
    Route::get('/asnaf', 'Admin\UserController@asnaf');

    // Products Control
    Route::get('products', 'Admin\ProductController@index');
    Route::get('need-approve-products', 'Admin\ProductController@needToApproveProducts');
    Route::get('product-is-active/{id}', 'Admin\ProductController@productIsActive');
    Route::get('/product/details/{id}', 'Admin\ProductController@product');

    // Comments
    Route::get('/comments', 'Admin\CommentController@index');
    Route::get('/comments/replies/{comment}', 'Admin\CommentController@create');
    Route::patch('/comments/{id}', 'Admin\CommentController@update');
    Route::delete('/comments/{id}', 'Admin\CommentController@destroy');

    // Business Control
    Route::get('/need-approve-businesses', 'Admin\BusinessController@needToApproveBusinesses');
    Route::get('business-is-active/{id}', 'Admin\BusinessController@businessIsActive');
    Route::get('approved-businesses', 'Admin\BusinessController@approvedBusinesses');
    Route::delete('/business/{business}', 'Admin\BusinessController@destroy');
    Route::get('/business/details/{id}', 'Admin\BusinessController@business');

    /*----------------------Financial Panel-------------------------*/
    // ordered Product page
    Route::get('sales/{id}', 'Admin\OrderController@orderDetails');

    // send alert to business for late send product
    Route::post('alert-to-business', 'Admin\OrderController@alertToBusiness');

    // Sales page
    Route::get('sales', 'Admin\OrderController@sales');
    Route::get('new-sales', 'Admin\OrderController@newSales');
    Route::get('sales-checkout', 'Admin\OrderController@salesCheckout');
    Route::get('sales-at-home', 'Admin\OrderController@salesAtHome');
    Route::post('send-checkout-msg', 'Admin\OrderController@sendCheckoutMsg');
    Route::post('cancel-buy', 'Admin\OrderController@cancelBuy');
    Route::post('delete-sell', 'Admin\OrderController@destroyFailedSell');

    // Finance
    Route::get('finance', 'Admin\FinanceController@index');
    Route::get('debts', 'Admin\FinanceController@debts');
    Route::get('withdraw-requests', 'Admin\FinanceController@withdrawRequests');
    Route::post('settle-user/{transactionId}', 'Admin\FinanceController@settleUser');
    Route::post('dont-settle-user/{transactionId}', 'Admin\FinanceController@dontSettleUser');
    Route::get('all-paids', 'Admin\FinanceController@allPaids');
    Route::match(['get', 'post'], 'user/wallet/{user?}', 'Admin\FinanceController@wallet');

    // List of sent messages
    Route::match(['get', 'post'], 'send-message/{user}', 'Admin\InboxController@sendMsgToUser');
    Route::match(['get', 'post'], 'group-message', 'Admin\InboxController@groupMessage');
    Route::get('sent-msgs', 'Admin\InboxController@sentMessages');
    Route::get('inbox', 'Admin\InboxController@inbox');
    Route::get('update-inbox', 'Admin\InboxController@updateInbox');

    // Complaints
    Route::get('complaints', 'Admin\ComplaintController@index');
    Route::get('update-complaint', 'Admin\ComplaintController@update');

    Route::get('disactive-users', 'Admin\UserController@disactiveUsers');
    Route::get('user-is-active/{id}', 'Admin\UserController@userIsActive');
});

// ====================================================ADMIN====================================================
Route::group(['middleware' => 'admin', 'prefix' => 'irenadmin'], function () {

    // Admin Dashboard and manage users
    Route::get('/', 'Admin\DashboardController@dashboard');

//    Route::get('/charts', 'Admin\ChartController@index');
    Route::get('/charts', 'Admin\DashboardController@charts');
    Route::post('/post-sells-chart', 'Admin\ChartController@sellsChart');
    Route::get('/sells-chart', 'Admin\ChartController@allSellsChart');
    Route::get('/users-chart', 'Admin\ChartController@usersChart');
    Route::get('/business-chart', 'Admin\ChartController@businessChart');

    Route::resource('/categories', 'Admin\CategoryController');
    Route::resource('/slides', 'Admin\SlideController');
    Route::resource('/advertising', 'Admin\AdvertiseController');
    Route::resource('/learns', 'Admin\LearnController');
});

// PEYK
Route::group(['middleware' => 'peyk'], function () {
    Route::get('peyk', 'Admin\PeykController@index');
    Route::get('peyk/order-detail/{id}', 'Admin\PeykController@saleDetails');
});

// ====================================================BUSINESS=================================================
Route::group(['middleware' => 'business', 'prefix' => 'businessadmin'], function () {
    // Dashboard
    Route::get('/', 'Business\DashboardController@index');
    Route::get('products', 'Business\DashboardController@products');

    Route::get('comments/{productId}', 'Business\DashboardController@products');

    /*-----------------Financial Panel-------------------------*/
    Route::get('sold', 'Business\FinanceController@businessSold');
    Route::post('sold/submit-delivery-code', 'Business\FinanceController@submitDeliveryCode');
});
