<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'client/auth', 'namespace' => 'API\Auth'], function () {
    Route::post('/login', 'AuthLoginController@api_login');
    Route::post('/logout', 'AuthLoginController@api_logout');

    Route::post('/verify_code', 'RegisterController@verifyCode');
    Route::post('/register', 'RegisterController@api_register');

    Route::post('/forget/password', 'ForgetPasswordController@forgetPassword');
    Route::post('/change/password', 'ForgetPasswordController@changePassword');
});

Route::group(['prefix' => 'client', 'namespace' => 'API\Client'], function () {
    Route::get('/splash', 'MainController@splash');
    Route::post('/search', 'MainController@searchByProductOrBusiness');
    Route::post('/newsletter', 'MainController@newsLetters');

    Route::get('/get/subcategories', 'CategoryController@getSubCategories');
    Route::get('/get/allcategories', 'CategoryController@getAllCategories');

    Route::get('/get/best/business', 'FilterController@getBestBusinessesByCategory');
    Route::get('/get/best/seller', 'FilterController@getBestBusinessesByCategory');
    Route::get('/get/best/product', 'FilterController@getBestProductsByCategory');
    Route::get('/get/best/selling', 'FilterController@getBestProductsByCategory');
    Route::post('/filter', 'FilterController@filterByCategories');
    Route::get('/get/business/filter/parameters', 'FilterController@businessFilterParameters');
    Route::get('/get/product/filter/parameters', 'FilterController@productFilterParameters');

    Route::get('/get/product/details', 'ProductController@getProductDetailsById');
//    Route::get('/get/newest/discount/products', 'ProductController@newestDiscountProducts');
    Route::group(['middleware' => 'jwt.auth', 'jwt.refresh'], function () {
        Route::post('/toggle/favorite/product', 'ProductController@toggleFavoriteProduct');
        Route::get('/check/discount/code', 'DiscountController@checkDiscountCode');
    });
    Route::get('/get/business/products', 'ProductController@getProductsByBusiness');
    Route::get('/get/relation/products', 'ProductController@relationProducts');
    Route::get('/all/discount/products', 'ProductController@allDiscountProducts');

    Route::get('/get/business/details', 'BusinessController@getBusinessDetailsById');
    Route::get('/get/newest/businesses', 'BusinessController@newestBusinesses');
    Route::get('/get/most/visited/businesses', 'BusinessController@mostVisitedBusinesses');

    Route::get('/get/categories/learning', 'LearningController@categories');
    Route::get('/get/learning', 'LearningController@learningByCategory');
    Route::get('/get/all/learning', 'LearningController@learningWithoutCategory');

    Route::post('/get/cities', 'PageController@citiesByState');
    Route::get('/get/states', 'PageController@allStates');
    Route::post('/send/contact/message', 'PageController@contactUsForm');
});

Route::group(['prefix' => 'business', 'namespace' => 'API\Business'], function () {
    Route::group(['middleware' => 'jwt.auth', 'jwt.refresh'], function () {
        Route::post('/get/dashboard', 'DashboardController@dashboard');
        Route::post('/products/update/{id}', 'ProductController@updateItem');
        Route::get('/products', 'ProductController@index');
        Route::post('/products', 'ProductController@store');
        Route::delete('/products/{product}', 'ProductController@destroy');
        Route::post('/products/filter', 'ProductController@filter');
        Route::post('/make/product/unavailable', 'ProductController@makeProductUnavailable');
        Route::delete('/product/image/delete/{id}', 'ProductController@destroyImage');

        Route::get('/get/self/comments', 'CommentController@commentsByUserId');
        Route::get('/get/products/comments', 'CommentController@productsComments');

        Route::post('/get/sales/history', 'FinanceController@salesHistory');
        Route::post('/submit/delivery/code', 'FinanceController@submitDeliveryCode');
        Route::post('/submit/delivery/code/home', 'FinanceController@submitDeliveryAtHome');
        Route::post('/get/buy/history', 'FinanceController@buyHistory');

        Route::post('/information/update', 'BusinessController@updateInfo');
        Route::post('/information/delete/image', 'BusinessController@destroyImage');
        Route::post('/information/delete/logo', 'BusinessController@destroyLogoOrHeader');

        Route::group(['namespace' => 'Productive', 'prefix' => 'productive'], function () {
            Route::post('/delete/all', 'ProductiveController@deleteAllItem');

            Route::post('/manu/update/{id}', 'ManufacturingController@updateItem');
            Route::resource('/manu', 'ManufacturingController');

            Route::post('/qc/update/{id}', 'QcController@updateItem');
            Route::resource('/qc', 'QcController');

            Route::post('/rnd/update/{id}', 'RndController@updateItem');
            Route::resource('/rnd', 'RndController');

            Route::post('/certificate/update/{id}', 'CertificateController@updateItem');
            Route::resource('/certificate', 'CertificateController');

            Route::post('/export/update/{id}', 'ExportController@updateItem');
            Route::resource('/export', 'ExportController');

            Route::post('/customer/export/update/{id}', 'CustomerExportController@updateItem');
            Route::post('/customer/export', 'CustomerExportController@store');
            Route::delete('/customer/export/{export}', 'CustomerExportController@destroy');
        });

        Route::post('/chart/statistics', 'ChartController@statistics');

        Route::post('/distributive/update', 'DistributiveController@update');

        Route::post('/service/update', 'ServiceController@update');

        Route::post('/discounts/update/{id}', 'DiscountCodeController@updateItem');
        Route::get('/all/discount/products', 'DiscountCodeController@allProducts');
        Route::resource('/discounts', 'DiscountCodeController');

        Route::get('/get/panel/alarm', 'PanelController@displayFinalPriceOfPanel');
        Route::post('/order/panel', 'PanelController@order');

        Route::get('/requests', 'RequestController@requestsForBusiness');
    });

    Route::get('/get/panels', 'PanelController@index');
    Route::get('/payment/panel', 'PanelController@payment');

    Route::post('/discount/check/code', 'DiscountCodeController@checkForRepeatCode');
});

Route::group(['prefix' => 'user', 'namespace' => 'API\User'], function () {
    Route::group(['middleware' => 'jwt.auth', 'jwt.refresh'], function () {
        Route::get('/get/all/favorites', 'FavoriteController@getAllFavoriteProducts');

        Route::post('/get/self/comments', 'CommentController@commentsByUserId');
        Route::post('/send/comment', 'CommentController@insertComment');

        Route::get('/get/buy/history', 'FinanceController@buyHistory');

        Route::get('/get/inbox', 'InboxController@inbox');
        Route::post('/message/seen', 'InboxController@seen');
//        Route::post('/message/reply', 'InboxController@reply');
        Route::post('/contact/messages', 'InboxController@allMessageByContact');
        Route::get('/unread/messages/count', 'InboxController@unreadMsgCountByToken');

        Route::post('/orders', 'OrderController@order');

        Route::post('/update/profile', 'ProfileController@updateProfile');
        Route::get('/remove/profile/photo', 'ProfileController@removeProfilePhoto');
        Route::post('/update/password', 'ProfileController@updatePassword');

        Route::get('/get/address', 'AddressController@index');
        Route::post('/add/address', 'AddressController@store');
        Route::post('/update/address/{id}', 'AddressController@update');

        Route::post('/send/business/request', 'RequestController@requestInPage');

        Route::get('/wallet', 'WalletController@index');
        Route::post('/wallet/settle', 'WalletController@settle');

        Route::get('/carts/delete/all', 'CartController@destroyAllItems');
        Route::resource('/carts', 'CartController');
    });

//    Route::post('/search/products', 'RequestController@searchProductOnKeyUp');

    Route::get('/payment/verify', 'PaymentController@verify');

});
